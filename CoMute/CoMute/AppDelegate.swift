//
//  AppDelegate.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 27/04/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import UserNotifications
import GoogleMaps
import CoreLocation
import Fabric
import Crashlytics
import SocketIO

let socket = SocketIOClient(socketURL: NSURL(string: SERVER_ADDRESS.Socket_URL)! as URL, config:  [.log(true), .forcePolling(true)])
  var isCurrentVC = false

@available(iOS 9.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deepLinkURL:NSURL? = nil
    //Orientation Flags.....
    var landscapeOrientationChangeFlag = false
    
    let locationManager = CLLocationManager()
    var latitude  = String()
    var longitude = String()
    var chatAlertBanner : ChatNotificationView!
    var userId = String()
    var chatUserInfo : [NSObject : AnyObject]?
  


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //Getting All data from user defaults to use Again.....
        
        
        Singleton.sharedInstance.userName =  (UserDefaults.standard.value(forKey: USER_DEFAULT.USER_NAME) ?? "") as! String
        Singleton.sharedInstance.userAccessToken = (UserDefaults.standard.value( forKey: USER_DEFAULT.ACCESS_TOKEN) ?? "") as! String
        Singleton.sharedInstance.userFullName = (UserDefaults.standard.value( forKey: USER_DEFAULT.USER_FULL_NAME) ?? "") as! String
        //Singleton.sharedInstance.currentUserType = (UserDefaults.standardUserDefaults().valueForKey(USER_DEFAULT.USER_TYPE) ?? "") as! String
        
        if let userID : String = UserDefaults.standard.value(forKey: USER_DEFAULT.USER_ID) as? String {
            
            Singleton.sharedInstance.userID = userID
        }
        
        //Registering for Internet Change Notification......
        //NotificationCenter.defaultCenter.addObserver(self, selector: #selector(AppDelegate.internetChanged(_:)), name: ReachabilityChangedNotification, object: nil)
        //Singleton.sharedInstance.reachability?.startNotifier()
        
        self.registerForPushNotifications()
        GMSServices.provideAPIKey(GOOGLE_MAPS_KEY.IOS_KEY)
        
              Fabric.with([Crashlytics.self])

        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //Activating Facebook App....
        FBSDKAppEvents.activateApp();
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
       // self.saveContext()
    }
    
    private func application(application: UIApplication, openURL url: URL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
//        if url.host == "open"{
//            
//            self.deepLinkURL = url as NSURL?
//            print(application.applicationState)
//            return true
//        }
//        
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
    }

    
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {
        
        let isFBURL = ((url.scheme?.hasPrefix("fb\(FBSDKSettings.appID()!)"))! && url.host == "authorize")
        
        if  isFBURL == true {
            
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url,
                                                                         sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                                         annotation: options[UIApplicationOpenURLOptionsKey.annotation] as? String)
        }
        
        return true
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let parsedUrl = BFURL(inboundURL: url, sourceApplication: sourceApplication)
        if ((parsedUrl?.appLinkData) != nil) {
            // this is an applink url, handle it here
           let targetUrl  = parsedUrl?.targetURL! as! NSURL
            UIAlertView(title: "Received link:", message: targetUrl.absoluteString!, delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "").show()
        }
        
        return true
    }
    
    //MARK: Orientation Delegates....
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        
        if landscapeOrientationChangeFlag {
            
            return UIInterfaceOrientationMask.all
            
        } else {
            
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        
        if token.isEmpty {
            
            
        }else {
            
            UserDefaults.standard.setValue(token, forKey: USER_DEFAULT.DEVICE_TOKEN)
            UserDefaults.standard.synchronize()
        }
    }
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable : Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let aps = userInfo["aps"] as! [String: AnyObject]
      print(aps)
        
        let state: UIApplicationState = application.applicationState
        if state != UIApplicationState.active {
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Pushreceived"), object: self, userInfo:userInfo)
         /*   ["sound": ping.aiff, "alert": {
                "action-loc-key" =     {
                    id = 59d615b4d07f1e333e023ce8;
                    role = LIKE;
                };
                body = "akilesh v liked your post";
                title = "post like notification";
                }]
*/
            
            /*
 body: { message: data.message }, title: 'message', action-loc-key: {role: 'message', id: 'userid'}
 */
            
        } else {
            let result: AnyObject? = aps as AnyObject
            let alertDic = result?["alert"] as? NSDictionary
            
            let titleMessage = (alertDic!["title"] as? String)!.uppercased()
            
            
            
            print(userInfo)
            
            if titleMessage == "MESSAGE" {
                
                 self.chatUserInfo = userInfo as [NSObject : AnyObject]
                self.addNotificationAlert(newdic: alertDic!)
                
                
            }else {
                
                let bodyMessage = alertDic?["body"] as? String
            
            let alert = UIAlertController(title: titleMessage, message:bodyMessage, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Pushreceived"), object: self, userInfo:userInfo)
                
                            }))
            
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
        }
        }
        
        
    }
    
    
    func addNotificationAlert(newdic : NSDictionary){
        if self.chatAlertBanner == nil {
            
            self.chatAlertBanner =  UINib(nibName: "ChatNotificationView", bundle: nil).instantiate(withOwner: ChatNotificationView.self, options: nil)[0] as! ChatNotificationView
            
            self.window!.addSubview(self.chatAlertBanner)
            
            
        }
        
        if let bodymessage = newdic["body"] as? NSDictionary {
        
        
        self.chatAlertBanner.userMessageLabel.text = bodymessage["message"] as? String
            
        }
        
        if let alocDic = newdic["action-loc-key"] as? NSDictionary{
            
            self.userId = (alocDic["id"] as? String)!

        }
        
        self.chatAlertBanner.userNameLabel.text = "CoMute :"
        
        
        self.chatAlertBanner.frame = CGRect(x: 0, y: -50, width: (self.window!.bounds.width), height: 50)
        
        let tapGuesture =  UITapGestureRecognizer(target: self, action: #selector(AppDelegate.fireChatNotification(userInfo:)))
        tapGuesture.numberOfTapsRequired = 1
        tapGuesture.numberOfTouchesRequired = 1
        self.chatAlertBanner.addGestureRecognizer(tapGuesture)
        
        UIView.animate(withDuration: 0.35, animations: {
            
            self.chatAlertBanner.frame.origin.y += 70
            
        }, completion: { (something) in
            
            self.perform(#selector(AppDelegate.dismissNotificationAlertBanner), with: nil, afterDelay: 2)
            
            //                                    if #available(iOS 10.0, *) {
            //                                        NSTimer(timeInterval: 3, repeats: false, block: { (NSTimer) in
            //
            //                                            UIView.animateWithDuration(2, animations: {
            //
            //                                                self.dismissNotificationAlertBanner()
            //                                            })
            //
            //                                        }).fire()
            //
            //                                    } else {
            //                                        // Fallback on earlier versions
            //
            //                                        NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector:#selector(self.dismissNotificationAlertBanner), userInfo: nil, repeats: false)
            //
            //                                    }
        })
        
        
    }
    
    
    
    
    // MARK: - Core Data stack

//    @available(iOS 9.0, *)
//    lazy var persistentContainer: NSPersistentContainer = {
//        /*
//         The persistent container for the application. This implementation
//         creates and returns a container, having loaded the store for the
//         application to it. This property is optional since there are legitimate
//         error conditions that could cause the creation of the store to fail.
//        */
//        let container = NSPersistentContainer(name: "CoMute")
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                 
//                /*
//                 Typical reasons for an error here include:
//                 * The parent directory does not exist, cannot be created, or disallows writing.
//                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
//                 * The device is out of space.
//                 * The store could not be migrated to the current model version.
//                 Check the error message to determine what the actual problem was.
//                 */
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//
//    // MARK: - Core Data Saving support
//
//    func saveContext () {
//        if #available(iOS 9.0, *) {
//            _ = persistentContainer.viewContext
//        } else {
//            // Fallback on earlier versions
//        }
//        
//    }
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                guard granted else { return }
                self.getNotificationSettings()

            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                print("Notification settings: \(settings)")
                
                guard settings.authorizationStatus == .authorized else { return }
                UIApplication.shared.registerForRemoteNotifications()
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func dismissNotificationAlertBanner(){
        
        UIView.animate(withDuration: 0.35, animations: {
            
            self.chatAlertBanner.frame.origin.y = -70
            
        }) { (Bool) in
            
            // self.chatAlertBanner.removeFromSuperview()
        }
        
    }
    
    func fireChatNotification(userInfo:AnyObject){
        
        if chatUserInfo != nil {
            
           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Pushreceived"), object: self, userInfo:chatUserInfo)
        }
        self.chatAlertBanner.removeFromSuperview()
    }

}

