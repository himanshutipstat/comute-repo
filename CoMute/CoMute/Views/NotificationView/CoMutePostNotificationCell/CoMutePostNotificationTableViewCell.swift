//
//  CoMutePostNotificationTableViewCell.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 19/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class CoMutePostNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var noOfPeopleLikedPostLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUserInterface()
        // Initialization code
    }
    
    
    func setUserInterface(){
        
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
