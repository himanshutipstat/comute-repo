//
//  RequestApprovedNotificationTableViewCell.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 19/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class RequestApprovedNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNAmeLabel: UILabel!
    @IBOutlet weak var userFull: UILabel!
    @IBOutlet weak var messageButtonOutlet: UIButton!
    
    
     var userID = String()
    
    
    //Private Variable
    var DataDictionary : NSDictionary? = nil {
        
        didSet {       //Property Observer...
            
            //self.SocketDataDictionary = newValue
            self.settingData()
        }
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUserInterface()
        // Initialization code
    }
    
    
    func setUserInterface(){
        
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func settingData(){
        
        
        /* friendRequestAccept =         (
         {
         "_id" = 5a01a337410856034263d003;
         firstname = Himanshu;
         image = "https://s3.ap-south-1.amazonaws.com/comut/af931b95-c3f9-4261-b870-8662ee66080e";
         lastname = Aggarwal;
         username = iceparticle;
         }
         );*/
        
       if let dict = self.DataDictionary {
        
        if let id = dict["_id"] as? String {
            
            self.userID = id
        }
        
        let userImage = dict["image"] as! String
        
        let userImageURL = NSURL(string: userImage)
        let userImageRequest = NSURLRequest(url: userImageURL! as URL)
        NSURLConnection.sendAsynchronousRequest(userImageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
            response, data, error in
            if error != nil {
                //      println("Image not found!")
            }else {
                DispatchQueue.main.async {
                    // println("success")
                    // if self.tag == indexPath.row {
                    let image = UIImage(data: data!)
                    self.userImageView.image = image
                    // }
                    
                }
                
            }
        })
        
        self.userNAmeLabel.text = dict["username"] as? String
        
        self.userFull.text = String(describing: dict["firstname"]!)+" "+String(describing: dict["lastname"]!)
        
        }
        
    }
    
}
