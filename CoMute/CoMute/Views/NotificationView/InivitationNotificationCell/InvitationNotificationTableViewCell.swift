//
//  InvitationNotificationTableViewCell.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 19/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

protocol invitationNotificationTableViewCellDelegate {
    
    func refresh()
}

class InvitationNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userFullNameLAbel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    //Private variables...
    var DataDictionary : NSDictionary? = nil  {
        
        didSet {       //Property Observer...
            
            //self.SocketDataDictionary = newValue
            self.settingData()
        }
    }
    
    var userID = String()
    var updateFriend = ComuteFriends()
    
    var delegate : invitationNotificationTableViewCellDelegate!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUserInterface()
        // Initialization code
    }
    
    
    func setUserInterface(){
        
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
    }
    
    func settingData(){
        
        /*
         {
         "_id" = 599c1aff1fcaef31363fe124;
         firstname = akilesh;
         image = "https://comut.s3.ap-south-1.amazonaws.com/1d1a1bb9-56cc-409c-9fd4-1650aa4557e2";
         lastname = vishwanath;
         username = akileshc;
         }
 */
        
         if let dict = self.DataDictionary {
            
            
            if let id = dict["_id"] as? String {
                
                self.userID = id
                
            }
            
            let userImage = dict["image"] as! String
            
            let userImageURL = NSURL(string: userImage)
            let userImageRequest = NSURLRequest(url: userImageURL! as URL)
            NSURLConnection.sendAsynchronousRequest(userImageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                response, data, error in
                if error != nil {
                    //      println("Image not found!")
                }else {
                    DispatchQueue.main.async {
                        // println("success")
                        // if self.tag == indexPath.row {
                        let image = UIImage(data: data!)
                        self.userImageView.image = image
                        // }
                        
                    }
                    
                }
            })
            
            self.userNameLabel.text = dict["username"] as? String
            
            self.userFullNameLAbel.text = String(describing: dict["firstname"]!)+" "+String(describing: dict["lastname"]!)
            
            if let message = dict["message"] as? String {
                
            self.messageLabel.text = message
            }
            
            
        }
        
        
        
    }
    @IBAction func acceptAction(_ sender: Any) {
        
        self.updateFriend.acceptComuteFriendsAPI(ID: self.userID) { (succeeded, response) in
            
            
            if succeeded {
                
               self.delegate.refresh()
                
            }
        }
        
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        self.updateFriend.cancelComuteFriendsAPI(ID: self.userID) { (succeeded, response) in
            
            if succeeded {
                
                self.delegate.refresh()
                
            }
        }
        
        
    }
    
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
