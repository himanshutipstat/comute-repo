//
//  ChatNotificationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 09/11/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation

import UIKit

class ChatNotificationView: UIView {
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var userMessageLabel: UILabel!
    @IBOutlet weak var chatSubView: UIView!
    
    //Initial View Initialization Functions....
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Setting Up Custom Fonts and UI...
        self.settingUpCustomFontsAndUI()
    }
    
    //MARK: Setting Up Customfonts and UI...
    func settingUpCustomFontsAndUI() {
        
        //Setting fonts...
        self.userNameLabel.font = UIFont(name: FONT.REGULAR, size: aspectWidth(width: 13))
        self.userMessageLabel.font = UIFont(name: FONT.REGULAR, size: aspectWidth(width: 10))
        
        chatSubView.layer.borderWidth = 1
        chatSubView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        
        //self.reciverTextLabel.sizeToFit()
    }
    
    
    
    
}
