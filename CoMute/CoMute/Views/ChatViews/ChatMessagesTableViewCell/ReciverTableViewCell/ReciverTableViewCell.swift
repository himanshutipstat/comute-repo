//
//  ReciverTableViewCell.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 21/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class ReciverTableViewCell: UITableViewCell {

    @IBOutlet weak var textHoldingLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    //Private variables...
    var SocketDataDictionary : NSDictionary? = nil  {
        
        didSet {       //Property Observer...
            
            //self.SocketDataDictionary = newValue
            self.settingData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //textHoldingLabel.layer.borderWidth = 1
        //textHoldingLabel.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        messageTextView.isEditable = false
        messageTextView.dataDetectorTypes = .all
        
       // self.textHoldingLabel.text = "fwefwefwfwewefewfef"
        // Initialization code
    }
    
    //MARK: Updating User Interface by Populating Data recieved via. Controller from Model.
    func settingData() {
        
        if let dict = self.SocketDataDictionary {
            
           // let currentDate = NSDate()
            
            
            //Populating Data on labels...
            self.messageTextView.text = dict["message"] as! String
            
            self.messageTextView.frame.size.width = self.messageTextView.contentSize.width
            
            print(self.messageTextView.text)
            
//            let dateFormatter = DateFormatter()
//            dateFormatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
//            let calendar = NSCalendar.current
            
//            if let date = dict["dateTime"]{
//                print(date)
//                print(currentDate)
//                
//                // Replace the hour (time) of both dates with 00:00
//                //let date1 = calendar.startOfDayForDate(date)
//               // let date2 = calendar.startOfDayForDate(currentDate)
//                
//                let flags = NSCalendar.Unit.day
//               // let components = calendar.components(flags, fromDate: date, toDate: date, options: [])
//                
//                print( components.day)
//                
//                if Int(components.day) == 0 {
//                    
//                    dateFormatter.dateFormat = "HH:mm"
//                    
//                    print(components.day)
//                    
//                }else {
//                    
//                    //dateFormatter.timeZone = NSTimeZone(name: "EST")
//                    dateFormatter.dateFormat = "dd-MM,HH:mm"
//                    
//                    print(components.day)
//                }
//                
//                self.reciverDateLabel.text = dateFormatter.stringFromDate(date)
//            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
