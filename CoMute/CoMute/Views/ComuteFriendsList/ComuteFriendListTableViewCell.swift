//
//  ComuteFriendListTableViewCell.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 07/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit



class ComuteFriendListTableViewCell: UITableViewCell {

    @IBOutlet weak var chatButtonOutlet: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        chatButtonOutlet.layer.borderWidth = 1
        chatButtonOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
    }

    @IBAction func chatScreenAction(_ sender: Any) {
       
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   /* {
    "_id" = 59944294163365027160f192;
    contact = 9891000355;
    firstname = Himanshu;
    image = "https://s3.ap-south-1.amazonaws.com/comut/63110ef7-d6ac-4df1-8a6c-03161480efaa";
    lastname = Aggarwal;
    username = himagg;
    }
    */
}
