//
//  AddFriendView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 12/10/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

protocol addFriendViewDeligate
{
    func cancelAction()
    func sendAction()
}

class AddFriendView : UIView {
    
    @IBOutlet weak var addFriendSubView: UIView!
    @IBOutlet weak var addMessageLabel: UILabel!
    @IBOutlet weak var addMessageTextView: UITextView!
    @IBOutlet weak var cancelOutlet: UIButton!
    @IBOutlet weak var sendOutlet: UIButton!
    
    var deligate : addFriendViewDeligate!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
        
        addFriendSubView.layer.borderWidth = 1
        addFriendSubView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        addMessageTextView.layer.borderWidth = 1
        addMessageTextView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        cancelOutlet.layer.borderWidth = 1
        cancelOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        sendOutlet.layer.borderWidth = 1
        sendOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        
    }
    
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        self.deligate.cancelAction()
        
        
    }
    
    @IBAction func sendRequestAction(_ sender: Any) {
        
        self.deligate.sendAction()
    }
    
    
    
}
