//
//  InviteFriendNavigationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 07/07/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

protocol inviteFriendNavigationViewDelegate {
    
    func popBack()
    func skipAction()
}

class InviteFriendNavigationView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rightButtonOutlet: UIButton!
     var delegate : inviteFriendNavigationViewDelegate!

    @IBAction func backButtonAction(_ sender: AnyObject) {
        
        delegate.popBack()
    }
    
    
    @IBAction func skipButtonAction(_ sender: AnyObject) {
        
        delegate.skipAction()
    }
   
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
