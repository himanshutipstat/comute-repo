//
//  MapLandScapeView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 25/10/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import GoogleMaps
import SocketIO

protocol mapLandScapeViewDelegate {
    
    func backButton()
    func viewProfileOption(id : String)
}

class MarkerPoint: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String
    
    
    init(position: CLLocationCoordinate2D, name: String) {
        self.position = position
        self.name = name
     
    }
}

class MapLandScapeView: UIView,CLLocationManagerDelegate,GMSMapViewDelegate,userMapDetailedViewDelegatre,GMUClusterManagerDelegate {
    @IBOutlet weak var navigationTitleLabel: UILabel!
    @IBOutlet weak var CoustmeLandscapeMapView: GMSMapView!
    
    
    let camera = GMSCameraPosition.camera(withLatitude: 40.792240, longitude: -73.138260, zoom: 8.0)
   // var mapLandscapeView = GMSMapView()
    var latitude1 = 40.75058
    var longitude1  = -73.99357999999999
    var latitude2 = 40.74584
    var longitude2 = -73.90297
    var locationCordArray = NSArray()
    var wtfStationCoordinates = NSArray()
    var  comuteUserCoordinates = NSArray()
    var userDetailedMapView = UserMapDetailedView()
      let socket = SocketIOClient(socketURL: NSURL(string: SERVER_ADDRESS.Socket_URL)! as URL, config:  [.log(true), .forcePolling(true)])
    let locationManager = CLLocationManager()
    var latitude  = String()
    var longitude = String()
    var ID = String()
    var isAuthorized = true
    var sendFriendRquest = ComuteFriends()
    let usersmarkeri = GMSMarker()
   
    
    
    var delegate : mapLandScapeViewDelegate!
    var clusterManager: GMUClusterManager!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         self.CoustmeLandscapeMapView.camera = camera
       // setMap ()
        //setWTFIcon()
       // setUserMap()
        
        self.isAuthorizedtoGetUserLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        //locationManager.startUpdatingHeading()
        self.CoustmeLandscapeMapView.delegate = self
        
        // Set up the cluster manager with default icon generator and renderer.
        var iconGenerator = GMUDefaultClusterIconGenerator()
        //let image = UIImage(named: "trainpoint")!
        //iconGenerator = GMUDefaultClusterIconGenerator(buckets: self.locationCordArray as! [NSNumber], backgroundImages: [image])
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: self.CoustmeLandscapeMapView, clusterIconGenerator: iconGenerator)
        self.clusterManager = GMUClusterManager(map: self.CoustmeLandscapeMapView, algorithm: algorithm, renderer: renderer)
        
        // Generate and add random items to the cluster manager.
        self.generateClusterItems()
        
        // Call cluster() after items have been added to perform the clustering and rendering on map.
        self.clusterManager.cluster()
        
        // Register self to listen to both GMUClusterManagerDelegate and GMSMapViewDelegate events.
        self.clusterManager.setDelegate(self, mapDelegate: self)
        
        self.socket.connect()
        let lac = ["lat": latitude,
                   "lon": longitude ]
        //MARK: Socket to Make Connection and Emit Registration
        let par = ["authorization": Singleton.sharedInstance.userAccessToken,
                   "location": lac] as [String : Any]
        print(par)
        
        //self.socket.emit("add user", with: [par])
        //   self.socket.emitWithAck("add user", with: [par])
        self.socket.on("connect") {[weak self] data, ack in
            
            print("ACK.......\(data), \(ack)")
            
            print("socket connected")
            
            self?.isAuthorized = true
            
            self?.socket.emit("add user", with: [par])
            
            return
        }
        
        
        
        self.socket.on("reconnect") {[weak self] data, ack in
            
            print("ACK.......\(data), \(ack)")
            
            print("socket reconnected")
            
            self?.socket.connect()
            
            // self?.socket.emit("add user", with: [par])
            
            return
        }
        
        // Check if User is Unauthorized
        self.socket.on("disconnect") { (data, ack) -> Void  in
            print("unauthorized")
            self.isAuthorized = false
            
            // print("data u\(data)")
            
            //  println("un :\(ack)")
            return
        }
        
        self.socket.on("getlocation"){ (data, ack) -> Void in
            
            /* ACK.......[{
             id = 599bd68193b8e77760a372bb;
             image = "https://s3.ap-south-1.amazonaws.com/comut/a80bb762-db0d-4e0b-a6ca-9d21a3c9b83d";
             location =     {
             lat = "40.0471";
             lon = "-73.95388";
             };
             }]*/
            print("ACK.......\(data), \(ack)")
            print(data)
            
            self.comuteUserCoordinates = (data as? NSArray)!
            self.setUserMap()
            
        }

        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        delegate.backButton()
       // self.socket.disconnect()
        self.locationManager.stopUpdatingLocation()
        self.CoustmeLandscapeMapView.removeFromSuperview()
    }
    
    
    func setMap (){
        
        print(locationCordArray)
        
       // self.mapLandscapeView = GMSMapView.map(withFrame: CGRect(x: 20, y: 60, width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width - 40, height: GLOBAL_KEY.CURRENT_DEVICE_SIZE.height - 120), camera: self.camera)
        
       // let cameraPosition = GMSCameraPosition.camera(withLatitude: self.latitude1, longitude: self.longitude1, zoom: 15.0)
       // self.CoustmeLandscapeMapView = GMSMapView.map(withFrame: CGRect.zero, camera: cameraPosition)
        
       
        
        //MArk1
        let path = GMSMutablePath()
        var rectangle  = GMSPolyline()
        
        UIView.animate(withDuration: 0.15, animations:{
        
        
        for  i in 0 ..< self.locationCordArray.count{
            
            
            
            
            if let cordinatesDic = self.locationCordArray[i] as? NSDictionary {
                
                let lat = cordinatesDic["stop_lat"] as? double_t
                let long = cordinatesDic["stop_lon"] as? double_t
                
                path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
                //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
                rectangle  = GMSPolyline(path: path)
                rectangle.strokeWidth = 2.0
                rectangle.strokeColor = UIColor(red: 255/11, green: 255/176, blue: 255/47, alpha: 1.0)
                
                rectangle.map = self.CoustmeLandscapeMapView
                
               
                
            }
            
      
        }
        }, completion: {finished in
            
            var bounds = GMSCoordinateBounds()
            
            for index in 1...path.count() {
                bounds = bounds.includingCoordinate(path.coordinate(at: index))
            }
            
            self.CoustmeLandscapeMapView.animate(with: GMSCameraUpdate.fit(bounds))
            
        }
        )
    
       // self.addSubview(mapLandscapeView)

    }
    
    
    func setUserMap(){
        
        /*
         /* ACK.......[{
         id = 599bd68193b8e77760a372bb;
         image = "https://s3.ap-south-1.amazonaws.com/comut/a80bb762-db0d-4e0b-a6ca-9d21a3c9b83d";
         location =     {
         lat = "40.0471";
         lon = "-73.95388";
         };
         }]*/
 
 */
        
        for i in 0 ..< self.comuteUserCoordinates.count {
            
            
        if let cordinatesDic = self.comuteUserCoordinates[i] as? NSDictionary {
            
           
            
            if let locDic = cordinatesDic["location"] as? NSDictionary {
                
                print(locDic)
            
               

                if let lat = locDic["lat"] as? double_t,let long = locDic["lon"] as? double_t {
           
                
            
            print(lat,long)
            
          
                    
            usersmarkeri.userData = cordinatesDic
            usersmarkeri.position = CLLocationCoordinate2D(latitude: Double(lat), longitude: Double(long))
              //  usersmarkeri.iconView = UINib(nibName: "UserMapDetailedView", bundle: nil).instantiate(withOwner: UserMapDetailedView.self, options: nil)[0] as! UserMapDetailedView
                }else if   let lat = locDic["lat"] as? String,let long = locDic["lon"] as? String {
                    
                    print(lat,long)
                    
                    
                    
                    
                    usersmarkeri.position = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
                    
                }
                
            if let imageurl = cordinatesDic["image"] as? String {
                
                let imageURL = NSURL(string: imageurl)
                let imageRequest = NSURLRequest(url: imageURL! as URL)
                NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                    response, data, error in
                    if error != nil {
                        
                        //
                        
                    }else {
                        // println("success")
                        let image = UIImage(data: data!)
                         self.usersmarkeri.icon = self.imageWithImage(image: image!, scaledToSize: CGSize(width: 30, height: 30))
                        
                        //self.spImageView.layer.cornerRadius = 45
                    }
                })
            }
           // markeri.icon =  UIImage(named: "Skull small") as UIImage?
            
            usersmarkeri.map  =  self.CoustmeLandscapeMapView
        }
            }
        
        
    }
    
    
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }


    func setWTFIcon(){
        
        // Creates a marker in the center of the map.
        
       
        
        
                for i in 0 ..< self.wtfStationCoordinates.count {
                    
                    if let cordinatesDic = self.locationCordArray[i] as? NSDictionary {
                        
                        let locDic = cordinatesDic["loc"] as? NSArray
                        
                        print(locDic)
                        
                        
                        let lat = locDic?[1] as? double_t
                        let long = locDic?[0] as? double_t
                        
                        print(lat!,long!)
                        
                        let markeri = GMSMarker()
                        
                        markeri.position = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
                        markeri.icon =  UIImage(named: "Skull small") as UIImage?
                        
                        markeri.map  =  self.CoustmeLandscapeMapView
                    }
                    
                    
                }
                
                // marker.title = "Sydney"
                //  marker.snippet = "Australia"
                
            }
    
    
    // MARK: - GMUClusterManagerDelegate
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                 zoom: CoustmeLandscapeMapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        CoustmeLandscapeMapView.moveCamera(update)
    
        return false
    }
    
   
    
    // MARK: - Private
    /// Randomly generates cluster items within some extent of the camera and adds them to the
    /// cluster manager.
    func generateClusterItems() {
        // let extent = 0.2
        for  i in 0 ..< self.locationCordArray.count{
            
            
            if let cordinatesDic = self.locationCordArray[i] as? NSDictionary {
                
                let mlat = cordinatesDic["stop_lat"] as? double_t
                let mlong = cordinatesDic["stop_lon"] as? double_t
                
                let lat = mlat!  // * randomScale()
                let lng = mlong!  //* randomScale()
                
                print(lat)
                print(lng)
                let name = cordinatesDic["stop_name"] as? String
                //let image = UIImage(named: "trainpoint")!
                let item = MarkerPoint(position: CLLocationCoordinate2DMake(lat, lng), name: name!)
            
                clusterManager.add(item)
                
            }
            
        }
    }

    
    //Market Tap Function
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
         if let poiItem = marker.userData as? MarkerPoint {
            
            print(poiItem.name)
            marker.title = poiItem.name
            
            
            
         }else {
        print("didTap marker \(String(describing: marker.userData))")
        
        let points = mapView.projection.point(for: marker.position)
        
        
//        // remove color from currently selected marker
//        if let selectedMarker = mapView.selectedMarker {
//            selectedMarker.icon = GMSMarker.markerImage(with: nil)
//        }
        
    
        
        // select new marker and make green
       // mapView.selectedMarker = marker
        
       
           self.userDetailedMapView =  UINib(nibName: "UserMapDetailedView", bundle: nil).instantiate(withOwner: UserMapDetailedView.self, options: nil)[0] as! UserMapDetailedView
        self.userDetailedMapView.clipsToBounds = true
        self.userDetailedMapView.delegate = self
        self.userDetailedMapView.frame = CGRect(x: points.x  , y: points.y, width: 141 , height: 65)
        self.userDetailedMapView.userImageView.image = marker.icon
        
        self.addSubview(self.userDetailedMapView)
        
        
      //  marker.iconView = self.userDetailedMapView
        
        let dic = marker.userData as! NSDictionary
        
        
        if let ID = dic["id"] as? String{
            
            
        self.ID = ID
        
        
     
      
            
        }
    }
    
        // tap event handled by delegate
        return false
    }
    
    // take care of the close event
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        userDetailedMapView.removeFromSuperview()
    }
    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus()
            != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            // print("User allowed us to access location")
            
            
            //do whatever init activities here.
        }
    }
    
    
    //this method is called by the framework on         locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("Did location updates is called")
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        
        let location = locations.last
        
        //        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        //
        //        self.customMapView?.animate(to: camera)
        //
        //        GeoAngle = self.setLatLonForDistanceAndAngle(userLocation: locationObj)
        //        currentmarker.position = (location?.coordinate)!
        
        self.latitude = "\(coord.latitude)"
        self.longitude = "\(coord.longitude)"
        
        let lac = ["lat": latitude,
                   "lon": longitude ]
        //MARK: Socket to Make Connection and Emit Registration
        let data = ["location": lac] as [String : Any]
        
        print(data)
        
        self.socket.emit("sendlocation", with: [data])
        
        print(coord.latitude)
        // print(coord.longitude)
        //store the user location here to firebase or somewhere
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        var direction = -newHeading.trueHeading as Double
        // currentmarker.icon = self.imageRotatedByDegrees(degrees: CGFloat(direction), image: UIImage(named: "arrow.png")!)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
    
    func addUser() {
        
        
        self.sendFriendRquest.requestComuteFriendsAPI(ID: self.ID, message: "Hello From the other Side", recievedResponse: { (succeeded, response) in
            
            if succeeded {
                
                  self.userDetailedMapView.removeFromSuperview()
            }
            
        })
        
    }
    
    func viewProfile() {
        
        self.delegate.viewProfileOption(id: self.ID)
        
        (UIApplication.shared.delegate as! AppDelegate).landscapeOrientationChangeFlag = false
        self.locationManager.stopUpdatingLocation()
        
        //self.socket.disconnect()
        self.CoustmeLandscapeMapView.removeFromSuperview()
        
//        let storyboard: UIStoryboard = UIStoryboard (name: "Main", bundle: nil)
//        
//        let userProfileVC = storyboard.instantiateViewController(withIdentifier: "ComuteProfileViewContoller") as! ComuteProfileViewContoller
//        userProfileVC.isCurrentUser = false
//        userProfileVC.notificationFlag = 0
//        userProfileVC.userID = self.ID
//        let currentController = self.getCurrentViewController()
//        print( self.getCurrentViewController()!)
//        
//        
//         currentController?.navigationController?.pushViewController(userProfileVC, animated: true)
        
          userDetailedMapView.removeFromSuperview()
        
        
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
        
    }



}
