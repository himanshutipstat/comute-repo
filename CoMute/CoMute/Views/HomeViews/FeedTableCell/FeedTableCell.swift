//
//  FeedTableCell.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 19/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

//protocol feedTableCellDelegate {
//    func present(index : Int!)
//}

class FeedTableCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    @IBOutlet weak var feedCollectionView: UICollectionView!
    @IBOutlet weak var mainView: UIView!
    
    var feedImageString = NSArray()
    var feedDataDic = NSDictionary()
    var viewController = UIViewController()
    var postImage = String()
   // var delegate : feedTableCellDelegate!
    
    func fillCollectionView(with feedImageString: NSArray!, controller: UIViewController!, image : String!) {
        viewController = controller
        self.feedImageString = feedImageString
        print(feedImageString)
        self.postImage = image
        self.feedCollectionView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print("Done.. ")
        
        feedCollectionView.dataSource = self
        feedCollectionView.delegate = self
        
        //Registering Collection View Classes...
        self.feedCollectionView.register(FeedCollectionCell.classForCoder(), forCellWithReuseIdentifier: "SubCell")
        self.feedCollectionView.register(UINib(nibName: "FeedCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SubCell")
        

        self.feedCollectionView.layer.borderWidth = 1
        self.feedCollectionView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
//        if self.feedImageString.count == 0 {
//            
//            self.feedCollectionView.backgroundView =  UINib(nibName: "NoView", bundle: nil).instantiate(withOwner: NoView.self, options: nil)[0] as! NoView
//        }
        
       // self.mainView.layer.borderWidth = 1
       // self.mainView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        

//        let layout = UICollectionViewFlowLayout()
//        
//            layout.scrollDirection = .horizontal
//        self.feedCollectionView.collectionViewLayout = layout
       // self.feedCollectionView.backgroundColor = UIColor.blue
        
    }
    
    //MARK: Collection view delegates and Data Sources...
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return aspectWidth(width: 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: aspectWidth(width: 0), left: aspectWidth(width: 20), bottom: aspectWidth(width: 0), right: aspectWidth(width: 20))
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return aspectWidth(width: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedImageString.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCell", for: indexPath) as! FeedCollectionCell
        
        if let articleDataDic = feedImageString[indexPath.row] as? NSDictionary {
         let articleDic = articleDataDic["articles"] as? NSDictionary
        
            if let picURL = articleDic?["image"] as? String {
            print(picURL)
        
        let imageURL = NSURL(string: picURL)
        let imageRequest = NSURLRequest(url: imageURL! as URL)
        NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
            response, data, error in
            if error != nil {
                //      println("Image not found!")
            }else {
                DispatchQueue.main.async {
                    // println("success")
                    
                    if    let image = UIImage(data: data!) {
                    cell.feeImageOutLet.image = image
                    }
                }
                
            }
        })
            }

    }
    
        //cell.frame  = CGRect(x: 20, y: 30, width: 150, height: 110)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //delegate.present(index: indexPath.row)
        let storyboard: UIStoryboard = UIStoryboard (name: "Main", bundle: nil)
        let vc: PostDetailedViewController = storyboard.instantiateViewController(withIdentifier: "PostDetailedViewController") as! PostDetailedViewController
        if let articleDataDic = feedImageString[indexPath.row] as? NSDictionary {
            
            let userImage  = articleDataDic["image"] as? String
            //self.postImage = userImage!
           vc.articleDict = (articleDataDic["articles"] as? NSDictionary)!
            vc.postImage = userImage!
            vc.userName = String(describing: articleDataDic["firstname"]!)+" "+String(describing: articleDataDic["lastname"]!)
            vc.fullName =  (articleDataDic["username"] as? String)!
        }
        //vc.articleDict = (feedImageString[indexPath.row] as? NSDictionary)!
        //let currentController = self.getCurrentViewController()
        viewController.present(vc, animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
        
    }
    
  
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let picDimension = self.frame.size.width / 2.0
//        return CGSize(width: picDimension, height: self.frame.height - 20 )
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        let leftRightInset = self.frame.size.width / 14.0
//        return UIEdgeInsetsMake(0, leftRightInset, 0, leftRightInset)
//    }
//    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
