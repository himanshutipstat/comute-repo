//
//  TabBarView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 12/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit


class TabBarView : UITabBar {
    

    @IBOutlet weak var homeScreenOutlet: UITabBarItem!
    @IBOutlet weak var myComuteScreenOutlet: UITabBarItem!
    @IBOutlet weak var firendlistOutlet: UITabBarItem!
    @IBOutlet weak var chatScreenOutlet: UITabBarItem!
    @IBOutlet weak var userProfileOutlet: UITabBarItem!
    
    //MARK: Initial Viewinitialization Functions...
    override func awakeFromNib() {
        
        
    }
    
    
    
}
