//
//  SegmentBarView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 03/07/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//


import Foundation
import UIKit
protocol segmentBarViewDelegate {
    
    
    func pushToAllPostVC()
    func pushToMyFeedVc()
    func pushToComuteTodayVC()
}

class SegmentBarView: UIView {
    
    
    @IBOutlet weak var allPostOutlet: UIButton!
    @IBOutlet weak var myFeedOutlet: UIButton!
    @IBOutlet weak var comuteTodayOutlet: UIButton!
    @IBOutlet weak var allButtonView: UIView!
    @IBOutlet weak var comuteTodayView: UIView!
    @IBOutlet weak var myFeedButtonView: UIView!
    @IBOutlet weak var myFeedLabel: UILabel!
    @IBOutlet weak var comuteTodayLabel: UILabel!
    @IBOutlet weak var myComuteLabel: UILabel!
    
    
    
    
    var delegate : segmentBarViewDelegate!
    
    //MARK: XIB Action...
    
    @IBAction func AllPostButtonAction(_ sender: AnyObject) {
        
        delegate.pushToAllPostVC()
        
        allButtonView.backgroundColor = UIColor.black
        comuteTodayView.backgroundColor = UIColor.white
        myFeedButtonView.backgroundColor = UIColor.white
        
        
        myFeedLabel.textColor = COLOR_CODE.BLACK_COLOR
        comuteTodayLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
        myComuteLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
        
        // sender.setTitleColor(COLOR_CODE.BLACK_COLOR, for: .normal)
       // sender.setTitleColor(COLOR_CODE.BLACK_COLOR, for: UIControlState.selected)
        // allPostOutlet.titleLabel?.textColor = COLOR_CODE.BLACK_COLOR
       // myFeedOutlet.setTitleColor(COLOR_CODE.LIGHT_GRAY_TEXT_COLOR, for: UIControlState.normal)
       // comuteTodayOutlet.setTitleColor(COLOR_CODE.LIGHT_GRAY_TEXT_COLOR, for: UIControlState./normal)
    }
    
    
    @IBAction func MyFeedButtonAction(_ sender: AnyObject) {
        delegate.pushToMyFeedVc()
        
        allButtonView.backgroundColor = UIColor.white
        comuteTodayView.backgroundColor = UIColor.white
        myFeedButtonView.backgroundColor = UIColor.black
        //  myFeedOutlet.titleLabel?.textColor = COLOR_CODE.BLACK_COLOR
        
        myFeedLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
        comuteTodayLabel.textColor = COLOR_CODE.BLACK_COLOR
        myComuteLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
       // allPostOutlet.setTitleColor(COLOR_CODE.LIGHT_GRAY_TEXT_COLOR, for: UIControlState.normal)
       // sender.setTitleColor(COLOR_CODE.BLACK_COLOR, for: UIControlState.selected)
       // comuteTodayOutlet.setTitleColor(COLOR_CODE.LIGHT_GRAY_TEXT_COLOR, for: UIControlState.normal)
        
    }
    
    @IBAction func ComuteTodayAction(_ sender: AnyObject) {
        
        delegate.pushToComuteTodayVC()
        
        allButtonView.backgroundColor = UIColor.white
        comuteTodayView.backgroundColor = UIColor.black
        myFeedButtonView.backgroundColor = UIColor.white
        
        myFeedLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
        myComuteLabel.textColor = COLOR_CODE.BLACK_COLOR
        comuteTodayLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
        // comuteTodayOutlet.titleLabel?.textColor = COLOR_CODE.BLACK_COLOR
        //allPostOutlet.setTitleColor(COLOR_CODE.LIGHT_GRAY_TEXT_COLOR, for: UIControlState.normal)
       // myFeedOutlet.setTitleColor(COLOR_CODE.LIGHT_GRAY_TEXT_COLOR, for: UIControlState.normal)
       // sender.setTitleColor(COLOR_CODE.BLACK_COLOR, for: UIControlState.selected)
        
    }
    
}
