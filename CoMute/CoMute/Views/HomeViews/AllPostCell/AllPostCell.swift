//
//  AllPostCell.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol allPostCellDelegate {
    
    func refreshData()
}

class AllPostCell : UITableViewCell {
    
    @IBOutlet weak var postImageView: UIImageView!
    
    @IBOutlet weak var postedUserImageView: UIImageView!
    @IBOutlet weak var postedUserName: UILabel!
    @IBOutlet weak var postedUserFullName: UILabel!
    @IBOutlet weak var likeButtonOutlet: UIButton!
    @IBOutlet weak var disLikeButtonOutlet: UIButton!
    @IBOutlet weak var shareButtonOutlet: UIButton!
    @IBOutlet weak var noOFLikes: UILabel!
    @IBOutlet weak var noOFDisLike: UILabel!
    @IBOutlet weak var discriptionTextView: ReadMoreTextView!
    
    @IBOutlet weak var viewProfileTap: UIButton!
    
    @IBOutlet weak var postType: UILabel!
    @IBOutlet weak var postedTime: UILabel!
    
    var updateArticle = ArticleModel()
    var tapGesture : UITapGestureRecognizer!
    var delegate : allPostCellDelegate!
    var artcileID = String()
    var userID = String()
    
    //Private variables...
    var PostDataDictionary : NSDictionary? = nil  {
        
        didSet {       //Property Observer...
            
            //self.SocketDataDictionary = newValue
            self.settingData()
        }
    }

    
    //let CharacterLimit = 100
        override func awakeFromNib() {
        super.awakeFromNib()
        //Initialization code
            
           // self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(AllPostCell.viewProfile))
           // tapGesture.numberOfTapsRequired = 1
            //tapGesture.numberOfTouchesRequired = 1
           // postedUserImageView.isUserInteractionEnabled = true
           // postedUserImageView.addGestureRecognizer(tapGesture)
           // postedUserFullName.addGestureRecognizer(tapGesture)
           // postedUserName.addGestureRecognizer(tapGesture)
            
            postImageView.contentMode = .scaleAspectFit
            
           
           
      // postDiscriptionTextView.delegate = self
        
    }
    
    func viewProfile(){
        
        if Singleton.sharedInstance.userID != self.userID {
            
            let storyboard: UIStoryboard = UIStoryboard (name: "Main", bundle: nil)
            
            let userProfileVC = storyboard.instantiateViewController(withIdentifier: "ComuteProfileViewContoller") as! ComuteProfileViewContoller
            
            userProfileVC.isCurrentUser = false
            userProfileVC.notificationFlag = 0
            userProfileVC.userID = self.userID
            let currentController = self.getCurrentViewController()
            //print( self.getCurrentViewController()!)
            
            
            currentController?.navigationController?.pushViewController(userProfileVC, animated: true)
            
            
        }
        
        
    }
    
    //MARK: Updating User Interface by Populating Data recieved via. Controller from Model.
    func settingData() {
        
        if let dict = self.PostDataDictionary {
            
            
            self.userID = dict["_id"] as! String
            let userArticleDict = dict["articles"] as! NSDictionary
            
            if let ID = userArticleDict["_id"] as? String {
                
                self.artcileID = ID
            }
            
            if  userArticleDict["isLikes"] as? NSNumber == 0 {
                
                self.likeButtonOutlet.setImage(UIImage(named: "Like"), for: .normal)
                //self.dislikeCountOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                
                
            }else {
                
                
                self.likeButtonOutlet.setImage(UIImage(named: "Like Active"), for: .normal)
                // self.dislikeCountOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                
            }
            
            if userArticleDict["isDisLikes"] as? NSNumber == 0 {
                
                self.disLikeButtonOutlet.setImage(UIImage(named: "dislike"), for: .normal)
            }else {
                
                self.disLikeButtonOutlet.setImage(UIImage(named: "dislike active"), for: .normal)
            }

            
            let likeCount = userArticleDict["likes"] as! NSNumber
            
            self.noOFLikes.text = String(describing: likeCount)
            
            let disLikeCount = userArticleDict["dislikes"] as! NSNumber
            
            self.noOFDisLike.text = String(describing: disLikeCount)
            
            let userImage = dict["image"] as! String
            
            let userImageURL = NSURL(string: userImage)
            let userImageRequest = NSURLRequest(url: userImageURL! as URL)
            NSURLConnection.sendAsynchronousRequest(userImageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                response, data, error in
                if error != nil {
                    //      println("Image not found!")
                }else {
                    DispatchQueue.main.async {
                        // println("success")
                       // if self.tag == indexPath.row {
                            let image = UIImage(data: data!)
                            self.postedUserImageView.image = image
                       // }
                        
                    }
                    
                }
            })
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
            
            //print(userArticleData["updateat"] as! String)
            
            if let dateString = userArticleDict["updateat"] as? String {
                
                let dateArr = dateString.components(separatedBy: ".")
                print(dateArr.first!)
                
                if let selectedDate = formatter.date(from: dateArr.first!) {
                    formatter.dateFormat = "h:mm a MMM dd, yyyy"
                    let date = formatter.string(from: selectedDate)
                    self.postedTime.text = date
                    
                }
            }
            
            let picURL = userArticleDict["image"] as? String
            
            let imageURL = NSURL(string: picURL!)
            let imageRequest = NSURLRequest(url: imageURL! as URL)
            NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                response, data, error in
                if error != nil {
                    //      println("Image not found!")
                }else {
                    DispatchQueue.main.async {
                        // println("success")
                       // if cell.tag == indexPath.row {
                            let image = UIImage(data: data!)
                            self.postImageView.image = image
                       // }
                        
                    }
                    
                }
            })
            let readMoreTextAttributes: [String: Any] = [
                NSForegroundColorAttributeName: UIColor.black,
                NSFontAttributeName: UIFont.boldSystemFont(ofSize: 9)
            ]
            // print(userArticleDict["image"] as? String!)
            
            if let text = userArticleDict["text"] as? String {
                self.discriptionTextView.text = text
                self.discriptionTextView.maximumNumberOfLines = 2
                self.discriptionTextView.attributedReadMoreText = NSAttributedString(string: "...More", attributes: readMoreTextAttributes)
                self.discriptionTextView.shouldTrim = true
                
            }else {
                
                // cell.discriptionTextView.text = userArticleDict["text"] as! String
                //cell.discriptionTextView.text = text
                self.discriptionTextView.maximumNumberOfLines = 2
                self.discriptionTextView.attributedReadMoreText = NSAttributedString(string: "...More", attributes: readMoreTextAttributes)
                self.discriptionTextView.shouldTrim = true
            }
            self.postType.text = userArticleDict["articletype"] as? String
            self.postedUserFullName.text = String(describing: dict["firstname"]!)+" "+String(describing: dict["lastname"]!)
            self.postedUserName.text =  dict["username"] as? String
            
        }
        
    }
    
    
    //MARK: Storyboard Action..
    
    @IBAction func viewProfileButtonAction(_ sender: Any) {
        
//        if Singleton.sharedInstance.userID != self.userID {
//            
//            let storyboard: UIStoryboard = UIStoryboard (name: "Main", bundle: nil)
//            
//            let userProfileVC = storyboard.instantiateViewController(withIdentifier: "ComuteProfileViewContoller") as! ComuteProfileViewContoller
//            
//            userProfileVC.isCurrentUser = false
//            userProfileVC.notificationFlag = 0
//            userProfileVC.userID = self.userID
//            let currentController = self.getCurrentViewController()
//            //print( self.getCurrentViewController()!)
//            
//            
//            
//            currentController?.navigationController?.pushViewController(userProfileVC, animated: true)
//            
//            
//        }
    }
    
    @IBAction func buttonAction(sender: UIButton) {
        switch sender.tag {
            
        case 101:
            
            print("Like")
             if Singleton.sharedInstance.userID != self.userID && self.userID != ""{
            self.updateArticle.likeArticleAPI(ID: self.artcileID, UserID: self.userID, recievedResponse: { (succeeded, response) in
                
                if succeeded {
                    
                    let dataDict = response["data"] as? NSDictionary
                    
                    if  dataDict?["isLikes"] as? NSNumber == 0 {
                        
                        self.likeButtonOutlet.setImage(UIImage(named: "Like"), for: .normal)
                        self.disLikeButtonOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                    }else {
                        
                        self.likeButtonOutlet.setImage(UIImage(named: "Like Active"), for: .normal)
                        self.disLikeButtonOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                        
                    }

                    
        
                    
                    //let count = Int(self.noOFLikes.text!)
                    self.noOFLikes.text = "\(String(describing: dataDict?["likes"] as! NSNumber))"
                    self.noOFDisLike.text = "\(String(describing: dataDict?["dislikes"] as! NSNumber))"
                    
                    self.delegate.refreshData()
                }
            })
            }
        case 102:
            
            print("disLike")
            if Singleton.sharedInstance.userID != self.userID && self.userID != ""{
            self.updateArticle.disLikeArticleAPI(ID: self.artcileID, UserID: self.userID, recievedResponse: { (succeeded, response) in
                
                if succeeded {
                    
                    let dataDict = response["data"] as? NSDictionary
                    
                    
                    
                    if  dataDict?["isDislikes"] as? NSNumber == 0 {
                        
                        self.likeButtonOutlet.setImage(UIImage(named: "Like"), for: .normal)
                        self.disLikeButtonOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                        
                        
                    }else {
                        
                        self.likeButtonOutlet.setImage(UIImage(named: "Like"), for: .normal)
                        self.disLikeButtonOutlet.setImage(UIImage(named: "dislike active"), for: .normal)
                        
                    }
                    
                    
                    //let count = Int(self.noOFDisLike.text!)
                    self.noOFDisLike.text = "\(String(describing: dataDict?["dislikes"] as! NSNumber))"
                    self.noOFLikes.text = "\(String(describing: dataDict?["likes"] as! NSNumber))"
                    
                    self.delegate.refreshData()
                    
                }
            })
            }
            
        case 103:
            
            let activityVC = UIActivityViewController(activityItems: ["dhdh "], applicationActivities: nil)
            
            //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            //
            
            activityVC.popoverPresentationController?.sourceView = sender as? UIView
            let currentController = self.getCurrentViewController()

            currentController?.present(activityVC, animated: true, completion: nil)
            
            print("share")
        default:
            
            break
        }
        
        
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let currentText = textView.text as NSString
//        let updatedText = currentText.replacingCharacters(in: range, with: text)
//        
//        return updatedText.characters.count <= CharacterLimit
//    }


}
