//
//  SlideMenuView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 09/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

protocol slideMenuViewDelegate {
    
    
    func popBack()
    func pushToChangeSettingVC()
    func pushToContactUsVC()
    func pushToTermsNConditionsVC()
    func pushToPrivacyNPolicy()
    
    
}

class SlideMenuView: UIView,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var slideMenuListTableView: UITableView!
    
    var slideMenuList  = ["Privacy Policy", "Terms & Conditions", "Settings", "Contact Us"]
    
     var delegate : slideMenuViewDelegate!
    //MARK: Initial Viewinitialization Functions...
    override func awakeFromNib() {
        
        slideMenuListTableView.delegate = self
        slideMenuListTableView.dataSource  = self
        
        
        //slideMenuListTableView.register(SlideMenuView.classForCoder(), forCellReuseIdentifier: "Cell")
        
    }
    
    //MARK: Setting Custom Fonts and UI...
    func settingCustomFontsAndUI() {
        
        

        
    }
    
    
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        
        delegate.popBack()
        
    }
    
    
    //MARK: TableVie Delegate & DataSource...
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell") as UITableViewCell
        cell.selectionStyle = .none
        cell.textLabel?.text = slideMenuList[indexPath.row]
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell") as UITableViewCell
        
        if indexPath.row == 0{
            
            delegate.pushToPrivacyNPolicy()
        }else if indexPath.row == 1{
            
            delegate.pushToTermsNConditionsVC()
        }else if indexPath.row == 2{
            
            delegate.pushToChangeSettingVC()
            
        }else if indexPath.row == 3 {
            
            delegate.pushToContactUsVC()
        }
    }
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
