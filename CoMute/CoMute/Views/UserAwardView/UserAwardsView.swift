//
//  UserAwardsView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 22/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

protocol userAwardsViewDelegate {
    
    func crossView()
}

class UserAwardsView: UIView,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var viewMoreAwardsTableView: UITableView!
    @IBOutlet weak var crossViewButtonOutlet: UIButton!
    
    var awardsArray = NSArray()
    var delegate : userAwardsViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewMoreAwardsTableView.delegate = self
        viewMoreAwardsTableView.dataSource = self
        
        viewMoreAwardsTableView.layer.borderWidth = 1
        viewMoreAwardsTableView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return  awardsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell") as UITableViewCell
        cell.selectionStyle = .none
        cell.textLabel?.text = awardsArray[indexPath.row] as? String
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont(name: "Proxima Nova Alt", size: 14)
        return cell

        
    }
    
    @IBAction func crossButtonAction(_ sender: Any) {
        
        delegate?.crossView()
    }
    
 

}
