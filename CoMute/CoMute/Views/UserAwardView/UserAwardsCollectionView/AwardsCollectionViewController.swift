//
//  AwardsCollectionViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 13/10/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class AwardsCollectionViewController: UICollectionViewController,userAwardsViewDelegate{
    
    var awardsArray = NSArray()
    var helperModel = HelperModel()
    var allAwardsDateArray = [String]()
    
    var userAwardsView = UserAwardsView()
     var tapGesture : UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        
        print("Cell")
        
        //Registering Collection View Classes...
        self.collectionView?.register(AwardsCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView?.register(UINib(nibName: "AwardsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        //Adding Tap Gesture...
        tapGesture = UITapGestureRecognizer(target: self, action:#selector(AwardsCollectionViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        //tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
       

        // Do any additional setup after loading the view.
    }
    
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        self.userAwardsView.removeFromSuperview()
    }

    func fillCollectionView(with awardCollectionArry: NSArray!) {
        self.awardsArray = awardCollectionArry
        self.collectionView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return aspectWidth(width: 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: aspectWidth(width: 20), left: aspectWidth(width: 30), bottom: aspectWidth(width: 20), right: aspectWidth(width: 30))
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return aspectWidth(width: 0)
    }

//    override func numberOfSections(in collectionView: UICollectionView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.awardsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemsPerRow:CGFloat = 2
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell =   collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! AwardsCollectionViewCell
        
        if let  userAwardData = self.awardsArray[indexPath.row] as? NSDictionary{
            
            cell.AwardsDataDictionary = userAwardData
            
            cell.restAllDateTimeOutlet.addTarget(self, action: #selector(AwardsCollectionViewController.showAllAwardsPopUp(sender:)), for: .touchUpInside)
            
        }
        
    
        // Configure the cell
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
    func crossView(){
        
        self.userAwardsView.removeFromSuperview()
        
    }
    
    
    func showAllAwardsPopUp(sender: UIButton){
        
       // self.allAwardsDateArray.removeAllObjects()
        
        
       
        
        let point : CGPoint = sender.convert(CGPoint.zero, to:collectionView)
        var indexPath = collectionView!.indexPathForItem(at: point)
       
            
        if let  userAwardData = self.awardsArray[(indexPath?.row)!] as? NSDictionary{
             if let dataTimeData = userAwardData["date"] as? NSArray {
                
                if !self.allAwardsDateArray.isEmpty {
                    
                    self.allAwardsDateArray = []
                }
                
                print(dataTimeData)
                
                let formatter = DateFormatter()
               
                
                UIView.animate(withDuration: 0.3, animations: {
                    
                    print(dataTimeData.count)
                    
                    for  i in 0..<dataTimeData.count {
                    
                  
                        
                        
                        
                        if  let dateString = dataTimeData[i] as? String {
                            
                            let dateArr = dateString.components(separatedBy: ".")
                            
                            print(dateArr.first!)
                            
                            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                            formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
                        
                            if let selectedDate = formatter.date(from: dateArr.first!) {
                            formatter.dateFormat = "MMMM dd, yyyy"
                            let date = formatter.string(from: selectedDate)
                            self.allAwardsDateArray.append(date)
                            }
                            
                        
                        
                        }
                        
                        
                 
                }
                
                }, completion: {finished in
                    
                    print( self.allAwardsDateArray)
                    let window = UIApplication.shared.keyWindow!
                    self.userAwardsView = UINib(nibName: "UserAwardsView", bundle: nil).instantiate(withOwner: UserAwardsView.self, options: nil)[0] as! UserAwardsView
                    self.userAwardsView.frame = CGRect(x: 0 , y: 0, width: self.view.layer.bounds.width, height: self.view.layer.bounds.height )
                    
                    self.userAwardsView.delegate = self
                    self.userAwardsView.awardsArray = self.allAwardsDateArray as NSArray
                    self.userAwardsView.viewMoreAwardsTableView.reloadData()
                    
                    
                    window.addSubview(self.userAwardsView)
                    
                   
                }
                )
                
     
           
            }
        
    }
       
    }
}
