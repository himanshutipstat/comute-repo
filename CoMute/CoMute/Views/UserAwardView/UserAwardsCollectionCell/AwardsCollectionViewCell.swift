//
//  AwardsCollectionViewCell.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 13/10/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class AwardsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var subCollectionView: UIView!
    @IBOutlet weak var articleTypeLabel: UILabel!
    @IBOutlet weak var awardsCountLabel: UILabel!
    @IBOutlet weak var firstDateTimeLabel: UILabel!
    @IBOutlet weak var secondDateTimeLabel: UILabel!
    @IBOutlet weak var thirdDateTimeLabel: UILabel!
    @IBOutlet weak var restAllDateTimeOutlet: UIButton!
    
    
    //Private variables...
    var AwardsDataDictionary : NSDictionary? = nil  {
        
        didSet {       //Property Observer...
            
            //self.SocketDataDictionary = newValue
            self.settingData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        subCollectionView.layer.borderWidth = 1
        subCollectionView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        self.restAllDateTimeOutlet.isExclusiveTouch = true
    }
    
    @IBAction func viewAllAwardsAction(_ sender: Any) {
        
        
    }
    
    
    //MARK: Updating User Interface by Populating Data recieved via. Controller from Model.
    
   /* {
    date =             (
    "2017-10-12T20:48:57.396Z",
    "2017-10-12T20:49:04.837Z"
    );
    name = DARWINS;
    },
    {
    date =             (
    "2017-10-12T20:49:12.995Z"
    );
    name = LIFERS;
    }*/

    func settingData() {
        
        if let dict = self.AwardsDataDictionary {
            
            self.articleTypeLabel.text = dict["name"] as? String
            
            if let dataTimeData = dict["date"] as? NSArray {
                
                self.awardsCountLabel.text = "\(dataTimeData.count)"
                
               
                
                if  dataTimeData.count > 0 {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
                    
                    if let dateString = dataTimeData[0] as? String {
                        
                        let dateArr = dateString.components(separatedBy: ".")
                        print(dateArr.first!)
                        
                        if let selectedDate = formatter.date(from: dateArr.first!) {
                            formatter.dateFormat = "MMMM dd, yyyy"
                            let date = formatter.string(from: selectedDate)
                            self.firstDateTimeLabel.text  = date
                            
                        }
                    }

                    
                   
                }
                
                if dataTimeData.count > 1 {
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
                    
                    if let dateString = dataTimeData[1] as? String {
                        
                        let dateArr = dateString.components(separatedBy: ".")
                        print(dateArr.first!)
                        
                        if let selectedDate = formatter.date(from: dateArr.first!) {
                            formatter.dateFormat = "MMMM dd, yyyy"
                            let date = formatter.string(from: selectedDate)
                            self.secondDateTimeLabel.text = date
                            
                        }
                    }

                    
                    
                }
                
                if dataTimeData.count > 2  {
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
                    
                    if let dateString = dataTimeData[2] as? String {
                        
                        let dateArr = dateString.components(separatedBy: ".")
                        print(dateArr.first!)
                        
                        if let selectedDate = formatter.date(from: dateArr.first!) {
                            formatter.dateFormat = "MMMM dd, yyyy"
                            let date = formatter.string(from: selectedDate)
                            self.thirdDateTimeLabel.text = date
                            
                        }
                    }
                     
                }
                
                if dataTimeData.count > 3 {
                    
                    self.restAllDateTimeOutlet.setTitle("View \(dataTimeData.count - 3) more.", for: .normal)
                }
            }

        }
        
    }
    
    

}
