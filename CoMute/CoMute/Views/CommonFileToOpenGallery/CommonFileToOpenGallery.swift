////
////  CommonFilkeTo.swift
////  CoMute
////
////  Created by Himanshu Aggarwal on 25/07/17.
////  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
////
//
//import UIKit
//import Photos
//
//class CommonFileToOpenGallary: NSObject {
//    
//    //Getting Library Referrence...
//    //    let libraryForFetchingGallaryPhotos = ALAssetsLibrary()
//    
//    var fetchResult: PHFetchResult<AnyObject>!
//    
//    
//    //MARK: Appearing Camera && Gallary...as Custom Gallary...
//    func Camera_GallaryOpen(imagePickerController: UIImagePickerController, completion: @escaping (_ succeeded: Bool)->()) {
//        
//        let actionSheet = UIAlertController(title: nil, message: "Get Photo:", preferredStyle:.actionSheet)
//        
//        //        let cameraAction = UIAlertAction(title: "Camera", style: .Default, handler: { (alert: UIAlertAction!) -> Void in
//        //
//        //            if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
//        //
//        //                imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera;
//        //                imagePickerController.allowsEditing = true
//        //                UIApplication.sharedApplication().delegate?.window!!.rootViewController!.presentViewController(imagePickerController, animated: true, completion: nil)
//        //
//        //            } else {
//        //
//        //                let alertView = UIAlertController(title: "", message: "Your device does not support camera", preferredStyle: UIAlertControllerStyle.Alert)
//        //                alertView.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
//        //
//        //                UIApplication.sharedApplication().delegate?.window!!.rootViewController!.presentViewController(alertView, animated: true, completion: nil)
//        //            }
//        //
//        //            print("Camera")
//        //        })
//        
//        let photoAction = UIAlertAction(title: "Photos", style: .default, handler: {(alert: UIAlertAction!) -> Void in
//            
//            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
//                
//                completion(true)
//                
//            } else {
//                
//                let alertView = UIAlertController(title: "", message: "Your device does not support Photos", preferredStyle: UIAlertControllerStyle.alert)
//                alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                
//                UIApplication.shared.delegate?.window!!.rootViewController!.present(alertView, animated: true, completion: nil)
//            }
//            
//            print("Photo")
//        })
//        
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) -> Void in
//            
//            print("Cancelled")
//        })
//        
//        //        actionSheet.addAction(cameraAction)
//        actionSheet.addAction(photoAction)
//        actionSheet.addAction(cancelAction)
//        
//        UIApplication.shared.delegate?.window!!.rootViewController!.present(actionSheet, animated: true, completion: nil)
//    }
//    
//    
//    //MARK: Appearing Camera && Gallary...as Custom Gallary...
//    func showingActionSheetForUploadImages(completion:@escaping(_ succeeded: Bool, _ actionNumber: Int)->()) {
//        
//        let actionSheet = UIAlertController(title: nil, message: "Upload Photo:", preferredStyle:.actionSheet)
//        
//        let gallaryAction = UIAlertAction(title: "Gallery", style: .default, handler: { (alert: UIAlertAction!) -> Void in
//            
//            if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
//                
//                completion(true, 1)
//                
//            } else {
//                
//                let alertView = UIAlertController(title: "", message: "Your device does not support Gallery", preferredStyle: UIAlertControllerStyle.alert)
//                alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                
//                UIApplication.shared.delegate?.window!!.rootViewController!.present(alertView, animated: true, completion: nil)
//                completion(false, 1)
//            }
//            
//            print("Camera")
//        })
//        
//        let facebookAction = UIAlertAction(title: "Facebook", style: .default, handler: {(alert: UIAlertAction!) -> Void in
//            
//            completion(true, 2)
//            print("Photo")
//        })
//        
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) -> Void in
//            
//            print("Cancelled")
//        })
//        
//        actionSheet.addAction(gallaryAction)
//        actionSheet.addAction(facebookAction)
//        actionSheet.addAction(cancelAction)
//        
//        UIApplication.shared.delegate?.window!!.rootViewController!.present(actionSheet, animated: true, completion: nil)
//    }
//    
//    
//    //MARK: Loading Photos from Gallaries...And Making Custom Gallary itself.
//    func loadPhotos(completionHandler:@escaping (_ scceeded: Bool, _ fatchResults: PHFetchResult<AnyObject>?)->()) {
//        
//        if PHPhotoLibrary.authorizationStatus() != .authorized {
//            
//            PHPhotoLibrary.requestAuthorization { (status) in
//                
//                if status == .authorized {
//                    
//                    ActivityIndicator.sharedInstance.showActivityIndicator()
//                    
//                    if self.fetchResult == nil {
//                        
//                        let allPhotosOptions = PHFetchOptions()
//                        allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
//                        self.fetchResult = PHAsset.fetchAssets(with: allPhotosOptions) as! PHFetchResult<AnyObject>
//                    }
//                    
//                    ActivityIndicator.sharedInstance.hideActivityIndicator()
//                    
//                    if self.fetchResult.count == 0 {
//                        
//                        completionHandler(false, self.fetchResult)
//                        
//                    } else {
//                        
//                        completionHandler(true, self.fetchResult)
//                    }
//                    
//                } else {
//                    
//                    completionHandler(false, self.fetchResult)
//                }
//            }
//            
//        } else {
//            
//            ActivityIndicator.sharedInstance.showActivityIndicator()
//            
//            if self.fetchResult == nil {
//                
//                let allPhotosOptions = PHFetchOptions()
//                allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
//                self.fetchResult = PHAsset.fetchAssets(with: allPhotosOptions) as! PHFetchResult<AnyObject>
//            }
//            
//            ActivityIndicator.sharedInstance.hideActivityIndicator()
//            
//            if self.fetchResult.count == 0 {
//                
//                completionHandler(false, self.fetchResult)
//                
//            } else {
//                
//                completionHandler(true, self.fetchResult)
//            }
//        }
//    }
//    
//    
//    //MARK: Making Default Camera and Gallary Open...
//    func default_Camera_Gallary_Open(imagePickerController: UIImagePickerController) {
//        
//        let actionSheet = UIAlertController(title: nil, message: "Get Photo:", preferredStyle:.actionSheet)
//        
//        //            let cameraAction = UIAlertAction(title: "Camera", style: .Default, handler: { (alert: UIAlertAction!) -> Void in
//        //
//        //                if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
//        //
//        //                    imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera;
//        //                    imagePickerController.allowsEditing = true
//        //                    UIApplication.sharedApplication().delegate?.window!!.rootViewController!.presentViewController(imagePickerController, animated: true, completion: nil)
//        //
//        //                } else {
//        //
//        //                    let alertView = UIAlertController(title: "", message: "Your device does not support camera", preferredStyle: UIAlertControllerStyle.Alert)
//        //                    alertView.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
//        //
//        //                    UIApplication.sharedApplication().delegate?.window!!.rootViewController!.presentViewController(alertView, animated: true, completion: nil)
//        //                }
//        //
//        //                print("Camera")
//        //            })
//        
//        let photoAction = UIAlertAction(title: "Photos", style: .default, handler: {(alert: UIAlertAction!) -> Void in
//            
//            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
//                
//                imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
//                imagePickerController.allowsEditing = true
//                UIApplication.shared.delegate?.window!!.rootViewController!.present(imagePickerController, animated: true, completion: nil)
//                
//            } else {
//                
//                let alertView = UIAlertController(title: "", message: "Your device does not support Photos", preferredStyle: UIAlertControllerStyle.alert)
//                alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                
//                UIApplication.shared.delegate?.window!!.rootViewController!.present(alertView, animated: true, completion: nil)
//            }
//            
//            print("Photo")
//        })
//        
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) -> Void in
//            
//            print("Cancelled")
//        })
//        
//        //            actionSheet.addAction(cameraAction)
//        actionSheet.addAction(photoAction)
//        actionSheet.addAction(cancelAction)
//        
//        UIApplication.shared.delegate?.window!!.rootViewController!.present(actionSheet, animated: true, completion: nil)
//    }
//}
//
