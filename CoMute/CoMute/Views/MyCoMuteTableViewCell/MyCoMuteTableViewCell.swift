//
//  MyCoMuteTableViewCell.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class MyCoMuteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainComuteView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var firstStationLabel: UILabel!
    @IBOutlet weak var secondStationLabe: UILabel!
    @IBOutlet weak var timeDurationLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusImageView: UIImageView!
    
    //Private variables...
    var ComuteDataDictionary : NSDictionary? = nil  {
        
        didSet {       //Property Observer...
            
            //self.SocketDataDictionary = newValue
            self.settingData()
        }
    }
    var sourceStation = String()
    var destinationStation = String()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func settingData(){
        
        /*
         {
         dest =             {
         "_id" = 59d88a56fd079855efde85b8;
         "agency_key" = LI;
         "arrival_time" = "12:16:00";
         "departure_time" = "12:16:00";
         "stop_id" = 9;
         "stop_sequence" = 1;
         "trip_id" = "GO302_17_102";
         };
         sorce =             {
         "_id" = 59d88a56fd079855efde85b9;
         "agency_key" = LI;
         "arrival_time" = "12:05:00";
         "departure_time" = "12:05:00";
         "stop_id" = 8;
         "stop_sequence" = 0;
         "trip_id" = "GO302_17_102";
         };
         },
 
 
 
 */
        
        //self.firstStationLabel.text = self.sourceStation
       // self.secondStationLabe.text = self.destinationStation
        self.timeDurationLabel.text = "On Time"
        if let dtatdic = ComuteDataDictionary {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm:ss"
            formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
            
            if let destrinationDic = dtatdic["dest"] as? NSDictionary {
                
                
                
                
            }
            if let sourceDic = dtatdic["sorce"] as? NSDictionary {
                
                if let arrivTime = sourceDic["arrival_time"] as? String {
                    
                    
                    if let selectedDate = formatter.date(from: arrivTime) {
                        
                        let dateRangeStart = Date()
                        let components = Calendar.current.dateComponents([.hour, .minute], from: dateRangeStart, to: selectedDate)
                        
                        print(dateRangeStart)
                        print(dateRangeStart)
                        print("difference is \(components.month ?? 0) months and \(components.weekOfYear ?? 0) weeks")
                            
                        
                        let months = components.month ?? 0
                        let weeks = components.weekOfYear ?? 0
                        
                        formatter.dateFormat = "h:mm a"
                        let date = formatter.string(from: selectedDate)
                        self.timeLabel.text = date
                    
                }
                
                
                
            }
            
            
        }
        
        
    }
    }
}
