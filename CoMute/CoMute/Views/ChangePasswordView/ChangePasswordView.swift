//
//  ChangePasswordView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 12/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

protocol changePasswordViewDelegate {
    
    
    func cancelViewButton()
    func SaveChangesButton()

}



class ChangePasswordView: UIView {
    
    @IBOutlet weak var oldPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    
    
    var delegate : changePasswordViewDelegate!
    
    //MARK: Storyboard Action...
    @IBAction func CancelButtonAction(_ sender: AnyObject) {
        
        delegate.cancelViewButton()
        
    }
    
    
    @IBAction func saveButtonAction(_ sender: AnyObject) {
        delegate.SaveChangesButton()
        
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
