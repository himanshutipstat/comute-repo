//
//  UserMapDetailedView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 27/10/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

protocol userMapDetailedViewDelegatre {
    
    func viewProfile()
    func addUser()
    
}

class UserMapDetailedView: UIView {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var viewProfileOutlet: UIButton!
    @IBOutlet weak var addFriendOutLet: UIButton!
    @IBOutlet weak var subView: UIView!
    
    var delegate : userMapDetailedViewDelegatre!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewProfileOutlet.layer.borderWidth = 1
        viewProfileOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        addFriendOutLet.layer.borderWidth = 1
        addFriendOutLet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        subView.layer.borderWidth = 1
        subView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
    }
    
    @IBAction func viewProfileButtonAction(_ sender: Any) {
        
        self.delegate.viewProfile()
        
        
        
    }
    
    @IBAction func addUserButtonAction(_ sender: Any) {
        
        self.delegate.addUser()
        
        
        
    }


}
