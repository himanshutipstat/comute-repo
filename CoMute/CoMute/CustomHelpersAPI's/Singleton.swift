//
//  Singleton.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit


final class Singleton {
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    
    static let sharedInstance = Singleton()
    
    // MARK: Local Variable
    
    static var token  = {0}()

    

 
//MARK: Rechability for checking internet connection....
    let reachability = Reachability()
    
//Check Internet Connection
    func checkInternetConnection() -> Bool {
        
        if reachability!.isReachable {
            
            return true
        }
        
        return false
    }
    
    
//MARK: Uploading Images in Background, sop is Global...
  //  let uploadingImageModel = UploadingImageModel()
    var uploadingImagesTasksIdentifiers = [Int]()
    var uploadingImagesURIIdentifiers = [String]()
    var uploadingImagesEntitiesName = [String]()
    var uploadingImagesForWorkIndexes = [Int]()
    var toBeDeletedImagesURI = [String]()
    
//Semaphore to safe the multithreading images upload....
    var commonSemaphore = DispatchSemaphore(value: 1)
//    var mutexForCoreData = pthread_mutex_t()
    
//MARK: Sign UP USER TYPE...
    var userFullName = String()
    var userName = String()
    var userImage = String()
    var userID = String()
    var userAccessToken = String()
    var userHomeStation = String()
    var userDestinationStation = String()
    var maximumUserStep = Int() //If user edit profile then this step will be sent to server.....
    var maxImageValidationFlag = Bool()  //Checking in minimum images are uploaded or not....
    var accessToken_RM = String()
    
    var castingDirectorVerifiedByAdmin = Int() //status of CD 
    var userMobile = ""
    
    // MARK: Chat InsertID..
    var insertID = Int() // Last Message Id we have to Stored
    
//MARK: App URL......
   // let appCurrentURL = "https://itunes.apple.com/us/app/kalakaar/id1148314174?ls=1&mt=8"
  
    
//MARK: Facebook Data...
    var facebookAccessToken = String()
    var facebookId = String()
//    var facebookBusinessToken = String()
    
//MARK: Range SLider TYPE: SINGLE_THIMB or DOUBLE_THUMB...
    var rangeSliderType = GLOBAL_KEY.RANGE_SLIDER_SINGLE_THUMB
    
//MARK: Image Cache...
   // var completeAppImageCache = NSCache()
   // var faceDetectionImages = NSCache()
    
    
//MARK: VALID EMAIL CHECK
    func isEmailValid(email: String) -> Bool {
        
        let emailRegEx = "(?:[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[A-Za-z0-9-]*[A-Za-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if emailTest.evaluate(with: email) {
            
            return true
        }
        return false
    }
    
    
    
//MARK: VALID URL CHECK
    func isURLValid(url: String) -> Bool {
        
        let urlRegEx = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&amp;=]*)?"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        
        if urlTest.evaluate(with: url) {
            
            return true
        }
        
        return false
    }
    
//MARK: Valid Password...
    func isValidPassword(password: String) -> Bool {
        
        
        
        if (password.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) as NSString).length >= 8 {
            
            return true
        }
        
        return false
    }
    
//MARK: Valid Youtube URL....
    func isValidYoutubeURL(url: String) -> Bool {
        
        let urlRegEx = "(http(s)?:\\/\\/)?(www\\.|m\\.)?youtu(be\\.com|\\.be)(\\/watch\\?([&=a-z]{0,})(v=[\\d\\w]{1,}).+|\\/[\\d\\w]{1,})"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        
        if urlTest.evaluate(with: url) {
            
            return true
        }
        
        return false
    }
    
    
//MARK: VALID PHONE NUMBER CHECK
    func isPhoneNumberValid(number:String) -> Bool {       //ask about how to make it
        
        let phoneRegExp = "[0123456789][0-9]{9}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegExp)
        if phoneTest.evaluate(with: number) {
            
            if (number as NSString).hasPrefix("0") {
                
                return false
            }
            
            return true
        }
        return false
    }

    
//    func validateUrl (stringURL : NSString) -> Bool {
//        
//        var urlRegEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
//        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[urlRegEx])
//        var urlTest = NSPredicate.predicateWithSubstitutionVariables(predicate)
//        
//        return predicate.evaluateWithObject(stringURL)
//    }
    
    
//MARK: Setting minimum date according to nCalander...
    func setMinimumDate(day: Int, month: Int, year: Int) -> NSDate {
        
        let currentDate: NSDate = NSDate()
        
    ///Setting Calander....
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        calendar.timeZone = NSTimeZone.system
        
    //Setting Component...
        let components: NSDateComponents = NSDateComponents()
        components.calendar = calendar as Calendar
        
    //Getting the current month and day..
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        
    //Current Day...
        let currentDay = formatter.string(from: currentDate as Date)
        
    //Current Month...
        formatter.dateFormat = "MM"
        let currentMonth = formatter.string(from: currentDate as Date)
        
        components.year = -year
        components.month = -Int(currentMonth)! + month
        components.day = -Int(currentDay)! + day
        let minDate: NSDate = calendar.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
        
        return minDate
    }
    //MARK: Setting maximum date according to nCalander...
    func setMaximumDate(day: Int, month: Int, year: Int) -> NSDate {
        
        let currentDate: NSDate = NSDate()
        
        ///Setting Calander....
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        calendar.timeZone = NSTimeZone.system
        
        //Setting Component...
        let components: NSDateComponents = NSDateComponents()
        components.calendar = calendar as Calendar
        
        //Getting the current month and day..
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        
        //Current Day...
        let currentDay = formatter.string(from: currentDate as Date)
        
        //Current Month...
        formatter.dateFormat = "MM"
        let currentMonth = formatter.string(from: currentDate as Date)
        
        components.year = +year
        components.month = +Int(currentMonth)! + month
        components.day = +Int(currentDay)! + day
        let minDate: NSDate = calendar.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
        
        return minDate
    }

    
    
    func convertingUTCDateToCurrentSystemDate(UTCDate: String?) -> (String, String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let UTCDate = dateFormatter.date(from: UTCDate!)
        
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let localDate = dateFormatter.string(from: UTCDate!)
        
        dateFormatter.dateFormat = "hh:mm a"
        let time = dateFormatter.string(from: UTCDate!)
        
        return (localDate, time)
    }
    
    
    func convertingServerSystemLocalTimeToMySystemTime(UTCDate: String?) -> (String, String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let UTCDate = dateFormatter.date(from: UTCDate!)
        print(UTCDate)
   
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        if UTCDate != nil {
        
            let localDate = dateFormatter.string(from: UTCDate!)
            
            dateFormatter.dateFormat = "hh:mm a"
            let time = dateFormatter.string(from: UTCDate!)
            
            return (localDate, time)
            
        } else {
            
            return ("", "")
        }
    }
    
    
    func getUTCFormateDate(localDate: String)->String {     //getting UTC date from current date...
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateFormatted = dateFormatter.date(from: localDate)
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        dateFormatter.dateFormat =  "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateString = dateFormatter.string(from: dateFormatted!)
        
        return dateString
    }
}
