//
//  KeyboardControls.swift
//  keyboardcontrols
//
//  Created by Ivan Milles on 09/02/15.
//  Copyright (c) 2015 Mr Green. All rights reserved.
//

import UIKit

enum KeyboardControl: UInt8 {
	case AllZeros = 0b00
	case PreviousNext = 0b01
	case Done = 0b10
	case AllButtons = 0b11		// Needed for pretty weak Swift NSOption support
}

enum KeyboardControlsDirection: Int {
	case Previous = 0
	case Next = 1
}

protocol KeyboardControlsDelegate {
	func keyboardControls(keyboardControls: KeyboardControls, selectedField field: UIView, inDirection direction: KeyboardControlsDirection);
	func keyboardControlsDonePressed(keyboardControls: KeyboardControls)
}

class KeyboardControls: UIView {
	var toolbar: UIToolbar!
	var doneButton: UIBarButtonItem!
	var previousButton: UIBarButtonItem!
	var nextButton: UIBarButtonItem!
	
	var delegate: KeyboardControlsDelegate?
	var visibleControls: KeyboardControl = .AllZeros {
		didSet {
			updateToolbar()
		}
	}
	var fields: [UITextField] = [] {		// TODO: Support for UITextView too
		didSet {
			installOnFields()
		}
	}
	
	var activeField: UITextField? {
		didSet {
			activeField?.becomeFirstResponder()
			updatePreviousNextEnabledStates()
		}
	}
	
	var barStyle: UIBarStyle {
		get {return toolbar.barStyle}
		set {toolbar.barStyle = newValue}
	}
	var barTintColor: UIColor? {
		get {return toolbar.barTintColor}
		set {toolbar.barTintColor = newValue}
	}
	var doneTintColor: UIColor? {
		get {return doneButton.tintColor}
		set {doneButton.tintColor = newValue}
	}
	var doneTitle: String? {
		get {return doneButton.title}
		set {doneButton.title = newValue}
	}
	
	required convenience init(coder aDecoder: NSCoder) {
		self.init(fields: [])
	}
	
	override convenience init(frame: CGRect) {
		self.init(fields: [])
	}
	
	init(fields: [UITextField]) {
		super.init(frame: CGRect(x: 0.0, y: 0.0, width: GLOBAL_KEY.TARGET_DEVICE_SIZE.width, height: 44.0))
		toolbar = UIToolbar(frame: self.frame)
		barStyle = .default
		toolbar.autoresizingMask = [.flexibleLeftMargin , .flexibleRightMargin]
		addSubview(toolbar)
		previousButton = UIBarButtonItem(title: "<", style: .plain, target: self, action: #selector(KeyboardControls.selectPreviousField))
		nextButton = UIBarButtonItem(title: ">", style: .plain, target: self, action: #selector(KeyboardControls.selectNextField))
		doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(KeyboardControls.doneButtonPressed))
		visibleControls = KeyboardControl(rawValue: KeyboardControl.PreviousNext.rawValue | KeyboardControl.Done.rawValue)!
		
		self.fields = fields

		// didSet observers not called from init()
		installOnFields()
		updateToolbar()
	}
	
	func installOnFields() {
		for field in fields {
			field.inputAccessoryView = self
		}
	}
	
	func updateToolbar() {
		toolbar.items = toolbarItems() as? [UIBarButtonItem]
	}
	
	func toolbarItems() -> [AnyObject] {
		var outItems = [AnyObject]()

		if visibleControls.rawValue & KeyboardControl.PreviousNext.rawValue > 0 {
			outItems.append(previousButton)
			outItems.append(nextButton)
		}
		
		if visibleControls.rawValue & KeyboardControl.Done.rawValue > 0 {
			outItems.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))
			outItems.append(doneButton)
		}
		
		return outItems
	}
	
	func updatePreviousNextEnabledStates() {
        


		if let index = fields.index(of: activeField!) {
			previousButton.isEnabled = (index > 0)
			nextButton.isEnabled = (index < fields.count - 1)
		}
	}
	
	func selectPreviousField() {
        
        ////////println(fields)

        if activeField == nil
        {
            activeField = fields[0]
        }
        
        
		if let index = fields.index(of: activeField!) {
			if index > 0 {
				activeField = fields[index - 1]
				delegate?.keyboardControls(keyboardControls: self, selectedField: activeField!, inDirection: .Previous)
			}
		}
	}

	func selectNextField() {
        ////////println(fields.count)
        ////////println(fields)

        if activeField == nil
        {
           activeField = fields[0]
        }
        
		if let index = fields.index(of: activeField!) {
			if index < fields.count - 1 {
				activeField = fields[index + 1]
				delegate?.keyboardControls(keyboardControls: self, selectedField: activeField!, inDirection: .Next)
			}
		}
	}
	
	func doneButtonPressed() {
		delegate?.keyboardControlsDonePressed(keyboardControls: self)
	}
}
