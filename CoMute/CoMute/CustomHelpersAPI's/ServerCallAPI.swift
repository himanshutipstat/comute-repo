//
//  ServerCallAPI.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 10/5/17.
//  Copyright (c) 2015 Tipstat. All rights reserved.
//

import Foundation
import UIKit

//MARK: CREATing a Json string in ios....
func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
    
    let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
    
    if JSONSerialization.isValidJSONObject(value) {
        
        var data: NSData?
        
        do {
            
            data = try JSONSerialization.data(withJSONObject: value, options: options) as NSData?
            
        } catch {
            
            print("Error")
        }
        
//        let data = NSJSONSerialization.dataWithJSONObject(value, options: options, error: nil)

        if let string = NSString(data: data! as Data, encoding: String.Encoding.utf8.rawValue) {
            
            return string as String
        }
    }
    
    return ""
}
//MARK: for Chat server address...>
func sendRequestToChatServer(url: NSString, params: String, relative:Bool = true, httpMethod: NSString, receivedResponse:@escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) -> URLSessionDataTask {
    
    var request: NSMutableURLRequest!
    
    if !relative{
        request = NSMutableURLRequest(url: NSURL(string: url as String)! as URL)
    }
    else{
        request = NSMutableURLRequest(url: NSURL(string: SERVER_ADDRESS.Socket_URL+(url as String))! as URL)
    }
    
    let session = URLSession.shared
    request.httpMethod = httpMethod as String
    request.timeoutInterval = 20
    
    //Request Type...
    if(httpMethod == "POST") {
        
        request.httpBody = params.data(using: String.Encoding.utf8)!
        request.addValue(Singleton.sharedInstance.userAccessToken, forHTTPHeaderField: "authorization")
        // request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        // request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
    } else if httpMethod == "GET" {
        
        request.httpBody = params.data(using: String.Encoding.utf8)!
        request.addValue(Singleton.sharedInstance.userAccessToken, forHTTPHeaderField: "authorization")
        
        
    } else if httpMethod == "PUT" {
        
        request.httpBody = params.data(using: String.Encoding.utf8)!
        request.addValue(Singleton.sharedInstance.userAccessToken, forHTTPHeaderField: "authorization")
    }
    else if httpMethod == "PATCH"{
        
        request.httpBody = params.data(using: String.Encoding.utf8)!
    }
    
    //Hitting URL to get data....
    let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
        //    print(task)
        
        print("Response: \(String(describing: response))")
        
        var statusCode = Int()
        
        if response as? HTTPURLResponse != nil {
            
            statusCode = (response as! HTTPURLResponse).statusCode
            
        } else {
            
            statusCode = 0
        }
        
        print(statusCode)
        var err: NSError?
        var json: [String: AnyObject]?
        
        //Serializing Json....
        do {
            
            if data != nil {
                
                json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String: AnyObject]
                
            } else {
                
                err = NSError(domain: "", code: 401, userInfo: ["Failure": "Json could't serialized."])
            }
            
        } catch {
            
            json = [:]
            err = NSError(domain: "", code: 401, userInfo: ["Failure": "Json could't serialized."])
        }
        
        if json?.count != 0 && response != nil && (err == nil) {
            
            json?.updateValue(Int(statusCode) as AnyObject , forKey: "statusCode")
        }
        
        //Did the JSONObjectWithData constructor return an error? If so, log the error to the console
        if(err != nil) {
            
            receivedResponse(false, [:])
            
        } else {
            
            //The JSONObjectWithData constructor didn't return an error. But, we should still
            //check and make sure that json has a value using optional binding.
            if let _ = json {
                
                //Okay, the parsedJSON is here, let's get the value for 'success' out of it
                receivedResponse(true, json! as NSDictionary)
                
            } else {
                
                //Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                receivedResponse(false, (json as NSDictionary?)!)
            }
        }
    })
    task.resume()
    
    return task
}


//MARK: for server address...>
func sendRequestToServer(url: NSString, params: String, relative:Bool = true, httpMethod: NSString, receivedResponse:@escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) -> URLSessionDataTask {
    
    var request: NSMutableURLRequest!
    
    if !relative{
        request = NSMutableURLRequest(url: NSURL(string: url as String)! as URL)
    }
    else{
        request = NSMutableURLRequest(url: NSURL(string: SERVER_ADDRESS.BASE_URL+(url as String))! as URL)
    }
    
    let session = URLSession.shared
    request.httpMethod = httpMethod as String
    request.timeoutInterval = 20
    
//Request Type...
    if(httpMethod == "POST") {
        
        request.httpBody = params.data(using: String.Encoding.utf8)!
        request.addValue(Singleton.sharedInstance.userAccessToken, forHTTPHeaderField: "authorization")
      // request.addValue("application/json", forHTTPHeaderField: "Content-Type")
       // request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
    } else if httpMethod == "GET" {
        
        request.httpBody = params.data(using: String.Encoding.utf8)!
        request.addValue(Singleton.sharedInstance.userAccessToken, forHTTPHeaderField: "authorization")

        
    } else if httpMethod == "PUT" {
        
        request.httpBody = params.data(using: String.Encoding.utf8)!
        request.addValue(Singleton.sharedInstance.userAccessToken, forHTTPHeaderField: "authorization")
    }
    else if httpMethod == "PATCH"{
        
        request.httpBody = params.data(using: String.Encoding.utf8)!
        
    }else if httpMethod == "DELETE" {
        
        request.httpBody = params.data(using: String.Encoding.utf8)!
        request.addValue(Singleton.sharedInstance.userAccessToken, forHTTPHeaderField: "authorization")
    }
    
//Hitting URL to get data....
    let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
    //    print(task)
        
        print("Response: \(String(describing: response))")
        
        var statusCode = Int()
        
        if response as? HTTPURLResponse != nil {
            
            statusCode = (response as! HTTPURLResponse).statusCode
            
        } else {
            
            statusCode = 0
        }
        
        print(statusCode)
        var err: NSError?
        var json: [String: AnyObject]?
        
    //Serializing Json....
        do {
            
            if data != nil {
                
                json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String: AnyObject]
                
            } else {
                
                err = NSError(domain: "", code: 401, userInfo: ["Failure": "Json could't serialized."])
            }
            
        } catch {
            
            json = [:]
            err = NSError(domain: "", code: 401, userInfo: ["Failure": "Json could't serialized."])
        }
        
        if json?.count != 0 && response != nil && (err == nil) {
            
            json!.updateValue(statusCode as AnyObject, forKey: "statusCode")
        }
        
    //Did the JSONObjectWithData constructor return an error? If so, log the error to the console
        if(err != nil) {

           receivedResponse(false, [:])
            
        } else {
            
        //The JSONObjectWithData constructor didn't return an error. But, we should still
        //check and make sure that json has a value using optional binding.
            if let _ = json {
                
            //Okay, the parsedJSON is here, let's get the value for 'success' out of it
                receivedResponse(true, json! as NSDictionary)

            } else {
                
            //Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                receivedResponse(false, json! as NSDictionary)
            }
        }
    })
    task.resume()
    
    return task
}

////MARK: Server Address with Image Uploading..
//func sendRequestToServerWithImage(url: NSString, params: String, relative:Bool = true, httpMethod: NSString, receivedResponse:@escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) -> URLSessionDataTask {
//    
//    var request: NSMutableURLRequest!
//    
//    if !relative{
//        request = NSMutableURLRequest(url: NSURL(string: url as String)! as URL)
//    }
//    else{
//        request = NSMutableURLRequest(url: NSURL(string: SERVER_ADDRESS.BASE_URL+(url as String))! as URL)
//    }
//    
//    let session = URLSession.shared
//    request.httpMethod = httpMethod as String
//    request.timeoutInterval = 20
//    
//    //Request Type...
//    if(httpMethod == "POST") {
//        
//        request.httpBody = params.data(using: String.Encoding.utf8)!
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        
//        
//    } else if httpMethod == "GET" {
//        
//        request.httpBody = params.data(using: String.Encoding.utf8)!
//        
//    } else if httpMethod == "PUT" {
//        
//        request.httpBody = params.data(using: String.Encoding.utf8)!
//    }
//    
//    //Hitting URL to get data....
//    let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
//        
//        print("Response: \(String(describing: response))")
//        
//        var statusCode = Int()G2]C7$l1(
//        
//        if response as? HTTPURLResponse != nil {
//            
//            statusCode = (response as! HTTPURLResponse).statusCode
//            
//        } else {
//            
//            statusCode = 0
//        }
//        
//        print(statusCode)
//        var err: NSError?
//        var json: [String: AnyObject]?
//        
//        //Serializing Json....
//        do {
//            
//            if data != nil {
//                
//                json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String: AnyObject]
//                
//            } else {
//                
//                err = NSError(domain: "", code: 401, userInfo: ["Failure": "Json could't serialized."])
//            }
//            
//        } catch {
//            
//            json = [:]
//            err = NSError(domain: "", code: 401, userInfo: ["Failure": "Json could't serialized."])
//        }
//        
//        if json?.count != 0 && response != nil && (err == nil) {
//            
//            json!.updateValue(statusCode as AnyObject, forKey: "statusCode")
//        }
//        
//        //Did the JSONObjectWithData constructor return an error? If so, log the error to the console
//        if(err != nil) {
//            
//            receivedResponse(false, [:])
//            
//        } else {
//            
//            //The JSONObjectWithData constructor didn't return an error. But, we should still
//            //check and make sure that json has a value using optional binding.
//            if let _ = json {
//                
//                //Okay, the parsedJSON is here, let's get the value for 'success' out of it
//                receivedResponse(true, json! as NSDictionary)
//                
//            } else {
//                
//                //Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
//                receivedResponse(false, json! as NSDictionary)
//            }
//        }
//    })
//    task.resume()
//    
//    return task
//}
//
//func uploadData(url: URL, data: Data!, params: [String: String])
//{
//    let cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData;
//    let request = NSMutableURLRequest(url: url, cachePolicy: cachePolicy, timeoutInterval: 6.0);
//    request.httpMethod = "POST";
//    
//    let boundary = generateBoundaryString()
//    
//    request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//    
//    
//    if(data == nil)  { return; }
//    
//    request.httpBody = createBodyWithParameters(parameters: params, filePathKey: "file", data: data, boundary: boundary)
//    
//    
//    
//    //myActivityIndicator.startAnimating();
//    
//    let task = URLSession.shared.dataTask(with: request as URLRequest) {
//        data, response, error in
//        
//        if error != nil {
//            print("error=\(error)")
//            return
//        }
//        
//        // You can print out response object
//        print("******* response = \(response)")
//        
//        // Print out reponse body
//        let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//        print("****** response data = \(responseString!)")
//        
//        do {
//            let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
//            
//            print(json)
//            
//            
//            
//        }catch
//        {
//            //if you recieve an error saying that the data could not be uploaded,
//            //make sure that the upload size is set to something higher than the size
//            print(error)
//        }
//        
//        
//    }
//    
//    task.resume()
//    
//}
//
//
//func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, data: Data!, boundary: String) -> Data {
//    var body = Data();
//    
//    if parameters != nil {
//        for (key, value) in parameters! {
//            body.appendString(string: "--\(boundary)\r\n")
//            body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//            body.appendString(string: "\(value)\r\n")
//        }
//    }
//    
//    let mimetype = "text/csv"
//    
//    body.appendString(string: "--\(boundary)\r\n")
//    body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(parameters!["filename"]!)\"\r\n")
//    body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
//    
//    
//    body.append(data)
//    body.appendString(string: "\r\n")
//    
//    body.appendString(string: "--\(boundary)--\r\n")
//    
//    return body
//}
//
//
//
//
//func generateBoundaryString() -> String {
//    return "Boundary-\(NSUUID().uuidString)"
//}
