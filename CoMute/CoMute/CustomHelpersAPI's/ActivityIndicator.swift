////
////  ActivityIndicator.swift
////  CoMute
////
////  Created by Himanshu Aggarwal on 10/5/17.
////  Copyright (c) 2015 Tipstat. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//class ActivityIndicator:UIView {
//    
//    var activityIndicator:UIActivityIndicatorView!
//    
//    class var sharedInstance: ActivityIndicator {
//        
//        
//        struct Static {
//            
//            static var instance = ActivityIndicator(frame: CGRect(x: 0, y: 0, width : UIScreen.main.bounds.width, height:  UIScreen.main.bounds.height))
//            static var token  = {0}()
//        }
//        
//        _ = Static.token
//        
//       // DispatchQueue.main.async {
//        
//       // Static.instance = ActivityIndicator(frame: CGRect(x: 0, y: 0, width : UIScreen.main.bounds.width, height:  UIScreen.main.bounds.height))
//        //}
//        
//        return Static.instance
//    }
//    
////    class SingletonData: ActivityIndicator {
////        
////        
////        static let sharedInstance : SingletonData = {
////            let instance = SingletonData()
////            return instance
////        }()
////        
////        //MARK: Local Variable
////        
////                   static var instance: ActivityIndicator?
////                static var token  = {0}()
////        
////        //MARK: Init
////        
////        convenience override init() {
////            self.init(array : [])
////        }
////        
////        //MARK: Init Array
////        
////        init( array : [String]) {
////            //        emptyStringArray = array
////        }
////        
////        required init(coder aDecoder: NSCoder) {
////            fatalError("init(coder:) has not been implemented")
////        }
////    }
//    
//    override init(frame: CGRect) {
//        
//        super.init(frame: frame)
//        
//        self.backgroundColor = COLOR_CODE.TRANS_COLOR
//    }
//
//    
//    required init(coder aDecoder: NSCoder) {
//        
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    func showActivityIndicator() {
//        
//       DispatchQueue.global(qos: .userInitiated).async {
//            
//            if self.activityIndicator != nil {
//                
//                self.activityIndicator.stopAnimating()
//                self.activityIndicator.removeFromSuperview()
//                self.activityIndicator = nil
//            }
//            
//            self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
//            self.activityIndicator.center = self.center
//            self.activityIndicator.hidesWhenStopped = true
//            self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
//            self.activityIndicator.color = UIColor.white
//            self.activityIndicator.startAnimating()
//            self.addSubview(self.activityIndicator)
//            
//            (UIApplication.shared.delegate as! AppDelegate).window?.addSubview(self)
//        }
//        
//        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//        
//        UIView.animate(withDuration: 0.1, delay: 0.0, usingSpringWithDamping: 0.0, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
//
//            self.transform = CGAffineTransform.identity;
//            
//        }, completion: nil)
//     }
//    
//    func hideActivityIndicator() {
//        
//        DispatchQueue.global(qos: .userInitiated).async {
//            
//            if self.activityIndicator != nil {
//                
//                self.transform = CGAffineTransform.identity
//                self.activityIndicator.stopAnimating()
//                self.activityIndicator.removeFromSuperview()
//                self.activityIndicator = nil
//                self.removeFromSuperview()
//            }
//        }
//    }
//}
