//
//  CoMute
//
//  Created by Himanshu Aggarwal on 02/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//
import Foundation
import UIKit
//import MoEngage_iOS_SDK

//MARK: SERVER ADDRESS
struct SERVER_ADDRESS {
    
    static let BASE_URL = "http://34.235.124.46/api"

    static let Socket_URL = "http://34.235.124.46:8081"
}


struct GOOGLE_MAPS_KEY {
    
    static let SERVER_KEY = "AIzaSyAnjsAdyrqSm9kiU3q3QLLwd9DPaUSLaf0"
    static let BROWSER_KEY = "AIzaSyCZNMdlMAJe7IWw0P-pzk5Tnu3blEEAB-E"
    static let IOS_KEY = "AIzaSyCzCkjFMIZLoJCp1hZjjRVz91OT-XHWjgk"
}


//MARK: NSUSERDEFAULT KEYS
struct USER_DEFAULT {
    
    static let FACEBOOK_DATA = "facebookData"
    static let DEVICE_TOKEN = "deviceToken"
    static let ACCESS_TOKEN = "accessToken"
    static let USER_STEP = "userStep"
    static let USER_TYPE = "userType"
    static let USER_DATA = "userData"
    static let USER_NAME = "userName"
    static let USER_FULL_NAME = "userFullName"
    static let USER_ID = "userId"
    static let FIREBASE_TOKEN = "firebaseToken"
    static let USER_LOCATION = "userLocation"
}


//MARK: GLOBAL KEYS
struct GLOBAL_KEY {
    
    static let TARGET_DEVICE_SIZE = CGSize(width: 320,height: 568)
    static let REAL_TARGET_DEVICE_SIZE = CGSize(width: 414,height: 736)
    static let CURRENT_DEVICE_SIZE  = UIScreen.main.bounds
    static let CURRENT_DEVICE_SCALE = UIScreen.main.scale
    static let RANGE_SLIDER_SINGLE_THUMB = 1
    static let RANGE_SLIDER_DOUBLE_THUMB = 2
}


/*//MARK: Resolutions......
struct RESOLUTION {
    
    static let TWO_G_RESOLUTION = CGSize(width: (UIScreen.mainScreen().bounds.width * 0.5), height: (UIScreen.mainScreen().bounds.height * 0.5))
    static let THREE_G_FOUR_G_RESOLUTION = CGSize(width: (UIScreen.mainScreen().bounds.width * 0.75), height: (UIScreen.mainScreen().bounds.height * 0.75))
}*/

//MARK:FONT
struct FONT {
    
    
    
    static let PRO_BOLD = "ProximaNovaA-Bold"
    static let PRO_LIGHT = "ProximaNovaA-Light"
    static let PRO_REGULAR = "ProximaNovaA-Regular"
    static let THIN = "Exo-Thin"
    static let THIN_ITALIC = "Exo-ThinItalic"
    static let BOLD = "Exo-Bold"
    static let BOLD_ITALIC = "Exo-BoldItalic"
    static let EXTRABOLD = "Exo-ExtraBold"
    static let EXTRABOLD_ITALIC = "Exo-ExtraBoldItalic"
    static let EXTRALIGHT = "Exo-ExtraLight"
    static let EXTRALIGHT_ITALIC = "Exo-ExtraLightItalic"
    static let ITALIC = "Exo-Italic"
    static let LIGHT = "Exo-Light"
    static let LIGHT_ITALIC = "Exo-LightItalic"
    static let MEDIUM = "Exo-Medium"
    static let MEDIUM_ITALIC = "Exo-MediumItalic"
    static let REGULAR = "Exo-Regular"
    static let SEMIBOLD = "Exo-SemiBold"
    static let SEMIBOLD_ITALIC = "Exo-SemiBoldItalic"
    static let BLACK = "Exo-Black"
    static let BLACK_ITALIC = "Exo-BlackItalic"
}

//MARK: COLORS
struct COLOR_CODE{

    static let TRANS_COLOR = UIColor(red: 0, green: 0.0, blue: 0.0, alpha: 0.5)
    static let WHITE_BG = UIColor(red: 1, green: 1, blue: 1, alpha: 0.9)
    static let WHITE_COLOR = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    static let GRAY_BG_COLOR = UIColor(red: 151/255.0, green: 151/255, blue: 151/255, alpha: 1)
    static let LIGHT_GRAY_TEXT_COLOR = UIColor(red: 138/255.0, green: 138/255, blue: 138/255, alpha: 1)
    static let LIGHT_GRAY_DIVIDER_COLOR = UIColor(red: 233/255.0, green: 233/255, blue: 233/255, alpha: 1)
    static let LIGHT_GRAY_BOUND_COLOR = UIColor(red: 170/255.0, green: 170/255, blue: 170/255, alpha: 0.2)
    static let MEDIUM_GREY_TEXT_COLOR = UIColor(red: 157/255, green: 157/255, blue: 157/255, alpha: 1)
    static let DARK_GRAY_TEXT_COLOR = UIColor(red: 103/255.0, green: 103/255, blue: 103/255, alpha: 1)
    static let VERY_LIGHT_GREY_TEXT_COLOR = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
    static let NAVIGATIONBAR_COLOR = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    static let YELLOW_COLOR = UIColor(red: 254/255, green: 205/255, blue: 8/255, alpha: 1)
    static let LIGHT_GREY_COLOR = UIColor(red: 36/255, green: 36/255, blue: 36/255, alpha: 1)
    static let DARK_GREY_BG = UIColor(red: 48/255, green: 48/255, blue: 48/255, alpha: 1)
    static let BLACK_COLOR = UIColor(red: 43/255, green: 43/255, blue: 43/255, alpha: 1)
}


//Sign Up User Type....
struct SIGN_UP_IN_USER_TYPE {
    
    static let MODEL = "MO"
    static let ACTOR = "AC"
    static let CASTING_DIRECTOR = "CD"
    static let PRODUCTION_HOUSE = "PH"
    static let AGENCY = "AG"
    static let RELATIONSHIP_MANAGER = "RM"
}

//MARK: OTP Screen Open Ststus...
struct OTP_SCREEN_OPEN_STATUS{
    
    static let FROM_SIGNUP_SCREEN = "SIGN_UP_SCREEN"
    static let FROM_SIGNIN_SCREEN = "SIGN_IN_SCREEN"
    static let FOR_MODEL = "FOR_MODEL"
    static let FOR_ACTOR = "FOR_ACTOR"
    static let FOR_CASTING_DIRECTOR = "FOR_CASTING_DIRECTOR"
    static let FOR_PRODUCTION_HOUSE = "FOR_PRODUCTION_HOUSE"
    static let FOR_AGENCY = "FOR_AGENCY"
}


//MARK: SIGN UP Screen Open Ststus...
struct SIGN_UP_SCREEN_OPEN_STSTUS {
    
    static let FOR_MODEL = "FOR_MODEL"
    static let FOR_ACTOR = "FOR_ACTOR"
    static let FOR_CASTING_DIRECTOR = "FOR_CASTING_DIRECTOR"
    static let FOR_PRODUCTION_HOUSE = "FOR_PRODUCTION_HOUSE"
    static let FOR_AGENCY = "FOR_AGENCY"
}

//MARK: SIGN IN Screen Open Ststus...
struct SIGN_IN_SCREEN_OPEN_STSTUS {
    
    static let FOR_MODEL = "FOR_MODEL"
    static let FOR_ACTOR = "FOR_ACTOR"
    static let FOR_CASTING_DIRECTOR = "FOR_CASTING_DIRECTOR"
    static let FOR_PRODUCTION_HOUSE = "FOR_PRODUCTION_HOUSE"
    static let FOR_AGENCY = "FOR_AGENCY"
    static let FOR_RELATIONSHIP_MANAGER = "FOR_RELATIONSHIP_MANAGER"
}

//MARK: Profile Screen Screen Open Ststus...
struct PROFILE_SCREEN_OPEN_STSTUS {
    
    static let FOR_MODEL = "FOR_MODEL"
    static let FOR_ACTOR = "FOR_ACTOR"
    static let FOR_CASTING_DIRECTOR = "FOR_CASTING_DIRECTOR"
    static let FOR_PRODUCTION_HOUSE = "FOR_PRODUCTION_HOUSE"
    static let FOR_AGENCY = "FOR_AGENCY"
    static let FOR_RELATIONSHIP_MANAGER = "FOR_RELATIONSHIP_MANAGER"
}


//MARK: Table View Information Selection (Stat and Detailed) Screen Open Ststus...
struct DATA_SELECTION_SCREEN_OPEN_STSTUS {
    
    static let SINGLE_SELECTION = "FOR_SINGLE_SELECTION"
    static let MULTIPLE_SELECTION = "FOR_MULTIPLE_SELECTION"
}


//MARK: Table View Information Selection (Stat and Detailed) Screen Open Ststus...
struct Attributes_SCREEN_SCREEN_OPEN_STSTUS {
    
    static let FOR_SEARCHING_TALENT = "FOR_SEARCHING_TALENT"
    static let FOR_NEW_POSTED_JOB = "FOR_NEW_POSTED_JOB"
}


//MARK: TellUs How To Contact You Screen Open Ststus...
struct TELL_US_HOW_TO_CONTECT_YOU_SCREEN_OPEN_STSTUS {
    
    static let FOR_MODEL = "FOR_MODEL"
    static let FOR_ACTOR = "FOR_ACTOR"
    static let FOR_CASTING_DIRECTOR = "FOR_CASTING_DIRECTOR"
    static let FOR_PRODUCTION_HOUSE = "FOR_PRODUCTION_HOUSE"
    static let FOR_AGENCY = "FOR_AGENCY"
}


//MARK: TellUs How To Contact You Screen Open Ststus...
struct EDIT_PROFILE_STSTUS {
    
    static let GENDER_SELECTION_SCREEN = "EDIT_PROFILE_STSTUS_GENDER_SELECTION_SCREEN"
    static let STAT_INFORMATION_SCREEN = "EDIT_PROFILE_STSTUS_STAT_INFORMATION_SCREEN"
    static let SKILLS_SCREEN = "EDIT_PROFILE_STSTUS_SKILLS_SCREEN"
    static let UPLOAD_PICTURES_SCREEN = "EDIT_PROFILE_STSTUS_UPLOAD_PICTURES_SCREEN"
    static let TELL_US_ABOUT_YOU_SCREEN = "EDIT_PROFILE_STSTUS_TELL_US_ABOUT_YOU_SCREEN"
    static let EDUCATION_DETAILS_SCREEN = "EDIT_PROFILE_STSTUS_EDUCATION_DETAILS_SCREEN"
    static let EXPERIENCE_SCREEN = "EDIT_PROFILE_STSTUS_EXPERIENCE_SCREEN"
    static let YOUR_WORK_SCREEN = "EDIT_PROFILE_STSTUS_YOUR_WORK_SCREEN"
    static let TELL_US_HOW_TO_CONTACT_YOU_SCREEN = "EDIT_PROFILE_STSTUS_TELL_US_HOW_TO_CONTACT_YOU_SCREEN"
    static let EDIT_CASTING_DIRECTOR_PROFILE_SCREEN = "EDIT_PROFILE_STSTUS_EDIT_CASTING_DIRECTOR_PROFILE_SCREEN"
}

struct GESTURE_SKILLS_SCREEN_STATUS  {
    static let STAT_INFORMATION_SCREEN = "STAT_INFORMATION_SCREEN"
    static let SKILLS_SCREEN = "SKILLS_SCREEN"
}


//MARK: Notification ststuses...
struct IMAGE_PICKED_FROM_CUSTOM_GALLARY_NOTIFICATION_NAME {
    
    static let FOR_UPLOADING_IMAGE_CONTROLLER = "ImagePickedFromCustomGallaryForUploadingImageController"
    static let FOR_YOUR_WORK_VIEW_CONTROLLER = "ImagePickedFromCustomGallaryForYourWorkController"
}

//MARK: Notification ststuses...
struct FACEBOOK_NOTIFICATION_NAME {
    
    static let REQUEST_TIMEOUT = "FacebookTimeOut"
    static let REQUEST_CANCEL = "FacebookCancel"
}

//MARK: App Update Notifications....
struct APP_UPDATE_NOTIFICATION {
    
    static let APP_VERSION_NOTIFICATION = "AppVersionNotification"
}


//MARK: Notification ststuses...For Search Controller....for selecting states and cities...
struct EDUCATION_DETAILS_SEARCH_NOTIFICATION_NAME {
    
    static let FOR_SELECTING_EDUCATION_DETAILS = "EducationDetailsSearchNotificationName"
}


//MARK: Notification ststuses...For Search Controller....for selecting states and cities...
struct COLLECTING_ADDED_URLS_NOTIFICATION {
    
    static let FOR_YOUR_WORK_CONTROLLER = "COLLECTINT_ADDED_URLS_NOTIFICATION_FOR_YOUR_WORK_CONTROLLER"
}

//MARK: Notification STEPS_COMPLETED_BY_USER_NOTIFICATION
struct STEPS_COMPLETED_BY_USER_NOTIFICATION {
    
    static let FOR_GENDER_SELECTION_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_GENDER_SELECTION_CONTROLLER"
    static let FOR_GESTURE_SELECTION_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_GESTURE_SELECTION_CONTROLLER"
    static let FOR_SKILLS_SELECTION_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_SKILLS_SELECTION_CONTROLLER"
    static let FOR_UPLOAD_IMAGE_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_UPLOAD_IMAGE_CONTROLLER"
    static let FOR_TELL_US_ABOUT_YOU_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_TELL_US_ABOUT_YOU_CONTROLLER"
    static let FOR_EDUCATION_DETAILS_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_EDUCATION_DETAILS_CONTROLLER"
    static let FOR_EXPERIANCE_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_EXPERIANCE_CONTROLLER"
    static let FOR_YOUR_WORK_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_YOUR_WORK_CONTROLLER"
    static let FOR_TELL_US_HOW_TO_CONTECT_YOU_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_TELL_US_HOW_TO_CONTECT_YOU_CONTROLLER"
    static let FOR_CREATE_PROFILE_CONTROLLER = "STEPS_COMPLETED_BY_USER_NOTIFICATION_FOR_CREATE_PROFILE_CONTROLLER"
}

//MARK: Stat Infromation And Sklls UI TYPE String Constants...
struct SKILLS_AND_STAT_INFO_DETAILS_UI_FLAGS {
    
    static let SLIDER = "slider"
    static let RADIO_BUTTON = "radiobutton"
}


////Disabling printf statement......
//func print(items: Any..., separator: String = "", terminator: String = "") {             //For Swift 1.2
//    
//    Swift.print("", separator:separator, terminator: terminator)
//}


//MARK:- Saving user data for user engagement

//func saveUserDetailsForMOEngage(userID:Int,mailID:String?,phoneNumber:String?){
//    
//    MoEngage.sharedInstance().setUserAttribute(userID, forKey: USER_ATTRIBUTE_UNIQUE_ID)
//    
//    if let mailID = mailID {
//         MoEngage.sharedInstance().setUserAttribute(mailID, forKey: USER_ATTRIBUTE_USER_EMAIL)
//    }
//    
//    if let phoneNumber = phoneNumber{
//        MoEngage.sharedInstance().setUserAttribute(phoneNumber, forKey: USER_ATTRIBUTE_USER_MOBILE)
//    }
//    
//}


/*//MARK: Device Resolution URL....
func preparingDeviceResolutionURL(actualURL: String) -> String {
    
//Getting URL of Thumbnail Image....
    
    if actualURL.containsString(SERVER_ADDRESS.BASE_URL) {
    
        let components = actualURL.componentsSeparatedByString(SERVER_ADDRESS.BASE_URL)
        
        var quality = Int()
        var reslutionWidth = Int()
        var resolutionHeight = Int()

        if components.count > 0 {
            
        //Internet check to download image....
            if (UIApplication.sharedApplication().delegate as! AppDelegate).internetType == "WIFI" {
                
                quality = 80
                reslutionWidth = Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * GLOBAL_KEY.CURRENT_DEVICE_SCALE)
                resolutionHeight = Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height * GLOBAL_KEY.CURRENT_DEVICE_SCALE)
                
            } else {    //2G, 3G, 4G...
                
                quality = 80
                reslutionWidth = Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * GLOBAL_KEY.CURRENT_DEVICE_SCALE) / 2
                resolutionHeight = Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height * GLOBAL_KEY.CURRENT_DEVICE_SCALE) / 2
            }
            
            let resolutionURl = "\(SERVER_ADDRESS.BASE_URL)/" + "image-v1/render/\(reslutionWidth)x\(resolutionHeight)x1x\(quality)" + components[1]
//            print("Actual URL = ", resolutionURl)
            return resolutionURl
        }
    }
    
    return actualURL
}*/


/*//MARK: Dynamic Size Thumbnail URL....
func preparingDynamicSizeThumbnailURL(actualURL: String, resolutionWidth: Int, resolutionHeight: Int) -> String {
    
//Getting URL of Thumbnail Image....
    if actualURL.containsString(SERVER_ADDRESS.BASE_URL) {
    
        let components = actualURL.componentsSeparatedByString(SERVER_ADDRESS.BASE_URL)
        
        var quality = Int()
        var reslutionWidth = Int()
        var reslutionHeight = Int()
        
        if components.count > 0 {
            
        //Internet check to download image....
            if (UIApplication.sharedApplication().delegate as! AppDelegate).internetType == "WIFI" {
                
                quality = 50
                reslutionWidth = resolutionWidth
                reslutionHeight = resolutionHeight
                
            } else {    //2G, 3G, 4G...
                
                quality = 80
                reslutionWidth = resolutionWidth / 2
                reslutionHeight = resolutionHeight / 2
            }
            
            let thumbnailURl = "\(SERVER_ADDRESS.BASE_URL)/" + "image-v1/render/\(reslutionWidth)x\(reslutionHeight)x1x\(quality)" + components[1]
//            print("Thumb URL = ", thumbnailURl)
            return thumbnailURl
        }
    }
    
    return actualURL
}*/

////MARK: FACE DETECTION.....
//func faceDetectedImage(imageURL: String, image: UIImage, recievedResponse:(image: UIImage)->()) -> UIImage {
//    
////image caching if existing then don't download else does.
//    if (Singleton.sharedInstance.faceDetectionImages.objectForKey("\(imageURL)") as? UIImage) != nil {
//        
//        return (Singleton.sharedInstance.faceDetectionImages.objectForKey("\(imageURL)") as? UIImage)!
//        
//    } else if (Singleton.sharedInstance.faceDetectionImages.objectForKey("\(imageURL)") as? UIImage) == nil {
//    
//        dispatch_async(dispatch_get_global_queue(0, 0)) {
//            
//        //DetectingFaces.....
//            let context = CIContext(options: nil)
//            let detector = CIDetector(ofType: CIDetectorTypeFace, context: context, options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
//            
//            let cgImage = CIImage(CGImage: image.CGImage!)
//            let festures = detector.featuresInImage(cgImage)
//            var rectOfFaces = CGRect()
//            
//        //Creating combied rect of faces....
//            for count in 0 ..< festures.count {
//                
//                if count == 0 {
//                    
//                    rectOfFaces = (festures[0] as! CIFaceFeature).bounds
//                    
//                } else {
//                    
//                    rectOfFaces = CGRectUnion(rectOfFaces, festures[count].bounds)
//                }
//            }
//            
//        //If face detected than sending....
//            if festures.count > 0 {
//                
//            //Making perfect square...
//                rectOfFaces.size.height = max(rectOfFaces.size.width, rectOfFaces.size.height)
//                rectOfFaces.size.width = rectOfFaces.size.height
//                
//            //Getting Image from Context....
//                let faceDetectedImage = UIImage(CGImage: context.createCGImage(cgImage, fromRect: CGRectInset(rectOfFaces, -rectOfFaces.width * 0.2, -rectOfFaces.width * 0.2))!)
//                
//            //Setting faceDetected Images to cache.....
//                Singleton.sharedInstance.faceDetectionImages.setObject(faceDetectedImage, forKey: imageURL)
//                
//            //Callback....
//                recievedResponse(image: faceDetectedImage)
//                
//            } else {
//                
//                recievedResponse(image: image)
//            }
//        }
//    }
//    
//    return image
//}


//MARK: Aspect Ratio function
func getValueFromTargetFunction(targetSuperViewSize: CGFloat, targetSubviewSize: CGFloat, currentSuperViewDeviceSize: CGFloat) -> CGFloat {
    
    var superVIewDeviceSize: CGFloat!
    
    if currentSuperViewDeviceSize == 480 {
        
        superVIewDeviceSize = 568
        
    } else {
        
        superVIewDeviceSize = currentSuperViewDeviceSize
    }
    
    return  superVIewDeviceSize/(targetSuperViewSize/targetSubviewSize)
}

func aspectHeight(height:CGFloat) -> CGFloat {
    
    
    return getValueFromTargetFunction(targetSuperViewSize: GLOBAL_KEY.TARGET_DEVICE_SIZE.height, targetSubviewSize: height, currentSuperViewDeviceSize:  GLOBAL_KEY.TARGET_DEVICE_SIZE.height)
    
}

func aspectWidth(width:CGFloat) -> CGFloat {
    
    return getValueFromTargetFunction(targetSuperViewSize: GLOBAL_KEY.TARGET_DEVICE_SIZE.width, targetSubviewSize: width, currentSuperViewDeviceSize: GLOBAL_KEY.TARGET_DEVICE_SIZE.width)
}
/*
//MARK: Setting the text of text field of actual length...
func textInTextFields(textField: UITextField, string: String) -> String {
    
    var searchText = String()
    
    if textField.text == "" {
        
        searchText = string
        
    } else {
        
        if string == "" {
            
            searchText = NSString(string: textField.text!).substring(to: NSString(string: textField.text!).length - 1)
            
        } else {
            
            searchText = textField.text! + string
        }
    }
    
    return searchText
}*/

/*
//MARK: Getting Thumbnail images....
func gettingThumbNailImage(image: UIImage, rect: CGRect) -> UIImage {
  
    UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
    image.drawInRect(rect)
    let thumbImage = UIGraphicsGetImageFromCurrentImageContext()
    let thumbOrientationImage = UIImage(CGImage: thumbImage!.CGImage!, scale: thumbImage!.scale, orientation: UIImageOrientation.Down)
    UIGraphicsEndImageContext()
    
    return thumbOrientationImage
}*/

/*
//MARK: Photo Image Caching
func downloadImageAsynchronously(imageURL:NSString, imageView:UIImageView, placeHolderImage:UIImage?, contentMode:UIViewContentMode) -> UIImage? {
    
    imageView.contentMode = contentMode
    imageView.clipsToBounds = true

//image caching if existing then don't download else does.
    if (Singleton.sharedInstance.completeAppImageCache.objectForKey("\(imageURL)") as? UIImage) != nil {

        return (Singleton.sharedInstance.completeAppImageCache.objectForKey("\(imageURL)") as? UIImage)!
    
    } else if (Singleton.sharedInstance.completeAppImageCache.objectForKey("\(imageURL)") as? UIImage) == nil {
        
    // The image isn't cached, download the img data
    // We should perform this in a background thread
        let url = NSURL(string: imageURL as String)
        let request: NSURLRequest = NSURLRequest(url: url! as URL)
        let mainQueue = OperationQueue.mainQueue

        NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            
            if error == nil {
                
            //Convert the downloaded data in to a UIImage object
                let image = UIImage(data: data!)
                
            //Store the image in to our cache
                if image != nil {
                    
                //Saving Thumbnail as well as Actual Image.......as Same Image so that it will be overridden by original image.......
                    Singleton.sharedInstance.completeAppImageCache.setObject(image!, forKey: "\(request.URL!.absoluteString!)")
                    
                    if "\(request.URL!.absoluteString!)".containsString("image-v1/render/50x50x1/") {
                    
                        Singleton.sharedInstance.completeAppImageCache.setObject(image!, forKey: ("\(request.URL!.absoluteString!)".componentsSeparatedByString("image-v1/render/50x50x1/") as NSArray).componentsJoinedByString(""))
                    }
                    
                    if "\(request.URL!.absoluteString!)".containsString("image-v1/render/\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x1/") {
            
                        Singleton.sharedInstance.completeAppImageCache.setObject(image!, forKey: ("\(request.URL!.absoluteString!)".componentsSeparatedByString("image-v1/render/\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x1/") as NSArray).componentsJoinedByString(""))
                    }
                    
//                    if "\(request.URL!.absoluteString)".containsString("image-v1/render/\(Int(RESOLUTION.TWO_G_RESOLUTION.width))x\(Int(RESOLUTION.TWO_G_RESOLUTION.height))x1/") {    //2G Internet Check...
//                        
//                        Singleton.sharedInstance.completeAppImageCache.setObject(image!, forKey: ("\(request.URL!.absoluteString)".componentsSeparatedByString("image-v1/render/\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x1/") as NSArray).componentsJoinedByString(""))
//                    }
//                    
//                    if "\(request.URL!.absoluteString)".containsString("image-v1/render/\(Int(RESOLUTION.THREE_G_FOUR_G_RESOLUTION.width))x\(Int(RESOLUTION.THREE_G_FOUR_G_RESOLUTION.height))x1/") {    //3G Internet check.....
//                        
//                        Singleton.sharedInstance.completeAppImageCache.setObject(image!, forKey: ("\(request.URL!.absoluteString)".componentsSeparatedByString("image-v1/render/\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x1/") as NSArray).componentsJoinedByString(""))
//                    }
                   
                //Update the Image View
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        imageView.image = image
                    })
                }
                
            } else {
                
                print("Error: ")
            }
        })
    }
    
    return placeHolderImage
}*/

/*
//MARK: Photo Image Caching
func downloadImageAsynchronously(imageURL:NSString, imageView:UIImageView?, placeHolderImage:UIImage?, contentMode:UIViewContentMode, recievedResponse:@escaping (URL: String, image: UIImage?)->()) -> UIImage? {
    
    if imageView != nil {

        imageView!.contentMode = contentMode
        imageView!.clipsToBounds = true
    }
    
//image caching if existing then don't download else does.
    if (Singleton.sharedInstance.completeAppImageCache.objectForKey("\(imageURL)") as? UIImage) != nil {
        
        return (Singleton.sharedInstance.completeAppImageCache.objectForKey("\(imageURL)") as? UIImage)!
        
    } else if (Singleton.sharedInstance.completeAppImageCache.objectForKey("\(imageURL)") as? UIImage) == nil {
        
    //The image isn't cached, download the img data
    //We should perform this in a background thread
        let url = NSURL(string: imageURL as String)
        let request: NSURLRequest = NSURLRequest(URL: url!)
        let mainQueue = NSOperationQueue.mainQueue()
        
        NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            
            if error == nil {
                
            //Convert the downloaded data in to a UIImage object
                let image = UIImage(data: data!)
                
            //Store the image in to our cache
                if image != nil {
                    
                //Update the Image View
                    dispatch_async(dispatch_get_main_queue(), {
                        
                    //Settimg image on image view.....
//                        imageView.image = image
                        
                    //Saving Thumbnail as well as Actual Image.......as Same Image so that it will be overridden by original image.......
                        Singleton.sharedInstance.completeAppImageCache.setObject(image!, forKey: "\(request.URL!.absoluteString!)")
                        
                    //Callback......
                        recievedResponse(URL: request.URL!.absoluteString!, image: image)
                        
                    //Saving Thumb URL....
//                        if "\(request.URL!.absoluteString)".containsString("image-v1/render/50x50x1/") {
//                            
//                            Singleton.sharedInstance.completeAppImageCache.setObject(image!, forKey: ("\(request.URL!.absoluteString)".componentsSeparatedByString("image-v1/render/50x50x1/") as NSArray).componentsJoinedByString(""))
//                            
//                        //Callback......
//                            recievedResponse(URL: ("\(request.URL!.absoluteString)".componentsSeparatedByString("image-v1/render/50x50x1/") as NSArray).componentsJoinedByString(""), image: image)
//                        }
//                        
//                    //Saving Resolution URL Image.......
//                        if "\(request.URL!.absoluteString)".containsString("image-v1/render/\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x1/") {
//                       
//                            Singleton.sharedInstance.completeAppImageCache.setObject(image!, forKey: ("\(request.URL!.absoluteString)".componentsSeparatedByString("image-v1/render/\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x1/") as NSArray).componentsJoinedByString(""))
//                            
//                        //Callback......
//                            recievedResponse(URL: ("\(request.URL!.absoluteString)".componentsSeparatedByString("image-v1/render/\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x\(Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height * GLOBAL_KEY.CURRENT_DEVICE_SCALE))x1/") as NSArray).componentsJoinedByString(""), image: image)
//                        }
                    })
                }
                
            } else {
                
                print("Error: ")
                recievedResponse(URL: "\(request.URL!.absoluteString!)", image: nil)
            }
        })
    }
    
    return placeHolderImage
}*/


/*extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.nextResponder()
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
*/

/*func +(lhs: NSAttributedString, rhs: NSAttributedString) -> NSAttributedString {
    
    let a = lhs.mutableCopy() as! NSMutableAttributedString
    let b = rhs.mutableCopy() as! NSMutableAttributedString
    
    a.append(b)
    
    return a.copy() as! NSAttributedString
}*/

/*func +(lhs: NSAttributedString, rhs: String) -> NSAttributedString {
    
    let a = lhs.mutableCopy() as! NSMutableAttributedString
    let b = NSMutableAttributedString(string: rhs)
    
    return a + b
}*/

/*func +(lhs: String, rhs: NSAttributedString) -> NSAttributedString {
    
    let a = NSMutableAttributedString(string: lhs)
    let b = lhs.mutableCopy() as! NSMutableAttributedString
    
    return a + b
}*/



/*func convertCentimetersToFeetInches(centimeters:Double) -> (Feet:Int,Inches:Int){
    
    
    let inches  = centimeters * 0.3937008
    
    let feet_decimal = inches/12
    
    //        print(inches)
    //
    //        print(feet_decimal)
    
    let ft_Parts = NSString(format: "%.2f", feet_decimal).componentsSeparatedByString(".")
    
    //        print(ft_Parts)
    
    let feet_Int = Int(ft_Parts[0])
    
    let inch_decimal = Double(ft_Parts[1])! * 0.12
    
    //        print(inch_decimal)
    
    let inch_Int = Int(round(inch_decimal))
    
    //        print(inch_Int)
    
    return ( feet_Int! , inch_Int )
    
}*/

/*func convertInchFeetToCentimeters(Feet:Double,Inches:Double) -> Int{
    
    let inches = (Feet * 12) +  Inches
    
    let centimeters = inches * 2.54
    
    return Int(centimeters)
    
}*/
/*
extension UILabel
{
    func addImage(imageName: String, afterLabel bolAfterLabel: Bool = false)
    {
        let attachment: NSTextAttachment = NSTextAttachment()
        attachment.image = UIImage(named: imageName)
        
        attachment.bounds = CGRect(x: 0, y: -5, width: attachment.image!.size.width, height: attachment.image!.size.height )
        let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)
        
        if (bolAfterLabel)
        {
            let strLabelText: NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
            strLabelText.appendAttributedString(attachmentString)
            
            self.attributedText = strLabelText
        }
        else
        {
            let strLabelText: NSAttributedString = NSAttributedString(string: self.text!)
            let mutableAttachmentString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
            
            let space = NSAttributedString(string: " ")
            
            mutableAttachmentString.appendAttributedString(space)
            
            mutableAttachmentString.appendAttributedString(strLabelText)
            
            self.attributedText = mutableAttachmentString
        }
    }
    
    func removeImage()
    {
        let text = self.text
        self.attributedText = nil
        self.text = text
    }
}*/
