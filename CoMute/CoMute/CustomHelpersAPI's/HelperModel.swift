//
//  HelperModel.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 10/5/17.
//  Copyright (c) 2015 Tipstat. All rights reserved.
//

import UIKit

class HelperModel: NSObject {
    
//Alert Controller Object...
    var controller : UIAlertController!
    
//MARK: Showing Alert Controller with Call Back...
    func showingAlertcontroller(title: String, message: String, cancelButton: String, receivedResponse:@escaping () -> ()) {
        
        if controller != nil {
            
            controller.dismiss(animated: true, completion: nil)
            controller = nil
        }
        
        controller = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        controller.addAction(UIAlertAction(title: cancelButton, style: UIAlertActionStyle.cancel, handler: { finished in
            
            receivedResponse()
        }))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true, completion: nil)
    }
    
    func showingOkCancelAlertcontroller(title: String, message: String, okButton: String, cancelButton: String, receivedResponse:@escaping (_ succeeded:Bool) -> ()) {
        
        if controller != nil {
            
            controller.dismiss(animated: true, completion: nil)
            controller = nil
        }
        
        controller = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        controller.addAction(UIAlertAction(title: cancelButton, style: UIAlertActionStyle.cancel, handler: { finished in
            
            receivedResponse(false)
        }))
        
        controller.addAction(UIAlertAction(title: okButton, style: UIAlertActionStyle.default, handler: { finished in
            
            receivedResponse(true)
        }))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true, completion: nil)
    }
}
