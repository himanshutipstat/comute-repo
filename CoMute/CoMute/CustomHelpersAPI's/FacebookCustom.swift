//
//  FacebookCustom.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 10/5/17.
//  Copyright (c) 2015 Tipstat. All rights reserved.
//


import Foundation
import FBSDKLoginKit
import NVActivityIndicatorView

class FacebookCustom {      //Creating Singleton.......
    
   
        
       
            
            static var sharedInstance = FacebookCustom()
            static var token  = {0}()
        
        
    
    
//Model Objects...
    var internetConnectionModel = InternetConnectionModel()
    var helperModel = HelperModel()
    var activityData : ActivityData!
    
    
//User Profile Data fetching......
    let facebookReadPermissions = ["public_profile", "email", "user_photos"]
//Some other options: "user_about_me", "user_birthday", "user_hometown", "user_likes", "user_interests", "user_photos", "friends_photos", "friends_hometown", "friends_location", "friends_education_history"
    
    
    func loginToFacebookWithSuccess(sender: AnyObject, successBlock:@escaping (_ response: AnyObject?) -> Void, failure:@escaping (_ error: NSError?) -> Void) {
        
        if FBSDKAccessToken.current() != nil {
           
        //For debugging, when we want to ensure that facebook login always happens
            FBSDKLoginManager().logOut()
            //Otherwise do:
            //return
        }
        
        
        FBSDKLoginManager().logIn(withReadPermissions: self.facebookReadPermissions, from: sender as! UINavigationController) { (result, error) in
            
            
    
            
            if error != nil {
                
                //According to Facebook:
                //Errors will rarely occur in the typical login flow because the login dialog
                //presented by Facebook via single sign on will guide the users to resolve any errors.
                // Process error
                FBSDKLoginManager().logOut()
                failure(error as NSError?)
                
            } else if (result?.isCancelled)! {
                
                // Handle cancellations
                FBSDKLoginManager().logOut()
                //                let userInfo = ["POP" : "TRUE"]
                //                let notification = NSNotification(name: FACEBOOK_NOTIFICATION_NAME.REQUEST_CANCEL, object: nil, userInfo: userInfo)
                //                NSNotificationCenter.defaultCenter().postNotification(notification)
                failure(error as NSError?)
                
            } else {
                
                if (result?.grantedPermissions.contains("email"))! && (result?.grantedPermissions.contains("public_profile"))! {
                    
                    print((result?.token.tokenString)!)
                    
                    // Do work
                    Singleton.sharedInstance.facebookAccessToken = (result?.token.tokenString)!
                    Singleton.sharedInstance.facebookId = (result?.token.userID)!
                    //                    Singleton.sharedInstance.userImageURL = "https://graph.facebook.com/\(result.token.userID)/picture?type=large"
                    
                    //new SDK graph API request...
                    let fbRequest = FBSDKGraphRequest(graphPath: "/me", parameters: NSDictionary(object: "picture.type(large),id,name,email,first_name,last_name,birthday, gender,photos.fields(name,picture,source)", forKey: "fields" as NSCopying) as [NSObject : AnyObject], httpMethod: "GET")
                    
                    
                    
                    fbRequest?.start(completionHandler: { (connection, userData, error) in
                        print("User Info : \(String(describing: userData))")
                        
                        if error == nil {
                            
                            //Saving Business token.....
                            //                            if (userData.allKeys as NSArray).containsObject("token_for_business") {
                            //
                            //                                Singleton.sharedInstance.facebookBusinessToken = (userData as! NSDictionary).valueForKey("token_for_business") as! String
                            //                            }
                            
                            UserDefaults.standard.setValue(userData, forKey: USER_DEFAULT.FACEBOOK_DATA)
                            print("User Info : \(String(describing: userData))")
                            successBlock("success" as AnyObject?)
                            
                        } else {
                            
                            print("Error Getting Info i \(String(describing: error))");
                            
                            //if request time out notification get fired..
                            //                            let userInfo = ["POP" : "TRUE"]
                            //                            let notification = NSNotification(name: FACEBOOK_NOTIFICATION_NAME.REQUEST_TIMEOUT, object: nil, userInfo: userInfo)
                            //                            NSNotificationCenter.defaultCenter().postNotification(notification)
                            failure(error as NSError?)
                        }
                    })
                    
                    
                } else {
                    
                    //The user did not grant all permissions requested
                    //Discover which permissions are granted
                    //and if you can live without the declined ones
                    
                    print("Error Getting Info 2 \(String(describing: error))");
                    
                    //cancel button notification get fied..
                    //                    let userInfo = ["POP" : "TRUE"]
                    //                    let notification = NSNotification(name: FACEBOOK_NOTIFICATION_NAME.REQUEST_TIMEOUT, object: nil, userInfo: userInfo)
                    //                    NSNotificationCenter.defaultCenter().postNotification(notification)
                    failure(error as NSError?)
                }
            }
        }
      
    }
    
//MARK: User Album Images Fetching.....
    func fetchingFacebookImagesAlbums(facebookID: String, facebookAccessToken: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: AnyObject)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
           // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
           // ActivityIndicator.sharedInstance.showActivityIndicator()
             NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
 
        //Facebook Graph Request.....
            let albumFetchingRequest = FBSDKGraphRequest(graphPath: "/\(facebookID)", parameters: NSDictionary(object: "albums", forKey: "fields" as NSCopying) as [NSObject : AnyObject], httpMethod: "GET")
            
            albumFetchingRequest?.start(completionHandler: { (connection, userData, error) in
                
                let data = userData as? NSDictionary ?? [:]
                DispatchQueue.global(qos: .userInitiated).async{
                    
                    //Hiding Actvity Indicator....
                   // ActivityIndicator.sharedInstance.hideActivityIndicator()
                     NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    //Parsing Data recieved....
                    if error == nil {
                        
                        print("User Info : \(String(describing: userData))")
                        
                        if let albums = data["albums"] as? NSDictionary  {
                            
                            if let albumsArray = albums["data"] as? NSArray {
                                
                                recievedResponse(true, albumsArray)
                            }
                            
                            recievedResponse(false, [:] as NSDictionary)
                        }
                        
                    } else {
                        
                        print("Error Getting Info i \(String(describing: error))")
                        recievedResponse(false, [:] as NSDictionary)
                    }
                }
            })
            
        }
    }
    
    
    
//MARK: User Album Images Fetching.....
    func fetchingFacebookAlbumsImages(facebookID: String, albumID: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: AnyObject)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
        //    UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
           // ActivityIndicator.sharedInstance.showActivityIndicator()
            
             NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
        //Facebook Graph Request.....
            let albumFetchingRequest = FBSDKGraphRequest(graphPath: "\(albumID)/photos", parameters: NSDictionary(object: "name,id,images,icon,picture", forKey: "fields" as NSCopying) as [NSObject : AnyObject], httpMethod: "GET")
            
            albumFetchingRequest?.start(completionHandler: { (connection, userData, error) in
                
                let data = userData as? NSDictionary ?? [:]
               DispatchQueue.global(qos: .userInitiated).async {
                    
                    //Hiding Actvity Indicator....
                   // ActivityIndicator.sharedInstance.hideActivityIndicator()
                
                 NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                
                    //Parsing Data recieved....
                    if error == nil {
                        
                        print("User Info : \(userData, connection)")
                        
                        if let albumsImagesArray = data["data"] as? NSArray {
                            
                            recievedResponse(true, albumsImagesArray)
                        }
                        
                        recievedResponse(false, [] as NSArray)
                        
                    } else {
                        
                        print("Error Getting Info i \(String(describing: error))")
                        recievedResponse(false, [] as NSArray)
                    }
                }
            })
            
          
        
        } else {
            
            self.helperModel.showingAlertcontroller(title: "", message: "Please check your Internet connection", cancelButton: "OK", receivedResponse: {})
        }
    }
}
