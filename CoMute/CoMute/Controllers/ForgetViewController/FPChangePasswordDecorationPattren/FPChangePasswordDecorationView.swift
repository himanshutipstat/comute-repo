//
//  FPChangePasswordDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 03/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension  FPChangePasswordViewController : CustomizedDefaultNavigationBarDelegate,UIGestureRecognizerDelegate{
    
    //MARK: Gesture recognizer delegates...
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        //Finding Which view is tapped....
        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
        
        //Recognizing gesture if Tapped on Image view......
        if newView!.isKind(of: UIImageView.self) {
            
            
            return false
        }
        
        return true
    }

    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
}
