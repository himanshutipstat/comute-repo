//
//  FPChangePasswordViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 02/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class FPChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var temPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    
    
    //MARK: Custom Views...
    var tapGesture : UITapGestureRecognizer!
     var navigation: CustomizedDefaultNavigationBar!
    
    var resetToken : String!
    //Helper Model...
    var helperModel = HelperModel()
    var reserPassword = PasswordModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setUPUIConstrain()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "Forget Password", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        //Setting Tap Gesture...
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(FPChangePasswordViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)

    }
    

    //MARK: Removing keyboard...And Setting zesture fopr zooming...h1#C1@f1#
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        //self.emailTextField.resignFirstResponder()b4(K3%E1&
    }
    
    
    //MARK: StoryBoard Action...
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        if self.temPasswordField.text! != "" && self.newPasswordField.text! != "" {
            
            reserPassword.resetPasswordAPI(resetpasswordtoken: resetToken, newpassword: self.newPasswordField.text!, resetpassword: self.temPasswordField.text!, recievedResponse: { (succeeded, response) in
                
                
                if succeeded {
                    
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                    self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
//                    let DashBoardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
//                    self.navigationController?.pushViewController(DashBoardVC, animated: true)
                    
                }
            })
            
        }
    }
    

}
