//
//  FPSendPasswordViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 02/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class FPSendPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    //MARK: Custom Views...
    var tapGesture : UITapGestureRecognizer!
     var navigation: CustomizedDefaultNavigationBar!
    
    //Helper Model...
    var helperModel = HelperModel()
    var resetPasswordRequest = PasswordModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        setUPUIConstrain()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "Forget Password", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        //Setting Tap Gesture...
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(FPSendPasswordViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    
    @IBAction func sendButtonAction(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if self.emailTextField.text != "" {
            
            var message:NSString = ""
            
            if(!Singleton.sharedInstance.isEmailValid(email: self.emailTextField.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines))) {
                
                message = "Please enter valid Email Address"
                
            } //else if Singleton.sharedInstance.facebookId.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) == "" && !Singleton.sharedInstance.isValidPassword(self.userDetailsEntryView[2].userDetailTextField.text!) {
            
            // message = "Password must be atleast 6 characters long!"
            // }
            
            //Validation Check....
            if(message.length > 0) {
                
                self.helperModel.showingAlertcontroller(title: "", message: message as String, cancelButton: "OK", receivedResponse: {})
                
            } else {
                
                resetPasswordRequest.resetPasswordRequestAPI(email: self.emailTextField.text!, recievedResponse: { (succeeded, response) in
                    if succeeded {
                        
                        if  let userSuccessDataDictionary = response["data"] as? NSDictionary {
                            
                            //let userId = userSignUpSuccessDataDictionary["user_id"] as! NSDictionary
                            if let resetToken = userSuccessDataDictionary["resetpasswordtoken"] as? String {
                                
                                let changePasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "FPChangePasswordViewController") as! FPChangePasswordViewController
                                changePasswordVC.resetToken = resetToken
                                self.navigationController?.pushViewController(changePasswordVC, animated: true)
                                
                                
                            }
                            
                        }
                        
                       

                        
                    }else {
                    
                    }
                })
                
                
                
            }
            
        }else {
            self.helperModel.showingAlertcontroller(title: "", message: "Please make sure you fill the field." as String, cancelButton: "OK", receivedResponse: {})
        }
        
       
    }
    

    //MARK: Removing keyboard...And Setting zesture fopr zooming...
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        self.emailTextField.resignFirstResponder()
    }
}
