//
//  DashBoardViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 15/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreLocation
import SocketIO



class DashBoardViewController: UITabBarController {
    
    let locationManager = CLLocationManager()
    var latitude  = String()
    var longitude = String()
    var isAuthorized = true
    var chatAlertBanner : ChatNotificationView!
    var userinfo =  NSDictionary()
    
    //Socket Variables & Constants
    

    override func viewDidLoad() {
        super.viewDidLoad()

        settingExtraFontsAndUI()
       // self.tabBar.items
        //self.tabBar = ui
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Pushreceived"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DashBoardViewController.methodOfReceivedNotification(notification:)), name: NSNotification.Name(rawValue: "Pushreceived"), object: nil)
        
//        self.isAuthorizedtoGetUserLocation()
//        
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        }
//        locationManager.requestAlwaysAuthorization()
//        locationManager.startUpdatingLocation()
        
        
        
        socket.connect()
        
        let lac = ["lat": latitude,
                   "lon": longitude ]
        //MARK: Socket to Make Connection and Emit Registration
        let par = ["authorization": Singleton.sharedInstance.userAccessToken,
                   "location": lac] as [String : Any]
        print(par)
        
        //self.socket.emit("add user", with: [par])
        //   self.socket.emitWithAck("add user", with: [par])
        socket.on("connect") {[weak self] data, ack in
            
            print("ACK.......\(data), \(ack)")
            
            print("socket connected")
            
            self?.isAuthorized = true
            
            socket.emit("add user", with: [par])
            
            return
        }
        
        
        socket.on("reconnect") {[weak self] data, ack in
            
            print("ACK.......\(data), \(ack)")
            
            print("socket reconnected")
            
            socket.connect()
            
            // self?.socket.emit("add user", with: [par])
            
            return
        }
        
        // Check if User is Unauthorized
        socket.on("disconnect") { (data, ack) -> Void  in
            print("unauthorized")
            self.isAuthorized = false
            
            // print("data u\(data)")
            
            //  println("un :\(ack)")
            return
        }
        
        //MARK: Socket To Recieve Send Message ACK....
        socket.on("newmessage"){ (data, ack) -> Void in
            
            
            print("ACK.......\(data), \(ack)")
            print(data)
            
            /*  Username = panditji;
             firstname = sandy;
             lastname = chodh;
             message = Heyaa;
             time = "2017-11-09T13:19:22.301Z";
             username = 5a01a337410856034263d003*/
            
            let newdic = data.first as! NSDictionary
            
            self.userinfo = newdic
            
            
            
            //            if self.chatDatasource.count > 0 {
            //
            //                self.chatMessagesTableView.scrollToRow(at: IndexPath(row: self.chatDatasource.count - 1, section: 0), at: UITableViewScrollPosition.top, animated: true)
            //            }
            
            if self.chatAlertBanner == nil {
                
                self.chatAlertBanner =  UINib(nibName: "ChatNotificationView", bundle: nil).instantiate(withOwner: ChatNotificationView.self, options: nil)[0] as! ChatNotificationView
                
                self.view!.addSubview(self.chatAlertBanner)
            }
            
            self.chatAlertBanner.userMessageLabel.text = newdic["message"] as? String
            
            
            //self.userID = (newdic["username"] as? String)!
            
            let fName = newdic["firstname"] as? String
            let lName = newdic["lastname"] as? String
            
            self.chatAlertBanner.userNameLabel.text = fName!+" "+lName!+" :"
            
            
            self.chatAlertBanner.frame = CGRect(x: 0, y: -50, width: (self.view!.bounds.width), height: 50)
            
            let tapGuesture =  UITapGestureRecognizer(target: self, action: #selector(DashBoardViewController.fireChatNotification))
            tapGuesture.numberOfTapsRequired = 1
            tapGuesture.numberOfTouchesRequired = 1
            self.chatAlertBanner.addGestureRecognizer(tapGuesture)
            
            UIView.animate(withDuration: 0.35, animations: {
                
                self.chatAlertBanner.frame.origin.y += 70
                
            }, completion: { (something) in
                
                self.perform(#selector(DashBoardViewController.dismissNotificationAlertBanner), with: nil, afterDelay: 2)
                
                //                                    if #available(iOS 10.0, *) {
                //                                        NSTimer(timeInterval: 3, repeats: false, block: { (NSTimer) in
                //
                //                                            UIView.animateWithDuration(2, animations: {
                //
                //                                                self.dismissNotificationAlertBanner()
                //                                            })
                //
                //                                        }).fire()
                //
                //                                    } else {
                //                                        // Fallback on earlier versions
                //
                //                                        NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector:#selector(self.dismissNotificationAlertBanner), userInfo: nil, repeats: false)
                //
                //                                    }
            })
           
            
        }
        
        

    }
    
    override func viewDidDisappear(_ animated: Bool) {
       // self.socket.disconnect()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
    }
    
    func methodOfReceivedNotification(notification: NSNotification){
        //Take Action on Notification
        
        let aps = notification.userInfo?["aps"] as! [String: AnyObject]
        let result: AnyObject? = aps as AnyObject
        let alertDic = result?["alert"] as? NSDictionary
        
        let titleMessage = (alertDic!["title"] as? String)!.uppercased()
        
        if titleMessage == "MESSAGE" {
            
            if isCurrentVC {
                
                
            }else {
            
              let chatMessageVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            
            if let alocDic = alertDic?["action-loc-key"] as? NSDictionary{
                
                chatMessageVC.userId = (alocDic["id"] as? String)!
                
            }
            
            self.navigationController?.pushViewController(chatMessageVC, animated: true)
            }
        }else {
        
            let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(notificationVC, animated: true)
        
        }
        
    }
    
    
    func fireChatNotification(){
        
        self.dismissNotificationAlertBanner()
        
        let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        //chatVC.longitude = self.longitude
        //chatVC.latitude = self.latitude
        
        
        
        
        
        
        let fName = userinfo["firstname"] as? String
        let lName = userinfo["lastname"] as? String
        chatVC.recieverUserFullName = fName!+" "+lName!
        chatVC.recieverImage = (userinfo["image"] as? String)!
        chatVC.recieverUserName = (userinfo["Username"] as? String)!+" :"
        chatVC.userId = (userinfo["username"] as? String)!
        
        
        self.navigationController?.pushViewController(chatVC, animated: true)
        
        
        
    }
    
    func dismissNotificationAlertBanner(){
        
        UIView.animate(withDuration: 0.35, animations: {
            
            self.chatAlertBanner.frame.origin.y = -70
            
        }) { (Bool) in
            
            //            self.chatAlertBanner.removeFromSuperview()
            //            self.chatAlertBanner = nil
        }
    }

    
//    
//    
//    //if we have no permission to access user location, then ask user for permission.
//    func isAuthorizedtoGetUserLocation() {
//        
//        if CLLocationManager.authorizationStatus()
//            != .authorizedWhenInUse     {
//            locationManager.requestWhenInUseAuthorization()
//        }
//    }
//    
//    
//    //this method will be called each time when a user change his location access preference.
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        if status == .authorizedWhenInUse {
//            // print("User allowed us to access location")
//            
//            
//            //do whatever init activities here.
//        }
//    }
//    
//    
//    //this method is called by the framework on         locationManager.requestLocation();
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        //print("Did location updates is called")
//        let locationArray = locations as NSArray
//        let locationObj = locationArray.lastObject as! CLLocation
//        let coord = locationObj.coordinate
//        
//        self.latitude = "\(coord.latitude)"
//        self.longitude = "\(coord.longitude)"
//        
//     
//
//        
//        //print(coord.latitude)
//        // print(coord.longitude)
//        //store the user location here to firebase or somewhere
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print("Did location updates is called but failed getting location \(error)")
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
