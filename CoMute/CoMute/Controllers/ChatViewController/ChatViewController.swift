//
//  ChatViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 07/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import SocketIO


class ChatViewController: UIViewController {
    
    @IBOutlet weak var chatMessagesTableView: UITableView!
    
    @IBOutlet weak var sendMessageTextView: UITextView!
    @IBOutlet weak var textButtonView: UIView!
    @IBOutlet weak var bottomSpaceOfTextAndButtonView: NSLayoutConstraint!
    @IBOutlet weak var chatMessageOutlet: UIButton!
    
    @IBOutlet weak var textViewHightConstant: NSLayoutConstraint!
    @IBOutlet weak var textSubViewHeightConstant: NSLayoutConstraint!
    //MARK: Custom Views...
    var navigation: CustomizedDefaultNavigationBar!
    var tilteBarView: ChatNavigationTitleView!
    var tapGesture : UITapGestureRecognizer!
    var chatDatasource = [NSDictionary]()
    var chatAlertBanner : ChatNotificationView!
    
    //HelperModel
    var helperModel = HelperModel()
    var getChatHistory = ChatModel()
    var userId = String()
    var latitude  = String()
    var longitude = String()
    var isAuthorized = true
    var recieverUserFullName = String()
    var recieverUserName = String()
    var recieverImage =  String()
    var userInfo = NSDictionary()
  
    
    //Socket Variables & Constants
  let chatsocket = SocketIOClient(socketURL: NSURL(string: SERVER_ADDRESS.Socket_URL)! as URL, config:  [.log(true), .forcePolling(true)])
 
    
    //DataBase Array
   // var chatDatasource = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        isCurrentVC = true
        settingExtraFontsAndUI()
        
        //Regestring table view class...
        
        self.chatMessagesTableView.register(SenderTableViewCell.classForCoder(), forCellReuseIdentifier: "SenderCell")
        
        self.chatMessagesTableView.register(UINib(nibName: "SenderTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderCell")
        
        self.chatMessagesTableView.register(ReciverTableViewCell.classForCoder(), forCellReuseIdentifier: "ReciverCell")
        
        self.chatMessagesTableView.register(UINib(nibName: "ReciverTableViewCell", bundle: nil), forCellReuseIdentifier: "ReciverCell")
        
        self.chatMessagesTableView.register(ChatTimeTableViewCell.classForCoder(), forCellReuseIdentifier: "TimeCell")
        
        self.chatMessagesTableView.register(UINib(nibName : "ChatTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "TimeCell")
        
        self.chatMessagesTableView.estimatedRowHeight = 70
        self.chatMessagesTableView.separatorStyle = .none
        self.chatMessagesTableView.rowHeight = UITableViewAutomaticDimension
        
        
        
        //Adding Tap Gesture...
        tapGesture = UITapGestureRecognizer(target: self, action:#selector(ChatViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
        self.getChatHistory.getAllChatHistoryAPI(ID: self.userId) { (succeeded, response) in
            
            if succeeded {
                
                if let dataArray = response["data"] as? NSDictionary {
                    
                    if let chatArray = dataArray["chat"] as? NSArray {
                
        
                    self.chatDatasource = chatArray as! [NSDictionary]
                         self.chatMessagesTableView.reloadData()
                        
                        if self.chatDatasource.count > 0 {
                            
                            self.chatMessagesTableView.scrollToRow(at: IndexPath(row: self.chatDatasource.count - 1, section: 0), at: UITableViewScrollPosition.top, animated: true)
                        }
                    
                    }
                }
                
        }
        }
    
        //self.chatMessagesTableView.tableHeaderView?.backgroundColor = UIColor.lightGray
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.socketEstablishment()
        isCurrentVC = true
         // self.socket.emit("add user", with: [par])
        
        
        
        
        //Notification center for keyboard notifications...
        let keyboardNotificationCenter = NotificationCenter.default
        keyboardNotificationCenter.addObserver(self, selector: #selector(ChatViewController.handleKeyBoardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        keyboardNotificationCenter.addObserver(self, selector: #selector(ChatViewController.handleKeyBoardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        //self.updateContentInsetForTableView(tableView: self.chatMessagesTableView, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Removing Observer...
        NotificationCenter.default.removeObserver(self)
        socket.connect()
        self.chatsocket.disconnect()
        isCurrentVC = false
    }
    
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Socket Establishment...
    func socketEstablishment(){
        
        self.chatsocket.connect()
        let lac = ["lat": latitude,
                   "lon": longitude ]
        //MARK: Socket to Make Connection and Emit Registration
        let par = ["authorization": Singleton.sharedInstance.userAccessToken,
                   "location": lac] as [String : Any]
         print(par)
        
        //self.socket.emit("add user", with: [par])
     //   self.socket.emitWithAck("add user", with: [par])
        self.chatsocket.on("connect") {[weak self] data, ack in
            
             print("ACK.......\(data), \(ack)")
            
            print("socket connected")
            
          self?.isAuthorized = true
            
            self?.chatsocket.emit("add user", with: [par])
            
            return
        }
        
        
       
        self.chatsocket.on("reconnect") {[weak self] data, ack in
            
            print("ACK.......\(data), \(ack)") 
            
            print("socket reconnected")
            
            self?.chatsocket.connect()
            
           // self?.socket.emit("add user", with: [par])
            
            return
        }
        
        // Check if User is Unauthorized
        self.chatsocket.on("disconnect") { (data, ack) -> Void  in
            print("unauthorized")
            self.isAuthorized = false

            // print("data u\(data)")
            
            //  println("un :\(ack)")
                       return
        }


        
        //MARK: Socket To Recieve Send Message ACK....
        self.chatsocket.on("newmessage"){ (data, ack) -> Void in
            
               let newdic = data.first as! NSDictionary
            self.userInfo = newdic
            
           // let viewControllers :  [UIViewController] = (self.navigationController?.viewControllers)!
        
            
           // for aViewController in viewControllers {
            
                if isCurrentVC{
                    
                
                print("ACK.......\(data), \(ack)")
                print(data)
                
                if newdic["username"]as? String == self.userId {
                    
                    
                    self.chatDatasource.append(newdic)
                    
                    
                    //            if self.chatDatasource.count > 0 {
                    //
                    //                self.chatMessagesTableView.scrollToRow(at: IndexPath(row: self.chatDatasource.count - 1, section: 0), at: UITableViewScrollPosition.top, animated: true)
                    //            }
                    self.chatMessagesTableView.reloadData()

                    
                }else {
                    
                    self.addNotificationAlert(newdic: newdic)
                    
                
                }
                
             

        
            }else {
                
                self.addNotificationAlert(newdic: newdic)
                
                           }
                
            }
          
        //}
        

        
    }
    
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "John Doe", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        self.tilteBarView = UINib(nibName: "ChatNavigationTitleView", bundle: nil).instantiate(withOwner: ChatNavigationTitleView.self, options: nil)[0] as! ChatNavigationTitleView
        self.tilteBarView.viewUserProfileOutlet.addTarget(self, action: #selector(ChatViewController.viewProfile(sender:)), for: .touchUpInside)
        
        self.tilteBarView.userFullNameLabel.text = self.recieverUserFullName
        self.tilteBarView.userNameLabel.text = self.recieverUserName
        
        let imageURL = NSURL(string: self.recieverImage)
        let imageRequest = NSURLRequest(url: imageURL! as URL)
        NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
            response, data, error in
            if error != nil {
                //      println("Image not found!")
            }else {
                // println("success")
                let image = UIImage(data: data!)
                //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                self.tilteBarView.userImageView.image = image
                //self.spImageView.layer.cornerRadius = 45
            }
        })
        
        navigation.navigationBarIten.titleView = tilteBarView
        navigation.navigationBarIten.titleView?.clipsToBounds = true
        
        
        textButtonView.layer.borderWidth = 1
        textButtonView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        
    }
    //MARK: StoryBoard Action.. 
    
    @IBAction func sendMessageAction(_ sender: Any) {
        
        let messsagestring =  sendMessageTextView.text as NSString
        let string = messsagestring.replacingOccurrences(of: "\n", with: "")
        _ = messsagestring.length
        let string1 = messsagestring.replacingOccurrences(of: " ", with: "")
        if sendMessageTextView.text != "" &&   sendMessageTextView.text != "Type a Message " && string != "" && string1 != ""{
            
            if self.isAuthorized {
        
       let  msgData = [
        "message" : sendMessageTextView.text,
        "to" : self.userId,
        "authorization" : Singleton.sharedInstance.userAccessToken
        ] as [String : Any]
        
        
        print(msgData)
            
          self.chatsocket.emit("chat", with: [msgData])
        
        let currentTimer    =  NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.string(from: currentTimer as Date)
        
        let dictionary = NSMutableDictionary()
        print(Singleton.sharedInstance.userID)
        dictionary.setValue(Singleton.sharedInstance.userID, forKey:"from")
        //dictionary.setValue(self.userId, forKey:"to")
        //dictionary.setValue(nil, forKey:"insertId")
        dictionary.setValue(sendMessageTextView.text, forKey:"message")
        dictionary.setValue(date, forKey: "time")
        
        self.chatDatasource.append(dictionary)
        
       // self.chatMessagesTableView.reloadData()
        
           /* {
                "from": "599bd68193b8e77760a372bb",
                "message": "sa",
                "time": "2017-09-15T11:11:37.953Z"
        }*/
        
        self.sendMessageTextView.text = ""
        self.textViewHightConstant.constant = 40
        self.textSubViewHeightConstant.constant = 40
        
        //self.updateContentInsetForTableView(tableView: chatMessagesTableView, animated: true)
        
        self.chatMessagesTableView.reloadData()
        
        //self.updateContentInsetForTableView(self.chatMessageTableView, animated: true)
        if self.chatDatasource.count > 0 {
            
           self.chatMessagesTableView.scrollToRow(at: IndexPath(row: self.chatDatasource.count - 1, section: 0), at: UITableViewScrollPosition.top, animated: true)
        }


            }else {
                
                 self.helperModel.showingAlertcontroller(title: "", message: "Please check your connection." as String, cancelButton: "OK", receivedResponse: {})
            }
        
    }
    }
    
    func viewProfile(sender : UIButton){
        
       
            

            
            if recieverUserName != "admin" {
                
                let storyboard: UIStoryboard = UIStoryboard (name: "Main", bundle: nil)
                
                let userProfileVC = storyboard.instantiateViewController(withIdentifier: "ComuteProfileViewContoller") as! ComuteProfileViewContoller
                
                userProfileVC.isCurrentUser = false
                userProfileVC.notificationFlag = 0
                userProfileVC.userID = self.userId
                // let currentController = self.getCurrentViewController()
                //print( self.getCurrentViewController()!)
                
                
                self.navigationController?.pushViewController(userProfileVC, animated: true)
                
            
            
        }
        
        
    }
    
    func fireChatNotification(){
        
        self.dismissNotificationAlertBanner()
        
        let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        //chatVC.longitude = self.longitude
        //chatVC.latitude = self.latitude
        
        
        
        
        
        
        let fName = userInfo["firstname"] as? String
        let lName = userInfo["lastname"] as? String
        chatVC.recieverUserFullName = fName!+" "+lName!
        chatVC.recieverImage = ""
        chatVC.recieverUserName = (userInfo["Username"] as? String)!
        chatVC.userId = (userInfo["username"] as? String)!
        
        
        self.navigationController?.pushViewController(chatVC, animated: true)
        
        
        
    }
    
    func dismissNotificationAlertBanner(){
        
        UIView.animate(withDuration: 0.35, animations: {
            
            self.chatAlertBanner.frame.origin.y = -70
            
        }) { (Bool) in
            
            //            self.chatAlertBanner.removeFromSuperview()
            //            self.chatAlertBanner = nil
        }
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
        
    }

    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
