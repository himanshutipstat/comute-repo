//
//  ChatDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 07/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit


extension ChatViewController : CustomizedDefaultNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate {
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
        
    
    
    //MARK: Gesture recognizer delegates...
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
        
        //Not recognizing gesture if table view/cell is there...
        if (newView?.isKind(of: UITableViewCell.self))! || newView!.isKind(of: UITableView.self) {
            
            return false
            
        } else {
            
            
            return true
        }
    }

    
    //MARK: TableView Delegate & DataSource...
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return chatDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell") as UITableViewCell
        
        /*
 
         {
         "from": "599bd68193b8e77760a372bb",
         "message": "sa",
         "time": "2017-09-15T11:11:37.953Z"
         }

 */
        if let chatdatadic = self.chatDatasource[indexPath.row] as? NSDictionary {
            
            if Singleton.sharedInstance.userID != chatdatadic["from"] as? String {
                
                let  cell = chatMessagesTableView.dequeueReusableCell(withIdentifier: "ReciverCell") as! ReciverTableViewCell
                
                cell.SocketDataDictionary = chatdatadic
                
                return cell
                
            }else {
                
               let  cell = chatMessagesTableView.dequeueReusableCell(withIdentifier: "SenderCell") as! SenderTableViewCell
                cell.SocketDataDictionary = chatdatadic
                
                return cell
                
            }
            
        }
        
        
//        if indexPath.row == 1 {
//            cell = chatMessagesTableView.dequeueReusableCell(withIdentifier: "TimeCell") as! ChatTimeTableViewCell
//            print(cell)
//            //cell.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
//            //cell.selectionStyle = .lightGray
//            
//            return cell
//        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    
    //MARK: Starting TableView Cells From  Bottom..
    func updateContentInsetForTableView(tableView: UITableView, animated: Bool) {
        let lastRow = self.tableView(tableView, numberOfRowsInSection: 0)
        let lastIndex = lastRow > 0 ? lastRow - 1 : 0
        let lastIndexPath = IndexPath(row: lastIndex, section: 0)
        let lastCellFrame = tableView.rectForRow(at: lastIndexPath)
        // top inset = table view height - top position of last cell - last cell height
        let topInset: CGFloat = max(tableView.frame.height - lastCellFrame.origin.y - lastCellFrame.height, 0)
        var contentInset = tableView.contentInset
        contentInset.top = topInset
        // var options = .UseUnicodeWordBoundaries
        UIView.animate(withDuration: animated ? 0.25 : 0.0, delay: 0.0, options: .beginFromCurrentState, animations: {() -> Void in
            tableView.contentInset = contentInset
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    //MARK: textView Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        print("textViewDidBeginEditing")
        if sendMessageTextView.text == "Type a Message "
        {
            sendMessageTextView.text = ""
        }
        
        /* // animate view frame as scroll while keyboard appears
         let textViewRect: CGRect = self.view.window!.convertRect(textView.bounds, fromView: textView)
         let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
         let midline: CGFloat = textViewRect.origin.y + 0.5 * textViewRect.size.height
         let numerator: CGFloat = midline - viewRect.origin.y - 0.3 * viewRect.size.height
         let denominator: CGFloat = 0.5 * viewRect.size.height
         var heightFraction: CGFloat = numerator / denominator
         if (heightFraction < 0.0)
         {
         heightFraction = 0.0;
         }
         else if (heightFraction > 1.0)
         {
         heightFraction = 1.0;
         }
         let theAppDelegate = UIApplication.sharedApplication()
         let orientation = theAppDelegate.statusBarOrientation
         if orientation.isPortrait
         {
         animatedDistance = floor(216 * heightFraction + 45)
         }
         else
         {
         animatedDistance = floor(162 * heightFraction + 45)
         }
         var viewFrame = self.view.frame
         viewFrame.origin.y -= animatedDistance //+ CGFloat(45)
         UIView.beginAnimations(nil, context: nil)
         UIView.setAnimationBeginsFromCurrentState(true)
         UIView.setAnimationDuration(0.3)
         self.view.frame = viewFrame
         UIView.commitAnimations()
         //messageTextView.contentInset =  UIEdgeInsetsMake(-45, 0, 0, 0)*/
        
    }
    
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        //        setInitialViewFrame()
        self.view.endEditing(true)
        
        if self.sendMessageTextView.text == "" {
            
            self.sendMessageTextView.text = "Type a Message "
            self.textViewHightConstant.constant = 40
            self.textSubViewHeightConstant.constant = 40
           
        }
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        //Changing height of text view...
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        //Setting Scroll View Insets....as per height to text view to visible...
        if newFrame.height > self.textViewHightConstant.constant && newFrame.height < self.view.frame.height / 5 {
            
            self.textViewHightConstant.constant = newFrame.size.height
            self.textSubViewHeightConstant.constant = self.textViewHightConstant.constant + 2
            //  self.chatMessageTableView.scrollRectToVisible(self.sendMessageTextView.frame, animated: true)
            
            if self.chatDatasource.count > 0 {
              
                self.chatMessagesTableView.scrollToRow(at: IndexPath(row: self.chatDatasource.count - 1, section: 0), at: UITableViewScrollPosition.top, animated: true)
            }
            
            //Animated Updation......
            UIView.animate(withDuration: 0.5, animations: {
                
                self.view.layoutIfNeeded()
            })
            
        } else if newFrame.height <= self.textViewHightConstant.constant && newFrame.height > 38 {
            
            self.textViewHightConstant.constant = newFrame.size.height
            self.textSubViewHeightConstant.constant = self.textViewHightConstant.constant + 2
            // self.chatMessageTableView.scrollRectToVisible(self.sendMessageTextView.frame, animated: true)
            
            if self.chatDatasource.count > 0 {
                
                self.chatMessagesTableView.scrollToRow(at: IndexPath(row: self.chatDatasource.count - 1, section: 0), at: UITableViewScrollPosition.top, animated: true)
            }
            
            //Animated Updation......
            UIView.animate(withDuration: 0.5, animations: {
                
                self.view.layoutIfNeeded()
            })
        }
        
        if textView.text == "" {
            
            self.textViewDidEndEditing(textView)
        }
    }


    
    
    //MARK: HANDLE KEYBOARD
    func handleKeyBoardWillHide(sender: NSNotification) {
        
        let userInfo = sender.userInfo
        
        if let info = userInfo {
            
            let animationDurationObject = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
            var animationDuration = 0.0
            animationDurationObject.getValue(&animationDuration)
            
            UIView.animate(withDuration: animationDuration, animations: {
                
                self.bottomSpaceOfTextAndButtonView.constant = 20
                
            })
        }
    }
    
    func handleKeyBoardWillShow(notification: NSNotification) {
        
        let userInfo = notification.userInfo
        
        if let info = userInfo {
            
            let animationDurationObject = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
            let keyboardEndRectObject = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            var animationDuration = 0.0
            var keyboardEndRect = CGRect.zero
            animationDurationObject.getValue(&animationDuration)
            keyboardEndRectObject.getValue(&keyboardEndRect)
            let intersectionOfKeyboadrRectAndWindowRect = view.frame.intersection(keyboardEndRect)
            
            UIView.animate(withDuration: animationDuration, animations: {
                
                self.bottomSpaceOfTextAndButtonView.constant = intersectionOfKeyboadrRectAndWindowRect.height  + 20
//                let indexPath = NSIndexPath(forRow: self.chatDatasource.count - 1, inSection: 0)
//                if indexPath.row >= 0 {
//                    
//                    DispatchQueue.main.asynchronously() {
//                        self.chatMessagesTableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
//                    }
//                }
            })
        }
    }
    
    
    func addNotificationAlert(newdic : NSDictionary){
        if self.chatAlertBanner == nil {
            
            self.chatAlertBanner =  UINib(nibName: "ChatNotificationView", bundle: nil).instantiate(withOwner: ChatNotificationView.self, options: nil)[0] as! ChatNotificationView
            
            self.view!.addSubview(self.chatAlertBanner)
        }
        
        self.chatAlertBanner.userMessageLabel.text = newdic["message"] as? String
        self.userId = (newdic["username"] as? String)!
        
        let fName = newdic["firstname"] as? String
        let lName = newdic["lastname"] as? String
        
        self.chatAlertBanner.userNameLabel.text = fName!+" "+lName!
        
        
        self.chatAlertBanner.frame = CGRect(x: 0, y: -50, width: (self.view!.bounds.width), height: 50)
        
        let tapGuesture =  UITapGestureRecognizer(target: self, action: #selector(ChatViewController.fireChatNotification))
        tapGuesture.numberOfTapsRequired = 1
        tapGuesture.numberOfTouchesRequired = 1
        self.chatAlertBanner.addGestureRecognizer(tapGuesture)
        
        UIView.animate(withDuration: 0.35, animations: {
            
            self.chatAlertBanner.frame.origin.y += 70
            
        }, completion: { (something) in
            
            self.perform(#selector(ChatViewController.dismissNotificationAlertBanner), with: nil, afterDelay: 2)
            
            //                                    if #available(iOS 10.0, *) {
            //                                        NSTimer(timeInterval: 3, repeats: false, block: { (NSTimer) in
            //
            //                                            UIView.animateWithDuration(2, animations: {
            //
            //                                                self.dismissNotificationAlertBanner()
            //                                            })
            //
            //                                        }).fire()
            //
            //                                    } else {
            //                                        // Fallback on earlier versions
            //
            //                                        NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector:#selector(self.dismissNotificationAlertBanner), userInfo: nil, repeats: false)
            //
            //                                    }
        })

        
    }

    
    
}
