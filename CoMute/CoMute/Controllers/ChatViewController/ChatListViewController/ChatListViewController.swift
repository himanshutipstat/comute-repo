//
//  ChatListViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 07/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreLocation
class ChatListViewController: UIViewController {
    
    
    @IBOutlet weak var chatListTableView: UITableView!

    //MARK: Custom Views...
    var navigation: CustomizedDefaultNavigationBar!
    
    let locationManager = CLLocationManager()
    var latitude  = String()
    var longitude = String()
    var chatListModel = ChatModel()
    var chatListArray = NSArray()
    var refresher: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingExtraFontsAndUI()
        
//        self.isAuthorizedtoGetUserLocation()
//        
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        }
//        locationManager.requestAlwaysAuthorization()
//        locationManager.startUpdatingLocation()
        
      
        
        //self.chatListTableView.backgroundView = UINib(nibName: "NoView", bundle: nil).instantiate(withOwner: NoView.self, options: nil)[0] as! NoView
        
        //Regestring table view class...
        
        self.chatListTableView.register(ChatListTableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        self.chatListTableView.register(UINib(nibName: "ChatListTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        self.chatListTableView.tableHeaderView?.backgroundColor = UIColor.lightGray
        //self.chatListTableView.rowHeight = 65
        self.chatListTableView.separatorStyle = .none
       // self.chatListTableView.rowHeight = UITableViewAutomaticDimension
        
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        self.chatListModel.getAllChatListAPI(ID: "") { (succeeded, response) in
            
            if succeeded {
                
                print(response)
                
                let dataDic = response["data"] as? NSArray
                
                print(dataDic!)
                
                self.chatListArray =  dataDic!
                
                print(self.chatListArray.count)
                self.chatListTableView.reloadData()
            }
        }

        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "WhiteBlank")!], buttonHighlightedImage: [UIImage(named: "WhiteBlank")!], numberOfButtons: 2, barTitle: "CHAT", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        refresher = UIRefreshControl()
        //refresher.attributedTitle = NSAttributedString(string: "pull to refresh")
        
        refresher.addTarget(self, action: #selector(ComuteFriendListViewController.refresh), for: UIControlEvents.valueChanged)
        self.chatListTableView.addSubview(refresher)

        
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
