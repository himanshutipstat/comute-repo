//
//  ChatListDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 07/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


extension ChatListViewController : CustomizedDefaultNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate {
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            
            
        }
    }
    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus()
            != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
           // print("User allowed us to access location")
            
            
            //do whatever init activities here.
        }
    }
    
    
    //this method is called by the framework on         locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("Did location updates is called")
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        
        self.latitude = "\(coord.latitude)"
        self.longitude = "\(coord.longitude)"
        
        //print(coord.latitude)
       // print(coord.longitude)
        //store the user location here to firebase or somewhere
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }

    
    //MARK: TableView Delegate & DataSource...
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.chatListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ChatListTableViewCell
        cell.backgroundColor = UIColor.white
        cell.selectionStyle = .none
        if let chatDataDic = self.chatListArray[indexPath.row] as? NSDictionary {
            
            let fName = chatDataDic["firstname"] as! String
            let lName = chatDataDic["lastname"] as! String
            
            cell.userFullName.text = fName+" "+lName
            
            if let image = chatDataDic["image"] as? String {
                
                // self.userProfileImage.addSubview(ActivityIndicator.sharedInstance)
                // ActivityIndicator.sharedInstance.showActivityIndicator()
                let imageURL = NSURL(string: image)
                let imageRequest = NSURLRequest(url: imageURL! as URL)
                NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                    response, data, error in
                    if error != nil {
                        //      println("Image not found!")
                    }else {
                        // println("success")
                        let image = UIImage(data: data!)
                        //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                        cell.userImageView.image = image
                        //self.spImageView.layer.cornerRadius = 45
                    }
                })
                
            }
            if let chatMessageDic = chatDataDic["chat"] as? NSDictionary{
                
                cell.lastMessageLabel.text = chatMessageDic["message"] as? String
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
                
                if let dateString = chatMessageDic["time"] as? String {
                    
                    let dateArr = dateString.components(separatedBy: ".")
                    print(dateArr.first!)
                    
                    if let selectedDate = formatter.date(from: dateArr.first!) {
                        formatter.dateFormat = "h:mm a MMM dd, yyyy"
                        let date = formatter.string(from: selectedDate)
                        cell.dateTimeLabel.text = date
                        
                    }
                }
                
            }

            
        }
        /*
         {
         "_id" = 5996cde2163365027160f193;
         chat =             {
         "_id" = 59dca80db66fc25048c2a251;
         from = 599bd68193b8e77760a372bb;
         message = sf;
         time = "2017-10-10T08:00:55.816Z";
         };
         firstname = akilesh;
         image = "https://comut.s3.ap-south-1.amazonaws.com/0ac34281-4f52-433f-8db1-522e492f34bd";
         lastname = v;
         username = akilesh;
         }

 */
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatVC.longitude = self.longitude
        chatVC.latitude = self.latitude
        
         if let chatDataDic = self.chatListArray[indexPath.row] as? NSDictionary {
            
            
        
            
            let fName = chatDataDic["firstname"] as! String
            let lName = chatDataDic["lastname"] as! String
            chatVC.recieverUserFullName = fName+" "+lName
            chatVC.recieverImage = (chatDataDic["image"] as? String)!
            chatVC.recieverUserName = chatDataDic["username"] as! String
            chatVC.userId = chatDataDic["_id"] as! String
            
       
        
        }
        self.navigationController?.pushViewController(chatVC, animated: true)
        
    }
    
    func refresh(){
        
        print("refreshed")
        self.refresher.endRefreshing()
        
         self.chatListModel.getAllChatListAPI(ID: "") { (succeeded, response) in
            
            if succeeded {
                
                print(response)
                
                let dataDic = response["data"] as? NSArray
                
                print(dataDic!)
                
                self.chatListArray =  dataDic!
                
                print(self.chatListArray.count)
                self.chatListTableView.reloadData()
            }
        }
    }

    
}

