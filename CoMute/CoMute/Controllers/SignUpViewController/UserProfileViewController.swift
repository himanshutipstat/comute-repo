//
//  UserProfileViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import GBHFacebookImagePicker

class UserProfileViewController: UIViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBOutlet weak var galleryButtonOutlet: UIButton!
    
    //MARK: Custom Views...
    var tapGesture : UITapGestureRecognizer!
    var navigation: CustomizedDefaultNavigationBar!
    
    //  var activeTextField: UITextField?
    
    //Helper Model...
    var helperModel = HelperModel()
    var userSignUpModel = UserSignUpModel()
    
    //MARK: Image Picker Views...
    var imagePickerController = UIImagePickerController()
    var facebookImagePicker = GBHFacebookImagePicker()
    var imageModels =  UIImage()
    
    
    //Local Variable..
    var emailField = ""
    var contactNo = ""
    var  contactinfo = ["",""]
    override func viewDidLoad() {
        super.viewDidLoad()
        setUPUIConstrain()
        
        self.imageModels = self.userProfileImageView.image!
        //If Facebook ID is not Present Only than Show Facebook Data......
        if Singleton.sharedInstance.facebookId.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) != "" {
            
            //Fetching facebook Data from USER DEFAULTS......
            let values:NSDictionary = (UserDefaults.standard.value(forKey: USER_DEFAULT.FACEBOOK_DATA) as? NSDictionary) != nil ? (UserDefaults.standard.value(forKey: USER_DEFAULT.FACEBOOK_DATA) as! NSDictionary) : [:]
            
            
            //Fetching FirstName of user from facebook....
            if (values.allKeys as NSArray).contains("first_name") {
                
                if values.value(forKey: "first_name") as? String != nil {
                    
                    self.firstNameTextField.text = values.value(forKey: "first_name") as! NSString as String
                    
                }
            }
            
            //Fetching LastName of user from facebook....
            if (values.allKeys as NSArray).contains("last_name") {
                
                if values.value(forKey: "last_name") as? String != nil {
                    
                    self.lastNameTextField.text = values.value(forKey: "last_name") as! NSString as String
                }
            }
            
            
            //Fetching image of user from facebook....
            if (values.allKeys as NSArray).contains("picture") {
                
                if values.value(forKey: "picture") as? NSDictionary != nil {
                    
                    let picDic = values.value(forKey: "picture") as! NSDictionary
                    
                    let dataDic = picDic.value(forKey: "data") as? NSDictionary
                    
                    let picURL = dataDic?.value(forKey: "url") as? String
                    
                    let imageURL = NSURL(string: picURL!)
                    let imageRequest = NSURLRequest(url: imageURL! as URL)
                    NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                        response, data, error in
                        if error != nil {
                            //      println("Image not found!")
                        }else {
                            // println("success")
                            let image = UIImage(data: data!)
                            self.userProfileImageView.image = image
                            self.imageModels = image!
                            //self.spImageView.layer.cornerRadius = 45
                        }
                    })

                }
            }

            
            
            // Do any additional setup after loading the view.
        }
        
        
        
        
        //Setting frames of scroll View Menually as by this time scroll view frame are not set...
        // self.mainScrollView.frame = CGRect(x: 0, y: 250, width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, height: GLOBAL_KEY.CURRENT_DEVICE_SIZE.height - 350)
        self.mainScrollView.keyboardDismissMode = .onDrag
        
        //Setting scroll view insets...
        self.automaticallyAdjustsScrollViewInsets = false
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Notification center for keyboard notifications...
        let keyboardNotificationCenter = NotificationCenter.default
        keyboardNotificationCenter.addObserver(self, selector: #selector(UserProfileViewController.handleKeyBoardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        keyboardNotificationCenter.addObserver(self, selector: #selector(UserProfileViewController.handleKeyBoardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Removing Observer...
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "SignUp", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        //Setting Tap Gesture...
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
        
        galleryButtonOutlet.layer.borderWidth = 1
        galleryButtonOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        //self.firstNameTextField.isSecureTextEntry = false
        //self.lastNameTextField.isSecureTextEntry = false
        //self.userNameTextField.isSecureTextEntry = false
    }
    
    //MARK: Storyboard Actions..
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        var message:NSString = ""
        
        if self.firstNameTextField.text != "" && self.lastNameTextField.text! != "" &&
            self.userNameTextField.text! != "" && self.passwordTextField.text! != "" {
            
            
            if Singleton.sharedInstance.facebookId.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) == "" && !Singleton.sharedInstance.isValidPassword(password: self.passwordTextField.text!) {
                
                message = "Password must be atleast 8 characters long!"
            }
            
            //Validation Check....
            if(message.length > 0) {
                
                self.helperModel.showingAlertcontroller(title: "", message: message as String, cancelButton: "OK", receivedResponse: {})
                
            } else {
                
                
                userSignUpModel.userNameCheckAPI(user_Name: self.userNameTextField.text!, recievedResponse: {  (succeeded, response) in
                    
                    if succeeded {
                        
                        
                        let chooseLineVC = self.storyboard?.instantiateViewController(withIdentifier: "UserLineViewController") as! UserLineViewController
                        chooseLineVC.profileScreenParametrs[0] = self.firstNameTextField.text!
                        chooseLineVC.profileScreenParametrs[1] = self.lastNameTextField.text!
                        chooseLineVC.profileScreenParametrs[2] = self.userNameTextField.text!
                        chooseLineVC.profileScreenParametrs[3] = self.passwordTextField.text!
                        chooseLineVC.profileImage = self.imageModels
                        chooseLineVC.contactinfo = self.contactinfo
                        
                        self.navigationController?.pushViewController(chooseLineVC, animated: true)
 
                        
                    }else {
                        
                     
                    }
                })
                
                
            }
        }else {
            self.helperModel.showingAlertcontroller(title: "", message: "Please make sure you fill all the field." as String, cancelButton: "OK", receivedResponse: {})
            
        }
    }
    
    @IBAction func openGallaryAction(_ sender: AnyObject) {  // Phone Gallary
        
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openPhotos()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        imagePickerController.delegate = self
        self.present(alert, animated: true, completion: nil)    }
    
    
    
    
    
    @IBAction func openFacebookImageAction(_ sender: AnyObject) { // Facebook Gallary
        
         facebookImagePicker.presentFacebookAlbumImagePicker(from: self, delegate: self as GBHFacebookImagePickerDelegate)
    }
    
    //MARK: Removing keyboard...And Setting zesture fopr zooming...
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        self.firstNameTextField.resignFirstResponder()
        self.lastNameTextField.resignFirstResponder()
        self.userNameTextField.resignFirstResponder()
        self.userNameTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
}
