//
//  ContactViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    
    
    //MARK: Custom Views...
    var tapGesture : UITapGestureRecognizer!
    var navigation: CustomizedDefaultNavigationBar!
    
    //Helper Model...
    var helperModel = HelperModel()
    var userSignUpModel = UserSignUpModel()
    
    //Facebook email verified flag is true if email comming from facebook is not editted else it will be false.......
    var facebookEmailVerifiedFlag = true
    var facebookEmail = String()
    
    
    //Helper Model...
    //var helperModel = HelperModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUPUIConstrain()
        
        //If Facebook ID is not Present Only than Show Facebook Data......
        if Singleton.sharedInstance.facebookId.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) != "" {
            
            //Fetching facebook Data from USER DEFAULTS......
            let values:NSDictionary = (UserDefaults.standard.value(forKey: USER_DEFAULT.FACEBOOK_DATA) as? NSDictionary) != nil ? (UserDefaults.standard.value(forKey: USER_DEFAULT.FACEBOOK_DATA) as! NSDictionary) : [:]
            
            
            //Fetching Email of user from facebook....
            if (values.allKeys as NSArray).contains("email") {
                
                if values.value(forKey: "email") as? String != nil {
                    
                    self.emailTextField.text = values.value(forKey: "email") as! NSString as String
                    self.facebookEmail = values.value(forKey: "email") as! NSString as String
                }
            }
            
            
            // Do any additional setup after loading the view.
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Removing Observer...
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Setting Extra Fonts and UI...
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "SignUp", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        //Setting Tap Gesture...
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(ContactViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    //MARK: Storyboard Actions...
    @IBAction func nextButtonAction(_ sender: AnyObject) {
        
        
        
        self.view.endEditing(true)
        
        if self.emailTextField.text! != "" && self.contactTextField.text! != ""{
            
            var message:NSString = ""
            
            if(!Singleton.sharedInstance.isPhoneNumberValid(number: self.contactTextField.text!.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil))) {
                
                message = "Please enter valid Phone Number"
                
            } else if(!Singleton.sharedInstance.isEmailValid(email: self.emailTextField.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines))) {
                
                message = "Please enter valid Email Address"
                
            } //else if Singleton.sharedInstance.facebookId.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) == "" && !Singleton.sharedInstance.isValidPassword(self.userDetailsEntryView[2].userDetailTextField.text!) {
            
            // message = "Password must be atleast 6 characters long!"
            // }
            
            //Validation Check....
            if(message.length > 0) {
                
                self.helperModel.showingAlertcontroller(title: "", message: message as String, cancelButton: "OK", receivedResponse: {})
                
            } else {
                
                userSignUpModel.userContactCheckAPI(email: self.emailTextField.text!, contact: self.contactTextField.text!, recievedResponse: { (succeeded, response) in
                    
                    if succeeded {
                        
                        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
                        profileVC.contactinfo[0] = self.emailTextField.text!
                        profileVC.contactinfo[1] = self.contactTextField.text!
                        
                        self.navigationController?.pushViewController(profileVC, animated: true)
                        
                    }
                })
                
                
                
                
            }
            
        }else {
            self.helperModel.showingAlertcontroller(title: "", message: "Please make sure you fill all the field." as String, cancelButton: "OK", receivedResponse: {})
        }
    }
    
    //MARK: Removing keyboard...And Setting zesture fopr zooming...
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        self.emailTextField.resignFirstResponder()
        self.contactTextField.resignFirstResponder()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
