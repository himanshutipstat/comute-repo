//
//  UserLineViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class UserLineViewController: UIViewController {
    
    
    @IBOutlet weak var homeStationTextField: UITextField!
    @IBOutlet weak var destinationStationTextField: UITextField!
    @IBOutlet weak var comuteYearTextField: UITextField!
    @IBOutlet weak var stationPickerView: UIPickerView!
    @IBOutlet weak var pickerViewBottomConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var homeStationOutlet: UIButton!
    @IBOutlet weak var destinationStationOutlet: UIButton!
    //MARK: Custom Views...
    var tapGesture : UITapGestureRecognizer!
    var navigation: CustomizedDefaultNavigationBar!
    var userSignUpModel = UserSignUpModel()
    var getLIRRStationModel = LIRRModel()
    var flag = 0
    
    // Input data into the Array:
    var pickerData = NSArray()

    
    // API's Parameters
     var profileScreenParametrs = ["","","",""]
    var profileImage =  UIImage()
     var  contactinfo = ["",""]

    //Helper Model...
    var helperModel = HelperModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stationPickerView.delegate = self
        stationPickerView.dataSource = self

        setUPUIConstrain()
        
//        self.getLIRRStationModel.getAllLIRRStationAPI(ID: "") { (succeeded, response) in
//            
//            if succeeded {
//                
//                self.pickerData = (response["data"]  as? NSArray)!
//                print(self.pickerData)
//                self.stationPickerView.reloadAllComponents()
//                
//            }
//            
//        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Removing Observer...
        NotificationCenter.default.removeObserver(self)
    }
    
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "SignUp", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        //Setting Tap Gesture...
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserLineViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        self.homeStationTextField.isUserInteractionEnabled = false
        self.destinationStationTextField.isUserInteractionEnabled = false
        
    }
    
    

    //MARK: Storyboard Action...
    @IBAction func nextButtonAction(_ sender: AnyObject) {
        
        if self.homeStationTextField.text != "" && self.destinationStationTextField.text! != "" &&
            self.comuteYearTextField.text! != ""{
            
            if self.homeStationTextField.text != self.destinationStationTextField.text! {
            
            print(profileImage)
            
            userSignUpModel.userSignUpAPI(user_Mobile: contactinfo[1], user_Email: contactinfo[0], first_Name: profileScreenParametrs[0], last_Name: profileScreenParametrs[1], home_Station: self.homeStationTextField.text!, destination_Station: self.destinationStationTextField.text!, comute_Years: self.comuteYearTextField.text!, profile_Pic:  UIImageJPEGRepresentation(self.profileImage, 0.0)!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)), password: profileScreenParametrs[3], user_Name: profileScreenParametrs[2], user_Fb_ID: Singleton.sharedInstance.facebookId, facebookEmailVerifiedFlag: false, recievedResponse: { (succeeded, response) in
                
                print(response)
              //  ActivityIndicator.sharedInstance.hideActivityIndicator()
                
                if succeeded{
                    
                    
                    let invitationVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectInvitationListViewController") as! SelectInvitationListViewController
                    self.navigationController?.pushViewController(invitationVC, animated: true)
                    
                }else {
                    
                    print(response)
                }
                
                }
            )
            }else {
                
                self.helperModel.showingAlertcontroller(title: "", message: "Home and Destination can not be same." as String, cancelButton: "OK", receivedResponse: {})
            }
            
        }else {
            
            self.helperModel.showingAlertcontroller(title: "", message: "Please make sure you fill all the field." as String, cancelButton: "OK", receivedResponse: {})
        }
        
      
        
        
       
    }
    
    @IBAction func HomeStationPickerAction(_ sender: Any) {
        
        self.flag = 1
        
        homeStationOutlet.isExclusiveTouch = true
        let searchStationVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchStationViewController") as! SearchStationViewController
        searchStationVC.delegate = self
        
        self.navigationController?.pushViewController(searchStationVC, animated: true)
            
            
      //  } else {
            
//            UIView.animate(withDuration: 0.15, animations: {
//                
//                self.pickerViewBottomConstrain.constant = -220
//                
//            }, completion: {finished in
//                
//                
//            }
//            )
//        }
    }
    
    @IBAction func DestinationStationPickerAction(_ sender: Any) {
        
        self.flag = 2
        
        destinationStationOutlet.isExclusiveTouch = true
        
        let searchStationVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchStationViewController") as! SearchStationViewController
        searchStationVC.delegate = self
        
        self.navigationController?.pushViewController(searchStationVC, animated: true)
            
            
            
      //  } else {
            
//            UIView.animate(withDuration: 0.15, animations: {
//                
//                self.pickerViewBottomConstrain.constant = -220
//                
//            }, completion: {finished in
//                
//                
//            }
//            )  
   //     }
    }
    @IBAction func closePickerAction(_ sender: Any) {
        
        self.pickerViewBottomConstrain.constant = -220
    }
  

    //MARK: Removing keyboard...And Setting zesture fopr zooming...
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        self.homeStationTextField.resignFirstResponder()
        self.destinationStationTextField.resignFirstResponder()
        self.comuteYearTextField.resignFirstResponder()
    }
}
