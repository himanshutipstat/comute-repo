//
//  UserProfileDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import GBHFacebookImagePicker

extension  UserProfileViewController : CustomizedDefaultNavigationBarDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,GBHFacebookImagePickerDelegate {
    
    //MARK: Text Field Delegates...
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        //If textfield secure text field entry...
        if textField == firstNameTextField || textField == lastNameTextField || textField == userNameTextField || textField == passwordTextField {
            
            //textField.isSecureTextEntry = true
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //Adding Keyboard Secure Text Field Entry....
        
        switch textField
        {
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            userNameTextField.becomeFirstResponder()
        case userNameTextField:
            passwordTextField.becomeFirstResponder()
            break
            
        default:
            textField.resignFirstResponder()
        }
        return true
        
    }
    
    
    // MARK: - GBHFacebookImagePicker Protocol
    func facebookImagePicker(imagePicker: UIViewController, successImageModels: [GBHFacebookImage], errorImageModels: [GBHFacebookImage], errors: [Error?]) {
        
        
        // self.imageModels.append(successImageModels as AnyObject)
        let image = successImageModels.first
        userProfileImageView.contentMode = .scaleAspectFill //3
        userProfileImageView.image = image!.image //4
        imageModels  = image!.image!
       // print(image?.image ?? <#default value#>)
        //print(successImageModels.first)
        print(imageModels)
    }
    
    func facebookImagePicker(imagePicker: UIViewController, didFailWithError error: Error?) {
        print("Cancelled Facebook Album picker with error")
        print(error.debugDescription)
    }
    
    // Optional
    func facebookImagePicker(didCancelled imagePicker: UIViewController) {
        print("Cancelled Facebook Album picker")
    }
    
    // Optional
    func facebookImagePickerDismissed() {
        print("Picker dismissed")
    }
    
    
    //MARK: Gesture recognizer delegates...
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        //Finding Which view is tapped....
        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
        
        //Recognizing gesture if Tapped on Image view......
        if newView!.isKind(of: UIImageView.self) {
            
            
            return false
        }
        
        return true
    }
    
    
    //MARK: UIIMage Picker Controller Delegates...
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var  chosenImage = UIImage()
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            chosenImage = image
        }else {
            
             chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        }
        userProfileImageView.contentMode = .scaleToFill //3
        print(chosenImage)
        userProfileImageView.image = chosenImage //4
        imageModels  = chosenImage
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Imagepicker Functins
    
    func openPhotos(){
        
        imagePickerController.allowsEditing = true
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    func openCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController.allowsEditing = false
            imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
            imagePickerController.cameraCaptureMode = .photo
            imagePickerController.modalPresentationStyle = .fullScreen
            present(imagePickerController,animated: true,completion: nil)
        } else {
            noCamera()
        }
        
        
    }
    
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    

    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
//    func keyboardControls(keyboardControls: KeyboardControls, selectedField field: UIView, inDirection direction:  KeyboardControlsDirection)
//    {
//        var view: UIView
//        view = field.superview!.superview!.superview!
//    }
//    
//    func keyboardControlsDonePressed(keyboardControls: KeyboardControls)
//    {
//        self.view.endEditing(true)
//    }
//    
//    
//    func accountForKeyboardNotifications() {
//        let notificationCenter = NotificationCenter.default
//        notificationCenter.addObserver(self,
//                                       selector: Selector(("keyboardWillShow:")),
//                                       name: NSNotification.Name.UIKeyboardWillShow,
//                                       object: nil)
//        notificationCenter.addObserver(self,
//                                       selector: Selector(("keyboardWillHide:")),
//                                       name: NSNotification.Name.UIKeyboardWillHide,
//                                       object: nil)
//    }
//    
//    func keyboardWillShow(sender: NSNotification) {
//        let info: NSDictionary = sender.userInfo! as NSDictionary
//        let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
//        let keyboardSize: CGSize = value.cgRectValue.size
//        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
//        mainScrollView.contentInset = contentInsets
//        mainScrollView.scrollIndicatorInsets = contentInsets
//        // If active text field is hidden by keyboard, scroll it so it's visible
//        var aRect: CGRect = self.view.frame
//        aRect.size.height -= keyboardSize.height
//        let activeTextFieldRect: CGRect? = activeTextField!.frame
//        let activeTextFieldOrigin: CGPoint? = activeTextFieldRect?.origin
//        
//        if (!aRect.contains(activeTextFieldOrigin!)) {
//            mainScrollView.scrollRectToVisible(activeTextFieldRect!, animated:true)
//        }
//    }
//    func keyboardWillHide(sender: NSNotification) {
//        let contentInsets: UIEdgeInsets = UIEdgeInsets.zero
//        mainScrollView.contentInset = contentInsets
//        mainScrollView.scrollIndicatorInsets = contentInsets
//    }
    
    
    
    //MARK: Keyboard notifications....
    func handleKeyBoardWillHide(sender: NSNotification) {
        
        let userInfo = sender.userInfo
        
        if let info = userInfo {
            
            let animationDurationObject = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
            var animationDuration = 0.0
            animationDurationObject.getValue(&animationDuration)
            
            UIView.animate(withDuration: animationDuration, animations: {
                
                self.mainScrollView.contentInset = UIEdgeInsets.zero
            })
        }
    }
    
    
    func handleKeyBoardWillShow(notification: NSNotification) {
        
        let userInfo = notification.userInfo
        
        if let info = userInfo {
            
            let animationDurationObject = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
            let keyboardEndRectObject = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            var animationDuration = 0.0
            var keyboardEndRect = CGRect.zero
            animationDurationObject.getValue(&animationDuration)
            keyboardEndRectObject.getValue(&keyboardEndRect)
            
            let intersectionOfKeyboadrRectAndWindowRect = self.view.frame.intersection(keyboardEndRect)
            
            UIView.animate(withDuration: animationDuration, animations: {[weak self] in
                
                //Frames of text view to be visible...
                self!.mainScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: intersectionOfKeyboadrRectAndWindowRect.size.height, right: 0)
                self!.mainScrollView.scrollRectToVisible((self?.firstNameTextField.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.lastNameTextField.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.userNameTextField.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.passwordTextField.frame)!, animated: true)
                })
        }
    }
    
    
}
