//
//  UserLineDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension  UserLineViewController : CustomizedDefaultNavigationBarDelegate,UIGestureRecognizerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,searchStationViewControllerDelegate {
    
    //MARK: Station Delegate ..
    
    func setLocationWithLatLong(station : String) {
        print(station)
        
        if flag == 1{
            
            self.homeStationTextField.text = station
        }else {
            
            self.destinationStationTextField.text = station
            
        }
        
        }
    
    //MARK: Gesture recognizer delegates...
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        //Finding Which view is tapped....
        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
        
        //Recognizing gesture if Tapped on Image view......
        if newView!.isKind(of: UIImageView.self) {
            
            
            return false
        }
        
        return true
    }
    
    //MARK: Picker Delegates....
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if self.flag == 1 {
            
            print(pickerData[row])
            
            self.homeStationTextField.text = pickerData[row] as? String
            
        }else if self.flag == 2 {
            
            
            
            self.destinationStationTextField.text = pickerData[row] as? String
        }
        //self.chooseTypeOutlet.setTitle(pickerData[row], for: .normal)
        return pickerData[row] as? String
    }
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
}
