//
//  EditProfileViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 15/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import GBHFacebookImagePicker

class EditProfileViewController: UIViewController {
    
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var gallaryButtonOutlet: UIButton!
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    
    @IBOutlet weak var firstName: UITextField!
    
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var comuteYear: UITextField! // Edit lines
    @IBOutlet weak var homeStation: UITextField!
    @IBOutlet weak var detstinationStation: UITextField!
    
    @IBOutlet weak var emailIDField: UITextField! // Edit contact
    @IBOutlet weak var contactField: UITextField!
    @IBOutlet weak var stationPickerView: UIPickerView!
    @IBOutlet weak var pickerViewBottomConstrain: NSLayoutConstraint!
    @IBOutlet weak var pickerParentView: UIView!
    
    //MARK: Custom Views...
    var tapGesture : UITapGestureRecognizer!
    var navigation: CustomizedDefaultNavigationBar!
    var titleBarView : InviteFriendNavigationView!
    
    //Helper Model...
    var helperModel = HelperModel()
    var userModel  = UserModel()
    var getLIRRStationModel = LIRRModel()
    
    //MARK: Image Picker Views...
    var imagePickerController = UIImagePickerController()
    var facebookImagePicker = GBHFacebookImagePicker()
    var imageModels  = [AnyObject]()
    var flag = 0
    
    var userDataDictionary = NSDictionary()
    
    // Input data into the Array:
    var pickerData = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getLIRRStationModel.getAllLIRRStationAPI(ID: "") { (succeeded, response) in
            
            if succeeded {
                
                self.pickerData = (response["data"]  as? NSArray)!
                print(self.pickerData)
                self.stationPickerView.reloadAllComponents()
                
            }
        }

       // self.mainScrollView.frame = CGRect(x: 0, y: 50, width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, height:  aspectHeight(height: 790))
        
        /*
         {
         "__v" = 0;
         "_id" = 5996e13cf957d414a54087de;
         articles =     (
         );
         comuteyear = 2015;
         contact = 9891000366;
         destinationstation = bangalore;
         email = "himanshu@tipstat.com";
         firstname = "himanshu ";
         friends =     (
         );
         friendsrequest =     (
         );
         homestation = delhi;
         image = "";
         lastname = Iceparticle;
         lines =     (
         );
         resetpassword = "<null>";
         resetpasswordexpiry = "<null>";
         resetpasswordtoken = "<null>";
         statusCode = 200;
         username = Iceparticle;
         }
         
         
         */
        
        if  let fName = userDataDictionary["firstname"] as? String {
            
            self.firstName.text = fName
            
        }
        if  let lName = userDataDictionary["lastname"] as? String {
            
            self.lastName.text = lName
            
        }
        if  let destinationstation = userDataDictionary["destinationstation"] as? String {
            
            self.detstinationStation.text = destinationstation
            
        }
        
        
        if  let userName = userDataDictionary["username"] as? String {
            
            self.userName.text = userName
            
        }
        
        if let comuteYear = userDataDictionary["comuteyear"] as? Int {
            
            self.comuteYear.text = "\(comuteYear)"
        }
        
        if let homeStation = userDataDictionary["homestation"] as? String {
            
            self.homeStation.text = homeStation
        }
        
        if let email = userDataDictionary["email"] as? String {
            
            self.emailIDField.text = email
        }
        if let contact = userDataDictionary["contact"] as? String {
            
            self.contactField.text = contact
        }
        if let image = userDataDictionary["image"] as? String {
            
            let imageURL = NSURL(string: image)
            let imageRequest = NSURLRequest(url: imageURL! as URL)
            NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                response, data, error in
                if error != nil {
                    //      println("Image not found!")
                }else {
                    // println("success")
                    let image = UIImage(data: data!)
                    self.userProfileImageView.image = image
                    //self.spImageView.layer.cornerRadius = 45
                }
            })
            
        }

        
        
       self.mainScrollView.keyboardDismissMode = .onDrag
        
        setUPUIConstrain()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Notification center for keyboard notifications...
        let keyboardNotificationCenter = NotificationCenter.default
        keyboardNotificationCenter.addObserver(self, selector: #selector(EditProfileViewController.handleKeyBoardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        keyboardNotificationCenter.addObserver(self, selector: #selector(EditProfileViewController.handleKeyBoardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
            }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Removing Observer...
        NotificationCenter.default.removeObserver(self)
    }
    
    
//    override func viewDidLayoutSubviews() {
//
//        super.viewDidLayoutSubviews()
//        
//        self.mainScrollView.contentSize = CGSize(width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, height: aspectHeight(height: 790))
//        
//        //self.mainScrollView.contentOffset.x = 0
//    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Setting Extra Fonts and UI...
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "Edit Profile", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        navigation!.delegateNavigation = self
        titleBarView = UINib(nibName: "InviteFriendNavigationView", bundle: nil).instantiate(withOwner: InviteFriendNavigationView.self, options: nil)[0] as! InviteFriendNavigationView
        navigation.navigationBarIten.titleView = titleBarView
        titleBarView.titleLabel.text = "Edit Profile"
        titleBarView.rightButtonOutlet.setTitle("Update", for: .normal)
        navigation.navigationBarIten.titleView?.clipsToBounds = true
        titleBarView.delegate  = self
        
        //Setting Tap Gesture...
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditProfileViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)

        self.homeStation.isUserInteractionEnabled = false
        self.detstinationStation.isUserInteractionEnabled = false

        
        self.view.addSubview(navigation!)
        
        gallaryButtonOutlet.layer.borderWidth = 1
        gallaryButtonOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
    }
    
    
    //MARK: Storyboard Action..
    @IBAction func openGallaryAction(_ sender: AnyObject) {  // Phone Gallary
        
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openPhotos()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        imagePickerController.delegate = self
        self.present(alert, animated: true, completion: nil)    }
    
    
    
    
    
    @IBAction func openFacebookImageAction(_ sender: AnyObject) { // Facebook Gallary
        
        facebookImagePicker.presentFacebookAlbumImagePicker(from: self, delegate: self as GBHFacebookImagePickerDelegate)
    }
    
    
    @IBAction func homeStationPicker(_ sender: Any) {
        self.flag = 1
        
        // if self.pickerViewBottomConstrain.constant == -220 {
        
        //UIApplication.shared.delegate?.window!!.addSubview(pickerParentView)
        print(pickerParentView.frame)
        homeStation.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.15, animations: {
            
            self.pickerViewBottomConstrain.constant = 0
            
            
        }, completion: {finished in
            
            
        }
        )
        
        
        
        //  } else {
        
        //            UIView.animate(withDuration: 0.15, animations: {
        //
        //                self.pickerViewBottomConstrain.constant = -220
        //
        //            }, completion: {finished in
        //
        //
        //            }
        //            )
        //        }
   
        
    }
    

    @IBAction func destinationSationPicker(_ sender: Any) {
        
        self.flag = 2
        print(pickerParentView.frame)
        
        detstinationStation.becomeFirstResponder()
        
       // UIApplication.shared.delegate?.window!!.addSubview(pickerParentView)
        //if self.pickerViewBottomConstrain.constant == -220 {
        
        UIView.animate(withDuration: 0.15, animations: {
            
            self.pickerViewBottomConstrain.constant = 0
            
            
        }, completion: {finished in
            
            
        }
        )
        
        
        
        //  } else {
        
        //            UIView.animate(withDuration: 0.15, animations: {
        //
        //                self.pickerViewBottomConstrain.constant = -220
        //
        //            }, completion: {finished in
        //
        //
        //            }
        //            )
        //     }
    }

    
    
    @IBAction func ClosePickerView(_ sender: Any) {
        
        self.pickerViewBottomConstrain.constant = -220
       // pickerParentView.removeFromSuperview()
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
