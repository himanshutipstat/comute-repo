//
//  EditProfileDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 15/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import GBHFacebookImagePicker

extension EditProfileViewController : CustomizedDefaultNavigationBarDelegate,GBHFacebookImagePickerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,inviteFriendNavigationViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    //MARK: Gesture recognizer delegates...
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        //Finding Which view is tapped....
        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
        
        //Recognizing gesture if Tapped on Image view......
        if newView!.isKind(of: UIImageView.self) {
            
            
            return false
        }
        
        return true
    }

    //MARK: Picker Delegates....
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.flag == 1 {
            
            self.homeStation.text = pickerData[row] as? String
            
        }else if self.flag == 2  {
            
            self.detstinationStation.text = pickerData[row] as? String
        }
        //self.chooseTypeOutlet.setTitle(pickerData[row], for: .normal)
        return pickerData[row] as? String
    }

    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.dismiss(animated: true, completion: { 
                print("done")
            })
            
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        //If textfield secure text field entry...
        if textField == firstName || textField == lastName || textField == userName || textField == comuteYear {
            
            //textField.isSecureTextEntry = true
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //Adding Keyboard Secure Text Field Entry....
        
        switch textField
        {
        case firstName:
            lastName.becomeFirstResponder()
        case lastName:
            userName.becomeFirstResponder()
        case userName:
            homeStation.becomeFirstResponder()
        case homeStation:
            detstinationStation.becomeFirstResponder()
        case detstinationStation:
            comuteYear.becomeFirstResponder()
        case comuteYear:
            emailIDField.becomeFirstResponder()
        case emailIDField:
            contactField.becomeFirstResponder()
            break
            
        default:
            textField.resignFirstResponder()
        }
        return true
        
    }
    
    
    // MARK: - GBHFacebookImagePicker Protocol
    func facebookImagePicker(imagePicker: UIViewController, successImageModels: [GBHFacebookImage], errorImageModels: [GBHFacebookImage], errors: [Error?]) {
        
        
        // self.imageModels.append(successImageModels as AnyObject)
        let image = successImageModels.first
        userProfileImageView.contentMode = .scaleAspectFill //3
        userProfileImageView.image = image!.image //4
        print(image?.image ?? "")
        print(successImageModels.first ?? "")
        print(imagePicker)
    }
    
    func facebookImagePicker(imagePicker: UIViewController, didFailWithError error: Error?) {
        print("Cancelled Facebook Album picker with error")
        print(error.debugDescription)
    }
    
    // Optional
    func facebookImagePicker(didCancelled imagePicker: UIViewController) {
        print("Cancelled Facebook Album picker")
    }
    
    // Optional
    func facebookImagePickerDismissed() {
        print("Picker dismissed")
    }

    //MARK: UIIMage Picker Controller Delegates...
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var  chosenImage = UIImage()
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            chosenImage = image
        }else {
            
            chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        }
        userProfileImageView.contentMode = .scaleToFill //3
        userProfileImageView.image = chosenImage //4
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Imagepicker Functins
    
    func openPhotos(){
        
        imagePickerController.allowsEditing = true
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    func openCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController.allowsEditing = false
            imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
            imagePickerController.cameraCaptureMode = .photo
            imagePickerController.modalPresentationStyle = .fullScreen
            present(imagePickerController,animated: true,completion: nil)
        } else {
            noCamera()
        }
        
        
    }
    
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    
    // MARK: Custom InviteFriendsNAvigationView Delegate
    
    func popBack() {
        //self.navigationController?.popViewController(animated: true)
    }
    
    func skipAction() {
        
        self.view.endEditing(true)
        
//        if self.emailIDField.text! != "" && self.contactField.text! != "" && self.firstName.text! != "" && self.lastName.text! != "" && self.userName.text! != ""  && self.homeStation.text! != "" && self.detstinationStation.text! != ""  && self.comuteYear.text! != ""{
//            
//            var message:NSString = ""
//            
//            if(!Singleton.sharedInstance.isPhoneNumberValid(number: self.contactField.text!.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil))) {
//                
//                message = "Please enter valid Phone Number"
//                
//            } else if(!Singleton.sharedInstance.isEmailValid(email: self.emailIDField.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines))) {
//                
//                message = "Please enter valid Email Address"
//                
//            } //else if Singleton.sharedInstance.facebookId.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) == "" && !Singleton.sharedInstance.isValidPassword(self.userDetailsEntryView[2].userDetailTextField.text!) {
//            
//            // message = "Password must be atleast 6 characters long!"
//            // }
//            
//            //Validation Check....
//            if(message.length > 0) {
//                self.dismiss(animated: true, completion: {
//                self.helperModel.showingAlertcontroller(title: "", message: message as String, cancelButton: "OK", receivedResponse: {})
//                    
//                })
//                
//            } else {
//                
                userModel.updateUserDetailAPI(user_Mobile: self.contactField.text!, user_Email: self.emailIDField.text!, first_Name: self.firstName.text!, last_Name: self.lastName.text!, home_Station: self.homeStation.text!, destination_Station: self.detstinationStation.text!, comute_Years: self.comuteYear.text!, profile_Pic: UIImageJPEGRepresentation(self.userProfileImageView.image!, 0.0)!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)), user_Name: self.userName.text!) { (succeeded, response) in
                    
                    if succeeded {
                        
                        self.dismiss(animated: true, completion: {
                            print("done")
                        })
                        
                    }else {
                        self.dismiss(animated: true, completion: {
                            print("done")
                        })
                    }
                }
            }
    
    //MARK: Removing keyboard...And Setting zesture fopr zooming...
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        self.firstName.resignFirstResponder()
        self.lastName.resignFirstResponder()
        self.userName.resignFirstResponder()
        self.homeStation.resignFirstResponder()
        self.detstinationStation.resignFirstResponder()
        self.emailIDField.resignFirstResponder()
        self.comuteYear.resignFirstResponder()
        self.contactField.resignFirstResponder()
    }
    
    //MARK: Keyboard notifications....
    func handleKeyBoardWillHide(sender: NSNotification) {
        
        let userInfo = sender.userInfo
        
        if let info = userInfo {
            
            let animationDurationObject = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
            var animationDuration = 0.0
            animationDurationObject.getValue(&animationDuration)
            
            UIView.animate(withDuration: animationDuration, animations: {
                
                self.mainScrollView.contentInset = UIEdgeInsets.zero
            })
        }
    }
    
    
    func handleKeyBoardWillShow(notification: NSNotification) {
        
        let userInfo = notification.userInfo
        
        if let info = userInfo {
            
            let animationDurationObject = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
            let keyboardEndRectObject = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            var animationDuration = 0.0
            var keyboardEndRect = CGRect.zero
            animationDurationObject.getValue(&animationDuration)
            keyboardEndRectObject.getValue(&keyboardEndRect)
            
            let intersectionOfKeyboadrRectAndWindowRect = self.view.frame.intersection(keyboardEndRect)
            
            UIView.animate(withDuration: animationDuration, animations: {[weak self] in
                
                //Frames of text view to be visible...
                self!.mainScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: intersectionOfKeyboadrRectAndWindowRect.size.height, right: 0)
                self!.mainScrollView.scrollRectToVisible((self?.firstName.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.lastName.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.userName.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.homeStation.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.detstinationStation.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.emailIDField.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.comuteYear.frame)!, animated: true)
                self!.mainScrollView.scrollRectToVisible((self?.contactField.frame)!, animated: true)
            })
        }
    }
    
//        }else {
//            self.dismiss(animated: true, completion: {
//                self.helperModel.showingAlertcontroller(title: "", message: "Please make sure you fill all the field." as String, cancelButton: "OK", receivedResponse: {
//                
//                })
//            })
//            
//        }
 //   }
}
