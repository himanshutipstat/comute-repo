//
//  NotificationViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 20/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var notificationListTableView: UITableView!
    
     var navigation: CustomizedDefaultNavigationBar!
    
    //HelperModel
    var helperModel = HelperModel()
    var notificationModel = NotificationModel()
    var notificationDictionar = NSDictionary()
    var reguestSendNotifiArray = NSArray()
    var articleLikesArray = NSArray()
    var requestAcceptArray  = NSArray()
    var refresher: UIRefreshControl!
   

    override func viewDidLoad() {
        super.viewDidLoad()

        setUPUIConstrain()
        
        //Regestring table view classes...
        
        
    // For Friend Invitation
    self.notificationListTableView.register(InvitationNotificationTableViewCell.classForCoder(), forCellReuseIdentifier: "InvitationCell")
        
    self.notificationListTableView.register(UINib(nibName: "InvitationNotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "InvitationCell")
        
        //For Request approve
    self.notificationListTableView.register(RequestApprovedNotificationTableViewCell.classForCoder(), forCellReuseIdentifier: "ApprovedCell")
        
    self.notificationListTableView.register(UINib(nibName: "RequestApprovedNotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "ApprovedCell")
        
        
        // For Comute Train staus
        self.notificationListTableView.register(CoMuteNotificationTableViewCell.classForCoder(), forCellReuseIdentifier: "TrainCell")
        
    self.notificationListTableView.register(UINib(nibName: "CoMuteNotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "TrainCell")
        
        // For comute Post status
    self.notificationListTableView.register(CoMutePostNotificationTableViewCell.classForCoder(), forCellReuseIdentifier: "PostCell")
        
    self.notificationListTableView.register(UINib(nibName: "CoMutePostNotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "PostCell")
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.notificationModel.getAllNotificationAPI(ID: "1") { (succeeded, response) in
            
            if succeeded {
                
            self.notificationDictionar  = response["data"] as! NSDictionary
                
             
                
              self.reguestSendNotifiArray = self.notificationDictionar["friends"] as! NSArray
            
                self.articleLikesArray = self.notificationDictionar["likes"] as! NSArray
                self.requestAcceptArray = self.notificationDictionar["friendRequestAccept"] as! NSArray
                
                
             
                    
                    self.notificationListTableView.reloadData()

                
                
                
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "Notifications", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        
        refresher = UIRefreshControl()
        //refresher.attributedTitle = NSAttributedString(string: "pull to refresh")
        
        refresher.addTarget(self, action: #selector(NotificationViewController.refresh), for: UIControlEvents.valueChanged)
        self.notificationListTableView.addSubview(refresher)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
