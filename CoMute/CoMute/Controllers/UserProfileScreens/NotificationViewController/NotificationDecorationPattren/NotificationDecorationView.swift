//
//  NotificationDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 20/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension NotificationViewController : CustomizedDefaultNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate,invitationNotificationTableViewCellDelegate{
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.navigationController?.popViewController(animated: true)
            
        }
        
        
    }
    
    
    
    // MARK: TableView DataSource and Delegate....
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.notificationDictionar.count
        
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        
//        
//        
//        return (self.notificationDictionar.allKeys[section] as? String)?.capitalized
//        
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(section)
        
        //        if section == 1 {
        //
        //        return self.reguestNotifiArray.count
        //
        //        }
        //        if section == 0 {
        //
        //            return self.articleLikesArray.count
        //        }
        return (self.notificationDictionar.allValues[section] as AnyObject).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell") as UITableViewCell
        
        //if indexPath.row == 0 {
        
        //        if indexPath.section == 1 {
        //            self.reguestNotifiArray.count
        //
        //        }else {
        //            self.articleLikesArray.count
        //
        //        }
      
        if indexPath.section == 0 {
            let cell = notificationListTableView.dequeueReusableCell(withIdentifier: "InvitationCell") as! InvitationNotificationTableViewCell
            cell.delegate = self
            
            if let dataDic = self.reguestSendNotifiArray[indexPath.row] as? NSDictionary {
                
                
                cell.DataDictionary = dataDic
                
            }
            return cell
        }else if indexPath.section == 1 {
            
            /*
 
             articletype = DARWINS;
             dislikes = 0;
             image = "https://s3.ap-south-1.amazonaws.com/comut/cea9652b-40ab-484b-b054-281e93930741";
             likenotification = 1;
             likes = 1;
             text = "Enter Descriptionyreeryer yet wetter\nYer\nYer\nYer\nYer\nYer\nY";
             updateat = "2017-09-04T09:37:42.802Z";
 
 */
            let  cell = notificationListTableView.dequeueReusableCell(withIdentifier: "PostCell") as! CoMutePostNotificationTableViewCell
            print(cell)
            cell.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
            
            if let dataDic = self.articleLikesArray[indexPath.row] as? NSDictionary{
                
                cell.noOfPeopleLikedPostLabel.text = String(describing: dataDic["likes"] as! NSNumber)+" People Liked your post"
                
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
                
                if let dateString = dataDic["updateat"] as? String {
                    
                    let dateArr = dateString.components(separatedBy: ".")
                    print(dateArr.first!)
                    
                    if let selectedDate = formatter.date(from: dateArr.first!) {
                        formatter.dateFormat = "h:mm a MMM dd, yyyy"
                        let date = formatter.string(from: selectedDate)
                        cell.dateTimeLabel.text = date
                        
                    }
                }

                
                
            }
            //cell.selectionStyle = .lightGray
            
            return cell
            
        }else if indexPath.section == 2 {
            
            let cell = notificationListTableView.dequeueReusableCell(withIdentifier: "ApprovedCell") as! RequestApprovedNotificationTableViewCell
            //cell.delegate = self
            
            if let dataDic = self.requestAcceptArray[indexPath.row] as? NSDictionary {
               // #selector(NotificationViewController.messageVC(sender : ))
                
                cell.DataDictionary = dataDic
                cell.messageButtonOutlet.addTarget(self, action: #selector(NotificationViewController.messageVC(sender:)), for: .touchUpInside)
                cell.messageButtonOutlet.tag = indexPath.row
                
            }
            return cell
            
            
        }
        //        cell.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        //
        //        if indexPath.section == 1 {
        //
        //            if let dataDic = self.reguestNotifiArray[indexPath.section] as? NSDictionary {
        //
        //
        //                cell.DataDictionary = dataDic
        //
        //            }
        //
        //
        //        }else {
        //
        //        }
        //
        
        
        
        return cell
        //        }
        //
        //        if indexPath.row == 1 {
        //             cell = notificationListTableView.dequeueReusableCell(withIdentifier: "ApprovedCell") as! RequestApprovedNotificationTableViewCell
        //            print(cell)
        //            cell.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        //            //cell.selectionStyle = .lightGray
        //
        //            return cell
        //        }
        //
        //        if indexPath.row == 2 {
        //             cell = notificationListTableView.dequeueReusableCell(withIdentifier: "TrainCell") as! CoMuteNotificationTableViewCell
        //            print(cell)
        //            cell.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        //            //cell.selectionStyle = .lightGray
        //
        //            return cell
        //        }
        //
        //        if indexPath.row == 3 {
        //             cell = notificationListTableView.dequeueReusableCell(withIdentifier: "PostCell") as! CoMutePostNotificationTableViewCell
        //            print(cell)
        //            cell.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        //            //cell.selectionStyle = .lightGray
        //
        //            return cell
        //        }
        //        return cell
        //    }
        //
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ComuteProfileViewContoller") as! ComuteProfileViewContoller
            userProfileVC.isCurrentUser = false
            if let dataDic = self.reguestSendNotifiArray[indexPath.row] as? NSDictionary {
                
                if let id = dataDic["_id"] as? String {
                    
                    userProfileVC.userID = id
                    
                }
                
                if let fName = dataDic["firstname"] as? String {
                    
                    if let lName = dataDic["lastname"] as? String {
                        
                        userProfileVC.userFullName  = fName+" "+lName
                    }
                }
                userProfileVC.isUserFriend = false
                userProfileVC.notificationFlag = 1
              //  userProfileVC.editProfileButtonOutlet.setTitle("Pending Reguest", for: .normal)
               // userProfileVC.editProfileButtonOutlet.isUserInteractionEnabled = false
                
                //            if let friendCheck = dataDic["isFriend"] as? NSNumber {
                //
                //
                //                if friendCheck == 1 {
                //                    userProfileVC.isUserFriend = true
                //
                //                }else {
                //
                //                    userProfileVC.isUserFriend = false
                //
                //                }
                //            }
                //
                //            if let requestCheck = dataDic["isrequest"] as? NSNumber {
                //
                //                if requestCheck == 1 {
                //
                //                    userProfileVC.isFriendRequested = true
                //                }else {
                //                    
                //                    userProfileVC.isFriendRequested = false
                //                }
                //                
                //            }
                
                self.navigationController?.pushViewController(userProfileVC, animated: true)
            }
            
        }else if indexPath.section == 1{
            
            let postDetailedVC = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailedViewController") as! PostDetailedViewController
            
            if let userArticleData = self.articleLikesArray[indexPath.row] as? NSDictionary{
                
                //let userArticleDict = userArticleData["articles"] as! NSDictionary
                //let userImage = userArticleData["image"] as! String
                postDetailedVC.articleDict = userArticleData
                postDetailedVC.postImage = Singleton.sharedInstance.userImage
                postDetailedVC.userName = Singleton.sharedInstance.userName
                postDetailedVC.fullName = Singleton.sharedInstance.userFullName
                postDetailedVC.userId = Singleton.sharedInstance.userID
            }
            self.present(postDetailedVC, animated: true) {
                print("done done")
            }
            
        }else if indexPath.section == 2 {
            
            let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ComuteProfileViewContoller") as! ComuteProfileViewContoller
            userProfileVC.isCurrentUser = false
            if let dataDic = self.requestAcceptArray[indexPath.row] as? NSDictionary {
                
                if let id = dataDic["_id"] as? String {
                    
                    userProfileVC.userID = id
                    
                }
                
                if let fName = dataDic["firstname"] as? String {
                    
                    if let lName = dataDic["lastname"] as? String {
                        
                        userProfileVC.userFullName  = fName+" "+lName
                    }
                }
                userProfileVC.isUserFriend = true
                userProfileVC.notificationFlag = 0
                //  userProfileVC.editProfileButtonOutlet.setTitle("Pending Reguest", for: .normal)
                // userProfileVC.editProfileButtonOutlet.isUserInteractionEnabled = false
                
                //            if let friendCheck = dataDic["isFriend"] as? NSNumber {
                //
                //
                //                if friendCheck == 1 {
                //                    userProfileVC.isUserFriend = true
                //
                //                }else {
                //
                //                    userProfileVC.isUserFriend = false
                //
                //                }
                //            }
                //
                //            if let requestCheck = dataDic["isrequest"] as? NSNumber {
                //
                //                if requestCheck == 1 {
                //
                //                    userProfileVC.isFriendRequested = true
                //                }else {
                //
                //                    userProfileVC.isFriendRequested = false
                //                }
                //
                //            }
                
                self.navigationController?.pushViewController(userProfileVC, animated: true)
            
        }
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
                if indexPath.section == 0 {
        
                    return 67
                }
                else  {
        
                    return 50
                }
        
       // return 67
    }
    
    func messageVC(sender : UIButton) {
        
        //if let cell = sender.superview?.superview as? RequestApprovedNotificationTableViewCell {
            let indexPath = sender.tag
            
            if let dataDic = self.requestAcceptArray[(indexPath)] as? NSDictionary {
                
                let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                //chatVC.longitude = self.longitude
                //chatVC.latitude = self.latitude
                
                
                    
                    
                    
                    
                    let fName = dataDic["firstname"] as! String
                    let lName = dataDic["lastname"] as! String
                    chatVC.recieverUserFullName = fName+" "+lName
                    chatVC.recieverImage = (dataDic["image"] as? String)!
                    chatVC.recieverUserName = dataDic["username"] as! String
                    chatVC.userId = dataDic["_id"] as! String
                    
                
                self.navigationController?.pushViewController(chatVC, animated: true)
                
            //}

            
        }
        
    }
    
    func refresh(){
        
        print("refreshed")
        self.refresher.endRefreshing()
        
        self.notificationModel.getAllNotificationAPI(ID: "1") { (succeeded, response) in
            
            if succeeded {
                
                self.notificationDictionar  = response["data"] as! NSDictionary
                
                self.reguestSendNotifiArray = self.notificationDictionar["friends"] as! NSArray
                
                self.articleLikesArray = self.notificationDictionar["likes"] as! NSArray
                
                self.requestAcceptArray = self.notificationDictionar["friendRequestAccept"] as! NSArray
                
                self.notificationListTableView.reloadData()
            }
        }
    }
    
}
