//
//  ChangeSettingDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 12/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension ChangeSettingViewController : CustomizedDefaultNavigationBarDelegate,changePasswordViewDelegate{
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.navigationController?.popViewController(animated: true)
            
        }
        
        
    }
    
    //MARK: ChangePAsswordViewDElegate...
    
    func cancelViewButton() {
        self.chnagePasswordView.removeFromSuperview()
    }
    func SaveChangesButton() {
        
        self.view.endEditing(true)
        
        var message:NSString = ""
        
        if self.chnagePasswordView.oldPasswordField.text! != "" && self.chnagePasswordView.newPasswordField.text! != "" &&
            self.chnagePasswordView.confirmPasswordField.text! != "" {
            
            if !Singleton.sharedInstance.isValidPassword(password: self.chnagePasswordView.newPasswordField.text!) {
                
                message = "Password must be atleast 8 characters long!"
                
            }else if self.chnagePasswordView.oldPasswordField.text == self.chnagePasswordView.newPasswordField.text! {
                
                 message = "New password shouldn't be the same as Current password."
            }else if self.chnagePasswordView.newPasswordField.text != self.chnagePasswordView.confirmPasswordField.text! {
                
                message = "New password and confirm password are not match"
            }
            
            //Validation Check....
            if(message.length > 0) {
                
                self.helperModel.showingAlertcontroller(title: "", message: message as String, cancelButton: "OK", receivedResponse: {})
                
            } else {
                
                self.changePassword.changePasswordAPI(oldPassword: self.chnagePasswordView.oldPasswordField.text!, newpassword: self.chnagePasswordView.newPasswordField.text!, recievedResponse: { (succeeded, response) in
                    
                    if succeeded {
                        
                         self.chnagePasswordView.removeFromSuperview()
                    }
                })
                
           
        }
        
         
    }
    }
}
