//
//  ChangeSettingViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 12/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class ChangeSettingViewController: UIViewController {
    
    @IBOutlet weak var publicOutlet: UIButton!
    @IBOutlet weak var privateOutlet: UIButton!
    @IBOutlet weak var incognitoOutlet: UIButton!
    @IBOutlet weak var notificationOutlet: UIButton!
    

    var navigation: CustomizedDefaultNavigationBar!
    var isNotification = true
    
    //Helper Model...
    var helperModel = HelperModel()
    var commoAPIModel = CommonAPIsModel()
    var userVisibility = UserModel()
    var changePassword = PasswordModel()
    var chnagePasswordView = ChangePasswordView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUPUIConstrain()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "Setting", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        publicOutlet.layer.borderWidth = 1
        publicOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        privateOutlet.layer.borderWidth = 1
        privateOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        incognitoOutlet.layer.borderWidth = 1
        incognitoOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        notificationOutlet.layer.borderWidth = 1
        notificationOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
    }
    
    //MARK: StoryBoard Action..
    
    @IBAction func buttonAction(sender: UIButton) {
        switch sender.tag {
    
        case 101 : //Public
            
            publicOutlet.backgroundColor = COLOR_CODE.YELLOW_COLOR
    
            privateOutlet.backgroundColor = COLOR_CODE.WHITE_COLOR
            
    
            incognitoOutlet.backgroundColor = COLOR_CODE.WHITE_COLOR
            
            userVisibility.userVisibilityAPI(visibility: "0", recievedResponse: { (suceeded, response) in
                if suceeded {
                     print(response)
                }else {
                    
                }
            })
            
        case 102 : //Private
            
            publicOutlet.backgroundColor = COLOR_CODE.WHITE_COLOR
            
            privateOutlet.backgroundColor = COLOR_CODE.YELLOW_COLOR
            
            incognitoOutlet.backgroundColor = COLOR_CODE.WHITE_COLOR
            
            userVisibility.userVisibilityAPI(visibility: "1", recievedResponse: { (suceeded, response) in
                if suceeded {
                    
                    print(response)
                }else {
                    
                }
            })
            
        case 103 : //Incognito
            
            publicOutlet.backgroundColor = COLOR_CODE.WHITE_COLOR
            
            privateOutlet.backgroundColor = COLOR_CODE.WHITE_COLOR
            
            
            incognitoOutlet.backgroundColor = COLOR_CODE.YELLOW_COLOR
            
            userVisibility.userVisibilityAPI(visibility: "2", recievedResponse: { (suceeded, response) in
                if suceeded {
                     print(response)
                }else {
                    
                }
            })
            
        case 201 : //Notification
            
            if isNotification {
            notificationOutlet.backgroundColor = COLOR_CODE.YELLOW_COLOR
                isNotification = false
            }else {
                
                 notificationOutlet.backgroundColor = COLOR_CODE.WHITE_COLOR
                isNotification = true
            }
            
        default:
            break
        }
        
        
    }
    @IBAction func changePasswordAction(_ sender: AnyObject) {
        //Initializing  View
        self.chnagePasswordView = UINib(nibName: "ChangePasswordView", bundle: nil).instantiate(withOwner: ChangePasswordView.self, options: nil)[0] as! ChangePasswordView
        
        self.chnagePasswordView.delegate = self
        chnagePasswordView.frame = CGRect(x: 0  , y: 0, width: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width), height: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height) )
        
        self.view.addSubview(chnagePasswordView)
    }
    
    
    @IBAction func logoutAction(_ sender: Any) {
        
         self.commoAPIModel.deletingUserAsLogoutOptByUser()
        
        //Pop to root view controller.....
        if let controllers = self.navigationController?.viewControllers {
            
            for controller in controllers {
                
                if controller.isKind(of: LoginViewController.self) {
                    
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        
    }
    
   
    

}
