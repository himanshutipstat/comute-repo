//
//  ContactUsViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 14/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: UIViewController {
    
    @IBOutlet weak var messageTextView: UITextView!
    
    
    var contactus = UserModel()
    
      var navigation: CustomizedDefaultNavigationBar!
    var tapGesture : UITapGestureRecognizer!
    var helperModel = HelperModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setUPUIConstrain()
        self.messageTextView.delegate  = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!,UIImage(named: "message")!], buttonHighlightedImage: [UIImage(named: "btn_back")!,UIImage(named: "message")!], numberOfButtons: 3, barTitle: "Contact Us", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        //Setting Tap Gesture...
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserLineViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
    }
    
    //MARK: Removing keyboard...And Setting zesture fopr zooming...
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
