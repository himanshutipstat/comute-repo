//
//  ContactUsDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 14/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

extension ContactUsViewController : CustomizedDefaultNavigationBarDelegate,MFMailComposeViewControllerDelegate,UITextViewDelegate,UIGestureRecognizerDelegate{
    
    
    
    //MARK: Gesture recognizer delegates...
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        //Finding Which view is tapped....
        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
        
        //Recognizing gesture if Tapped on Image view......
        if newView!.isKind(of: UIImageView.self) {
            
            
            return false
        }
        
        return true
    }
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.navigationController?.popViewController(animated: true)
            
        }else if sender.tag == 20 {
            
            let messsagestring =  messageTextView.text as NSString
            let string = messsagestring.replacingOccurrences(of: "\n", with: "")
            _ = messsagestring.length
            let string1 = messsagestring.replacingOccurrences(of: " ", with: "")
            if messageTextView.text != "" &&   messageTextView.text != "Please enter a message for our administrators." && string != "" && string1 != ""{
            
            self.contactus.contactUS(message: self.messageTextView.text, recievedResponse: { (succeeded, response) in
                
                if succeeded {
                    
                   self.helperModel.showingAlertcontroller(title: "", message: "Your message has been sent to the administrators" as String, cancelButton: "OK", receivedResponse: {
                    
                    self.navigationController?.popViewController(animated: true)
                   
                   })
                }
            })
            
//            let mailComposeViewController = configuredMailComposeViewController()
//            if MFMailComposeViewController.canSendMail() {
//                self.present(mailComposeViewController, animated: true, completion: nil)
//            } else {
//                self.showSendMailErrorAlert()
//            }
           // sendEmail()
        }
            
            
        }
        
        
    }
    
    //MARK: textView Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        print("textViewDidBeginEditing")
        if messageTextView.text == "Please enter a message for our administrators."
        {
            messageTextView.text = ""
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        //        setInitialViewFrame()
        self.view.endEditing(true)
        
        if self.messageTextView.text == "" {
            
            self.messageTextView.text = "Please enter a message for our administrators."
            
        }
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["ekansh@tipstat.com"])
        mailComposerVC.setSubject("Contact through CoMute")
        mailComposerVC.setMessageBody(self.messageTextView.text, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    internal func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
   
    
    
//    func sendEmail() {
//        
//        
//        let composeVC = MFMailComposeViewController()
//        composeVC.mailComposeDelegate = self
//        // Configure the fields of the interface.
//        composeVC.setToRecipients(["himanshu@tipstat.com"])
//        composeVC.setSubject("Contact through CoMute")
//        composeVC.setMessageBody(self.messageTextView.text, isHTML: false)
//        // Present the view controller modally.
//        self.navigationController?.pushViewController(composeVC, animated: true)
//        
//    }

    
    
}
