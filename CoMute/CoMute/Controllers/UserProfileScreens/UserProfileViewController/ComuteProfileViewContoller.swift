//
//  ComuteProfileViewContoller.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 08/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class ComuteProfileViewContoller: UIViewController {

  
    @IBOutlet weak var userDetailView: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var StoriesView: UIView!
    @IBOutlet weak var userStoriesTableView: UITableView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var comuteYears: UILabel!
    @IBOutlet weak var homestation: UILabel!
    @IBOutlet weak var editProfileButtonOutlet: UIButton!
    @IBOutlet weak var storiesButtonOutLet: UIButton!
    @IBOutlet weak var storiesLabel: UILabel!
    @IBOutlet weak var awardsLabel: UILabel!
    @IBOutlet weak var awardButtonOutLet: UIButton!
    @IBOutlet weak var storiesSepratorView: UIView!
    @IBOutlet weak var awardsSepratoreView: UIView!
    
    //MARK: Custom Views...
    var navigation: CustomizedDefaultNavigationBar!
     var slideMenuView : SlideMenuView!
    
    var addMessageView = AddFriendView()
    //var awardsView : UserAwardsView!
    var userModel = UserModel()
    var sendFriendRquest = ComuteFriends()
    var isCurrentUser = true
    var userFullName = String()
    //var userName = String()
    var userIDName = ""
    var userImageString = ""
    var isUserFriend = false
    var isFriendRequested = false
    var isRequestFrom = false
    var userID = String()
    var notificationFlag = 0
    
    var UserDataDictionary = NSDictionary()
    var userArticleArray = NSArray()
    var userAwardsArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         settingExtraFontsAndUI()
        
//        let c = UIViewController()
//        present(c, animated: false) { _ in }
//        dismiss(animated: false) { _ in }

       
        self.mainScrollView.delegate = self
        //Regestring table view class...
        self.userStoriesTableView.register(AllPostCell.classForCoder(), forCellReuseIdentifier: "Cell")
        self.userStoriesTableView.register(UINib(nibName: "AllPostCell", bundle: nil), forCellReuseIdentifier: "Cell")
        // Do any additional setup after loading the view.
        
        userDetailView.layer.borderWidth = 1
        userDetailView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
         self.mainScrollView.contentOffset.x = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.mainScrollView.contentOffset.x = 0
        
        self.slideMenuView = nil
       // self.slideMenuView.removeFromSuperview()
        
        // Get user Details
        
        if isCurrentUser{
            
        userModel.getUserDetailAPI(token: Singleton.sharedInstance.userAccessToken) { (succeeded, response) in
            
            if succeeded {
                
               
               // self.notificationFlag = 0
                self.UserDataDictionary = response
                self.userStoriesTableView.reloadData()
                
                
                  if  let userName = response["username"] as? String {
                    
                    self.userName.text = userName
                    Singleton.sharedInstance.userName = userName
    
                    
                    
                }
                
                if let comuteYear = response["comuteyear"] as? Int {
                    
                    self.comuteYears.text = "\(comuteYear)"
                    
                }
                
                if let homeStation = response["homestation"] as? String {
                    
                    self.homestation.text = homeStation
                    Singleton.sharedInstance.userHomeStation = homeStation
                    
                }
                
                if let destination = response["destinationstation"] as? String{
                    
                    Singleton.sharedInstance.userDestinationStation = destination
                }
                
                 self.userFullName = String(describing: response["firstname"]!)+" "+String(describing: response["lastname"]!)
                
                self.settingExtraFontsAndUI()

                
                if let image = response["image"] as? String {
                    self.userImageString = image
                    Singleton.sharedInstance.userImage = image
                    
                   // self.userProfileImage.addSubview(ActivityIndicator.sharedInstance)
                     // ActivityIndicator.sharedInstance.showActivityIndicator()
                    let imageURL = NSURL(string: image)
                    let imageRequest = NSURLRequest(url: imageURL! as URL)
                    NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                        response, data, error in
                        if error != nil {
                            //      println("Image not found!")
                        }else {
                            // println("success")
                            let image = UIImage(data: data!)
                          // ActivityIndicator.sharedInstance.hideActivityIndicator()
                            self.userProfileImage.image = image
                            //self.spImageView.layer.cornerRadius = 45
                        }
                    })
                    
                }
 
                
                if  let articleArr = response["articles"] as? NSArray {
                    
                self.userArticleArray = articleArr
                    
                self.userStoriesTableView.reloadData()
                    
                    
                }
                
                if let awardsArr = response["awards"] as? NSArray {
                    
                    self.userAwardsArray = awardsArr
                     self.addingAwardsView()
                    self.userStoriesTableView.reloadData()
                }

                
            }else {
                
            }
        }
        }else {
            self.userModel.getfriendsDetailAPI(id: userID, recievedResponse: { (succeeded, response) in
                
                if succeeded {
                    
                    self.UserDataDictionary = response
                    
                    
                    if  let userName = response["username"] as? String {
                        
                        self.userName.text = userName
                        self.userIDName = userName
                        //Singleton.sharedInstance.userName = userName
                        
                        
                    }
                    
                    if let comuteYear = response["comuteyear"] as? Int {
                        
                        self.comuteYears.text = "\(comuteYear)"
                        
                    }
                    
                    if let homeStation = response["homestation"] as? String {
                        
                        self.homestation.text = homeStation
                    }
                    
                    
                    if let requestFrom = response["isRequestFrom"] as? NSNumber{
                        
                        if requestFrom == 1 {
                            
                             self.editProfileButtonOutlet.setTitle("Accept", for: .normal)
                            self.isRequestFrom = true
                            
                        }else {
                             self.isRequestFrom = false
                            
                            if let request = response["friendRequest"] as? NSNumber{
                                
                                if request == 0 {
                                    
                                    self.isFriendRequested = false
                                    self.editProfileButtonOutlet.setTitle("Pending Request", for: .normal)
                                    
                                }else {
                                    
                                    self.isFriendRequested = true
                                    self.editProfileButtonOutlet.setTitle("Add Friend", for: .normal)
                                }
                                
                                
                            }
                            
                            if let isFriend = response["isFriend"] as? NSNumber {
                                
                                if isFriend == 0 {
                                    
                                    self.isUserFriend = false
                                    self.editProfileButtonOutlet.setTitle("Add Friend", for: .normal)
                                }else {
                                    self.isUserFriend = true
                                    
                                    self.editProfileButtonOutlet.setTitle("Message", for: .normal)
                                    
                                }
                                
                                
                            }

                            
                        }
                    }
                    
                    self.userFullName = String(describing: response["firstname"]!)+" "+String(describing: response["lastname"]!)
                    
                    self.settingExtraFontsAndUI()

                    
                    if let image = response["image"] as? String {
                        
                        self.userImageString = image
                        
                       // self.userProfileImage.addSubview(ActivityIndicator.sharedInstance)
                       // ActivityIndicator.sharedInstance.showActivityIndicator()
                        let imageURL = NSURL(string: image)
                        let imageRequest = NSURLRequest(url: imageURL! as URL)
                        NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                            response, data, error in
                            if error != nil {
                                //      println("Image not found!")
                            }else {
                                // println("success")
                                let image = UIImage(data: data!)
                             //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                                self.userProfileImage.image = image
                                //self.spImageView.layer.cornerRadius = 45
                            }
                        })
                        
                    }
                    
                    
                    if  let articleArr = response["articles"] as? NSArray {
                        
                        self.userArticleArray = articleArr
                        
                        self.userStoriesTableView.reloadData()
                        
                        
                    }
                    
                    if let awardsArr = response["awards"] as? NSArray {
                        
                        self.userAwardsArray = awardsArr
                        self.addingAwardsView()
                        
                    }
                    
                    
                }else {
                    
                }
            })
            
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    override func viewDidDisappear(_ animated: Bool) {
        if self.slideMenuView != nil {
        self.slideMenuView.removeFromSuperview()
            self.slideMenuView = nil
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        mainScrollView.contentSize = CGSize(width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * 2, height: aspectHeight(height: 299) )
        
        self.mainScrollView.contentOffset.x = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Removing Observer...
        NotificationCenter.default.removeObserver(self)
        
    }
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        //Adding Navigation Bar...
        if isCurrentUser{
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "menu")!,UIImage(named: "Notification")!], buttonHighlightedImage: [UIImage(named: "menu")!,UIImage(named: "Notification")!], numberOfButtons: 3, barTitle: userFullName as NSString, alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
            self.editProfileButtonOutlet.setTitle("Edit Profile", for: .normal)
        }else {
            
            if isUserFriend {
                
                 self.editProfileButtonOutlet.setTitle("Message", for: .normal)
                
            }else {
                
                if isRequestFrom{
                    self.editProfileButtonOutlet.setTitle("Accept", for: .normal)
                    
                }else {
                
                if isFriendRequested{
                    
                    self.editProfileButtonOutlet.setTitle("Pending Request", for: .normal)
                }else {
                    
                    self.editProfileButtonOutlet.setTitle("Add Friend", for: .normal)
                }
                }
                
                
            }
            navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: self.userFullName as NSString, alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
            
            navigation!.delegateNavigation = self
            
            self.view.addSubview(navigation!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Strory board Action...
    @IBAction func editProfileAction(_ sender: Any) {
        
        if self.notificationFlag == 0{
         if isCurrentUser{
        
        let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        
        editProfileVC.userDataDictionary  =  UserDataDictionary
        self.present(editProfileVC, animated: true) {
            
            
            print("done done")
        }
         }else {
            
            if isRequestFrom {
                
                self.sendFriendRquest.acceptComuteFriendsAPI(ID: self.userID) { (succeeded, response) in
                    
                    
                    if succeeded {
                        
                        
                        self.isUserFriend = true
                        self.isRequestFrom = false
                        
                        self.editProfileButtonOutlet.setTitle("Message", for: .normal)
                        
                    }
                }
                
            }else {
            
            if isUserFriend {
                
                let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatVC.userId = self.userID
                chatVC.recieverUserName = self.userIDName
                chatVC.recieverUserFullName = self.userFullName
                chatVC.recieverImage = self.userImageString
                self.navigationController?.pushViewController(chatVC, animated: true)
                
            }else {
                
                if isFriendRequested{
                    
                    
                    
                    
                }else {
                    
                    self.addMessageView = UINib(nibName: "AddFriendView", bundle: nil).instantiate(withOwner: AddFriendView.self, options: nil)[0] as! AddFriendView
                    
                    self.addMessageView.deligate = self
                    addMessageView.frame = CGRect(x: 0  , y: 0, width: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width), height: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height) )
                    
                    self.view.addSubview(addMessageView)
                    
                    

                }
                
                
            }
            }
        }
    }
    }
    @IBAction func StoriesButtonAction(_ sender: Any) {
        
        self.mainScrollView.contentOffset.x = 0
        
        storiesSepratorView.backgroundColor = UIColor.black
        storiesLabel.textColor = UIColor.black

        // storiesButtonOutLet.titleLabel?.textColor = COLOR_CODE.BLACK_COLOR
        awardsLabel.textColor = UIColor.lightGray
        // awardButtonOutLet.titleLabel?.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
        awardsSepratoreView.backgroundColor = UIColor.lightGray
        
    }
    @IBAction func awardsButtonAction(_ sender: Any) {
        
       self.mainScrollView.contentOffset.x = GLOBAL_KEY.CURRENT_DEVICE_SIZE.width
        
        storiesSepratorView.backgroundColor = UIColor.lightGray
        storiesLabel.textColor = UIColor.lightGray
        
        // storiesButtonOutLet.titleLabel?.textColor = COLOR_CODE.BLACK_COLOR
        awardsLabel.textColor = UIColor.black
        // awardButtonOutLet.titleLabel?.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
        awardsSepratoreView.backgroundColor = UIColor.black
       
        
    }
    
    
    
    
    // ScrollView setting
    
    func addingAwardsView(){
        
        
        mainScrollView.contentSize = CGSize(width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * 2, height: aspectHeight(height: 299))
        
        
        
        let second = self.storyboard?.instantiateViewController(withIdentifier: "AwardsCollectionViewController") as! AwardsCollectionViewController
                
        second.collectionView?.frame = CGRect(x: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, y: 0, width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, height: self.mainScrollView.frame.height )
        second.fillCollectionView(with: self.userAwardsArray)
        
         self.mainScrollView.addSubview(second.collectionView!)
        
        print(second.collectionView?.frame as Any)
        
        second.willMove(toParentViewController: self)
        self.addChildViewController(second)
        
        print(self.childViewControllers)

        
//        //Initializing  View
//        self.awardsView = UINib(nibName: "UserAwardsView", bundle: nil).instantiate(withOwner: UserAwardsView.self, options: nil)[0] as! UserAwardsView
//        //self.awardsView = UINib(nibName: "UserAwardsView", bundle: nil).instantiate(withOwner: UserAwardsView.self, options: nil)[0] as! UserAwardsView
//        self.awardsView.frame = CGRect(x: Int(userStoriesTableView.bounds.width) , y: 0, width: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width), height: Int(aspectHeight(height: 384)) )
//        //slideMenuView.delegate = self
//        
//        self.mainScrollView.addSubview(self.awardsView)
//        self.mainScrollView.isPagingEnabled = true
//        
//        mainScrollView.contentSize.width =  GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * 2

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
