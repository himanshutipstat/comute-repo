//
//  ComuteProfileDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 08/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension ComuteProfileViewContoller : CustomizedDefaultNavigationBarDelegate, UITableViewDelegate,UITableViewDataSource,slideMenuViewDelegate,addFriendViewDeligate {
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
       
        
        if sender.tag == 10 {
            
    
            
            if isCurrentUser{
                
            if self.slideMenuView == nil {
            
            //Initializing  View
            self.slideMenuView = UINib(nibName: "SlideMenuView", bundle: nil).instantiate(withOwner: SlideMenuView.self, options: nil)[0] as! SlideMenuView
            slideMenuView.delegate = self
            self.slideMenuView.frame = CGRect(x: -Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width)  , y: 0, width: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width), height: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height) - 20 )
            //slideMenuView.delegate = self
            self.view.addSubview(self.slideMenuView)
            
            UIView.animate(withDuration: 0.2, animations: {
                self.slideMenuView.frame.origin.x = 0
               
            })
            }
            }else {
                
                self.navigationController?.popViewController(animated: true)
            }
        }else if sender.tag == 20 {
            
            let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(notificationVC, animated: true)
        }
    }
    
    func popBack() {
        UIView.animate(withDuration: 0.2, animations: {
            self.slideMenuView.frame.origin.x = -GLOBAL_KEY.CURRENT_DEVICE_SIZE.width
            
        }){ (true) in
            
            self.slideMenuView.removeFromSuperview()
            self.slideMenuView = nil
        }
        
        

        
    }
    func pushToPrivacyNPolicy() {
        print("PnP")
    }
    func pushToTermsNConditionsVC() {
        print("T&C")
    }
    func pushToChangeSettingVC() {
        
       // popBack()
        self.slideMenuView.removeFromSuperview()
        let changeSettingVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangeSettingViewController") as! ChangeSettingViewController
        self.navigationController?.pushViewController(changeSettingVC, animated: true)
    }
    func pushToContactUsVC() {
        
        self.slideMenuView.removeFromSuperview()
        
        let changeSettingVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        self.navigationController?.pushViewController(changeSettingVC, animated: true)
        
    }
    
//    //MARK: ScrollView Delegates...
    
    //MARK: ScrollView Delegates...
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //Dismissing Keyboard...
        self.view.endEditing(true)
        if scrollView == self.mainScrollView {
            
            //Scrolling the corresponding view...
            let indexofViewToBeSelected = Int(scrollView.contentOffset.x / GLOBAL_KEY.CURRENT_DEVICE_SIZE.width)
            
          /*  if indexofViewToBeSelected == 0{
                storiesSepratorView.backgroundColor = UIColor.black
                storiesButtonOutLet.setTitleColor(UIColor.black, for: .normal)
               // storiesButtonOutLet.titleLabel?.textColor = COLOR_CODE.BLACK_COLOR
                awardButtonOutLet.setTitleColor(UIColor.lightGray, for: .normal)
               // awardButtonOutLet.titleLabel?.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                awardsSepratoreView.backgroundColor = UIColor.lightGray
                
                
            }else if indexofViewToBeSelected == 1 {
                
                storiesSepratorView.backgroundColor = UIColor.lightGray
                storiesButtonOutLet.setTitleColor(UIColor.lightGray, for: .normal)
                // storiesButtonOutLet.titleLabel?.textColor = COLOR_CODE.BLACK_COLOR
                awardButtonOutLet.setTitleColor(UIColor.black, for: .normal)
                // awardButtonOutLet.titleLabel?.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                awardsSepratoreView.backgroundColor = UIColor.black

            }*/
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        //Dismissing Keyboard...
        self.view.endEditing(true)
        if scrollView == self.mainScrollView {
            
            //Scrolling the corresponding view...
            let indexofViewToBeSelected = Int(scrollView.contentOffset.x / GLOBAL_KEY.CURRENT_DEVICE_SIZE.width)
            
            if indexofViewToBeSelected == 0{
                
                storiesSepratorView.backgroundColor = UIColor.black
                storiesSepratorView.backgroundColor = UIColor.black
                storiesLabel.textColor = UIColor.black
                
                // storiesButtonOutLet.titleLabel?.textColor = COLOR_CODE.BLACK_COLOR
                awardsLabel.textColor = UIColor.lightGray
                // awardButtonOutLet.titleLabel?.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                awardsSepratoreView.backgroundColor = UIColor.lightGray
                
                
            }else if indexofViewToBeSelected == 1 {
                
                storiesSepratorView.backgroundColor = UIColor.lightGray
                storiesLabel.textColor = UIColor.lightGray
                
                // storiesButtonOutLet.titleLabel?.textColor = COLOR_CODE.BLACK_COLOR
                awardsLabel.textColor = UIColor.black
                // awardButtonOutLet.titleLabel?.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                awardsSepratoreView.backgroundColor = UIColor.black
                
            }
        }
        
    }
    
    
    
    
    //MARK: TableView Delegate & DataSource...
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(self.userArticleArray.count)
        
        return self.userArticleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AllPostCell
        cell.backgroundColor = UIColor.white
        cell.selectionStyle = .none
        
        
      //  let userImage = self.UserDataDictionary["image"] as! String
        
       
        
        /*
         {
         "_id" = 59b903a61e9bc95593ce1f26;
         approved = 0;
         articletype = DARWINS;
         block = 0;
         createat = 1505291849647;
         dislikes =         (
         );
         image = "https://s3.ap-south-1.amazonaws.com/comut/8d7a6a6d-0e2c-44f4-9dbc-e7b89ad9deab";
         likes =         (
         );
         text = "This Article is just for testing there is nothing to do with reality\n";
         updateat = 1505291849647;
         }
 
 */
        if let userArticleDict = self.userArticleArray[indexPath.row] as? NSDictionary{
            
            let userImageURL = NSURL(string: self.userImageString)
            let userImageRequest = NSURLRequest(url: userImageURL! as URL)
            NSURLConnection.sendAsynchronousRequest(userImageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                response, data, error in
                if error != nil {
                    //      println("Image not found!")
                }else {
                    DispatchQueue.main.async {
                        // println("success")
                        
                            let image = UIImage(data: data!)
                            cell.postedUserImageView.image = image
                        
                        
                    }
                    
                }
            })
            
            
            let picURL = userArticleDict["image"] as? String
            
            let imageURL = NSURL(string: picURL!)
            let imageRequest = NSURLRequest(url: imageURL! as URL)
            NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                response, data, error in
                if error != nil {
                    //      println("Image not found!")
                }else {
                    // println("success")
                    let image = UIImage(data: data!)
                    cell.postImageView.image = image
                    
                }
            })
            
            cell.discriptionTextView.text = userArticleDict["text"] as! String
            cell.postType.text = userArticleDict["articletype"] as? String
            cell.postedUserFullName.text = self.userFullName
            cell.postedUserName.text = self.userName.text
            
            if  let likeCount = userArticleDict["likes"] as? NSNumber{
            
            cell.noOFLikes.text = String(describing: likeCount)
            }
            
            if let disLikeCount = userArticleDict["dislikes"] as? NSNumber {
            
            cell.noOFDisLike.text = String(describing: disLikeCount)
            }

            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
            
          if let dateString = userArticleDict["updateat"] as? String {
            
            let dateArr = dateString.components(separatedBy: ".")
            print(dateArr.first!)
            
            if let selectedDate = formatter.date(from: dateArr.first!) {
                formatter.dateFormat = "h:mm a MMM dd, yyyy"
                let date = formatter.string(from: selectedDate)
                cell.postedTime.text = date
                
            }
        
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let postDetailedVC = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailedViewController") as! PostDetailedViewController
        self.present(postDetailedVC, animated: true) {
            print("done done")
        }
    }
    
    //MARK: AddViewDelegate:
    
    func cancelAction() {
        
        self.addMessageView.removeFromSuperview()
    }
    
    func sendAction() {
        
        self.sendFriendRquest.requestComuteFriendsAPI(ID: userID, message: self.addMessageView.addMessageTextView.text, recievedResponse: { (succeeded, response) in
            
            if succeeded {
                
                self.editProfileButtonOutlet.setTitle("Pending Request", for: .normal)
                self.isFriendRequested = true
                self.addMessageView.removeFromSuperview()
                
            }
            
        })
        
    }
    
    
    
    
    
    
    
}
