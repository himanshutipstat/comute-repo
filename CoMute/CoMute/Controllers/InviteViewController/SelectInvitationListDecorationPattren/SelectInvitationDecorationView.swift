//
//  SelectInvitationDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 26/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import ContactsUI
import FBSDKShareKit
extension SelectInvitationListViewController : CustomizedDefaultNavigationBarDelegate,inviteFriendNavigationViewDelegate,CNContactPickerDelegate,FBSDKAppInviteDialogDelegate {
    
    
    
    //MARK: Gesture recognizer delegates...
//    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
//        
//        //Removing Keyboard...
//        self.view.endEditing(true)
//        
//        //Finding Which view is tapped....
//        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
//        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
//        
//        //Recognizing gesture if Tapped on Image view......
//        if newView!.isKind(of: UIImageView.self) {
//            
//            
//            return false
//        }
//        
//        return true
//    }
    
    //MARK: FBSDKAppInviteDialogDelegate
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
        
        
        print(results)
        
    }
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        print("error made") 
    }
   
    
    
    //MARK:- CNContactPickerDelegate Method
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        contacts.forEach { contact in
            
          //  print(contact)
            let name  = contact.givenName + " " + contact.familyName
             print("Name is = \(name)")
           
            if contact.phoneNumbers.count > 0 {
                let  number = (contact.phoneNumbers[0].value ).value(forKey: "digits") as? String
//                let pLabel = contact.phoneNumbers[0].label //_$!<Mobile>!$_
//                let pLabel2 = pLabel?.characters.split(separator: <#T##Character#>, maxSplits: <#T##Int#>, omittingEmptySubsequences: <#T##Bool#>)
//                split("<").map(String.init) //[_$!<, Mobile>!$_]
//                let pLabel3 = pLabel2?[1].characters.split(">").map(String.init) //[Mobile, >!$_]
               //let type  = pLabel3?[0] //Mobile
                
                print("number = \(String(describing: number))")
                //print("type = \(String(describing: type))")
            } else {
                
            }
        }
    }
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
        self.navigationController?.popViewController(animated: true)
            
        }else if sender.tag == 20 {
            
          //  let DashBoardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
           // self.navigationController?.pushViewController(DashBoardVC, animated: true)
        }
    }
    
    // MARK: Custom InviteFriendsNAvigationView Delegate 
    
    func popBack() {
        //self.navigationController?.popViewController(animated: true)
    }
    
    func skipAction() {
        let DashBoardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(DashBoardVC, animated: true)
    }
}
