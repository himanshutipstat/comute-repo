//
//  SelectInvitationListViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 02/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import ContactsUI
import FBSDKCoreKit
import FBSDKShareKit

class SelectInvitationListViewController: UIViewController {
    
    
    
    @IBOutlet weak var contactButtonOutlet: UIButton!

    //MARK: Custom Views...
   // var tapGesture : UITapGestureRecognizer!
    var navigation: CustomizedDefaultNavigationBar!
    var titleBarView : InviteFriendNavigationView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        settingExtraFontsAndUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "Invite Friends", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        titleBarView = UINib(nibName: "InviteFriendNavigationView", bundle: nil).instantiate(withOwner: InviteFriendNavigationView.self, options: nil)[0] as! InviteFriendNavigationView
        navigation.navigationBarIten.titleView = titleBarView
        //navigation.navigationBarIten.titleView.
        navigation.navigationBarIten.titleView?.clipsToBounds = true
        titleBarView.delegate  = self
        
        self.view.addSubview(navigation!)
        
        self.contactButtonOutlet.layer.borderWidth = 1
        self.contactButtonOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
//        //Setting Tap Gesture...
//        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.removeKeyboard(sender:)))
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        tapGesture.delegate = self
//        self.view.addGestureRecognizer(tapGesture)
        
        
    }
    
    //MARK: StoryBoard Actions...
    
    @IBAction func chooseFromContactAction(_ sender: AnyObject) {
        
        let cnPicker = CNContactPickerViewController()
//        cnPicker.displayedPropertyKeys =
//            [CNContactGivenNameKey
//                , CNContactPhoneNumbersKey]
        cnPicker.delegate = self
        cnPicker.predicateForEnablingContact = NSPredicate(format: "phoneNumbers.@count > 0")
        cnPicker.predicateForSelectionOfContact = NSPredicate(format:"phoneNumbers.@count == 1")
        cnPicker.predicateForSelectionOfProperty = NSPredicate(format:"key == phoneNumber")
        self.present(cnPicker, animated: true, completion: nil)
        
//                let sendInvitationVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectContactFromInvitationListViewController") as! SelectContactFromInvitationListViewController
//                self.navigationController?.pushViewController(sendInvitationVC, animated: true)
    }
    
    @IBAction func chooseFromFacebookFriendsAction(_ sender: AnyObject) {
        
        
        
        let content = FBSDKAppInviteContent()
        content.appLinkURL = NSURL(string: "https://fb.me/1719685684727776")! as URL
        FBSDKAppInviteDialog.show(from: self, with: content, delegate: self)
        
//      // let facebookReadPermissions = ["public_profile", "email", "user_photos"]
//        
//        let content = FBSDKAppInviteContent()
//        content.appLinkURL = URL(string: "https://www.mydomain.com/myapplink")
//        //optionally set previewImageURL
//        content.appInvitePreviewImageURL = URL(string: "https://www.mydomain.com/my_invite_image.jpg")
//        // Present the dialog. Assumes self is a view controller
//        // which implements the protocol `FBSDKAppInviteDialogDelegate`.
//        FBSDKAppInviteDialog.show(from: self, with: content, delegate: self as! FBSDKAppInviteDialogDelegate)
        
//        let fbRequest = FBSDKGraphRequest(graphPath:"/me/friends", parameters: NSDictionary(object: "picture.type(large),id,name,email,first_name,last_name,birthday, gender,photos.fields(name,picture,source)", forKey: "fields" as NSCopying) as [NSObject : AnyObject], httpMethod: "GET")
//        fbRequest?.start(completionHandler: { (connection, result , error) in
//            
//            
//            
//            if error == nil {
//                
//                print("Friends are : \(String(describing: result))")
//                
//            } else {
//                
//                print("Error Getting Friends \(String(describing: error))");
//                
//            }
//            
//            
//        }  )
//        fbRequest?.startWithCompletionHandler { (connection : FBSDKGraphRequestConnection!, result : AnyObject!, error : NSError!) -> Void in
//            
//            if error == nil {
//                
//                println("Friends are : \(result)")
//                
//            } else {
//                
//                println("Error Getting Friends \(error)");
//                
//            }
//        }
        
               // let sendInvitationVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectContactFromInvitationListViewController") as! SelectContactFromInvitationListViewController
               // self.navigationController?.pushViewController(sendInvitationVC, animated: true)
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
