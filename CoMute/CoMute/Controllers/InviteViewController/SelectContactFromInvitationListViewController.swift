//
//  SelectContactFromInvitationListViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 02/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class SelectContactFromInvitationListViewController: UIViewController {
    
   
    @IBOutlet weak var contactListTableView: UITableView!
    
    //MARK: Custom Views...
    //var tapGesture : UITapGestureRecognizer!
    var navigation: CustomizedDefaultNavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingExtraFontsAndUI()
        
        //Regestring table view class...
        self.contactListTableView.register(InvitationTableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        self.contactListTableView.register(UINib(nibName: "InvitationTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "Invite Friend", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
//        //Setting Tap Gesture...
//        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.removeKeyboard(sender:)))
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        tapGesture.delegate = self
//        self.view.addGestureRecognizer(tapGesture)
        
        
    }
    
    
    //MARK: StoryBoard Actions..
    @IBAction func sendInvitationAction(_ sender: AnyObject) {
        
        let DashBoardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(DashBoardVC, animated: true)
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
