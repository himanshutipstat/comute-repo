//
//  SelectContactFromInvitationDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 26/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension SelectContactFromInvitationListViewController :CustomizedDefaultNavigationBarDelegate, UITableViewDelegate,UITableViewDataSource {
    
    
    
//    //MARK: Gesture recognizer delegates...
//    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
//        
//        //Removing Keyboard...
//        self.view.endEditing(true)
//        
//        //Finding Which view is tapped....
//        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
//        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
//        
//        //Recognizing gesture if Tapped on Image view......
//        if newView!.isKind(of: UIImageView.self) {
//            
//            
//            return false
//        }
//        
//        return true
//    }
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.navigationController?.popViewController(animated: true)
          
            
        }
    }
    
    
    //MARKL: TableView Delegates and DataSources...
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! InvitationTableViewCell
        cell.backgroundColor = UIColor.white
        cell.selectionStyle = .none
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
