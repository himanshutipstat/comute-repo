 //
//  HomeViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var homeTabBar: UITabBarItem!
    
    //MARK: Custom Views...
    var navigation: CustomizedDefaultNavigationBar!
    var customSegmentBarView : SegmentBarView!
    var allArticles =  ArticleModel()
    
    var allArticleDataDic =  NSDictionary()
    
    var myFeedArticleArray =  NSArray()
    
    var myComuteArticleArray =  NSDictionary()
    
    var comuteTodayArticleArray =  NSDictionary()

    
   // var first:NewPostViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingExtraFontsAndUI()
        self.mainScrollView.delegate = self
        if #available(iOS 10.0, *) {
            homeTabBar.badgeColor = UIColor.yellow
        } else {
            // Fallback on earlier versions
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.allArticles.getAllArticleAPI(ID: "") { (suceeded, response) in
            
            if suceeded {
                
                self.allArticleDataDic = response["data"] as! NSDictionary
                
                self.myFeedArticleArray = self.allArticleDataDic["myfeed"] as! NSArray
                
                self.myComuteArticleArray = self.allArticleDataDic["user"] as! NSDictionary
                
                self.comuteTodayArticleArray = self.allArticleDataDic["line"] as! NSDictionary
                
                print(self.allArticleDataDic)
                print(self.myFeedArticleArray)
                print(self.myComuteArticleArray)
                print(self.comuteTodayArticleArray)
                
                
                self.addingViewControllerInScroll()
                
                
            }
        }
        

    }
    
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        mainScrollView.contentSize = CGSize(width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * 3, height: aspectHeight(height: 450) )
        
        self.mainScrollView.contentOffset.x = 0
        
         customSegmentBarView.frame = CGRect(x: 0  , y: 70 , width: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width), height: 45)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Removing Observer...
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "Post")!,UIImage(named: "Notification")!], buttonHighlightedImage: [UIImage(named: "Post")!,UIImage(named: "Notification")!], numberOfButtons: 3, barTitle: "Home", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        
        //Initializing  View
        self.customSegmentBarView = UINib(nibName: "SegmentBarView", bundle: nil).instantiate(withOwner: SegmentBarView.self, options: nil)[0] as! SegmentBarView
        
        customSegmentBarView.frame = CGRect(x: 0  , y: 70 , width: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width), height: 45)
        customSegmentBarView.delegate = self
        
        print(customSegmentBarView.frame)
    
       // customSegmentBarView.clipsToBounds = true
        self.view.addSubview(customSegmentBarView)
        
        
        self.mainScrollView.frame = CGRect(x: 0, y: Int(aspectHeight(height: 115)) , width: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width), height: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height ) - Int(aspectHeight(height: 115)) )
   
        
        print(self.mainScrollView.frame)
        
        
    }
    
    //MARK: Adding ViewControllers in ScrollView....
    
    func addingViewControllerInScroll(){
        
        
//         first = self.storyboard?.instantiateViewController(withIdentifier: "NewPostViewController") as! NewPostViewController
//        self.mainScrollView.addSubview(first.view)
//        first.view.frame = CGRect(x: 0, y: 0, width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, height: GLOBAL_KEY.CURRENT_DEVICE_SIZE.height)
//        first.willMove(toParentViewController: self)
//        self.addChildViewController(first)
        
         mainScrollView.contentSize = CGSize(width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width * 3, height: aspectHeight(height: 450) )
        
        let second = self.storyboard?.instantiateViewController(withIdentifier: "MyFeedPostViewController") as! MyFeedPostViewController
        second.feedArticleArray = myFeedArticleArray
        self.mainScrollView.addSubview(second.view)
        second.view.frame = CGRect(x: 0, y: -20, width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, height: self.mainScrollView.frame.height )
        
        
        second.willMove(toParentViewController: self)
        self.addChildViewController(second)
        
        
        let third = self.storyboard?.instantiateViewController(withIdentifier: "ComuteTodayPostViewController") as! ComuteTodayPostViewController
        third.comuteTodayArticleArray = comuteTodayArticleArray
        self.mainScrollView.addSubview(third.view)
        third.view.frame = CGRect(x: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, y: -20, width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, height: self.mainScrollView.frame.height  )
        third.willMove(toParentViewController: self)
        self.addChildViewController(third)
        
        
        
        let fourth = self.storyboard?.instantiateViewController(withIdentifier: "MyComutePostViewController") as! MyComutePostViewController
        fourth.myComuteTodayArticleArray = myComuteArticleArray
        self.mainScrollView.addSubview(fourth.view)
        fourth.view.frame = CGRect(x: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width*2, y: -20, width: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, height: self.mainScrollView.frame.height )
        fourth.willMove(toParentViewController: self)
        self.addChildViewController(fourth)
        
        self.mainScrollView.contentOffset.x = 0
        
        //        first.view.frame.origin = CGPoint(x: -GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, y: 0)
        //        second.view.frame.origin = CGPoint.zero
        //        third.view.frame.origin = CGPoint(x: GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, y: 0)
        //        fourth.view.frame.origin = CGPoint(x: 2 * GLOBAL_KEY.CURRENT_DEVICE_SIZE.width, y: 0)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
