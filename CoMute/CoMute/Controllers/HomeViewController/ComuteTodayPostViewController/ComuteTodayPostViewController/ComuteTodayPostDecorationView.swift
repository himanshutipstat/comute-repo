//
//  MyFeedDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension ComuteTodayPostViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    
    
    // MARK: TableView DataSource and Delegate....
    func numberOfSections(in tableView: UITableView) -> Int {
        return comuteTodayArticleArray.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        
        return comuteTodayArticleArray.allKeys[section] as? String
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headerView = UIView()
        let dividerView = UIView()
        dividerView.frame = CGRect(x: 50, y: 0, width: 275, height: 1)
        dividerView.backgroundColor = COLOR_CODE.LIGHT_GRAY_DIVIDER_COLOR
        if section != 0{
            
            headerView.addSubview(dividerView)
        }
        headerView.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 10, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height - 10))
        headerLabel.font = UIFont(name: FONT.PRO_REGULAR, size: 15)
        headerLabel.textColor = COLOR_CODE.BLACK_COLOR
        headerLabel.textAlignment = NSTextAlignment.center
        headerLabel.clipsToBounds = true
        headerLabel.text = self.tableView(myFeedsTableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        
        //tableView.backgroundColor = UIColor.red
        //45610928407
        headerView  = headerLabel
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("Something..")
        let cell = myFeedsTableView.dequeueReusableCell(withIdentifier: "FeedCell") as! FeedTableCell
        //cell.delegate = self as feedTableCellDelegate

        //print(self.myComuteTodayArticleArray.allValues)
        for i in count ..< comuteTodayArticleArray.allKeys.count  {
            
            if  let articleArray = self.comuteTodayArticleArray.allValues[i] as? NSArray {
                print(articleArray)
                
                cell.fillCollectionView(with: articleArray, controller: self, image: "")
                count  = count + 1
                break
                
            }
            
            
        }

        cell.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        //cell.selectionStyle = .lightGray
        
        return cell
    }
    
    
    func refresh(){
        
        
        print("refreshed")
        self.refresher.endRefreshing()
        self.comuteToday.getComuteTodayArticleAPI(ID: "") { (suceeded, response) in
            
            if suceeded {
                
                print(response)
                self.comuteTodayArticleArray = (response["data"] as? NSDictionary)!
                self.myFeedsTableView.reloadData()
                self.count = 0
                
            }
        }
        
        
    }
}
