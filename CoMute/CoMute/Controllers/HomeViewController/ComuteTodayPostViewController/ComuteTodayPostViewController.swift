//
//  ComuteTodayPostViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class ComuteTodayPostViewController: UIViewController {
    
    @IBOutlet weak var myFeedsTableView: UITableView!
    var feedSections = ["Top Darwins", "Top Hotties", "Top Lifers", "Top NFG"]
    
    //MARK: Custome Views...
    
    var comuteToday  = ArticleModel()
    var comuteTodayArticleArray = NSDictionary()
    var refresher: UIRefreshControl!
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //Regestring table view class...
        
        self.myFeedsTableView.register(FeedTableCell.classForCoder(), forCellReuseIdentifier: "FeedCell")
        
        self.myFeedsTableView.register(UINib(nibName: "FeedTableCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
        
        self.myFeedsTableView.tableHeaderView?.backgroundColor = UIColor.lightGray
        self.myFeedsTableView.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        
        refresher = UIRefreshControl()
        //refresher.attributedTitle = NSAttributedString(string: "pull to refresh")
        
        refresher.addTarget(self, action: #selector(ComuteTodayPostViewController.refresh), for: UIControlEvents.valueChanged)
        self.myFeedsTableView.addSubview(refresher)
        //refresh()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //
    //        self.comuteToday.getComuteTodayArticleAPI(ID: "") { (suceeded, response) in
    //
    //            if suceeded {
    //                print(response)
    //            }
    //        }
    //        }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
