//
//  MyFeedPostViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class MyFeedPostViewController: UIViewController {
    @IBOutlet weak var allPostTableView: UITableView!
    
    //MARK: Custom Views...
    
    var feedArticle  = ArticleModel()
    var feedArticleArray = NSArray()
    var refresher: UIRefreshControl!
    var userArticleDict = NSDictionary()
    var userID = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        //Regestring table view class...
        self.allPostTableView.register(AllPostCell.classForCoder(), forCellReuseIdentifier: "Cell")
        self.allPostTableView.register(UINib(nibName: "AllPostCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        refresher = UIRefreshControl()
        //refresher.attributedTitle = NSAttributedString(string: "pull to refresh")
        
        refresher.addTarget(self, action: #selector(MyFeedPostViewController.refresh), for: UIControlEvents.valueChanged)
        self.allPostTableView.addSubview(refresher)
       // refresh()
    
       print(self.feedArticleArray.count)
      //  self.allPostTableView.reloadData()


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        
//        self.feedArticle.getFeedsArticleAPI(ID: "") { (suceeded, response) in
//            
//            if suceeded {
//                
//               print(response)
//            }
//        }
//    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
