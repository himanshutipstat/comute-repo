//
//  AllPostDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension MyFeedPostViewController : UITableViewDelegate, UITableViewDataSource,allPostCellDelegate {
    
    //MARK: TableView Delegate & DataSource...
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.feedArticleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AllPostCell
        cell.backgroundColor = UIColor.white
        cell.selectionStyle = .none
         cell.viewProfileTap.addTarget(self, action: #selector(MyFeedPostViewController.viewProfile(sender:)), for: .touchUpInside)
        cell.viewProfileTap.tag = indexPath.row
        cell.delegate = self
        //cell.tag = indexPath.row
        
        /*
         {
         "_id" = 59a9290ce03e073f83fbc200;
         articles =                 {
         "_id" = 59a92939e03e073f83fbc201;
         articletype = abcd;
         createat = "2017-09-01T09:29:38.080Z";
         dislikes = 0;
         image = "https://s3.ap-south-1.amazonaws.com/comut/3780109a-9e35-4644-9482-b16308e5fea4";
         isDisLikes = 0;
         isLikes = 0;
         likes = 0;
         text = "this is new text";
         updateat = "2017-09-01T09:29:38.080Z";
         };
         firstname = akilesh;
         image = "https://s3.ap-south-1.amazonaws.com/comut/29edbe30-01b4-43af-a6ca-ac4f474e8b63";
         lastname = v;
         
         */
        
        print(self.feedArticleArray[indexPath.row])
        
        if let userArticleData = self.feedArticleArray[indexPath.row] as? NSDictionary{
            
            cell.PostDataDictionary = userArticleData
           
            
        
            
          
            
        }
        
        return cell
    }
    
    func refreshData(){
        
        print("refreshed")
        self.refresher.endRefreshing()
        
        self.feedArticle.getFeedsArticleAPI(ID: "") { (suceeded, response) in
            
            if suceeded {
                
                print(response)
                self.feedArticleArray = (response["data"] as? NSArray)!
                
            }
        }
        
        
    }
    
    func refresh(){
        
        print("refreshed")
        self.refresher.endRefreshing()
        
        self.feedArticle.getFeedsArticleAPI(ID: "") { (suceeded, response) in
            
            if suceeded {
                
                print(response)
                self.feedArticleArray = (response["data"] as? NSArray)!
                self.allPostTableView.reloadData()
                
            }
        }
        
        
    }
    
    func viewProfile(sender : UIButton){
        
       // if let cell =  sender.superview?.superview as? AllPostCell {
            
           // let indexPath = allPostTableView.indexPath(for: cell)
         let indexPath = sender.tag
        
         if let userArticleData = self.feedArticleArray[(indexPath)] as? NSDictionary{
            print(userArticleData)
        
         self.userID = userArticleData["_id"] as! String
    
            let userName = userArticleData["username"] as! String
        
        if Singleton.sharedInstance.userID != self.userID {
            
            if userName != "admin" {
            
            let storyboard: UIStoryboard = UIStoryboard (name: "Main", bundle: nil)
            
            let userProfileVC = storyboard.instantiateViewController(withIdentifier: "ComuteProfileViewContoller") as! ComuteProfileViewContoller
            
            userProfileVC.isCurrentUser = false
            userProfileVC.notificationFlag = 0
            userProfileVC.userID = self.userID
           // let currentController = self.getCurrentViewController()
            //print( self.getCurrentViewController()!)
            
            
            self.navigationController?.pushViewController(userProfileVC, animated: true)
            }
            }
        
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let postDetailedVC = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailedViewController") as! PostDetailedViewController
        if let userArticleData = self.feedArticleArray[indexPath.row] as? NSDictionary{
            
            let userArticleDict = userArticleData["articles"] as! NSDictionary
              let userImage = userArticleData["image"] as! String
            postDetailedVC.articleDict = userArticleDict
            postDetailedVC.postImage = userImage
            postDetailedVC.userId = userArticleData["_id"] as! String
            postDetailedVC.fullName = String(describing: userArticleData["firstname"]!)+" "+String(describing: userArticleData["lastname"]!)
            postDetailedVC.userName =  (userArticleData["username"] as? String)!
        }
        self.present(postDetailedVC, animated: true) {
            print("done done")
        }
    }
}
