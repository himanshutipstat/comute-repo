//
//  NewPostDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension NewPostViewController:  CustomizedDefaultNavigationBarDelegate,inviteFriendNavigationViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate,UITextViewDelegate {

    //MARK: Gesture recognizer delegates...
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        //Finding Which view is tapped....
        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
        
        //Recognizing gesture if Tapped on Image view......
        if newView!.isKind(of: UIImageView.self) {
            
            
            return false
        }
        
        return true
    }
    
    //MARK: textView Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        print("textViewDidBeginEditing")
        if postDiscriptionTextView.text == "Enter Description"
        {
            postDiscriptionTextView.text = ""
        }

    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        //        setInitialViewFrame()
        self.view.endEditing(true)
        
        if self.postDiscriptionTextView.text == "" {
            
            self.postDiscriptionTextView.text = "Enter Description"
            
        }
        
    }

    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.dismiss(animated: true, completion: {
                print("done")
            })

        }
    }
    //MARK: Picker Delegates....
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.chooseTypeOutlet.setTitle(pickerData[row], for: .normal)
        return pickerData[row]
    }
    
    //MARK: UIIMage Picker Controller Delegates...
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var  chosenImage = UIImage()
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            chosenImage = image
        }else {
            
            chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        }
        uploadImageView.contentMode = .scaleToFill //3
        uploadImageView.image = chosenImage //4
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Imagepicker Functins
    
    func openPhotos(){
        
        imagePickerController.allowsEditing = true
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    func openCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController.allowsEditing = false
            imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
            imagePickerController.cameraCaptureMode = .photo
            imagePickerController.modalPresentationStyle = .fullScreen
            present(imagePickerController,animated: true,completion: nil)
        } else {
            noCamera()
        }
        
        
    }
    
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }

    
    // MARK: Custom InviteFriendsNAvigationView Delegate
    
    func popBack() {
        //self.navigationController?.popViewController(animated: true)
    }
    
    func skipAction() {
    
        if self.uploadImageView.image != nil {
        
        self.postArticle.postArticleAPI(text: postDiscriptionTextView.text!, type: (self.chooseTypeOutlet.titleLabel?.text!)!, image: UIImageJPEGRepresentation(self.uploadImageView.image!, 0.0)!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)), recievedResponse: { (succeeded, response) in
            
            if succeeded {
                
                print(response)
                self.dismiss(animated: true, completion: {
                    print("done")
                })
                
            }else {
                
                self.dismiss(animated: true, completion: {
                    print("done")
                })
            }
        
        }
        )
        }else {
            
            self.helperModel.showingAlertcontroller(title: "", message: "Please add a image with this post", cancelButton: "Ok", receivedResponse: { 
            
            })
        }
    
    
    }
}
