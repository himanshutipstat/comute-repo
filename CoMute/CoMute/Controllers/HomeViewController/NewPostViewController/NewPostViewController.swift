//
//  NewPostViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class NewPostViewController: UIViewController {

    
    @IBOutlet weak var postDiscriptionTextView: UITextView!
    
    @IBOutlet weak var uploadImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var chooseTypeOutlet: UIButton!
    @IBOutlet weak var typePickerView: UIPickerView!
    @IBOutlet weak var typePickerViewBottmConstrain: NSLayoutConstraint!
    
    
    
    //MARK: Custom Views...
    var tapGesture : UITapGestureRecognizer!
    var navigation: CustomizedDefaultNavigationBar!
    var titleBarView : InviteFriendNavigationView!
    
    //Helper Model...
    var helperModel = HelperModel()
    var postArticle = ArticleModel()
    
    // Input data into the Array:
   var pickerData = ["DARWINS", "HOTTIES", "LIFERS", "NFG"]


    //MARK: Image Picker Views...
    var imagePickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUPUIConstrain()
        self.postDiscriptionTextView.delegate = self
        
        postArticle.getArticleTypeAPI(ID: "") { (succeeded, response) in
            
            if succeeded {
                
                print(response)
                let array = response["data"] as? NSArray
                self.pickerData = array as! [String]
                self.typePickerView.reloadAllComponents()
            }
            
        }
        
        
        
        
        if Singleton.sharedInstance.userFullName != ""{
            
            userFullNameLabel.text = Singleton.sharedInstance.userFullName
            
        }
        if Singleton.sharedInstance.userName != ""{
            
            userNameLabel.text = Singleton.sharedInstance.userName
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func setUPUIConstrain(){
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "Submit a post", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        titleBarView = UINib(nibName: "InviteFriendNavigationView", bundle: nil).instantiate(withOwner: InviteFriendNavigationView.self, options: nil)[0] as! InviteFriendNavigationView
        navigation.navigationBarIten.titleView = titleBarView
        titleBarView.titleLabel.text = "Submit a post"
        titleBarView.rightButtonOutlet.setTitle("Send", for: .normal)
        navigation.navigationBarIten.titleView?.clipsToBounds = true
        titleBarView.delegate  = self

        
        self.view.addSubview(navigation!)
        
        //Setting Tap Gesture...
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(NewPostViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
        postDiscriptionTextView.layer.borderWidth = 1
        postDiscriptionTextView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        chooseTypeOutlet.layer.borderWidth = 1
        chooseTypeOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        typePickerView.layer.borderWidth = 1
        typePickerView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
//        //Setting Tap Gesture...
//        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.removeKeyboard(sender:)))
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        tapGesture.delegate = self
//        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    //MARK: Storyboard Action..
    @IBAction func selectTypeAction(_ sender: Any) {
        
        self.typePickerViewBottmConstrain.constant = -220
        //self.chooseTypeOutlet.setTitle(typePickerView, for: .normal)
        
    }
    
    @IBAction func OpenPickerAction(_ sender: Any) {
        
        if self.typePickerViewBottmConstrain.constant == -220 {
            
            UIView.animate(withDuration: 0.15, animations: {
                
                self.typePickerViewBottmConstrain.constant = 0
                
                
            }, completion: {finished in
                
              
            }
            )
       
            

        } else {
            
            UIView.animate(withDuration: 0.15, animations: {
                
                self.typePickerViewBottmConstrain.constant = -220
                
            }, completion: {finished in
                
                
            }
            )        }
    }
    
    @IBAction func openGallaryAction(_ sender: AnyObject) {  //Phone Gallary
        
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openPhotos()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        imagePickerController.delegate = self
        self.present(alert, animated: true, completion: nil)    }
    
    //MARK: Removing keyboard...And Setting zesture fopr zooming...
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        self.postDiscriptionTextView.resignFirstResponder()
       
    }


    /*
    //MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
