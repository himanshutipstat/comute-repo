//
//  MyComutePostViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class MyComutePostViewController: UIViewController {
    
    
    @IBOutlet weak var myComuteTableView: UITableView!
    
    
    
    var feedSections = ["Darwins", "Hotties", "Lifers", "NFG"]
    var myComute = ArticleModel()
    var myComuteTodayArticleArray = NSDictionary()
    var refresher: UIRefreshControl!
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //
        //                self.myComute.getMyComuteArticleAPI(ID: "") { (suceeded, response) in
        //
        //                    if suceeded {
        //
        //                        print(response)
        //
        //                    }
        //                }
        //
        //Regestring table view class...
        
        self.myComuteTableView.register(FeedTableCell.classForCoder(), forCellReuseIdentifier: "FeedCell")
        
        self.myComuteTableView.register(UINib(nibName: "FeedTableCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
        self.myComuteTableView.reloadData()
        
        self.myComuteTableView.tableHeaderView?.backgroundColor = UIColor.lightGray
        self.myComuteTableView.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        self.myComuteTableView.clipsToBounds = true
        
        refresher = UIRefreshControl()
        //refresher.attributedTitle = NSAttributedString(string: "pull to refresh")
        
        refresher.addTarget(self, action: #selector(MyComutePostViewController.refresh), for: UIControlEvents.valueChanged)
        self.myComuteTableView.addSubview(refresher)
      //.  refresh()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
