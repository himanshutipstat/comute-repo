
//
//  ComuteTodayDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension MyComutePostViewController : UITableViewDataSource,UITableViewDelegate{
    
    
    // MARK: TableView DataSource and Delegate....
    func numberOfSections(in tableView: UITableView) -> Int {
        return myComuteTodayArticleArray.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
    // print( myComuteTodayArticleArray.allKeys[section])
        
       // count = myComuteTodayArticleArray.allKeys.count
        return myComuteTodayArticleArray.allKeys[section] as? String
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headerView = UIView()
        let dividerView = UIView()
        dividerView.frame = CGRect(x: 50, y: 0, width: 275, height: 1)
        dividerView.backgroundColor = COLOR_CODE.LIGHT_GRAY_DIVIDER_COLOR
        if section != 0{
            
            headerView.addSubview(dividerView)
        }
        headerView.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 10, width:
           tableView.bounds.size.width, height: tableView.bounds.size.height - 10))
        headerLabel.font = UIFont(name: FONT.PRO_REGULAR, size: 15)
        headerLabel.textColor = COLOR_CODE.BLACK_COLOR
        headerLabel.textAlignment = NSTextAlignment.center
        headerLabel.clipsToBounds = true
        headerLabel.frame = headerView.frame
        headerLabel.text = self.tableView(myComuteTableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        
        //tableView.backgroundColor = UIColor.red
        //45610928407
         headerView  = headerLabel
       // headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("Something..")
        let cell = myComuteTableView.dequeueReusableCell(withIdentifier: "FeedCell") as! FeedTableCell
        
        //print(self.myComuteTodayArticleArray.allValues)
        for i in count ..< myComuteTodayArticleArray.allKeys.count  {
            
            if  let articleArray = self.myComuteTodayArticleArray.allValues[i] as? NSArray {
                print(articleArray)
                
                                
                cell.fillCollectionView(with: articleArray, controller: self, image: "")
            count  = count + 1
                break
            
        }
       
        
        }
        
       cell.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        //cell.selectionStyle = .lightGray
        
        return cell
    }
 
    func refresh(){
         self.refresher.endRefreshing() 
        
        print("refreshed")
        
        self.myComute.getMyComuteArticleAPI(ID: "") { (suceeded, response) in
            
            if suceeded {
                
                print(response)
                self.myComuteTodayArticleArray = (response["data"] as? NSDictionary)!
                self.myComuteTableView.reloadData()
                self.count = 0
               
            }
        }
        
        
    }
    
    
    
}
