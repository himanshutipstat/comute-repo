//
//  HomeDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 17/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension  HomeViewController : CustomizedDefaultNavigationBarDelegate, segmentBarViewDelegate, UIScrollViewDelegate {
    
  
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            let newPostVC = self.storyboard?.instantiateViewController(withIdentifier: "NewPostViewController") as! NewPostViewController
            self.present(newPostVC, animated: true) {
                print("done done")
            }


        }else if sender.tag == 20 {
    
    let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
    self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    }

    //MARK: ScrollView Delegates...
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //Dismissing Keyboard...
        self.view.endEditing(true)
        if scrollView == self.mainScrollView {
            
            //Scrolling the corresponding view...
            let indexofViewToBeSelected = Int(scrollView.contentOffset.x / GLOBAL_KEY.CURRENT_DEVICE_SIZE.width)
            
            if indexofViewToBeSelected == 0{
                
                customSegmentBarView.allButtonView.backgroundColor = UIColor.black
                customSegmentBarView.comuteTodayView.backgroundColor = UIColor.white
                customSegmentBarView.myFeedButtonView.backgroundColor = UIColor.white
                 customSegmentBarView.myFeedLabel.textColor = COLOR_CODE.BLACK_COLOR
                 customSegmentBarView.comuteTodayLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                 customSegmentBarView.myComuteLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                
            }else if indexofViewToBeSelected == 1 {
                
                customSegmentBarView.allButtonView.backgroundColor = UIColor.white
                customSegmentBarView.comuteTodayView.backgroundColor = UIColor.white
                customSegmentBarView.myFeedButtonView.backgroundColor = UIColor.black
                
                customSegmentBarView.myFeedLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                customSegmentBarView.comuteTodayLabel.textColor = COLOR_CODE.BLACK_COLOR
                customSegmentBarView.myComuteLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                
            }else if indexofViewToBeSelected == 2 {
                
                customSegmentBarView.allButtonView.backgroundColor = UIColor.white
                customSegmentBarView.comuteTodayView.backgroundColor = UIColor.black
                customSegmentBarView.myFeedButtonView.backgroundColor = UIColor.white
                
                customSegmentBarView.myFeedLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                customSegmentBarView.myComuteLabel.textColor = COLOR_CODE.BLACK_COLOR
                customSegmentBarView.comuteTodayLabel.textColor = COLOR_CODE.LIGHT_GRAY_TEXT_COLOR
                
            }
            
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        //Dismissing Keyboard...
        self.view.endEditing(true)
        if scrollView == self.mainScrollView {
            
            //Scrolling the corresponding view...
            let indexofViewToBeSelected = Int(scrollView.contentOffset.x / GLOBAL_KEY.CURRENT_DEVICE_SIZE.width)
            
            if indexofViewToBeSelected == 0{
                
                        customSegmentBarView.allButtonView.backgroundColor = UIColor.black
                        customSegmentBarView.comuteTodayView.backgroundColor = UIColor.white
                        customSegmentBarView.myFeedButtonView.backgroundColor = UIColor.white
                
            }else if indexofViewToBeSelected == 1 {
                
                        customSegmentBarView.allButtonView.backgroundColor = UIColor.white
                        customSegmentBarView.comuteTodayView.backgroundColor = UIColor.white
                        customSegmentBarView.myFeedButtonView.backgroundColor = UIColor.black
                
            }else if indexofViewToBeSelected == 2 {
                
                        customSegmentBarView.allButtonView.backgroundColor = UIColor.white
                        customSegmentBarView.comuteTodayView.backgroundColor = UIColor.black
                        customSegmentBarView.myFeedButtonView.backgroundColor = UIColor.white
                
            }
            
        }

    }
    
    
    //MARK: Segment Controller Delegate..
    func pushToAllPostVC() {
        
         self.mainScrollView.contentOffset.x = 0

        //        let allPostVC = self.storyboard?.instantiateViewController(withIdentifier: "MyFeedPostViewController") as! MyFeedPostViewController
//        self.navigationController?.pushViewController(allPostVC, animated: true)
    }
    func pushToMyFeedVc() {
        
         self.mainScrollView.contentOffset.x = GLOBAL_KEY.CURRENT_DEVICE_SIZE.width

//        let myFeedVC = self.storyboard?.instantiateViewController(withIdentifier: "ComuteTodayPostViewController") as! ComuteTodayPostViewController
//        self.navigationController?.pushViewController(myFeedVC, animated: true)
    }
    
    func pushToComuteTodayVC() {
        
         self.mainScrollView.contentOffset.x = GLOBAL_KEY.CURRENT_DEVICE_SIZE.width*2

//        let comuteTodayVC = self.storyboard?.instantiateViewController(withIdentifier: "MyComutePostViewController") as! MyComutePostViewController
//        self.navigationController?.pushViewController(comuteTodayVC, animated: true)
    }
    
}
