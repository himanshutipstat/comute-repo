//
//  PostDetailedViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 29/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class PostDetailedViewController: UIViewController {
    
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var articleDiscriptionTextView: UITextView!
    @IBOutlet weak var postedDateLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var disLikeCountLabel: UILabel!
    @IBOutlet weak var likeCountOutlet: UIButton!
    @IBOutlet weak var dislikeCountOutlet: UIButton!
    
    
    
    //MARK: Custom Views...
    //var tapGesture : UITapGestureRecognizer!
    var navigation: CustomizedDefaultNavigationBar!
    var tapGesture : UITapGestureRecognizer!
    var articleDict = NSDictionary()
    var postImage = String()
    var updateArticle = ArticleModel()
    var artcileID = String()
    var userId = String()
    var fullName = String()
    var userName = String()
    var myComute = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*
         "_id" = 59d899a95a6a725811187445;
         articles =     {
         "_id" = 59db1db2bfc3982160cd19c9;
         articletype = DARWINS;
         createat = "2017-10-07T10:26:19.470Z";
         dislikes = 0;
         image = "https://s3.ap-south-1.amazonaws.com/comut/678ee8f8-e48a-4076-9b24-7d4c97b7163b";
         isDisLikes = 0;
         isLikes = 0;
         likes = 0;
         text = "Enter Description and the new year will be the first step of the game and for all of your ";
         updateat = "2017-10-07T10:26:19.470Z";
         };
         firstname = himdwq;
         image = "https://s3.ap-south-1.amazonaws.com/comut/412c6677-3910-4d48-a7b4-cff49ac9e31c";
         lastname = ferret;
         username = teeter;
         
         dislike active
         Like Active
         Like
         dislike
 */
        
        print(self.articleDict)
        
        if  self.articleDict.count != 0 {
            
           // print(articleDict["_id"] as? String!!)
            
            if let ID = articleDict["_id"] as? String {
                
                self.artcileID = ID
            }
            
            
            
            if  articleDict["isLikes"] as? NSNumber == 0 {
                
                self.likeCountOutlet.setImage(UIImage(named: "Like"), for: .normal)
                //self.dislikeCountOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                
                
            }else {
                
                
                self.likeCountOutlet.setImage(UIImage(named: "Like Active"), for: .normal)
               // self.dislikeCountOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                
            }
            
            if articleDict["isDisLikes"] as? NSNumber == 0{
                
               self.dislikeCountOutlet.setImage(UIImage(named: "dislike"), for: .normal)
            }else {
                
                self.dislikeCountOutlet.setImage(UIImage(named: "dislike active"), for: .normal)
            }
            
            let picURL = articleDict["image"] as? String
            
            let imageURL = NSURL(string: picURL!)
            let imageRequest = NSURLRequest(url: imageURL! as URL)
            NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                response, data, error in
                if error != nil {
                    //      println("Image not found!")
                }else {
                    // println("success")
                    let image = UIImage(data: data!)
                    self.articleImageView.image = image
                    
                }
            })
            
            let postimageURL = NSURL(string: postImage)
            let postimageRequest = NSURLRequest(url: postimageURL! as URL)
            NSURLConnection.sendAsynchronousRequest(postimageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                response, data, error in
                if error != nil {
                    //      println("Image not found!")
                }else {
                    // println("success")
                    let image = UIImage(data: data!)
                    self.userImageView.image = image
                    
                }
            })

            
           // createat = "2017-09-01T09:29:38.080Z
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
            
            if let dateString = articleDict["updateat"] as? String {
            
          let dateArr = dateString.components(separatedBy: ".")
            print(dateArr.first!)
            
            if let selectedDate = formatter.date(from: dateArr.first!) {
                formatter.dateFormat = "h:mm a MMM dd, yyyy"
                let date = formatter.string(from: selectedDate)
                self.postedDateLabel.text = date
                
            }
            }
            
            let likeCount = articleDict["likes"] as! NSNumber
            
            self.likeCountLabel.text = String(describing: likeCount)
            
            let disLikeCount = articleDict["dislikes"] as! NSNumber
            
            self.disLikeCountLabel.text = String(describing: disLikeCount)
            
            
            self.articleDiscriptionTextView.text = articleDict["text"] as! String
            
            self.fullNameLabel.text = self.fullName
            self.userNameLabel.text = self.userName
            
            
        }
        settingExtraFontsAndUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "ViewPost", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        self.articleDiscriptionTextView.isEditable = false
        articleImageView.contentMode = .scaleAspectFit
//        
//        //Setting Tap Gesture...
//        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.removeKeyboard(sender:)))
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        tapGesture.delegate = self
//        self.view.addGestureRecognizer(tapGesture)
//        
    }
    
    //MARK: Storyboard Action..
   @IBAction func buttonAction(sender: UIButton) {
    switch sender.tag {
        
    case 101:
        
        print("Like")
       if Singleton.sharedInstance.userID != self.userId && self.userId != ""{
        self.updateArticle.likeArticleAPI(ID: self.artcileID, UserID: self.userId, recievedResponse: { (succeeded, response) in
            
            if succeeded {
                
                let dataDict = response["data"] as? NSDictionary
                
                if  dataDict?["isLikes"] as? NSNumber == 0 {
                
                    self.likeCountOutlet.setImage(UIImage(named: "Like"), for: .normal)
                    self.dislikeCountOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                }else {
                    
                    self.likeCountOutlet.setImage(UIImage(named: "Like Active"), for: .normal)
                    self.dislikeCountOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                }
                
                
                
              //  let count = Int(self.likeCountLabel.text!)
                self.likeCountLabel.text = "\(String(describing: dataDict?["likes"] as! NSNumber))"
                 self.disLikeCountLabel.text = "\(String(describing: dataDict?["dislikes"] as! NSNumber))"
                
            }
        })
        }
        
    case 102:
        
        print("disLike")
        
          if Singleton.sharedInstance.userID != self.userId && self.userId != ""{
        
        self.updateArticle.disLikeArticleAPI(ID: self.artcileID, UserID: self.userId, recievedResponse: { (succeeded, response) in
            
            if succeeded {
                let dataDict = response["data"] as? NSDictionary
                
                if  dataDict?["isDislikes"] as? NSNumber == 0 {
                    
                    self.likeCountOutlet.setImage(UIImage(named: "Like"), for: .normal)
                    self.dislikeCountOutlet.setImage(UIImage(named: "dislike"), for: .normal)
                    
                    
                }else {
                    
                    self.likeCountOutlet.setImage(UIImage(named: "Like"), for: .normal)
                    self.dislikeCountOutlet.setImage(UIImage(named: "dislike active"), for: .normal)
                    
                }
                
                
                //let count = Int(self.disLikeCountLabel.text!)
                self.disLikeCountLabel.text = "\(String(describing: dataDict?["dislikes"] as! NSNumber))"
                self.likeCountLabel.text = "\(String(describing: dataDict?["likes"] as! NSNumber))"
                
            }
        })
        }
        
    case 103:
        
        let activityVC = UIActivityViewController(activityItems: ["dhdh "], applicationActivities: nil)
        
        //New Excluded Activities Code
        activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
        //
        
        activityVC.popoverPresentationController?.sourceView = sender as? UIView
       // let currentController = self.getCurrentViewController()
        
        self.present(activityVC, animated: true, completion: nil)
        
        print("share")
    default:
        
        break
    }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
