//
//  LoginDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 10/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation

import UIKit

extension  LoginViewController : UITextFieldDelegate, UIGestureRecognizerDelegate  {
    
    //MARK: Text fields delegates...
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.emailTextField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    
     //MARK: Gesture recognizer delegates...
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        //Removing Keyboard...
        self.view.endEditing(true)
        
        //Finding Which view is tapped....
        let points = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
        let newView = gestureRecognizer.view?.hitTest(points, with: nil)
        
        //Recognizing gesture if Tapped on Image view......
        if newView!.isKind(of: UIImageView.self) {
            
            
            return false
        }
        
        return true
    }
    
    
    //MARK: Login with Facebook....
    func loginWithFacebookAction() {
        
        //Removing All Facebook Data before New Login with Facebook...
        Singleton.sharedInstance.facebookId = ""
        Singleton.sharedInstance.facebookAccessToken = ""
        //        Singleton.sharedInstance.facebookBusinessToken = ""
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.FACEBOOK_DATA)
        
        //Removing Keyboard before Facebook Login....
        self.view.endEditing(true)
        
        
        //Login with Facebook....
        self.commoAPIModel.openFacebookControllerToFetchDataOfUser { (succeeded, values) in
            
            if succeeded {
                
                print(values)
                

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                    
                   // let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
                   // self.navigationController?.pushViewController(signUpVC, animated: true)
                    
                    self.commoAPIModel.facebookIDValidationCheckAPI(fb_ID: Singleton.sharedInstance.facebookAccessToken, recievedResponse: { (succeeded, response) in
                        
                        print(response)
                        
                        if succeeded {
                            
                           print(response)
                            
                            let DashBoardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                            self.navigationController?.pushViewController(DashBoardVC, animated: true)

                        } else {
                            
                       print(UserDefaults.standard.value(forKey: USER_DEFAULT.FACEBOOK_DATA) ?? "")
                            print( Singleton.sharedInstance.facebookId )
                            
                            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
                            self.navigationController?.pushViewController(signUpVC, animated: true)
                        }
                    })
                }
            }
        }
    }
    

    
}
