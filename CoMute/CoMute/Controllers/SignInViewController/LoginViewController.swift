//
//  LoginViewController.swift
//  CoMute
//
//  Created by himanshu aggarwal on 08/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var SplashImageView: UIImageView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButtonOutlet: UIButton!
    
    
    //MARK: Custom Views...
    var tapGesture : UITapGestureRecognizer!
    var helperModel = HelperModel()
    var commoAPIModel =  CommonAPIsModel()
    var internetConnectionModel: InternetConnectionModel!
    var userSignInModel = UserSignInModel()
     var userModel = UserModel()

    
    override func viewDidLoad() {
        super.viewDidLoad()

      settingExtraFontsAndUI()
        self.emailTextField.text! = ""
        self.passwordTextField.text! = ""
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.emailTextField.text! = ""
        self.passwordTextField.text! = ""
        
        if (UserDefaults.standard.value(forKey: USER_DEFAULT.ACCESS_TOKEN) ?? "") as? String != ""{
            
            let token = (UserDefaults.standard.value(forKey: USER_DEFAULT.ACCESS_TOKEN) ?? "") as? String
            
            userModel.getUserDetailAPI(token: token!, recievedResponse: { (succeeded, response) in
                
                if succeeded {
                    
                    let DashBoardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                    self.navigationController?.pushViewController(DashBoardVC, animated: true)
                    
                }
            })
            
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Setting Tap Gesture...
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.removeKeyboard(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
        registerButtonOutlet.layer.borderWidth = 1
        registerButtonOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
    }
    
    
    
    //MARK: Storyboard Actions...
    
    @IBAction func forgotPasswordAction(_ sender: Any) { //Forgot Password
        
        let forgetPasswordVc = self.storyboard?.instantiateViewController(withIdentifier: "FPSendPasswordViewController") as! FPSendPasswordViewController
        self.navigationController?.pushViewController(forgetPasswordVc, animated: true)
    }
    
    @IBAction func loginButtonAction(_ sender: Any) { //LoginButton
        
        

        var message:NSString = ""
        
        //Validation for Simple and Facebook Sign in....
        if Singleton.sharedInstance.facebookId.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) == "" {
            
            if(!Singleton.sharedInstance.isEmailValid(email: self.emailTextField.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines))) {
                
                if self.emailTextField.text == "" {
                    
                    message = "Please enter a username or Email Id"
                    
                } else if !Singleton.sharedInstance.isValidPassword(password: self.passwordTextField.text!) {
                    
                    message = "Password must be atleast 8 characters long!"
                }
                
            } else  {
                
                if !Singleton.sharedInstance.isEmailValid(email:self.emailTextField.text!.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines)) {
                    
                    message = "Please enter valid email or User Name"
                    
                }  else if !Singleton.sharedInstance.isValidPassword(password: self.passwordTextField.text!) {
                    
                    message = "Password must be atleast 8 characters long!"
                }
            }
        }
        
        //Validation Check....
        if(message.length > 0) {
            
            self.helperModel.showingAlertcontroller(title: "", message: message as String, cancelButton: "OK", receivedResponse: {})
            
        } else {
            
            userSignInModel.userSignInAPI(password: self.passwordTextField.text!, user_Name:  self.emailTextField.text!, user_Fb_ID: "", facebookEmailVerifiedFlag: false, recievedResponse: { (succeeded, response) in
                print(response)
                
                if succeeded{
                    
                    
                     print(response)
                    
                    self.emailTextField.text! = ""
                    self.passwordTextField.text! = ""
                    
                    let DashBoardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                    self.navigationController?.pushViewController(DashBoardVC, animated: true)
                    
                    
                }else {
                    
                    print(response)
                }
                

            })

        }
        
        
    }
    
    @IBAction func facebookConnectionAction(_ sender: Any) { //FacebookButton
        
        self.loginWithFacebookAction()
        
           }
    
    @IBAction func RegisterButtonAction(_ sender: Any) { // New User Signup
        
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    
    //MARK: Removing keyboard...And Setting zesture fopr zooming...
    func removeKeyboard(sender: UITapGestureRecognizer) {
        
        self.emailTextField.resignFirstResponder()
    }

}
