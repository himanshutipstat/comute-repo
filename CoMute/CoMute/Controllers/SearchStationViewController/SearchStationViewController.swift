//
//  SearchStationViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 16/11/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

  protocol searchStationViewControllerDelegate {
    
    func setLocationWithLatLong(station : String)
}

class SearchStationViewController: UIViewController {

    
    @IBOutlet weak var stationListTableView: UITableView!
    
    //MARK: Custom Views...
    var navigation: CustomizedDefaultNavigationBar!
    var getLIRRStationModel = LIRRModel()
    
    // Input data into the Array:
    var pickerData = [String]()
    var filteredDataArray = [String]()
    var selectedRows = [Bool]()
    
    //Delegate
    var delegate : searchStationViewControllerDelegate?
    
    //Search Controller...For Searching...
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingExtraFontsAndUI()
        
        stationListTableView.delegate = self
        stationListTableView.dataSource = self
        self.getLIRRStationModel.getAllLIRRStationAPI(ID: "") { (succeeded, response) in
            
            if succeeded {
                
                self.pickerData = (response["data"]  as? [String])!
                print(self.pickerData)
                self.stationListTableView.reloadData()
              
                
            }
        }
        
        self.automaticallyAdjustsScrollViewInsets = false
        stationListTableView.backgroundColor = UIColor(red: 255/250, green: 255/250, blue: 255/250, alpha: 0.1)
        
        if selectedRows.count == 0 {
            for _ in pickerData{
                
                selectedRows.append(false)
            }
        }
        
        print(self.pickerData.count)

        // Do any additional setup after loading the view.
    }
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: "Comute Stations", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.placeholder = "Search"
        self.searchController.dimsBackgroundDuringPresentation = false
        self.definesPresentationContext = true
        self.searchController.searchBar.barTintColor = COLOR_CODE.WHITE_COLOR
        
        //If available..in iOS 9.0
        if #available(iOS 9.0, *) {
            
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.classForCoder() as! UIAppearanceContainer.Type]).setTitleTextAttributes([NSForegroundColorAttributeName: COLOR_CODE.YELLOW_COLOR], for: UIControlState.normal)
            
           // UIBarButtonItem.appearanceWhenContainedInInstancesOfClasses([UISearchBar.classForCoder()]).setTitleTextAttributes([NSForegroundColorAttributeName: COLOR_CODE.YELLOW_COLOR], forState: UIControlState.Normal)
            
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.classForCoder() as! UIAppearanceContainer.Type]).font = UIFont(name: FONT.REGULAR, size: aspectHeight(height: 12))
            
            
        } else {
            
            // Fallback on earlier versions
        }
        stationListTableView.tableHeaderView = self.searchController.searchBar
        // tableView.tableHeaderView?.backgroundColor = UIColor.blackColor()
        // self.tableView.backgroundColor = UIColor.blackColor()
       // self.btnDone.hidden = true
        //  self.tableView.backgroundColor = UIColor.blackColor()
        //tableViewBottomConstrain.constant = 0
        stationListTableView.bounces = false
        // self.view.backgroundColor = UIColor.blackColor()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
