//
//  SearchStationDecorationPattren.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 16/11/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit



extension SearchStationViewController : CustomizedDefaultNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate, UISearchResultsUpdating{
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            self.navigationController?.popViewController(animated: true)
            
            
            
        }
    }
    
    //MARK: TableView Delegate & DataSource...

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchController.isActive && self.searchController.searchBar.text != "" {
            
            return self.filteredDataArray.count
        }
        
        return self.pickerData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        
        if let UnwrappedCell = tableView.dequeueReusableCell(withIdentifier: "Cell"){
            cell = UnwrappedCell
        }
        else{
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
        cell.backgroundColor = UIColor(red: 255/250, green: 255/250, blue: 255/250, alpha: 0.1)
        cell.textLabel?.textColor = COLOR_CODE.BLACK_COLOR
        cell.textLabel?.font = UIFont(name: FONT.REGULAR, size: aspectWidth(width: 13))
        cell.selectionStyle = .none
        
        let dataString: String
        
        //Setting either filtered or normal data...
        if self.searchController.isActive && self.searchController.searchBar.text != "" {
            
            dataString = (self.filteredDataArray[indexPath.row] as? String)!
            
        }else {
            
             dataString = (self.pickerData[indexPath.row] as? String)!
            
            
        }
        
        cell.textLabel?.text = dataString
        
        tableView.separatorStyle = .none
        //cell.imageView?.contentMode = .ScaleAspectFit
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // tableView.deselectRow(at: indexPath, animated: false)
        
       // let cell = tableView.cellForRow(at: indexPath)
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" {
            
            print( self.filteredDataArray[indexPath.row])
            let trainstation = self.filteredDataArray[indexPath.row]
            print(trainstation)
            
            self.delegate?.setLocationWithLatLong(station: "\(trainstation)" )
        
        }else{
             let trainstation = self.pickerData[indexPath.row]
            print(trainstation)
             self.delegate?.setLocationWithLatLong(station: "\(trainstation)")
           
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: UISearch Results Updating Delegate...
    
    func updateSearchResults(for searchController: UISearchController) {
        
        self.filterContentForSearchText(searchText: searchController.searchBar.text!)

    }

    
    
    //MARK:-UISearchBar Delegate...
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
        self.filterContentForSearchText(searchText: searchBar.text!)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func filterContentForSearchText(searchText: String) {
        
        //  _ = NSPredicate(format: "SELF contains[c] %@", searchText)
        //let filteredArray = (self.jobTypeNameArray as NSArray).filteredArrayUsingPredicate(searchResult)
        //filteredDataArray = filteredArray as! [String]
        
        //self.filteredDataArray = pickerData.filter { $0.pickerData.contains(where: { petArr.contains($0) }) }
        
       filteredDataArray = pickerData.filter{($0).contains(searchText) }
        print(filteredDataArray)
        
        if filteredDataArray.count > 0 && ((searchText as NSString).length > 0) {
            
            stationListTableView.reloadData()
            
        } else {
            if searchText == "" {
                
                stationListTableView.reloadData()
                
                if pickerData.count > 0 {
                    
                    self.stationListTableView.backgroundView = nil
                }
                
            } else {
                stationListTableView.reloadData()
            }
        }
        
    }
    
    
}
