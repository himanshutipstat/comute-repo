//
//  ComuteFriendListViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 07/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class ComuteFriendListViewController: UIViewController {
    
    @IBOutlet weak var comuteFriendListTableView: UITableView!
    
    @IBOutlet weak var syncedView: UIView!
    @IBOutlet weak var syncedFbButtonOutlet: UIButton!
    @IBOutlet weak var syncedContactButtonOutlet: UIButton!
    
    //MARK: Custom Views...
    var navigation: CustomizedDefaultNavigationBar!
    
    //HelperModel
    var helperModel = HelperModel()
    var getComutePeople = ComuteFriends()
    var comuteListArray = NSArray()
    var refresher: UIRefreshControl!
    var deleteIndexPath = Int()
    var userID = String()
    var userFullName = String()
    var userName = String()
    var userImage = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingExtraFontsAndUI()

        //Regestring table view class...
        
        self.comuteFriendListTableView.register(ComuteFriendListTableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        self.comuteFriendListTableView.register(UINib(nibName: "ComuteFriendListTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        self.comuteFriendListTableView.tableHeaderView?.backgroundColor = UIColor.lightGray
        
        getComutePeople.getComuteFriendsAPI(ID: "") { (succeeded, response) in
            
            
            if succeeded {
                
                print(response)
                
                if let arrayList = response["data"] as? NSArray{
                    
                    self.comuteListArray = arrayList
                    self.comuteFriendListTableView.reloadData()
                }
            }
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "WhiteBlank")!], buttonHighlightedImage: [UIImage(named: "WhiteBlank")!], numberOfButtons: 2, barTitle: "CoMute Friends", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        
        syncedView.layer.borderWidth = 1
        syncedView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        syncedFbButtonOutlet.layer.borderWidth = 1
        syncedFbButtonOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        syncedContactButtonOutlet.layer.borderWidth = 1
        syncedContactButtonOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        refresher = UIRefreshControl()
        //refresher.attributedTitle = NSAttributedString(string: "pull to refresh")
        
        refresher.addTarget(self, action: #selector(ComuteFriendListViewController.refresh), for: UIControlEvents.valueChanged)
        self.comuteFriendListTableView.addSubview(refresher)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
