//
//  ComuteFriendDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 07/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit


extension ComuteFriendListViewController : CustomizedDefaultNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate{
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            
            
        }
    }
    
    //MARK: TableView Delegate & DataSource...
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.comuteListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ComuteFriendListTableViewCell
        cell.chatButtonOutlet.addTarget(self, action: #selector(ComuteFriendListViewController.chatAction(sender:)), for: .touchUpInside)
        
        if let dataDic = self.comuteListArray[indexPath.row] as? NSDictionary {
       /*
            data =     (
                {
                    "_id" = 59a9290ce03e073f83fbc200;
                    contact = 1213213;
                    firstname = akilesh;
                    image = "https://s3.ap-south-1.amazonaws.com/comut/29edbe30-01b4-43af-a6ca-ac4f474e8b63";
                    lastname = v;
                    username = akilesh7;
                }
            );*/
            
//            if let friendCheck = dataDic["isFriend"] as? NSNumber {
//                
//                
//                if friendCheck == 1 {
//                    cell.chatButtonOutlet.isHidden = false

            
            if let imageurl = dataDic["image"] as? String {
                
                let imageURL = NSURL(string: imageurl)
                let imageRequest = NSURLRequest(url: imageURL! as URL)
                NSURLConnection.sendAsynchronousRequest(imageRequest as URLRequest, queue: OperationQueue.main, completionHandler: {
                    response, data, error in
                    if error != nil {
                        
                        //
                        
                    }else {
                        // println("success")
                        let image = UIImage(data: data!)
                        cell.userImageView.image = image
                        //self.spImageView.layer.cornerRadius = 45
                    }
                })
            }
                if let fName = dataDic["firstname"] as? String {
                    
                    if let lName = dataDic["lastname"] as? String {
                        
                        cell.userNameLabel.text = fName+" "+lName
                    }
                }
                }
//            }else {
//                
//               
//            }
//            if let friendCheck = dataDic["isFriend"] as? NSNumber {
//                
//                
//                if friendCheck == 1 {
//                    cell.chatButtonOutlet.isHidden = false
//                    
//                }else {
//                    
//                   cell.chatButtonOutlet.isHidden = true
//                }
//            }
        
            
       // }
        cell.backgroundColor = UIColor.white
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ComuteProfileViewContoller") as! ComuteProfileViewContoller
        userProfileVC.isCurrentUser = false
        userProfileVC.notificationFlag = 0
         if let dataDic = self.comuteListArray[indexPath.row] as? NSDictionary {
            
            if let id = dataDic["_id"] as? String {
                
                userProfileVC.userID = id
                self.userID = id
                
            }
            
            if let fName = dataDic["firstname"] as? String {
                
                if let lName = dataDic["lastname"] as? String {
                    
                   userProfileVC.userFullName  = fName+" "+lName
                    self.userFullName = fName+" "+lName

                }
            }
            if let userName = dataDic["username"] as? String{
                
                self.userName = userName
                
            }
            if let imageurl = dataDic["image"] as? String {
                
                self.userImage = imageurl
            }
             userProfileVC.isUserFriend = true
            
//            if let friendCheck = dataDic["isFriend"] as? NSNumber {
//                
//                
//                if friendCheck == 1 {
//                    userProfileVC.isUserFriend = true
//                    
//                }else {
//                    
//                    userProfileVC.isUserFriend = false
//                    
//                }
//            }
//            
//            if let requestCheck = dataDic["isrequest"] as? NSNumber {
//                
//                if requestCheck == 1 {
//                    
//                    userProfileVC.isFriendRequested = true
//                }else {
//                    
//                    userProfileVC.isFriendRequested = false
//                }
//                
//            }
            
            self.navigationController?.pushViewController(userProfileVC, animated: true)
        }
        
       // cell.chatButtonOutlet.
        
    }
    
    
    
        func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
            
            //let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ComuteFriendListTableViewCell
            if let dataDic = self.comuteListArray[indexPath.row] as? NSDictionary {
                
                //if let friendCheck = dataDic["isFriend"] as? NSNumber {
                    
                    
                  //  if friendCheck == 1 {
                      
                        let remove = UITableViewRowAction(style: .normal, title: "Remove Friend") { action, index in
                            self.deleteIndexPath = indexPath.row
                            
                             if let id = dataDic["_id"] as? String {
                            self.getComutePeople.removeComuteFriendsAPI(ID: id, recievedResponse: { (succeeded, response) in
                                
                                if succeeded {
                                   

                                
                                  self.refresh()
                                }
                                
                            })
                            
                            }
                        }
                            
                        remove.backgroundColor = UIColor.red
                        
                        return[remove]
                   // }else {
                        
//                        if let requestCheck = dataDic["isrequest"] as? NSNumber {
//                            
//                            if requestCheck == 1 {
//                                
//                                  let pendingRequest = UITableViewRowAction(style: .normal, title: "Request Pending") { action, index in
//                                    
//                                }
//                                pendingRequest.backgroundColor = UIColor.lightGray
//                                
//                                
//                                return[pendingRequest]
//
//                                
//                            }else {
//                        
//                        let addFriend = UITableViewRowAction(style: .normal, title: "Add Friend") { action, index in
//                            
//                            if let id = dataDic["_id"] as? String {
//                                self.getComutePeople.requestComuteFriendsAPI(ID: id, message: "Hello from the other side.", recievedResponse: { (succeeded, response) in
//                                    
//                                    if succeeded {
//                                        
//                                        
//                                        
//                                        self.refresh()
//                                    }
//                                    
//                                })
//                                
//                            }
//                            
//                        }
//                        addFriend.backgroundColor = UIColor.blue
//                                
//                        
//                        return[addFriend]
//                                
//                            }
//                        
//                    }
                    
                  //  }
             //   }

                
            }
            
    
           
    
            return []
        }
    
        func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            // the cells you would like the actions to appear needs to be editable
            return true
        }
    
    func refresh(){
        
        print("refreshed")
        self.refresher.endRefreshing()
        
        self.getComutePeople.getComuteFriendsAPI(ID: "") { (succeeded, response) in
            
            
            if succeeded {
                
                print(response)
                
                if let arrayList = response["data"] as? NSArray{
                    
                    self.comuteListArray = arrayList
                    self.comuteFriendListTableView.reloadData()
                }
            }
        }
    }
    
    func chatAction(sender: UIButton){
        
        
        if let cell = sender.superview?.superview as? ComuteFriendListTableViewCell {
            let indexPath = comuteFriendListTableView.indexPath(for: cell)
            
             if let dataDic = self.comuteListArray[(indexPath?.row)!] as? NSDictionary {
        
        
        let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatVC.userId = (dataDic["_id"] as? String)!
        chatVC.recieverUserName = self.userName
                
                if let fName = dataDic["firstname"] as? String {
                    
                    if let lName = dataDic["lastname"] as? String {
                        
                        chatVC.recieverUserFullName = fName+" "+lName
                        
                    }
                }

        chatVC.recieverImage =  (dataDic["image"] as? String)!
        self.navigationController?.pushViewController(chatVC, animated: true)
            }
        }
        
    }
    
}
