//
//  MyCoMuteViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class MyCoMuteViewController: UIViewController {
    
    
    
    
    @IBOutlet weak var stationView: UIView!
    @IBOutlet weak var swapButtonOutlet: UIButton!
    @IBOutlet weak var myCoMuteTableView: UITableView!
    @IBOutlet weak var fromStationField: UITextField!
    @IBOutlet weak var toStationField: UITextField!
    
    @IBOutlet weak var stationPickerView: UIPickerView!
    @IBOutlet weak var pickerViewBottomConstrain: NSLayoutConstraint!
   
    @IBOutlet weak var fromStationButtonOutlet: UIButton!
    @IBOutlet weak var pickerParentView: UIView!
    @IBOutlet weak var toStationButtonOutlet: UIButton!
    
    //MARK: Custom Views...
    var navigation: CustomizedDefaultNavigationBar!
    var getLIRRStationModel = LIRRModel()
    
    var tripID = String()
    var sourceSeq = NSNumber()
    var destinationSeq = NSNumber()
    
    // Input data into the Array:
    var pickerData = NSArray()
     var flag = 0
    var allCoMuteTrainStatus = NSArray()
  

    override func viewDidLoad() {
        super.viewDidLoad()

        settingExtraFontsAndUI()
        

         self.stationPickerView.isExclusiveTouch = true
        //Regestring table view class...
        
        self.myCoMuteTableView.register(MyCoMuteTableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        self.myCoMuteTableView.register(UINib(nibName: "MyCoMuteTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        self.myCoMuteTableView.tableHeaderView?.backgroundColor = UIColor.lightGray
        
        self.fromStationField.text =  Singleton.sharedInstance.userHomeStation
        self.toStationField.text =  Singleton.sharedInstance.userDestinationStation
        
        self.getLIRRStationModel.getLIRRStationETAAPI(sourceStation:  Singleton.sharedInstance.userHomeStation, destinationData: Singleton.sharedInstance.userDestinationStation) { (succeeded, response) in
            
            if succeeded {
                
                self.allCoMuteTrainStatus = (response["data"] as? NSArray)!
                self.myCoMuteTableView.reloadData()
                
            }
            
        }
        
    
//        
//        self.getLIRRStationModel.getAllLIRRStationAPI(ID: "") { (succeeded, response) in
//            
//            if succeeded {
//                
//                self.pickerData = (response["data"]  as? NSArray)!
//                print(self.pickerData)
//                self.stationPickerView.reloadAllComponents()
//                
//            }
//        }

         (UIApplication.shared.delegate as! AppDelegate).landscapeOrientationChangeFlag = false
      

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        
//        self.myComute.getMyComuteArticleAPI(ID: "") { (suceeded, response) in
//            
//            if suceeded {
//                
//                print(response)
//                
//            }
//        }
//    }
    
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "WhiteBlank")!], buttonHighlightedImage: [UIImage(named: "WhiteBlank")!], numberOfButtons: 2, barTitle: "MyCoMute", alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        stationView.layer.borderWidth = 1
        stationView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        swapButtonOutlet.layer.borderWidth = 1
        swapButtonOutlet.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor

        self.fromStationField.isUserInteractionEnabled = false
        self.toStationField.isUserInteractionEnabled = false

        
    }
    
    //MARK: StoryBoard Action..
    
    
    @IBAction func fromStationPickerOpen(_ sender: Any) {
        
        fromStationButtonOutlet.isExclusiveTouch = true
        
        
        let searchStationVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchStationViewController") as! SearchStationViewController
            searchStationVC.delegate = self
        
        self.navigationController?.pushViewController(searchStationVC, animated: true)
        
        self.flag = 1
        
//        // if self.pickerViewBottomConstrain.constant == -220 {
//        
//        //UIApplication.shared.delegate?.window!!.addSubview(pickerParentView)
//        print(pickerParentView.frame)
//        fromStationField.becomeFirstResponder()
//        
//        UIView.animate(withDuration: 0.15, animations: {
//            
//            self.pickerViewBottomConstrain.constant = 0
//            
//            
//        }, completion: {finished in
//            
//            
//        }
//        )

    }
    
    @IBAction func closePickerViewAction(_ sender: Any) {
        
        self.pickerViewBottomConstrain.constant = -220
        
        self.getLIRRStationModel.getLIRRStationETAAPI(sourceStation: self.fromStationField.text!, destinationData: self.toStationField.text!) { (succeeded, response) in
            
            if succeeded {
                
                self.allCoMuteTrainStatus = (response["data"] as? NSArray)!
                self.myCoMuteTableView.reloadData()
                
            }
            
        }
    }
    
    @IBAction func swpButtonAction(_ sender: Any) {
        
        self.getLIRRStationModel.getLIRRStationETAAPI(sourceStation: self.toStationField.text!, destinationData:  self.fromStationField.text! ) { (succeeded, response) in
            
            if succeeded {
                
                self.allCoMuteTrainStatus = (response["data"] as? NSArray)!
                self.myCoMuteTableView.reloadData()
                
                let thirdVar = self.fromStationField.text!
                
                self.fromStationField.text! = self.toStationField.text!
                
                self.toStationField.text! = thirdVar
                
            }
            
        }
        
        print(self.fromStationField.text!,self.toStationField.text!)
        
        
       
        
    }
    
    @IBAction func toStationPickerOpen(_ sender: Any) {
        
        toStationButtonOutlet.isExclusiveTouch = true
        
        let searchStationVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchStationViewController") as! SearchStationViewController
        searchStationVC.delegate = self
        
        self.navigationController?.pushViewController(searchStationVC, animated: true)
        
        self.flag = 2
//        print(pickerParentView.frame)
//        
//        toStationField.becomeFirstResponder()
//        
//        // UIApplication.shared.delegate?.window!!.addSubview(pickerParentView)
//        //if self.pickerViewBottomConstrain.constant == -220 {
//        
//        UIView.animate(withDuration: 0.15, animations: {
//            
//            self.pickerViewBottomConstrain.constant = 0
//            
//            
//        }, completion: {finished in
//            
//            
//        }
//        )

        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
