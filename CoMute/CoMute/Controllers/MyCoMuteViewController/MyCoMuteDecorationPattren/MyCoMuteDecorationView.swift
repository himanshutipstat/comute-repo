//
//  MyCoMuteDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/06/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit


extension MyCoMuteViewController : CustomizedDefaultNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,searchStationViewControllerDelegate {
    
      //MARK: Station Delegate ..
    
    func setLocationWithLatLong(station : String) {
        print(station)
        
                if flag == 1{
        
                    self.fromStationField.text = station
                }else {
        
                    self.toStationField.text = station
                    
                }
        
        self.getLIRRStationModel.getLIRRStationETAAPI(sourceStation:  self.fromStationField.text!, destinationData: self.toStationField.text!) { (succeeded, response) in
            
            if succeeded {
                
                self.allCoMuteTrainStatus = (response["data"] as? NSArray)!
                self.myCoMuteTableView.reloadData()
                
            }
        }
    }
    
    
//    func setLocationWithLatLong(station: String) {
//        
//        if flag == 1{
//            
//            self.fromStationField.text = station
//        }else {
//            
//            self.toStationField.text = station
//            
//        }
//    }

    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
            
            
            
        }
    }
  
    
    
    
    //MARK: Picker Delegates....
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.flag == 1 {
            

            //self.pickerView(self.stationPickerView, titleForRow: self.stationPickerView.selectedRow(inComponent: 0), forComponent: 0)
            
            self.fromStationField.text = pickerData[row] as? String
            
            print(pickerData[row],row)
            
        }else if self.flag == 2  {
            
            self.toStationField.text = pickerData[row] as? String
        }
        //self.chooseTypeOutlet.setTitle(pickerData[row], for: .normal)
        return pickerData[row] as? String
    }
    
    
    
    //MARK: TableView Delegate & DataSource...
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.allCoMuteTrainStatus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MyCoMuteTableViewCell
        cell.backgroundColor = UIColor.white
        cell.selectionStyle = .none
        
        if let stationdic = self.allCoMuteTrainStatus[indexPath.row] as? NSDictionary {
            
            cell.ComuteDataDictionary = stationdic
            cell.firstStationLabel.text = self.fromStationField.text!
            cell.secondStationLabe.text  = self.toStationField.text!
            
            cell.mainComuteView.backgroundColor = UIColor(red: 171/255, green: 213/255, blue: 166/255, alpha: 1.0)
            cell.statusImageView.image = UIImage(named: "on time")!
        }
        //        if indexPath.row == 1 {
        //
        //            cell.mainComuteView.backgroundColor = UIColor(red: 251/255, green: 193/255, blue: 194/255, alpha: 1.0)
        //
        //            cell.statusImageView.image = UIImage(named: "Hourglass")!
        //        }
        //        if indexPath.row == 2 {
        //            cell.mainComuteView.backgroundColor = UIColor(red: 252/255, green: 122/255, blue: 125/255, alpha: 1.0)
        //
        //            cell.statusImageView.image = UIImage(named: "Hourglass")!
        //
        //        }
        //        if indexPath.row == 3 {
        //
        //            cell.mainComuteView.backgroundColor = UIColor(red: 255/255, green: 57/255, blue: 66/255, alpha: 1.0)
        //
        //            cell.statusImageView.image = UIImage(named: "Skull big")!
        //        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let comuteStatusVC = self.storyboard?.instantiateViewController(withIdentifier: "ComuteDetailedViewController") as! ComuteDetailedViewController
        comuteStatusVC.originAddress = self.fromStationField.text!
        comuteStatusVC.destinationAddress = self.toStationField.text!
        if let stationdic = self.allCoMuteTrainStatus[indexPath.row] as? NSDictionary {
            
            if let sourceDic = stationdic["sorce"] as? NSDictionary {
                
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
                
                if let arrivTime = sourceDic["arrival_time"] as? String {
                    
                    comuteStatusVC.arrivalTimeString = arrivTime
                    comuteStatusVC.fromStation = self.fromStationField.text!
                    
                    
                    
                    if let selectedDate = formatter.date(from: arrivTime) {
                        
                      //  formatter.dateFormat = "h:mm a"
                       // let date = formatter.string(from: selectedDate)
                        
                       // comuteStatusVC.scheduleTimeLabel.text = date
                        
                        let dateRangeStart = Date()
                        let cuurentDateString = formatter.string(from: dateRangeStart)
                        
                        formatter.dateFormat = "HH:mm:ss"
                        formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
                        
                        let currentDate = formatter.date(from: cuurentDateString)
                        
                        let components = Calendar.current.dateComponents([.hour, .minute, .second], from: currentDate!, to: selectedDate)
                        
                        print(currentDate!)
                        print(selectedDate)
                       
                        
                        print(components.hour ?? 0)
                        print(components.minute ?? 0)
                        print(components.second ?? 0)
                        
                        comuteStatusVC.etaText = " - \(String(describing: components.hour!)) : \(String(describing: components.minute!)) : \(String(describing: components.second!)) "
                        
                        
                    }
                    
                    
                    
                }
                
                
            }
            
            if let locationDic = stationdic["location"] as? NSDictionary {
                
               if let destLoc = locationDic["dest"] as? NSDictionary{
                
                comuteStatusVC.longitude2 = destLoc["lon2"] as! double_t
                comuteStatusVC.latitude2 = destLoc["lat2"] as! double_t
                }
                
               if let sorcLoc = locationDic["sorce"] as? NSDictionary{
                
                comuteStatusVC.latitude1 = sorcLoc["lat1"] as! double_t
                comuteStatusVC.longitude1 = sorcLoc["lon1"] as! double_t                }
                
                if let destinationDic = stationdic["dest"] as? NSDictionary {
                    
                    
                    let tripId = destinationDic["trip_id"] as? String
                    comuteStatusVC.tripID = tripId!
                    
                    let destSeq = destinationDic["stop_sequence"] as? NSNumber
                    comuteStatusVC.destinationSeq = destSeq!
                    
                    
                }
                
                if let sourceDic = stationdic["sorce"] as? NSDictionary{
                    
                    let sorceSeq = sourceDic["stop_sequence"] as? NSNumber
                    
                    comuteStatusVC.sourceSeq = sorceSeq!
                }
                    
                
                self.getLIRRStationModel.getLIRRLinesCordinatesAPI(tripID: comuteStatusVC.tripID, startSeq: "\(comuteStatusVC.sourceSeq)", stopSeq: "\(comuteStatusVC.destinationSeq)") { (succeeded, response) in
                    
                    
                    if succeeded {
                        
                        print(response)
                        
                        comuteStatusVC.locationCordArray = response["data"]  as! NSArray
                        
                        self.navigationController?.pushViewController(comuteStatusVC, animated: true)                    }
                }
                
                
                
            }
            
        }
       
        
        
    
    }
    
}
