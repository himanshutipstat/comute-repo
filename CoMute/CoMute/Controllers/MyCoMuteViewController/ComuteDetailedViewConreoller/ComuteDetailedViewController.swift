//
//  ComuteDetailedViewController.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 10/07/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import GoogleMaps
import SocketIO
import CoreLocation

class MarkerModel: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String
    
    init(position: CLLocationCoordinate2D, name: String) {
        self.position = position
        self.name = name
    }
}

class ComuteDetailedViewController: UIViewController {
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var searchTextView: UITextField!
    @IBOutlet weak var comuteUserCollectionView: UICollectionView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var etaLabel: UILabel!
    @IBOutlet weak var wtfOutlet: UIButton!
    @IBOutlet weak var scheduleTimeLabel: UILabel!
    @IBOutlet weak var nearestStationLabel: UILabel!
    
    
    
    @IBOutlet weak var customMapView: GMSMapView!
    @IBOutlet weak var customMapLeadingConstant: NSLayoutConstraint!
    @IBOutlet weak var coustomMapWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var coustomMapHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var coustomMapTopConstant: NSLayoutConstraint!
    
    
    
    //MARK: Custom Views...
    var navigation: CustomizedDefaultNavigationBar!
    
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    
    var selectedRoute: Dictionary<NSObject, AnyObject>!
    
    var overviewPolyline: Dictionary<NSObject, AnyObject>!
    
    var originCoordinate: CLLocationCoordinate2D!
    
    var destinationCoordinate: CLLocationCoordinate2D!
    
    var originAddress = String()
    
    var destinationAddress = String()
    var latitude1 = 40.75058
    var longitude1  = -73.99357999999999
    var latitude2 = 40.74584
    var longitude2 = -73.90297
    var getCordinates = LIRRModel()
    let camera = GMSCameraPosition.camera(withLatitude: 40.792240, longitude: -73.138260, zoom: 8.0)
    //var mapView = GMSMapView()
    var locationCordArray = NSArray()
    var subLocCordArray = NSArray()
    var wtfStationCoordinates = NSArray()
    var comuteUserCoordinates = NSArray()
    
    var tripID = String()
    var sourceSeq = NSNumber()
    var destinationSeq = NSNumber()
    
    var mapLandScapeView = MapLandScapeView()
    //Socket Variables & Constants
    let socket = SocketIOClient(socketURL: NSURL(string: SERVER_ADDRESS.Socket_URL)! as URL, config:  [.log(true), .forcePolling(true)])
    
    let locationManager = CLLocationManager()
    var latitude  = String()
    var longitude = String()
    var isAuthorized = true
    var GeoAngle = 0.0
    var currentmarker = GMSMarker()
    var userDetailedMapView = UserMapDetailedView()
    var clusterManager: GMUClusterManager!
    
    var etaText = String()
    var arrivalTimeString = String()
    var myTimer =  Timer()
    var fromStation = String()
    
    let kClusterItemCount = 10000
    let kCameraLatitude = -33.8
    let kCameraLongitude = 151.2
    
    let iconGenerator = GMUDefaultClusterIconGenerator()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
       // comuteUserCollectionView.dataSource = self
       // comuteUserCollectionView.delegate = self
        
        //mapView.delegate = self
        customMapView.delegate = self
        
          myTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(ComuteDetailedViewController.update), userInfo: nil, repeats: true)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
        if let selectedDate = formatter.date(from: arrivalTimeString) {
            
            formatter.dateFormat = "h:mm a"
            let date = formatter.string(from: selectedDate)
            self.scheduleTimeLabel.text = "SCHEDULE:"+date
            
        }
        //RunLoop.current.add(myTimer, forMode: RunLoopMode.commonModes)
        
        UIView.animate(withDuration: 0.15, animations: {
            
            self.isAuthorizedtoGetUserLocation()
            
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            }
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.startUpdatingLocation()
            
            // Set up the cluster manager with default icon generator and renderer.
            
            let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
            let renderer = GMUDefaultClusterRenderer(mapView: self.customMapView, clusterIconGenerator: self.iconGenerator)
            self.clusterManager = GMUClusterManager(map: self.customMapView, algorithm: algorithm, renderer: renderer)
            
            // Generate and add random items to the cluster manager.
            self.generateClusterItems()
            
            // Call cluster() after items have been added to perform the clustering and rendering on map.
            self.clusterManager.cluster()
            
            // Register self to listen to both GMUClusterManagerDelegate and GMSMapViewDelegate events.
            self.clusterManager.setDelegate(self, mapDelegate: self)
            
            
        }, completion: {finished in
            
              self.setWTFIcon()
        }
        )
        
       
       // locationManager.startUpdatingHeading()
        
        //self.mapView = GMSMapView.map(withFrame: CGRect(x: 15, y: 90, width: 345, height: 230), camera: self.camera)
        
        //let cameraPosition = GMSCameraPosition.camera(withLatitude: self.latitude1, longitude: self.longitude1, zoom: 15.0)
        self.customMapView.camera = camera
        //self.customMapView.
        
        self.etaLabel.text = "ETA"+etaText
        
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        self.setMapView()
        
        /*
         
         data =     (
         {
         loc =             (
         "-73.75054",
         "40.60913"
         );
         }
         );
         */
       
        
        self.socket.connect()
        let lac = ["lat": latitude,
                   "lon": longitude ]
        //MARK: Socket to Make Connection and Emit Registration
        let par = ["authorization": Singleton.sharedInstance.userAccessToken,
                   "location": lac] as [String : Any]
        print(par)
        
        //self.socket.emit("add user", with: [par])
        //   self.socket.emitWithAck("add user", with: [par])
        self.socket.on("connect") {[weak self] data, ack in
            
            print("ACK.......\(data), \(ack)")
            
            print("socket connected")
            
            self?.isAuthorized = true
            
            self?.socket.emit("add user", with: [par])
            
            return
        }
        
        
        
        self.socket.on("reconnect") {[weak self] data, ack in
            
            print("ACK.......\(data), \(ack)")
            
            print("socket reconnected")
            
            self?.socket.connect()
            
            // self?.socket.emit("add user", with: [par])
            
            return
        }
        
        // Check if User is Unauthorized
        self.socket.on("disconnect") { (data, ack) -> Void  in
            print("unauthorized")
            self.isAuthorized = false
            
            // print("data u\(data)")
            
            //  println("un :\(ack)")
            return
        }
	
        
        
        //MARK: Socket To Recieve Send Message ACK....
        self.socket.on("wtf"){ (data, ack) -> Void in
            
            
            print("ACK.......\(data), \(ack)")
            print(data)
            self.setWTFIcon()
            
           // let newdic = data.first as! NSDictionary
            
            
            
        }
        
//        self.socket.on("getlocation"){ (data, ack) -> Void in
//            
//            /* ACK.......[{
//             id = 599bd68193b8e77760a372bb;
//             image = "https://s3.ap-south-1.amazonaws.com/comut/a80bb762-db0d-4e0b-a6ca-9d21a3c9b83d";
//             location =     {
//             lat = "40.0471";
//             lon = "-73.95388";
//             };
//             }]*/
//            print("ACK.......\(data), \(ack)")
//            print(data)
//            
//             self.comuteUserCoordinates = (data as? NSArray)!
//            
//        }
        
        //        self.getCordinates.getLIRRLinesCordinatesAPI(tripID: tripID, startSeq: "\(sourceSeq)", stopSeq: "\(destinationSeq)") { (succeeded, response) in
        //
        //
        //            if succeeded {
        //
        //                print(response)
        //
        //                self.locationCordArray = response["data"]  as! NSArray
        //
        //
        //            }
        //        }
        //
        //        self.getCordinates.getLIRRStationCordinatesAPI(sourceStation: "") { (succeeded, response) in
        //
        //            if succeeded {
        //
        //                self.locationCordArray = response["data"]  as! [NSArray]
        //                print(self.locationCordArray.count)
        //
        //
        //                self.setMapView()
        //            }
        //
        //
        //        }
        
        //self.view.addSubview(self.mapView)
        
        settingExtraFontsAndUI()
        
        //®self.customMapView.clear()
        
        
        //        // Creates a marker in the center of the map.
        //        let marker = GMSMarker()
        //        marker.position = CLLocationCoordinate2D(latitude: 73.9935, longitude: 73.9935)
        //        marker.title = "Sydney"
        //        marker.snippet = "Australia"
        //      marker.map  =  self.customMapView
        //
        //        let path = GMSMutablePath()
        //        path.add(CLLocationCoordinate2D(latitude: 73.9935, longitude: 73.9935))
        //       // path.add(CLLocationCoordinate2D(latitude: -33.70, longitude: 151.40))
        //        path.add(CLLocationCoordinate2D(latitude: 37.4299, longitude: 122.2539))
        //        let polyline = GMSPolyline(path: path)
        //        polyline.map = self.customMapView
        //        polyline.strokeColor = .green
        //        polyline.strokeWidth = 10.0
        //        polyline.geodesic = true
        
        //customMapView.delegate = self
        //customMapView.settings.myLocationButton = true
        // customMapView.isMyLocationEnabled = true
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //Change Orientation Flag...
        (UIApplication.shared.delegate as! AppDelegate).landscapeOrientationChangeFlag = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Change Orientation Flag...
        (UIApplication.shared.delegate as! AppDelegate).landscapeOrientationChangeFlag = false
        myTimer.invalidate()
        self.socket.disconnect()
       // self.customMapView.removeFromSuperview()
        
        
    }
    
    
    //MARK: Setting Extra Fonts and UI...
    func settingExtraFontsAndUI() {
        
        
        //Adding Navigation Bar...
        navigation = CustomizedDefaultNavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 70), buttonImage: [UIImage(named: "btn_back")!], buttonHighlightedImage: [UIImage(named: "btn_back")!], numberOfButtons: 2, barTitle: self.originAddress+" to "+self.destinationAddress as NSString, alignmentOfTitle: "Center", indexOFButtonToSetTitle: 2)
        
        navigation!.delegateNavigation = self
        
        self.view.addSubview(navigation!)
        
        statusView.layer.borderWidth = 1
        statusView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        searchTextView.layer.borderWidth = 1
        searchTextView.layer.borderColor = COLOR_CODE.LIGHT_GRAY_BOUND_COLOR.cgColor
        
        //Registering Collection View Classes...
        //self.comuteUserCollectionView.register(UserCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "Cell")
       // self.comuteUserCollectionView.register(UINib(nibName: "UserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
//        if UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown {
//            
//            self.mapLandScapeView.removeFromSuperview()
//        }
        if UIDevice.current.orientation.isLandscape {
            
            print("Landscape")
            
            self.myTimer.invalidate()
            
            self.mapLandScapeView = UINib(nibName: "MapLandScapeView", bundle: nil).instantiate(withOwner: MapLandScapeView.self, options: nil)[0] as! MapLandScapeView
            
            //self.mapLandScapeView.deligate = self
            mapLandScapeView.frame = CGRect(x: 0  , y: 0, width: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.width), height: Int(GLOBAL_KEY.CURRENT_DEVICE_SIZE.height) )
            
            self.mapLandScapeView.latitude1 = latitude1
            self.mapLandScapeView.latitude2 = latitude2
            self.mapLandScapeView.longitude1 = longitude1
            self.mapLandScapeView.longitude2 = longitude2
            self.mapLandScapeView.locationCordArray = locationCordArray
            self.mapLandScapeView.navigationTitleLabel.text = self.originAddress+" to "+self.destinationAddress
            self.mapLandScapeView.clipsToBounds = true
            self.mapLandScapeView.wtfStationCoordinates = self.wtfStationCoordinates
           // self.mapLandScapeView.comuteUserCoordinates = self.comuteUserCoordinates
            self.mapLandScapeView.delegate = self
            
            
            self.view.addSubview(mapLandScapeView)
            
            self.mapLandScapeView.setMap()
            self.mapLandScapeView.setWTFIcon()
            self.mapLandScapeView.generateClusterItems()
           // self.mapLandScapeView.setUserMap()
            //(UIApplication.shared.delegate as! AppDelegate).landscapeOrientationChangeFlag = false
            
            //
            
            
        } else {
            
            self.mapLandScapeView.removeFromSuperview()
            self.myTimer.fire()
            
        }
    }
    
    //MARK: StoryBoard Action..
    
    @IBAction func wtfButtonAction(_ sender: Any) {
        
        let  msgData = ["lat": latitude,
                        "lon": longitude,
                        "home": fromStation]
        
        print(msgData)
        
       self.socket.emit("wtf", with: [msgData])
        
         self.setWTFIcon()
    }
    
    @objc func update(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
        
        
        if let selectedDate = formatter.date(from: arrivalTimeString) {
            
            let dateRangeStart = Date()
            let cuurentDateString = formatter.string(from: dateRangeStart)
            
            formatter.dateFormat = "HH:mm:ss"
            formatter.timeZone = NSTimeZone(name: "EST")! as TimeZone
            
            let currentDate = formatter.date(from: cuurentDateString)
            
            let components = Calendar.current.dateComponents([.hour, .minute, .second], from: currentDate!, to: selectedDate)
            
            print(currentDate!)
            print(selectedDate)
            
            
            print(components.hour ?? 0)
            print(components.minute ?? 0)
            print(components.second ?? 0)
            
            if components.minute != 0 {
            
            self.etaText = " - \(String(describing: components.hour!)):\(String(describing: components.minute!)):\(String(describing: components.second!)) "
            
             self.etaLabel.text = "ETA"+etaText
            }else {
                
                
            }
            
        }
        

        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
