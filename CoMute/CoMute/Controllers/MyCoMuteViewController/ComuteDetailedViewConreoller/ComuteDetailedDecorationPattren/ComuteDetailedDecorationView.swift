//
//  ComuteDetailedDecorationView.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 10/07/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import GoogleMaps

extension ComuteDetailedViewController : CustomizedDefaultNavigationBarDelegate,UICollectionViewDataSource,UICollectionViewDelegate,mapLandScapeViewDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,GMUClusterManagerDelegate{
    
    
    //MARK: Custom Navigation Delegatess...
    func barItemsAction(sender: UIButton) {
        
        if sender.tag == 10 {
             
            self.navigationController?.popViewController(animated: true)
            
            //Change Orientation Flag...
            (UIApplication.shared.delegate as! AppDelegate).landscapeOrientationChangeFlag = false
            
            
        }
    }
    
    //LandScapeMode Delegate
    
    func backButton(){
        
        self.navigationController?.popViewController(animated: true)
        
        //Change Orientation Flag...
        (UIApplication.shared.delegate as! AppDelegate).landscapeOrientationChangeFlag = false
    
    }
    func viewProfileOption(id : String){
        
        self.mapLandScapeView.removeFromSuperview()
        
        
        
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ComuteProfileViewContoller") as! ComuteProfileViewContoller
        userProfileVC.isCurrentUser = false
        userProfileVC.notificationFlag = 0
        userProfileVC.userID = id
        //let currentController = self.getCurrentViewController()
        //print( self.getCurrentViewController()!)
        
        
        self.navigationController?.pushViewController(userProfileVC, animated: true)

    }
    
    
    // MARK: - GMUClusterManagerDelegate
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                 zoom: customMapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        customMapView.moveCamera(update)
        return false
    }
    
    // MARK: - GMUMapViewDelegate
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? MarkerModel {
            print("Did tap marker for cluster item \(poiItem.name)")
            marker.title = poiItem.name
        } else {
            print("Did tap a normal marker")
        }
        return false
    }
    
    // MARK: - Private
    /// Randomly generates cluster items within some extent of the camera and adds them to the
    /// cluster manager.
     func generateClusterItems() {
       // let extent = 0.2
        for  i in 0 ..< self.locationCordArray.count{
            
            
            if let cordinatesDic = self.locationCordArray[i] as? NSDictionary {
                
                let mlat = cordinatesDic["stop_lat"] as? double_t
                let mlong = cordinatesDic["stop_lon"] as? double_t
                
                let lat = mlat!  // * randomScale()
                let lng = mlong!  //* randomScale()
                
                print(lat)
                print(lng)
                let name = cordinatesDic["stop_name"] as? String
                
                
                let item = MarkerModel(position: CLLocationCoordinate2DMake(lat, lng), name: name!)
                
                clusterManager.add(item)
                
            }
           
        }
    }
    
    /// Returns a random value between -1.0 and 1.0.
     func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    
   
    
    //MARK: Collection view delegates and Data Sources...
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return aspectWidth(width: 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: aspectWidth(width: 0), left: aspectWidth(width: 20), bottom: aspectWidth(width: 0), right: aspectWidth(width: 20))
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return aspectWidth(width: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! UserCollectionViewCell
        
        //cell.frame  = CGRect(x: 20, y: 30, width: 150, height: 110)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    //MARK: Set WTF Icon.
    func setWTFIcon(){
        
                // Creates a marker in the center of the map.
        
        self.getCordinates.getLIRRWTFCordinatesAPI(lat: latitude, Long: longitude, homeStation: fromStation) { (succeeded, response) in
            
            
            if succeeded {
                
                print(response)
                let locDic = response["data"] as? NSDictionary
                
                let nearDic = locDic?["near"] as? NSDictionary
                
                let stopName = nearDic?["stop_name"] as? String
                
                self.nearestStationLabel.text = stopName
                
    
                
                self.wtfStationCoordinates = (locDic?["wtf"]  as? NSArray
                    )!
            
        for i in 0 ..< self.wtfStationCoordinates.count {
            
            if let cordinatesDic = self.locationCordArray[i] as? NSDictionary {
                
                let locDic = cordinatesDic["loc"] as? NSArray
                
                
                let lat = locDic?[1] as? double_t
                let long = locDic?[0] as? double_t
                
                print(lat!,long!)
                
                let markeri = GMSMarker()

                 markeri.position = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
                markeri.icon =  UIImage(named: "Skull small") as UIImage?
                
                
                markeri.map  =  self.customMapView
            }
            
            
        }
        
               // marker.title = "Sydney"
              //  marker.snippet = "Australia"
        
    }
}
    }
    
//    //Market Tap Function
//    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//        
//        userDetailedMapView = UINib(nibName: "UserMapDetailedView", bundle: nil).instantiate(withOwner: UserMapDetailedView.self, options: nil)[0] as! UserMapDetailedView
//        
//        //self.mapLandScapeView.deligate = self
//        userDetailedMapView.frame = CGRect(x: 100  , y: 100, width: 154 , height: 55 )
//        
//        
//        self.view.addSubview(userDetailedMapView)
//        
//        return true
//    }
 
    
    
    func setMapView(){
        
        //MArk1
        let path = GMSMutablePath()
        var rectangle  = GMSPolyline()
        for  i in 0 ..< self.locationCordArray.count{
            
          
            
            
            if let cordinatesDic = self.locationCordArray[i] as? NSDictionary {
                
                let lat = cordinatesDic["stop_lat"] as? double_t
                let long = cordinatesDic["stop_lon"] as? double_t
                
                path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
                //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
                rectangle  = GMSPolyline(path: path)
                rectangle.strokeWidth = 2.0
                rectangle.strokeColor = UIColor(red: 255/11, green: 255/176, blue: 255/47, alpha: 1.0)
                
                rectangle.map = self.customMapView
                
                
                
            }
            
            var bounds = GMSCoordinateBounds()
            
            for index in 1...path.count() {
                bounds = bounds.includingCoordinate(path.coordinate(at: index))
            }
            
            self.customMapView.animate(with: GMSCameraUpdate.fit(bounds))
            
        }
        /* //MArk2
         
         for  _ in 0 ..< self.locationCordArray[2].count{
         
         self.subLocCordArray = self.locationCordArray[2]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor.darkGray
         
         rectangle.map = self.mapView
         
         }
         //MArk3
         
         for  _ in 0 ..< self.locationCordArray[3].count{
         
         self.subLocCordArray = self.locationCordArray[3]
         
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor.cyan
         rectangle.map = self.mapView
         
         }
         //MArk4
         
         for  _ in 0 ..< self.locationCordArray[4].count{
         
         self.subLocCordArray = self.locationCordArray[4]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor.red
         rectangle.map = self.mapView
         
         }
         //MArk5
         
         for  _ in 0 ..< self.locationCordArray[5].count{
         
         self.subLocCordArray = self.locationCordArray[5]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor.blue
         
         rectangle.map = self.mapView
         
         }
         //MArk6
         
         for  _ in 0 ..< self.locationCordArray[6].count{
         
         self.subLocCordArray = self.locationCordArray[6]
         
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor.brown
         
         rectangle.map = self.mapView
         
         }
         
         //MArk7
         
         for  _ in 0 ..< self.locationCordArray[7].count{
         
         self.subLocCordArray = self.locationCordArray[7]
         
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor.gray
         
         rectangle.map = self.mapView
         
         }
         
         //MArk8
         
         for  _ in 0 ..< self.locationCordArray[8].count{
         
         self.subLocCordArray = self.locationCordArray[8]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor.yellow
         
         rectangle.map = self.mapView
         
         }
         
         //MArk9
         
         for  _ in 0 ..< self.locationCordArray[9].count{
         
         self.subLocCordArray = self.locationCordArray[9]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor.orange
         
         rectangle.map = self.mapView
         
         }
         
         //MArk10
         
         for  _ in 0 ..< self.locationCordArray[10].count{
         
         self.subLocCordArray = self.locationCordArray[10]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor.purple
         
         rectangle.map = self.mapView
         
         }
         
         //MArk11
         
         for  _ in 0 ..< self.locationCordArray[11].count{
         
         self.subLocCordArray = self.locationCordArray[11]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor(red: 255/200, green: 255/200, blue: 255/200, alpha: 1.0)
         
         rectangle.map = self.mapView
         
         }
         
         //MArk12
         
         for  _ in 0 ..< self.locationCordArray[12].count{
         
         self.subLocCordArray = self.locationCordArray[12]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor(red: 255/150, green: 255/150, blue: 255/150, alpha: 1.0)
         
         rectangle.map = self.mapView
         
         }
         
         //MArk13
         
         for  _ in 0 ..< self.locationCordArray[13].count{
         
         self.subLocCordArray = self.locationCordArray[13]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor(red: 255/100, green: 255/100, blue: 255/100, alpha: 1.0)
         
         rectangle.map = self.mapView
         
         }
         
         //MArk14
         
         for  _ in 0 ..< self.locationCordArray[14].count{
         
         self.subLocCordArray = self.locationCordArray[14]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor(red: 255/170, green: 255/170, blue: 255/170, alpha: 1.0)
         
         rectangle.map = self.mapView
         
         }
         
         //MArk15
         
         for  _ in 0 ..< self.locationCordArray[14].count{
         
         self.subLocCordArray = self.locationCordArray[14]
         let path = GMSMutablePath()
         var rectangle  = GMSPolyline()
         
         for j in 0 ..< self.subLocCordArray.count {
         
         
         let cordinatesDic = self.subLocCordArray[j] as? NSDictionary
         
         let lat = cordinatesDic?["lat"] as? double_t
         let long = cordinatesDic?["lng"] as? double_t
         
         
         
         
         path.add(CLLocationCoordinate2DMake(CDouble((lat)!), CDouble((long)!)))
         //path.add(CLLocationCoordinate2DMake(CDouble((self.latitude2)), CDouble((self.longitude2))))
         
         
         
         }
         rectangle  = GMSPolyline(path: path)
         rectangle.strokeWidth = 2.0
         rectangle.strokeColor = UIColor(red: 255/210, green: 255/210, blue: 255/210, alpha: 1.0)
         
         rectangle.map = self.mapView
         
         }
         */
        
        
        
        
        // self.customMapView.isMyLocationEnabled = true
        //                let marker1 = GMSMarker()
        //                marker1.position = CLLocationCoordinate2DMake(self.latitude1, self.longitude1)
        //                // marker.icon = UIImage(named: "aaa.png")!
        //                marker1.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        //                marker1.title = self.originAddress
        //                marker1.map = self.mapView
        //
        //                let marker2 = GMSMarker()
        //                marker2.position = CLLocationCoordinate2DMake(self.latitude2, self.longitude2)
        //                // marker.icon = UIImage(named: "aaa.png")!
        //                marker2.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        //                marker2.title = self.destinationAddress
        //                marker2.map = self.mapView
 
    }
    
    func RadiansToDegrees(radians: Double) -> Double {
        return radians * 180.0/Double.pi
    }
    
    func DegreesToRadians(degrees: Double) -> Double {
        return degrees * Double.pi / 180.0
    }
    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus()
            != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            // print("User allowed us to access location")
            
            
            //do whatever init activities here.
        }
    }
    
    
    //this method is called by the framework on         locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("Did location updates is called")
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        
        let location = locations.last
        
//        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
//        
//        self.customMapView?.animate(to: camera)
//        
//        GeoAngle = self.setLatLonForDistanceAndAngle(userLocation: locationObj)
//        currentmarker.position = (location?.coordinate)!
        
        self.latitude = "\(coord.latitude)"
        self.longitude = "\(coord.longitude)"
        
        let lac = ["lat": latitude,
                   "lon": longitude ]
        //MARK: Socket to Make Connection and Emit Registration
        let data = ["location": lac] as [String : Any]
        
        print(data)
        
        self.socket.emit("sendlocation", with: [data])
        
        print(coord.latitude)
        // print(coord.longitude)
        //store the user location here to firebase or somewhere
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        var direction = -newHeading.trueHeading as Double
       // currentmarker.icon = self.imageRotatedByDegrees(degrees: CGFloat(direction), image: UIImage(named: "arrow.png")!)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
    
    func imageRotatedByDegrees(degrees: CGFloat, image: UIImage) -> UIImage{
        var size = image.size
        
        UIGraphicsBeginImageContext(size)
        var context = UIGraphicsGetCurrentContext()
        
        context!.translateBy(x: 0.5*size.width, y: 0.5*size.height)
        context!.rotate(by: CGFloat(DegreesToRadians(degrees: Double(degrees))))
        
        image.draw(in: CGRect(origin: CGPoint(x: -size.width*0.5, y: -size.height*0.5), size: size))
        var newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func setLatLonForDistanceAndAngle(userLocation: CLLocation) -> Double {
        var lat1 = DegreesToRadians(degrees: userLocation.coordinate.latitude)
        var lon1 = DegreesToRadians(degrees: userLocation.coordinate.longitude)
        
        var lat2 = DegreesToRadians(degrees: 37.7833);
        var lon2 = DegreesToRadians(degrees: -122.4167);
        
        var dLon = lon2 - lon1;
        
        var y = sin(dLon) * cos(lat2);
        var x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
        var radiansBearing = atan2(y, x);
        if(radiansBearing < 0.0)
        {
            radiansBearing += 2*Double.pi;
        }
        
        return radiansBearing;
    }
    
    
}
