//
//  PasswordModel.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 18/08/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView

class PasswordModel: NSObject {
    
    //Model Objects...
    var internetConnectionModel: InternetConnectionModel!
    var helperModel: HelperModel!
    var activeData : ActivityData!
    
    
    //Data store object...that manages all insertion deletion and updations of Data in Batabase...
    //var persistence: Persistent!
    
    
    //MARK: Model Initialization Functions...
    override init() {
        
        self.internetConnectionModel = InternetConnectionModel()
        self.helperModel = HelperModel()
        self.activeData = ActivityData()
        //Creating Core Data Model Object....
        // self.persistence = Persistent()
    }

    
    //MARK: Function Used for Request for reset password...
    func resetPasswordRequestAPI(email: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
             //UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activeData)
            resetPasswordRequestAPIHIT(email: email, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                  //  ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                               
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }

                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func resetPasswordRequestAPIHIT( email : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
      
            
            params = NSString(format: "email=%@",email) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/auth/forgetpassword/\(email)" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used  for reset password...
    func resetPasswordAPI(resetpasswordtoken: String,newpassword: String,resetpassword: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
             //UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
           //  ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activeData)
            resetPasswordAPIHIT(resetpasswordtoken: resetpasswordtoken, newpassword: newpassword, resetpassword: resetpassword, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Successv1(h1>h4# but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {}) t2*b2$E1$
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func resetPasswordAPIHIT(resetpasswordtoken : String, newpassword : String, resetpassword: String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...v1<K2[f1$
        let params : String?
        
        
        
        params = NSString(format: "newpassword=%@&resetpassword=%@",newpassword,resetpassword) as String
        
        //print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/auth/forgetpassword/reset/\(resetpasswordtoken)" as NSString, params: params!, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }

    //MARK: Function Used  for change password...
    func changePasswordAPI(oldPassword: String,newpassword: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            //UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            //  ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activeData)
            changePasswordAPIHIT(oldpassword: oldPassword, newpassword: newpassword, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Successv1(h1>h4# but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {}) t2*b2$E1$
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func changePasswordAPIHIT(oldpassword : String, newpassword : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...v1<K2[f1$
        let params : String?
        
        
        
        params = NSString(format: "oldpassword=%@&newpassword=%@",oldpassword,newpassword) as String
        
        //print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/changepassword", params: params!, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }


}
