//
//  CommonAPIsModels.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 11/05/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView

class CommonAPIsModel: NSObject {
    
    //Model Objects...
    var internetConnectionModel: InternetConnectionModel!
    var helperModel: HelperModel!
    var activeData : ActivityData!
    
    //MARK: Model Initialization Functions...
    override init() {
        
        self.internetConnectionModel = InternetConnectionModel()
        self.helperModel = HelperModel()
        self.activeData = ActivityData()
    }
    
    
    //MARK: Saving User Data in Database...
    func deletingUserAsLogoutOptByUser() {
        
        //Removing All Facebook Data before New Login with Facebook...
        Singleton.sharedInstance.facebookId = ""
        Singleton.sharedInstance.facebookAccessToken = ""
       // Singleton.sharedInstance.currentUserType = ""
      //  Singleton.sharedInstance.completeAppImageCache.removeAllObjects()
       // Singleton.sharedInstance.faceDetectionImages.removeAllObjects()
        Singleton.sharedInstance.userName = ""
        Singleton.sharedInstance.userID = ""
        Singleton.sharedInstance.userAccessToken = ""
        Singleton.sharedInstance.maximumUserStep = 0
        Singleton.sharedInstance.insertID = 0
        Singleton.sharedInstance.accessToken_RM = ""
        Singleton.sharedInstance.userFullName = ""
        
        
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.FACEBOOK_DATA)
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.USER_TYPE)
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.USER_STEP)
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.USER_ID)
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.USER_NAME)
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.ACCESS_TOKEN)
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.USER_LOCATION)
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.USER_FULL_NAME)
        
            }

    
    //MARK: Registeration with Facebook to fetch Photos.....IF User is not login From Facebook.......
    func openFacebookControllerToFetchDataOfUser(recievedResponse:@escaping (_ succeeded: Bool, _ values: NSDictionary)->()) {
        //Removing All Facebook Data before New Registration.....
        Singleton.sharedInstance.facebookId = ""
        Singleton.sharedInstance.facebookAccessToken = ""
        //        Singleton.sharedInstance.facebookBusinessToken = ""
        UserDefaults.standard.removeObject(forKey: USER_DEFAULT.FACEBOOK_DATA)
        
        //Initial reachability check.....
        if self.internetConnectionModel.connectedToNetwork() {
            
            if UIApplication.shared.keyWindow?.rootViewController != nil {
                
                //Regestring with Facebook....
                FacebookCustom.sharedInstance.loginToFacebookWithSuccess(sender: (UIApplication.shared.keyWindow?.rootViewController!)!, successBlock: ({(response:AnyObject?) -> Void in
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){         //placing recieved values from facebook to text fields......
                        
                        //Fetching facebook Data from USER DEFAULTS......
                        let values:NSDictionary = (UserDefaults.standard.value(forKey: USER_DEFAULT.FACEBOOK_DATA) as? NSDictionary) != nil ? (UserDefaults.standard.value(forKey: USER_DEFAULT.FACEBOOK_DATA) as! NSDictionary) : [:]
                        print(values)
                        
                        //Saving Business token.....
                        //                        if (values.allKeys as NSArray).containsObject("token_for_business") {
                        //
                        //                            Singleton.sharedInstance.facebookBusinessToken = values.valueForKey("token_for_business") as! String
                        //                        }
                        
                        //Callback for passing data......
                        recievedResponse(true, values)
                    }
                    
                    } )){(error:NSError?) -> Void in
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            
                            if error?.localizedDescription != nil {
                                
                                self.helperModel.showingAlertcontroller(title: "", message: error?.localizedDescription != nil ? error!.localizedDescription : "Error Occured in Facebook Registration!", cancelButton: "OK", receivedResponse: {})
                                
                                //Callback for passing data......
                                recievedResponse(false, [:])
                            }
                        }
                }
                
            } else {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)  {
                    
                   // ActivityIndicator.sharedInstance.hideActivityIndicator()
                    self.helperModel.showingAlertcontroller(title: "", message: "Please check your internet connection.", cancelButton: "OK", receivedResponse: {})
                    
                    //Callback for passing data......
                    recievedResponse(false, [:])
                }
            }
        }

    }
    
    //MARK: Function Used for Update Profile of User...
    func facebookIDValidationCheckAPI(fb_ID: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if self.internetConnectionModel.connectedToNetwork() {
            
            //Showing Activity Indicator....
           // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
           // ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activeData)
            
            //FB_IDCHECK Use API Hit....
            self.facebookIDValidationCheckAPIHIT(fb_ID: fb_ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){

                    
                    //ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                //Setting Data to self as Model...
                                if  let userSignUpSuccessDataDictionary = response["data"] as? NSDictionary {
                                    
                                    //let userId = userSignUpSuccessDataDictionary["user_id"] as! NSDictionary
                                    let userToken = userSignUpSuccessDataDictionary["authtoken"] as! String
                                    
                                    UserDefaults.standard.setValue(userToken , forKey: USER_DEFAULT.ACCESS_TOKEN)
                                    
                                    Singleton.sharedInstance.userID  = userSignUpSuccessDataDictionary["_id"] as! String
                                    UserDefaults.standard.setValue(userSignUpSuccessDataDictionary["_id"] as! String, forKey: USER_DEFAULT.USER_ID)
                                    
                                    Singleton.sharedInstance.userName =  userSignUpSuccessDataDictionary["username"] as! String
                                    
                                    Singleton.sharedInstance.userHomeStation = userSignUpSuccessDataDictionary["homestation"] as! String
                                    Singleton.sharedInstance.userDestinationStation = userSignUpSuccessDataDictionary["destinationstation"] as! String
                                    
                                    if let image = userSignUpSuccessDataDictionary["image"] as? String{
                                        
                                        Singleton.sharedInstance.userImage = image
                                    }
                                    
                                    UserDefaults.standard.setValue(userSignUpSuccessDataDictionary["username"] as! String, forKey: USER_DEFAULT.USER_NAME)
                                    
                                    Singleton.sharedInstance.userFullName = String(describing: userSignUpSuccessDataDictionary["firstname"]!)+" "+String(describing: userSignUpSuccessDataDictionary["lastname"]!)

                                    UserDefaults.standard.setValue(String(describing: userSignUpSuccessDataDictionary["firstname"]!)+" "+String(describing: userSignUpSuccessDataDictionary["lastname"]!), forKey: USER_DEFAULT.USER_FULL_NAME)
                                    
                                    //Setting USer Is in Singleton as well...
                                    // Singleton.sharedInstance.userID = userId as! Int
                                    Singleton.sharedInstance.userAccessToken = userToken
                                    // UserDefaults.standard.setValue(userId as! Int, forKey: USER_DEFAULT.USER_ID)
                                    // //UserDefaults.standard.setValue(Singleton.sharedInstance.currentUserType, forKey: USER_DEFAULT.USER_TYPE)
                                    
                                    // // UserDefaults.standard.setValue(Singleton.sharedInstance.currentUserType, forKey: USER_DEFAULT.USER_TYPE)
                                    
                                    //Sending user details to MoEnage for user engagement
                                    //  saveUserDetailsForMOEngage(self.userSignUpSuccessDataDictionary["user"] as! Int, mailID: user_Email, phoneNumber: user_Mobile)
                                    
                                    //Setting User Id to Database...
                                    //self.populatingDataBase(user_Mobile, email: user_Email)
                                    recievedResponse(true, response as NSDictionary)
                                    
                                }
                                
                                break
                                
                            default:
                                
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            recievedResponse(false, [:])
        }
    }
    
    
    func facebookIDValidationCheckAPIHIT(fb_ID: String, receivedResponse:@escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        var params = String()
        
        
        //Setting authentication Access Token of User...and send to server...after Stringify...
        params = NSString(format: "devicetoken=%@", (UserDefaults.standard.value( forKey: USER_DEFAULT.DEVICE_TOKEN) ?? "") as! String) as String
        print(params)
        
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/auth/facebook/\(fb_ID)" as NSString , params: params, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            print(response)
            
            if(succeeded) {
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }

    

}
