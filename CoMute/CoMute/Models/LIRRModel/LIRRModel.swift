//
//  LIRRModel.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 07/10/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView

class LIRRModel : NSObject {
    
    //Model Objects...
    var internetConnectionModel: InternetConnectionModel!
    var helperModel: HelperModel!
    var activityData : ActivityData!
    
    
    //Data store object...that manages all insertion deletion and updations of Data in Batabase...
    //var persistence: Persistent!
    
    
    //MARK: Model Initialization Functions...
    override init() {
        
        self.internetConnectionModel = InternetConnectionModel()
        self.helperModel = HelperModel()
        self.activityData = ActivityData()
        //Creating Core Data Model Object....
        // self.persistence = Persistent()
    }
    
    
    //MARK: Function Used for Get All LIRR Station....
    func getAllLIRRStationAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getAllLIRRStationAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getAllLIRRStationAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/auth/lirr/stations" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Get All LIRR B/w Stations Data....
    func getLIRRStationETAAPI(sourceStation : String,destinationData : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getLIRRStationETAAPIHIT(sourceStation : sourceStation,destinationData : destinationData, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getLIRRStationETAAPIHIT(sourceStation : String,destinationData : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let _ : String?
       //
        
        
        //params = NSString(format: "id=%@",sourceStation) as String
        

        
        //Sending request to server....
        
        sendRequestToServer(url: "/v1/users/lirr/schedules/\(sourceStation.replacingOccurrences(of: " ", with: "-"))/\(destinationData.replacingOccurrences(of: " ", with: "-"))" as NSString, params: "", httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Get All LIRR B/w Stations Data....
    func getLIRRStationCordinatesAPI(sourceStation : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getLIRRStationCordinatesAPIHIT(sourceStation : sourceStation, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getLIRRStationCordinatesAPIHIT(sourceStation : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let _ : String?
        //
        
        
        //params = NSString(format: "id=%@",sourceStation) as String
        
        
        
        //Sending request to server....
        
        sendRequestToServer(url: "/v1/users/lirr/location" as NSString, params: "", httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Get All WTF Stations Data....
    func getLIRRWTFCordinatesAPI(lat : String,Long : String, homeStation : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getLIRRWTFCordinatesAPIHIT(lat : lat, Long: Long,homeStation : homeStation, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getLIRRWTFCordinatesAPIHIT(lat : String,Long : String, homeStation : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let _ : String?
        //
        
        
        //params = NSString(format: "id=%@",sourceStation) as String
        
        
        
        //Sending request to server....
        
        sendRequestToChatServer(url: "/api/v1/users/wtf/\(lat)/\(Long)/\(homeStation.replacingOccurrences(of: " ", with: "-"))" as NSString, params: "", httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Get All LIRR B/w Stations Data....
    func getLIRRLinesCordinatesAPI(tripID : String, startSeq : String, stopSeq : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getLIRRLinesCordinatesAPIHIT(tripID : tripID, startSeq: startSeq, stopSeq: stopSeq, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getLIRRLinesCordinatesAPIHIT(tripID : String, startSeq : String, stopSeq : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let _ : String?
        //
        
        
        //params = NSString(format: "id=%@",sourceStation) as String
        
        
        
        //Sending request to server....
        
        sendRequestToServer(url: "/v1/users/lirr/trip/\(tripID)/\(startSeq)/\(stopSeq)" as NSString, params: "", httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }

    
    
}
