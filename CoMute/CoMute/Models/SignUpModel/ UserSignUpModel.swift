//
//  UserSignUpModel.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 19/07/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView

class UserSignUpModel: NSObject {
    
    //Model Objects...
    var internetConnectionModel: InternetConnectionModel!
    var helperModel: HelperModel!
    var activeData : ActivityData!
    
    //Controller Data...
    var userSignUpSuccessDataDictionary = NSDictionary()
    
    //Data store object...that manages all insertion deletion and updations of Data in Batabase...
    //var persistence: Persistent!
    
    
    //MARK: Model Initialization Functions...
    override init() {
        
        self.internetConnectionModel = InternetConnectionModel()
        self.helperModel = HelperModel()
        self.activeData = ActivityData()
        
        //Creating Core Data Model Object....
        // self.persistence = Persistent()
    }
    
    //MARK: Savind User Id in Database...
    //    func populatingDataBase(contactNo: String, email: String) {
    //
    //        if let moc = self.persistence.managedObjectContext {
    //
    //            let fetchRequest = NSFetchRequest(entityName: "Users")
    //            var usersArray = [AnyObject]()
    //
    //            do { usersArray = try moc.executeFetchRequest(fetchRequest) } catch { }
    //
    //            //If User Not Exists then create new one..
    //            var userFound = false
    //
    //            for i in 0 ..< usersArray.count {
    //
    //                if (usersArray[i] as! Users).user_ID == self.userSignUpSuccessDataDictionary["user"] as? String {
    //
    //                    (usersArray[i] as! Users).user_ID = Singleton.sharedInstance.userID
    //                    (usersArray[i] as! Users).user_Mobile = contactNo
    //                    (usersArray[i] as! Users).user_Email = email
    //
    //                    userFound = true
    //                }
    //            }
    //
    //
    //            //Create a new user...
    //            if !userFound {
    //
    //                let managedObject = NSEntityDescription.insertNewObjectForEntityForName("Users", inManagedObjectContext: moc) as! Users
    //                managedObject.user_ID = Singleton.sharedInstance.userID
    //                managedObject.user_Mobile = contactNo
    //                managedObject.user_Email = email
    //            }
    //
    //            //Saving data after Populating into Database...
    //            self.persistence.saveContext({})
    //        }
    //    }
    
    
    
    //MARK: Function Used for Signing Up User...
    func userSignUpAPI(user_Mobile: String, user_Email: String, first_Name: String, last_Name: String,home_Station: String, destination_Station: String,comute_Years: String, profile_Pic : String, password: String, user_Name : String, user_Fb_ID: String, facebookEmailVerifiedFlag: Bool, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
          //  UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
          //  ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activeData)
            
            userSignUpAPIAPIHIT(user_Mobile: user_Mobile, user_Email: user_Email, first_Name: first_Name, last_Name: last_Name, home_Station: home_Station, destination_Station: destination_Station, comute_Years: comute_Years, profile_Pic: profile_Pic , user_Name: user_Name, password: password, user_Fb_ID: user_Fb_ID, facebookEmailVerifiedFlag: facebookEmailVerifiedFlag, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                   // UIApplication.shared.delegate?.window!!.removeFromSuperview()
                    
                  // ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                               
                                
                                //If Real Success...Happens...
                                //if response["status"] as! Int == 1 {
                                    
                                    //Setting Data to self as Model...
                                if  let userSignUpSuccessDataDictionary = response["data"] as? NSDictionary {
                                    
                                   //let userId = userSignUpSuccessDataDictionary["user_id"] as! NSDictionary
                                    let userToken = userSignUpSuccessDataDictionary["authtoken"] as! String
                                    
                             
                                    Singleton.sharedInstance.userAccessToken = userToken 
                               
                                    UserDefaults.standard.setValue(userToken , forKey: USER_DEFAULT.ACCESS_TOKEN)
                                 
                                    if let id  = userSignUpSuccessDataDictionary["_id"] as? String{
                                    Singleton.sharedInstance.userID  = id
                                    UserDefaults.standard.setValue(id, forKey: USER_DEFAULT.USER_ID)
                                        
                                    }
                                    
                                    if let uname =  self.userSignUpSuccessDataDictionary["username"] as? String {
                                    Singleton.sharedInstance.userName = uname
                                    UserDefaults.standard.setValue(Singleton.sharedInstance.userName, forKey: USER_DEFAULT.USER_NAME)
                                    }
                                    
                                    if let image = self.userSignUpSuccessDataDictionary["image"] as? String{
                                        
                                        Singleton.sharedInstance.userImage = image
                                    }
                                    if let hStation  = self.userSignUpSuccessDataDictionary["homestation"] as? String {
                                    Singleton.sharedInstance.userHomeStation = self.userSignUpSuccessDataDictionary["homestation"] as! String
                                    }
                                    if let dStation = self.userSignUpSuccessDataDictionary["destinationstation"] as? String {
                                    Singleton.sharedInstance.userDestinationStation = self.userSignUpSuccessDataDictionary["destinationstation"] as! String
                                }
                                    if let fName = self.userSignUpSuccessDataDictionary["firstname"] as? String {
                                        
                                        if let lName = self.userSignUpSuccessDataDictionary["lastname"] as? String {
                                    Singleton.sharedInstance.userFullName = fName+" "+lName
                                    
                                    UserDefaults.standard.setValue(Singleton.sharedInstance.userFullName, forKey: USER_DEFAULT.USER_FULL_NAME)
                                            
                                        }
                                        
                                    }
                                    
                                    //Sending user details to MoEnage for user engagement
                                    //  saveUserDetailsForMOEngage(self.userSignUpSuccessDataDictionary["user"] as! Int, mailID: user_Email, phoneNumber: user_Mobile)
                                    
                                    //Setting User Id to Database...
                                    //self.populatingDataBase(user_Mobile, email: user_Email)
                                     recievedResponse(true, response as NSDictionary)
                                    
                                }else {
                                    
                                    let message = response["message"] as! NSDictionary
                                    
                                    if let msg = message["msg"] as? String {
                                        
                                        self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                    }
                                    
                                    if let validation = message["validation"] as? NSArray {
                                        
                                        
                                        self.helperModel.showingAlertcontroller(title: "", message: "\(validation[0])", cancelButton: "OK", receivedResponse: {})
                                        
                                    }
                                    recievedResponse(true, response as NSDictionary)
                                }
                                
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break

                                
                                    
                         //       } else {
                                    
                            //        self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: "OK", receivedResponse: {})
                               // }
                                
                               // break
                                
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                         //ActivityIndicator.sharedInstance.hideActivityIndicator()
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {
                       })
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func userSignUpAPIAPIHIT(user_Mobile: String, user_Email: String, first_Name: String, last_Name: String,home_Station: String, destination_Station: String,comute_Years: String, profile_Pic : String, user_Name : String, password: String, user_Fb_ID: String, facebookEmailVerifiedFlag: Bool, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
                    //var params = NSDictionary()
        
        if user_Fb_ID.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) != "" {
            
            /*
             
             photo optional	String
             the user photo, facebook image link.
             
             image optional	File
             user uplaoded image.
             
             first	String
             name first name of the user.
             
             last	String
             name of the user.
             
             username	String
             user name on the app.
             
             email	String
             user email address.
             
             password	String	
             user password.
             
             contact	String	
             user phone number.
             
             homestation	String	
             user phone number.
             
             destinationstation	String	
             user phone number.
             
             comuteyear
             
             
 
 */
            

            
            params = NSString(format: "contact=%@&email=%@&firstname=%@&lastname=%@&homestation=%@&destinationstation=%@&comuteyear=%@&image=%@&username=%@&fbid=%@&password=%@", user_Mobile, user_Email, first_Name,last_Name,home_Station,destination_Station,comute_Years,profile_Pic,user_Name, user_Fb_ID,password) as String
            
        } else {
            

            params = NSString(format: "contact=%@&email=%@&firstname=%@&lastname=%@&homestation=%@&destinationstation=%@&comuteyear=%@&password=%@&image=%@&username=%@&devicetoken=%@", (user_Mobile), user_Email, first_Name,last_Name,home_Station,destination_Station,comute_Years, password, profile_Pic, user_Name,(UserDefaults.standard.value( forKey: USER_DEFAULT.DEVICE_TOKEN) ?? "") as! String) as String
        }
        
    print(params!)
        
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/auth/signup", params: params!, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
        
        
    }
    
    
    //MARK: Function Used for  Check UserName ...
    func userNameCheckAPI(user_Name : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
           // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
           // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activeData)
            
            userNameCheckAPIHIT(user_Name: user_Name, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    // UIApplication.shared.delegate?.window!!.removeFromSuperview()
                    
                //    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                //If Real Success...Happens...
                                //if response["status"] as! Int == 1 {
                                
                                //Setting Data to self as Model...
                                if  (response["data"] as? NSDictionary) != nil {
                                    
                                    //let userId = userSignUpSuccessDataDictionary["user_id"] as! NSDictionary
                                   // let userToken = userSignUpSuccessDataDictionary["authtoken"] as! String
                                    
                                    //Setting USer Is in Singleton as well...
                                 // Singleton.sharedInstance.userID = userId as! Int
                                //    Singleton.sharedInstance.userAccessToken = userToken
                                //    // UserDefaults.standard.setValue(userId as! Int, forKey: USER_DEFAULT.USER_ID)
                                    // //UserDefaults.standard.setValue(Singleton.sharedInstance.currentUserType, forKey: USER_DEFAULT.USER_TYPE)
                                //    UserDefaults.standard.setValue(userToken , forKey: USER_DEFAULT.ACCESS_TOKEN)
                                    // // UserDefaults.standard.setValue(Singleton.sharedInstance.currentUserType, forKey: USER_DEFAULT.USER_TYPE)
                                    
                                    //Sending user details to MoEnage for user engagement
                                    //  saveUserDetailsForMOEngage(self.userSignUpSuccessDataDictionary["user"] as! Int, mailID: user_Email, phoneNumber: user_Mobile)
                                    
                                    //Setting User Id to Database...
                                    //self.populatingDataBase(user_Mobile, email: user_Email)
                                    recievedResponse(true, response as NSDictionary)
                                    
                                }else {
                                    
                                   // let message = response["message"] as! NSDictionary
                                    
                                    if let msg = response["message"] as? String {
                                        
                                        self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                    }
                                    
//                                    if let validation = message["validation"] as? NSArray {
//                                        
//                                        
//                                        self.helperModel.showingAlertcontroller(title: "", message: "\(validation[0])", cancelButton: "OK", receivedResponse: {})
//                                        
//                                    }
                                    recievedResponse(true, response as NSDictionary)
                                }
                                
                            case 400 :
                                
                                
                              //  let message = response["message"] as! NSDictionary
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }

                                
                                //       } else {
                                
                                //        self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: "OK", receivedResponse: {})
                                // }
                                
                                // break
                                
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})C3#E1$C1$
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                      //  ActivityIndicator.sharedInstance.hideActivityIndicator()
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {
                        })
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func userNameCheckAPIHIT(user_Name : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
      
            
            
            params = NSString(format: "username=%@", user_Name) as String
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/auth/checkusername/\(user_Name)" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
        
        
    }
    
    //MARK: Function Used for  Check Email and contact ...
    func userContactCheckAPI(email : String, contact : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
             //UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activeData)
            
            
            userContactCheckAPIHIT(email: email,contact : contact, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    // UIApplication.shared.delegate?.window!!.removeFromSuperview()
                    
                   // ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                //If Real Success...Happens...
                                //if response["status"] as! Int == 1 {
                                
                                //Setting Data to self as Model...
                                if  (response["data"] as? NSDictionary) != nil {
                                    
                                    //let userId = userSignUpSuccessDataDictionary["user_id"] as! NSDictionary
                                    // let userToken = userSignUpSuccessDataDictionary["authtoken"] as! String
                                    
                                    //Setting USer Is in Singleton as well...
                                    // Singleton.sharedInstance.userID = userId as! Int
                                    //    Singleton.sharedInstance.userAccessToken = userToken
                                    //    // UserDefaults.standard.setValue(userId as! Int, forKey: USER_DEFAULT.USER_ID)
                                    // //UserDefaults.standard.setValue(Singleton.sharedInstance.currentUserType, forKey: USER_DEFAULT.USER_TYPE)
                                    //    UserDefaults.standard.setValue(userToken , forKey: USER_DEFAULT.ACCESS_TOKEN)
                                    // // UserDefaults.standard.setValue(Singleton.sharedInstance.currentUserType, forKey: USER_DEFAULT.USER_TYPE)
                                    
                                    //Sending user details to MoEnage for user engagement
                                    //  saveUserDetailsForMOEngage(self.userSignUpSuccessDataDictionary["user"] as! Int, mailID: user_Email, phoneNumber: user_Mobile)
                                    
                                    //Setting User Id to Database...
                                    //self.populatingDataBase(user_Mobile, email: user_Email)
                                    recievedResponse(true, response as NSDictionary)
                                    
                                }else {
                                    
                                    // let message = response["message"] as! NSDictionary
                                    
                                    if let msg = response["message"] as? String {
                                        
                                        self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                    }
                                    
                                    //                                    if let validation = message["validation"] as? NSArray {
                                    //
                                    //
                                    //                                        self.helperModel.showingAlertcontroller(title: "", message: "\(validation[0])", cancelButton: "OK", receivedResponse: {})
                                    //
                                    //                                    }
                                    recievedResponse(true, response as NSDictionary)
                                }
                                
                            case 400 :
                                
                                
                                //  let message = response["message"] as! NSDictionary
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                
                                //       } else {
                                
                                //        self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: "OK", receivedResponse: {})
                                // }
                                
                                // break
                                
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})C3#E1$C1$
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                       // ActivityIndicator.sharedInstance.hideActivityIndicator()
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {
                        })
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func userContactCheckAPIHIT(email : String, contact : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        
        params = NSString(format: "email=%@,contact=%@", email, contact) as String
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/auth/check/\(email)/\(contact)" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
        
        
    }

    
    

    
    
}
