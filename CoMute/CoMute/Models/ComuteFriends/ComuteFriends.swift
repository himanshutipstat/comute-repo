//
//  ComuteFriends.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 05/10/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView

class ComuteFriends: NSObject {
    
    //Model Objects...
    var internetConnectionModel: InternetConnectionModel!
    var helperModel: HelperModel!
    var activityData : ActivityData!
    
    //MARK: Model Initialization Functions...
    override init() {
        
        self.internetConnectionModel = InternetConnectionModel()
        self.helperModel = HelperModel()
        self.activityData = ActivityData()
        
    }
    
    //MARK: Function Used for Get Comute friends....
    func getComuteFriendsAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getComuteFriendsAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getComuteFriendsAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/contacts/unknown" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Remove Friend....
    func removeComuteFriendsAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            removeComuteFriendsAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func removeComuteFriendsAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/friends/\(ID)" as NSString, params: params!, httpMethod: "DELETE") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Accept Friend....
    func acceptComuteFriendsAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            acceptComuteFriendsAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func acceptComuteFriendsAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/friends/\(ID)" as NSString, params: params!, httpMethod: "PUT") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    
    //MARK: Function Used for Accept Friend....
    func cancelComuteFriendsAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            cancelComuteFriendsAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func cancelComuteFriendsAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/friends/request/\(ID)" as NSString, params: params!, httpMethod: "DELETE") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }


    
    //MARK: Function Used for Request Friend....
    func requestComuteFriendsAPI(ID : String,message : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            requestComuteFriendsAPIHIT(ID: ID, message: message, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func requestComuteFriendsAPIHIT(ID : String,message : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@&message=%@",ID,message ) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/friends/\(ID)" as NSString, params: params!, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }


    
}
