//
//  UserModel.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 22/08/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import NVActivityIndicatorView

class UserModel: NSObject {
    
    //Model Objects...
    var internetConnectionModel: InternetConnectionModel!
    var helperModel: HelperModel!
    
    var activityData : ActivityData!
   
    
    
    //Data store object...that manages all insertion deletion and updations of Data in Batabase...
    //var persistence: Persistent!
    
    
    //MARK: Model Initialization Functions...
    override init() {
        
        self.internetConnectionModel = InternetConnectionModel()
        self.helperModel = HelperModel()
        self.activityData = ActivityData()
        
        //Creating Core Data Model Object....
        // self.persistence = Persistent()
}
    
    
    
    //MARK: Function Used for Getting User Details....
    func getUserDetailAPI(token: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
          //   UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
           //  ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getUserDetailAPIHIT(token: token, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
              //      ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    

                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                //Setting Data to self as Model...
                                if  let userSignUpSuccessDataDictionary = response as? NSDictionary {
                                    
                                    //let userId = userSignUpSuccessDataDictionary["user_id"] as! NSDictionary
                                   // let userToken = userSignUpSuccessDataDictionary["authtoken"] as! String
                                    
                                    
                                    //Singleton.sharedInstance.userAccessToken = userToken
                                    
                                   // UserDefaults.standard.setValue(userToken , forKey: USER_DEFAULT.ACCESS_TOKEN)
                                    
                                    if let id  = userSignUpSuccessDataDictionary["_id"] as? String{
                                        Singleton.sharedInstance.userID  = id
                                        UserDefaults.standard.setValue(id, forKey: USER_DEFAULT.USER_ID)
                                        
                                    }
                                    
                                    if let image = userSignUpSuccessDataDictionary["image"] as? String{
                                        
                                        Singleton.sharedInstance.userImage = image
                                    }
                                    
                                    if let uname =  userSignUpSuccessDataDictionary["username"] as? String {
                                        Singleton.sharedInstance.userName = uname
                                        UserDefaults.standard.setValue(Singleton.sharedInstance.userName, forKey: USER_DEFAULT.USER_NAME)
                                    }
                                    
                                    if let hStation  = userSignUpSuccessDataDictionary["homestation"] as? String {
                                        Singleton.sharedInstance.userHomeStation = userSignUpSuccessDataDictionary["homestation"] as! String
                                    }
                                    if let dStation = userSignUpSuccessDataDictionary["destinationstation"] as? String {
                                        Singleton.sharedInstance.userDestinationStation = userSignUpSuccessDataDictionary["destinationstation"] as! String
                                    }
                                    if let fName = userSignUpSuccessDataDictionary["firstname"] as? String {
                                        
                                        if let lName = userSignUpSuccessDataDictionary["lastname"] as? String {
                                            Singleton.sharedInstance.userFullName = fName+" "+lName
                                            
                                            UserDefaults.standard.setValue(Singleton.sharedInstance.userFullName, forKey: USER_DEFAULT.USER_FULL_NAME)
                                            
                                        }
                                        
                                    }
                                    
                                    //Sending user details to MoEnage for user engagement
                                    //  saveUserDetailsForMOEngage(self.userSignUpSuccessDataDictionary["user"] as! Int, mailID: user_Email, phoneNumber: user_Mobile)
                                    
                                    //Setting User Id to Database...
                                    //self.populatingDataBase(user_Mobile, email: user_Email)
                                    recievedResponse(true, response as NSDictionary)
                                    
                                }else {
                                    
                                    let message = response["message"] as! NSDictionary
                                    
                                    if let msg = message["msg"] as? String {
                                        
                                        self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                    }
                                    
                                    if let validation = message["validation"] as? NSArray {
                                        
                                        
                                        self.helperModel.showingAlertcontroller(title: "", message: "\(validation[0])", cancelButton: "OK", receivedResponse: {})
                                        
                                    }
                                    recievedResponse(true, response as NSDictionary)
                                }
                                
                                
                               // recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        //NVActivityIndicatorPresenter.sharedInstance.setMessage("Server not responding!")
                        recievedResponse(false, [:])
                        
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getUserDetailAPIHIT( token : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "email=%@",token) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    
    //MARK: Function Used for Getting User Details....
    func getfriendsDetailAPI(id: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            //   UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            //  ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getfriendsDetailAPIHIT(id: id, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //      ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        //NVActivityIndicatorPresenter.sharedInstance.setMessage("Server not responding!")
                        recievedResponse(false, [:])
                        
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getfriendsDetailAPIHIT( id : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",id) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/\(id)" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }

    //MARK: Function Used for Set User Visibility....
    func userVisibilityAPI(visibility: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
           //  UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
             //ActivityIndicator.sharedInstance.showActivityIndicator()
             NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            userVisibilityAPIHIT(visibility: visibility, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                  //  ActivityIndicator.sharedInstance.hideActivityIndicator()
                  //  ActivityIndicator.sharedInstance.removeFromSuperview()
                     NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                       // ActivityIndicator.sharedInstance.hideActivityIndicator()
                       // ActivityIndicator.sharedInstance.removeFromSuperview()
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func userVisibilityAPIHIT( visibility : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "visibility=%@",visibility) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/visibility/\(visibility)" as NSString, params: params!, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    
    //MARK: Function Used for Set User Contact us....
    func contactUS(message: String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            //  UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            //ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            contactUSHIT(message: message, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //  ActivityIndicator.sharedInstance.hideActivityIndicator()
                    //  ActivityIndicator.sharedInstance.removeFromSuperview()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        // ActivityIndicator.sharedInstance.hideActivityIndicator()
                        // ActivityIndicator.sharedInstance.removeFromSuperview()
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func contactUSHIT( message : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "message=%@",message) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/admin/sendemail" as NSString, params: params!, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }

    //MARK: Function Used for Updating User Details...
    func updateUserDetailAPI( user_Mobile: String, user_Email: String, first_Name: String, last_Name: String,home_Station: String, destination_Station: String,comute_Years: String, profile_Pic : String, user_Name : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
           //  UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
           //  ActivityIndicator.sharedInstance.showActivityIndicator()
             NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            updateUserDetailAPIHIT(user_Mobile: user_Mobile, user_Email: user_Email, first_Name: first_Name, last_Name: last_Name, home_Station: home_Station, destination_Station: destination_Station, comute_Years: comute_Years, profile_Pic: profile_Pic, user_Name: user_Name, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                   // ActivityIndicator.sharedInstance.hideActivityIndicator()
                     NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                                              
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func updateUserDetailAPIHIT( user_Mobile: String, user_Email: String, first_Name: String, last_Name: String,home_Station: String, destination_Station: String,comute_Years: String, profile_Pic : String, user_Name : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
     params = NSString(format: "contact=%@&email=%@&firstname=%@&lastname=%@&homestation=%@&destinationstation=%@&comuteyear=%@&image=%@&username=%@", (user_Mobile), user_Email, first_Name,last_Name,home_Station,destination_Station,comute_Years, profile_Pic, user_Name) as String
        
        

         
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users" as NSString, params: params!, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }

}
