//
//  Article Model.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 31/08/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//


import UIKit
import CoreData
import NVActivityIndicatorView

class ArticleModel: NSObject {
    
    //Model Objects...
    var internetConnectionModel: InternetConnectionModel!
    var helperModel: HelperModel!
    var activityData : ActivityData!
    
    
    //Data store object...that manages all insertion deletion and updations of Data in Batabase...
    //var persistence: Persistent!
    
    
    //MARK: Model Initialization Functions...
    override init() {
        
        self.internetConnectionModel = InternetConnectionModel()
        self.helperModel = HelperModel()
        self.activityData = ActivityData()
        //Creating Core Data Model Object....
        // self.persistence = Persistent()
}
    
    //MARK: Function Used for Post a Article....
    func postArticleAPI(text : String, type : String, image : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
          //  UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
          //  ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            
            postArticleAPIHIT(text: text, type: type, image: image, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func postArticleAPIHIT( text : String, type : String, image : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "text=%@&articletype=%@&image=%@",text,type,image) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles" as NSString, params: params!, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Update a Article....
    func UpdateArticleAPI(ID : String ,text : String, type : String, image : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            //UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
          //  ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            UpdateArticleAPIHIT(ID: ID, text: text, type: type, image: image, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func UpdateArticleAPIHIT(ID : String, text : String, type : String, image : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@&text=%@&articletype=%@&image=%@",ID,text,type,image) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles/:id" as NSString, params: params!, httpMethod: "PUT") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Delete a Article....
    func deleteArticleAPI(ID : String ,text : String, type : String, image : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
           // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
           // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            deleteArticleAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                 //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func deleteArticleAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles/:id" as NSString, params: params!, httpMethod: "DELETE") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Like a Article....
    func likeArticleAPI(ID : String,UserID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
           // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
           // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            likeArticleAPIHIT(ID: ID, UserID: UserID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                  //  ActivityIndicator.sharedInstance.hideActivityIndicator()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    //http://34.230.77.54/api/v1/users/articles/all
    
    func likeArticleAPIHIT(ID : String,UserID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles/likes/\(ID)/\(UserID)" as NSString, params: params!, httpMethod: "PUT") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Like a Article....
    func disLikeArticleAPI(ID : String,UserID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            disLikeArticleAPIHIT(ID: ID, UserID: UserID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //  ActivityIndicator.sharedInstance.hideActivityIndicator()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    //http://34.230.77.54/api/v1/users/articles/all
    
    func disLikeArticleAPIHIT(ID : String,UserID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles/dislikes/\(ID)/\(UserID)" as NSString, params: params!, httpMethod: "PUT") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Get My Fees Article....
    func getFeedsArticleAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getFeedsArticleAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getFeedsArticleAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles/myfeed" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Get My Article Types....
    func getArticleTypeAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getArticleTypeAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getArticleTypeAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles/types" as NSString, params: "", httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }

    
    //MARK: Function Used for Get MyComute Article....
    func getMyComuteArticleAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getMyComuteArticleAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getMyComuteArticleAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }

    //MARK: Function Used for Get ComuteToday Article....
    func getComuteTodayArticleAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getComuteTodayArticleAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getComuteTodayArticleAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles/lines" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
    
    //MARK: Function Used for Get ALL Type Of  Article....
    func getAllArticleAPI(ID : String, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
            // UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
            // ActivityIndicator.sharedInstance.showActivityIndicator()
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
            getAllArticleAPIHIT(ID: ID, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    //   ActivityIndicator.sharedInstance.hideActivityIndicator()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                
                                recievedResponse(true, response as NSDictionary)
                                
                                // } else {
                                
                                //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                                //   }
                                
                                break
                            case 400 :
                                
                                if let msg = response["message"] as? String {
                                    
                                    self.helperModel.showingAlertcontroller(title: "", message: msg, cancelButton: "OK", receivedResponse: {})
                                }
                                
                                break
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func getAllArticleAPIHIT(ID : String, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        
        
        params = NSString(format: "id=%@",ID) as String
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/users/articles/all" as NSString, params: params!, httpMethod: "GET") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }

}
