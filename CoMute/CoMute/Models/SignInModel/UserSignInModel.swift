//
//  UserSignInModel.swift
//  CoMute
//
//  Created by Himanshu Aggarwal on 19/07/17.
//  Copyright © 2017 Tipstat Infotech Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView

class UserSignInModel: NSObject {
    
    //Model Objects...
    var internetConnectionModel: InternetConnectionModel!
    var helperModel: HelperModel!
    var activeData : ActivityData!
    
    //Controller Data...
    var userSignUpSuccessDataDictionary = NSDictionary()
    
    //Data store object...that manages all insertion deletion and updations of Data in Batabase...
    //var persistence: Persistent!
    
    
    //MARK: Model Initialization Functions...
    override init() {
        
        self.internetConnectionModel = InternetConnectionModel()
        self.helperModel = HelperModel()
        self.activeData = ActivityData()
        //Creating Core Data Model Object....
        // self.persistence = Persistent()
    }
    
    //MARK: Savind User Id in Database...
    //    func populatingDataBase(contactNo: String, email: String) {
    //
    //        if let moc = self.persistence.managedObjectContext {
    //
    //            let fetchRequest = NSFetchRequest(entityName: "Users")
    //            var usersArray = [AnyObject]()
    //
    //            do { usersArray = try moc.executeFetchRequest(fetchRequest) } catch { }
    //
    //            //If User Not Exists then create new one..
    //            var userFound = false
    //
    //            for i in 0 ..< usersArray.count {
    //
    //                if (usersArray[i] as! Users).user_ID == self.userSignUpSuccessDataDictionary["user"] as? String {
    //
    //                    (usersArray[i] as! Users).user_ID = Singleton.sharedInstance.userID
    //                    (usersArray[i] as! Users).user_Mobile = contactNo
    //                    (usersArray[i] as! Users).user_Email = email
    //
    //                    userFound = true
    //                }
    //            }
    //
    //
    //            //Create a new user...
    //            if !userFound {
    //
    //                let managedObject = NSEntityDescription.insertNewObjectForEntityForName("Users", inManagedObjectContext: moc) as! Users
    //                managedObject.user_ID = Singleton.sharedInstance.userID
    //                managedObject.user_Mobile = contactNo
    //                managedObject.user_Email = email
    //            }
    //
    //            //Saving data after Populating into Database...
    //            self.persistence.saveContext({})
    //        }
    //    }
    
    
    
    //MARK: Function Used for Signing Up User...
    func userSignInAPI(password: String, user_Name : String, user_Fb_ID: String, facebookEmailVerifiedFlag: Bool, recievedResponse:@escaping (_ succeeded: Bool, _ response: NSDictionary)->()) {
        
        if internetConnectionModel.connectedToNetwork() {
            
         //  UIApplication.shared.delegate?.window!!.addSubview(ActivityIndicator.sharedInstance)
          // ActivityIndicator.sharedInstance.showActivityIndicator()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activeData)
            
            userSignInAPIAPIHIT(user_Name: user_Name, password: password, user_Fb_ID: user_Fb_ID, facebookEmailVerifiedFlag: facebookEmailVerifiedFlag, receivedResponse: {(succeeded, response) -> () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                  //  ActivityIndicator.sharedInstance.hideActivityIndicator()
                     NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if succeeded {
                        
                        print(response)
                        
                        if ((response as NSDictionary).allKeys as NSArray).contains("statusCode") {   //Response Success
                            
                            //Type of Data recieved if contains data what we expect...
                            switch (response as NSDictionary)["statusCode"] as! Int {
                                
                            case 200:
                                
                                //If Real Success...Happens...
                               // if response["status"] as! Int == 1 {
                                
                                /*
                                 data =     {
                                 "_id" = 5996e13cf957d414a54087de;
                                 authtoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoIjoiNTk5NmUxM2NmOTU3ZDQxNGE1NDA4N2RlIiwiY2hlY2tBdXRoIjoidXNlciIsImV4cCI6MTUzNDg1MjQ4NywiaWF0IjoxNTAzMzE2NDg3fQ.BrFFpFyC2mlnKkgbs344a-Zr5CNra9sYuWKzKkYTnaE";
                                 comuteyear = 2015;
                                 contact = 9891000366;
                                 destinationstation = bangalore;
                                 email = "himanshu@tipstat.com";
                                 firstname = "himanshu ";
                                 homestation = delhi;
                                 image = "";
                                 lastname = Iceparticle;
                                 username = Iceparticle;
                                 };
 */
                                    
                                    //Setting Data to self as Model...
                                    self.userSignUpSuccessDataDictionary = response["data"] as! NSDictionary
                                    
                                    
                                    //Setting USer Is in Singleton as well...
                                    Singleton.sharedInstance.userAccessToken = self.userSignUpSuccessDataDictionary["authtoken"] as! String
                                    UserDefaults.standard.setValue(self.userSignUpSuccessDataDictionary["authtoken"] as! String, forKey: USER_DEFAULT.ACCESS_TOKEN)
                                    Singleton.sharedInstance.userID  = self.userSignUpSuccessDataDictionary["_id"] as! String
                                     UserDefaults.standard.setValue(self.userSignUpSuccessDataDictionary["_id"] as! String, forKey: USER_DEFAULT.USER_ID)
                                    
                                    if let image = self.userSignUpSuccessDataDictionary["image"] as? String{
                                        
                                        Singleton.sharedInstance.userImage = image
                                    }
                                    
                                    Singleton.sharedInstance.userName =  self.userSignUpSuccessDataDictionary["username"] as! String
                                    Singleton.sharedInstance.userHomeStation = self.userSignUpSuccessDataDictionary["homestation"] as! String
                                    Singleton.sharedInstance.userDestinationStation = self.userSignUpSuccessDataDictionary["destinationstation"] as! String
                                    
                                    UserDefaults.standard.setValue(self.userSignUpSuccessDataDictionary["username"] as! String, forKey: USER_DEFAULT.USER_NAME)
                                    
                                    Singleton.sharedInstance.userFullName = String(describing: self.userSignUpSuccessDataDictionary["firstname"]!)+" "+String(describing: self.userSignUpSuccessDataDictionary["lastname"]!)
                                    
                                    UserDefaults.standard.setValue(String(describing: self.userSignUpSuccessDataDictionary["firstname"]!)+" "+String(describing: self.userSignUpSuccessDataDictionary["lastname"]!), forKey: USER_DEFAULT.USER_FULL_NAME)
                                    
                                   // UserDefaults.standard.setValue(Singleton.sharedInstance.currentUserType, forKey: USER_DEFAULT.USER_TYPE)
                                    
                                    //Sending user details to MoEnage for user engagement
                                    //  saveUserDetailsForMOEngage(self.userSignUpSuccessDataDictionary["user"] as! Int, mailID: user_Email, phoneNumber: user_Mobile)
                                    
                                    //Setting User Id to Database...
                                    //self.populatingDataBase(user_Mobile, email: user_Email)
                                    recievedResponse(true, response as NSDictionary)
                                    
                               // } else {
                                    
                               //     self.helperModel.showingAlertcontroller(title: "", message: response["msg"] as! String, cancelButton: //"OK", receivedResponse: {})
                             //   }
                                
                                break
                                
                            default:
                                
                                //                                self.helperModel.showingAlertcontroller("Alert!", message: "User did't get registered.", cancelButton: "OK", receivedResponse: {})
                                recievedResponse(false, [:])
                            }
                            
                        } else {    //Response Success but response is not as per expectations...
                            
                            //                            self.helperModel.showingAlertcontroller("Alert!", message: "Something went wrong!", cancelButton: "OK", receivedResponse: {})
                            recievedResponse(false, [:])
                        }
                        
                    } else {
                        
                        self.helperModel.showingAlertcontroller(title: "", message: "Server not responding!", cancelButton: "OK", receivedResponse: {})
                        recievedResponse(false, [:])
                    }
                }
            })
            
        } else {
            
            helperModel.showingAlertcontroller(title: "", message: "Please Check your internet Connection!", cancelButton: "OK", receivedResponse: {})
            recievedResponse(false, [:])
        }
    }
    
    func userSignInAPIAPIHIT( user_Name : String, password: String, user_Fb_ID: String, facebookEmailVerifiedFlag: Bool, receivedResponse: @escaping (_ succeeded:Bool, _ response:NSDictionary) -> ()) {
        
        //Setting Data of Post Request to send...
        let params : String?
        
        if user_Fb_ID.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) != "" {
            
            params = NSString(format: "email=%@&fbid=%@", user_Name, user_Fb_ID, facebookEmailVerifiedFlag as CVarArg) as String
            
        } else {
            
            params = NSString(format: "password=%@&email=%@&devicetoken=%@",password, user_Name,(UserDefaults.standard.value( forKey: USER_DEFAULT.DEVICE_TOKEN) ?? "") as! String) as String
        }
        
        print(params!)
        
        //Sending request to server....
        sendRequestToServer(url: "/v1/auth/signin", params: params!, httpMethod: "POST") {(succeeded:Bool, response:NSDictionary) -> () in
            
            if(succeeded) {
                
                print(response)
                
                receivedResponse(true, response)
                
            } else {
                
                receivedResponse(false, response)
            }
        }
    }
}
